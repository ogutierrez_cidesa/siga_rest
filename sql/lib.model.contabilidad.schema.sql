
-----------------------------------------------------------------------
-- contaba
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "contaba" CASCADE;

CREATE TABLE "contaba"
(
    "codemp" VARCHAR(3) NOT NULL,
    "loncta" NUMERIC(2) NOT NULL,
    "numrup" NUMERIC(2) NOT NULL,
    "forcta" VARCHAR(32) NOT NULL,
    "sitfin" VARCHAR(32),
    "sitfis" VARCHAR(32),
    "ganper" VARCHAR(32),
    "ejepre" VARCHAR(32),
    "hacmun" VARCHAR(32),
    "ctlgas" VARCHAR(32),
    "ctling" VARCHAR(32),
    "fecini" DATE NOT NULL,
    "feccie" DATE NOT NULL,
    "codtes" VARCHAR(32),
    "codhac" VARCHAR(32),
    "codpre" VARCHAR(32),
    "codord" VARCHAR(32),
    "codtesact" VARCHAR(32),
    "codhacact" VARCHAR(32),
    "codhacpat" VARCHAR(32),
    "codtespas" VARCHAR(32),
    "codhacpas" VARCHAR(32),
    "codind" VARCHAR(32),
    "codinh" VARCHAR(32),
    "codegd" VARCHAR(32),
    "codegh" VARCHAR(32),
    "codres" VARCHAR(32),
    "codejepre" VARCHAR(32),
    "codctd" VARCHAR(32),
    "codcta" VARCHAR(32),
    "codresant" VARCHAR(32),
    "etadef" VARCHAR(1),
    "codctagas" VARCHAR(32),
    "codctaban" VARCHAR(32),
    "codctaret" VARCHAR(32),
    "codctaben" VARCHAR(32),
    "codctaart" VARCHAR(32),
    "codctagashas" VARCHAR(32),
    "codctabanhas" VARCHAR(32),
    "codctarethas" VARCHAR(32),
    "codctabenhas" VARCHAR(32),
    "codctaarthas" VARCHAR(32),
    "codctapageje" VARCHAR(32),
    "codctaingdevn" VARCHAR(32),
    "codctaingdev" VARCHAR(32),
    "unidad" VARCHAR(10),
    "corcomp" VARCHAR(100),
    "btnelicomanu" BOOLEAN DEFAULT 'f',
    "btnmodcom" BOOLEAN DEFAULT 't',
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "contaba" IS 'Configuración del módulo de contabilidad';

COMMENT ON COLUMN "contaba"."codemp" IS 'Código Empresa';

COMMENT ON COLUMN "contaba"."loncta" IS 'Longitud de la cuenta';

COMMENT ON COLUMN "contaba"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- contaba1
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "contaba1" CASCADE;

CREATE TABLE "contaba1"
(
    "fecini" DATE NOT NULL,
    "feccie" DATE NOT NULL,
    "pereje" VARCHAR(2) NOT NULL,
    "fecdes" DATE NOT NULL,
    "fechas" DATE NOT NULL,
    "status" VARCHAR(1) DEFAULT 'A',
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

-----------------------------------------------------------------------
-- contabb
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "contabb" CASCADE;

CREATE TABLE "contabb"
(
    "codcta" VARCHAR(32) NOT NULL,
    "descta" VARCHAR(250) NOT NULL,
    "fecini" DATE NOT NULL,
    "feccie" DATE NOT NULL,
    "salant" NUMERIC(14,2),
    "debcre" VARCHAR(1) NOT NULL,
    "cargab" VARCHAR(1) NOT NULL,
    "salprgper" NUMERIC(14,2),
    "salacuper" NUMERIC(14,2),
    "salprgperfor" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

-----------------------------------------------------------------------
-- contabb1
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "contabb1" CASCADE;

CREATE TABLE "contabb1"
(
    "codcta" VARCHAR(32) NOT NULL,
    "fecini" DATE NOT NULL,
    "feccie" DATE NOT NULL,
    "pereje" VARCHAR(2) NOT NULL,
    "totdeb" NUMERIC(14,2),
    "totcre" NUMERIC(14,2),
    "salact" NUMERIC(14,2),
    "salprgper" NUMERIC(14,2),
    "salprgperfor" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "contabb1"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- contabc
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "contabc" CASCADE;

CREATE TABLE "contabc"
(
    "numcom" VARCHAR(8) NOT NULL,
    "feccom" DATE NOT NULL,
    "descom" VARCHAR(1000),
    "moncom" NUMERIC(14,2),
    "stacom" VARCHAR(1) NOT NULL,
    "tipcom" VARCHAR(3),
    "reftra" VARCHAR(20),
    "loguse" VARCHAR(50),
    "usuanu" VARCHAR(50),
    "codtiptra" VARCHAR(3),
    "staapr" VARCHAR(1) DEFAULT '',
    "fecapr" DATE,
    "usuapr" VARCHAR(50),
    "coddirec" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "contabc"."staapr" IS 'Estatus de Aprobación';

COMMENT ON COLUMN "contabc"."fecapr" IS 'Fecha de Aprobación';

COMMENT ON COLUMN "contabc"."usuapr" IS 'Usuario de Aprobación';

COMMENT ON COLUMN "contabc"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "contabc"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- contabc1
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "contabc1" CASCADE;

CREATE TABLE "contabc1"
(
    "numcom" VARCHAR(8) NOT NULL,
    "feccom" DATE NOT NULL,
    "debcre" VARCHAR(1) NOT NULL,
    "codcta" VARCHAR(32) NOT NULL,
    "numasi" NUMERIC(3),
    "refasi" VARCHAR(20),
    "desasi" VARCHAR(250),
    "monasi" NUMERIC(14,2),
    "codcencos" VARCHAR(32),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "contabc1" IS 'Detalle de los comprobantes contables';

COMMENT ON COLUMN "contabc1"."numcom" IS 'Número de Comprobante';

COMMENT ON COLUMN "contabc1"."feccom" IS 'Fecha del comprobante';

COMMENT ON COLUMN "contabc1"."debcre" IS 'Débito o Crédito';

COMMENT ON COLUMN "contabc1"."codcta" IS 'Cuenta Contable';

COMMENT ON COLUMN "contabc1"."monasi" IS 'Monto asignado';

COMMENT ON COLUMN "contabc1"."codcencos" IS 'Centro de Costo';

COMMENT ON COLUMN "contabc1"."id" IS 'Identificador Único del registro';

CREATE INDEX "contabc1_index_01" ON "contabc1" ("numcon","codcta","feccom");

-----------------------------------------------------------------------
-- codefcencos
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "codefcencos" CASCADE;

CREATE TABLE "codefcencos"
(
    "codcencos" VARCHAR(32) NOT NULL,
    "descencos" VARCHAR(500) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "codefcencos"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- codetcencos
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "codetcencos" CASCADE;

CREATE TABLE "codetcencos"
(
    "numcom" VARCHAR(8) NOT NULL,
    "codcta" VARCHAR(32) NOT NULL,
    "codcencos" VARCHAR(32) NOT NULL,
    "moncencos" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "codetcencos"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- contabcm
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "contabcm" CASCADE;

CREATE TABLE "contabcm"
(
    "numcom" VARCHAR(8) NOT NULL,
    "feccom" DATE NOT NULL,
    "descom" VARCHAR(1000),
    "moncom" NUMERIC(14,2),
    "stacom" VARCHAR(1) NOT NULL,
    "tipcom" VARCHAR(3),
    "reftra" VARCHAR(8),
    "loguse" VARCHAR(50),
    "usuanu" VARCHAR(50),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "contabcm"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- contabc1m
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "contabc1m" CASCADE;

CREATE TABLE "contabc1m"
(
    "numcom" VARCHAR(8) NOT NULL,
    "feccom" DATE NOT NULL,
    "debcre" VARCHAR(1) NOT NULL,
    "codcta" VARCHAR(32) NOT NULL,
    "numasi" NUMERIC(3),
    "refasi" VARCHAR(8),
    "desasi" VARCHAR(250),
    "monasi" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "contabc1m" IS 'Detalle de los comprobantes contables';

COMMENT ON COLUMN "contabc1m"."numcom" IS 'Número de Comprobante';

COMMENT ON COLUMN "contabc1m"."feccom" IS 'Fecha del comprobante';

COMMENT ON COLUMN "contabc1m"."debcre" IS 'Débito o Crédito';

COMMENT ON COLUMN "contabc1m"."codcta" IS 'Cuenta Contable';

COMMENT ON COLUMN "contabc1m"."monasi" IS 'Monto asignado';

COMMENT ON COLUMN "contabc1m"."id" IS 'Identificador Único del registro';

CREATE INDEX "contabc1m_index_01" ON "contabc1m" ("numcon","codcta","feccom");

-----------------------------------------------------------------------
-- codeftiplot
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "codeftiplot" CASCADE;

CREATE TABLE "codeftiplot"
(
    "codlot" VARCHAR(3) NOT NULL,
    "deslot" VARCHAR(100) NOT NULL,
    "numlot" VARCHAR(20) NOT NULL,
    "tipcom" VARCHAR(3) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "codeftiplot"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cotiptra
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cotiptra" CASCADE;

CREATE TABLE "cotiptra"
(
    "codtiptra" VARCHAR(3) NOT NULL,
    "destiptra" VARCHAR(1000) NOT NULL,
    "modtiptra" BOOLEAN,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cotiptra"."modtiptra" IS 'Permite asignar el tipo de transación en los comprobantes de otros modulos';

COMMENT ON COLUMN "cotiptra"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- contabbhis
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "contabbhis" CASCADE;

CREATE TABLE "contabbhis"
(
    "codcta" VARCHAR(32) NOT NULL,
    "descta" VARCHAR(250) NOT NULL,
    "fecini" DATE NOT NULL,
    "feccie" DATE NOT NULL,
    "salant" NUMERIC(14,2),
    "debcre" VARCHAR(1) NOT NULL,
    "cargab" VARCHAR(1) NOT NULL,
    "salprgper" NUMERIC(14,2),
    "salacuper" NUMERIC(14,2),
    "salprgperfor" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

-----------------------------------------------------------------------
-- contabb1his
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "contabb1his" CASCADE;

CREATE TABLE "contabb1his"
(
    "codcta" VARCHAR(32) NOT NULL,
    "fecini" DATE NOT NULL,
    "feccie" DATE NOT NULL,
    "pereje" VARCHAR(2) NOT NULL,
    "totdeb" NUMERIC(14,2),
    "totcre" NUMERIC(14,2),
    "salact" NUMERIC(14,2),
    "salprgper" NUMERIC(14,2),
    "salprgperfor" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "contabb1his"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- contatit
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "contatit" CASCADE;

CREATE TABLE "contatit"
(
    "codtit" VARCHAR(3) NOT NULL,
    "destit" VARCHAR(500) NOT NULL,
    "ordtit" NUMERIC(2,0) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "contatit" IS 'Contiene los registros de los Titulos del Estado de Flujo Efectivo';

COMMENT ON COLUMN "contatit"."codtit" IS 'Código del Titulo del Estado de Flujo Efectivo';

COMMENT ON COLUMN "contatit"."destit" IS 'Descripción del Titulo del Estado de Flujo Efectivo';

COMMENT ON COLUMN "contatit"."ordtit" IS 'Orden del Titulo del Estado de Flujo Efectivo';

COMMENT ON COLUMN "contatit"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- contadettit
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "contadettit" CASCADE;

CREATE TABLE "contadettit"
(
    "codtitdet" VARCHAR(3) NOT NULL,
    "codtit" VARCHAR(3) NOT NULL,
    "destitdet" VARCHAR(500) NOT NULL,
    "ordtitdet" NUMERIC(2,0) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "contadettit" IS 'Contiene los registros del detalle de los Titulos del Estado de Flujo Efectivo';

COMMENT ON COLUMN "contadettit"."codtitdet" IS 'Código del detalle de Titulo del Estado de Flujo Efectivo';

COMMENT ON COLUMN "contadettit"."codtit" IS 'Código del detalle de Titulo del Estado de Flujo Efectivo';

COMMENT ON COLUMN "contadettit"."destitdet" IS 'Descripción del detalle de Titulo del Estado de Flujo Efectivo';

COMMENT ON COLUMN "contadettit"."ordtitdet" IS 'Orden del detalle de Titulo del Estado de Flujo Efectivo';

COMMENT ON COLUMN "contadettit"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- contacuetit
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "contacuetit" CASCADE;

CREATE TABLE "contacuetit"
(
    "codtitdet" VARCHAR(3) NOT NULL,
    "codcta" VARCHAR(32),
    "descta" VARCHAR(250),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "contacuetit" IS 'Contiene los registros de cuentas asociadas a detalle de título';

COMMENT ON COLUMN "contacuetit"."codtitdet" IS 'Código del detalle de Titulo del Estado de Flujo Efectivo';

COMMENT ON COLUMN "contacuetit"."codcta" IS 'Código de la cuenta';

COMMENT ON COLUMN "contacuetit"."descta" IS 'Descripción de la cuenta';

COMMENT ON COLUMN "contacuetit"."id" IS 'Identificador Único del registro';

ALTER TABLE "contabb1" ADD CONSTRAINT "contabb1_FK_1"
    FOREIGN KEY ("codcta")
    REFERENCES "contabb" ("codcta");

ALTER TABLE "contabc1" ADD CONSTRAINT "contabc1_FK_1"
    FOREIGN KEY ("numcom")
    REFERENCES "contabc" ("numcom");

ALTER TABLE "contabc1" ADD CONSTRAINT "contabc1_FK_2"
    FOREIGN KEY ("codcta")
    REFERENCES "contabb" ("codcta");

ALTER TABLE "contabc1m" ADD CONSTRAINT "contabc1m_FK_1"
    FOREIGN KEY ("numcom")
    REFERENCES "contabcm" ("numcom");

ALTER TABLE "contabc1m" ADD CONSTRAINT "contabc1m_FK_2"
    FOREIGN KEY ("codcta")
    REFERENCES "contabb" ("codcta");

ALTER TABLE "contabb1his" ADD CONSTRAINT "contabb1his_FK_1"
    FOREIGN KEY ("codcta")
    REFERENCES "contabb" ("codcta");
