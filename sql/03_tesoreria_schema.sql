
-----------------------------------------------------------------------
-- opasiordapr
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opasiordapr" CASCADE;

CREATE TABLE "opasiordapr"
(
    "ctagas" VARCHAR(32),
    "ctapag" VARCHAR(32),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opasiordapr" IS 'null';

COMMENT ON COLUMN "opasiordapr"."ctagas" IS 'null';

COMMENT ON COLUMN "opasiordapr"."ctapag" IS 'null';

COMMENT ON COLUMN "opasiordapr"."id" IS 'Identificador Únic odel registro';

-----------------------------------------------------------------------
-- opautpag
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opautpag" CASCADE;

CREATE TABLE "opautpag"
(
    "numord" VARCHAR(8) NOT NULL,
    "tipcau" VARCHAR(4) NOT NULL,
    "fecemi" DATE NOT NULL,
    "cedrif" VARCHAR(15) NOT NULL,
    "nomben" VARCHAR(250) NOT NULL,
    "monord" NUMERIC(14,2) NOT NULL,
    "desord" VARCHAR(1000) NOT NULL,
    "mondes" NUMERIC(14,2),
    "monret" NUMERIC(14,2),
    "numche" VARCHAR(20),
    "ctaban" VARCHAR(20),
    "ctapag" VARCHAR(32),
    "numcom" VARCHAR(8),
    "status" VARCHAR(1) NOT NULL,
    "coduni" VARCHAR(30),
    "fecenvcon" DATE,
    "fecenvfin" DATE,
    "ctapagfin" VARCHAR(32),
    "nombensus" VARCHAR(250),
    "fecanu" DATE,
    "fecrecfin" DATE,
    "anopre" VARCHAR(4),
    "fecpag" DATE,
    "monpag" NUMERIC(16,2),
    "obsord" VARCHAR(1000),
    "numtiq" VARCHAR(8),
    "peraut" VARCHAR(500),
    "desanu" VARCHAR(250),
    "cedaut" VARCHAR(100),
    "nomper1" VARCHAR(50),
    "nomper2" VARCHAR(50),
    "horcon" VARCHAR(10),
    "feccon" DATE,
    "nomcat" VARCHAR(250),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opautpag" IS 'null';

COMMENT ON COLUMN "opautpag"."numord" IS 'null';

COMMENT ON COLUMN "opautpag"."tipcau" IS 'null';

COMMENT ON COLUMN "opautpag"."fecemi" IS 'null';

COMMENT ON COLUMN "opautpag"."cedrif" IS 'null';

COMMENT ON COLUMN "opautpag"."nomben" IS 'null';

COMMENT ON COLUMN "opautpag"."monord" IS 'null';

COMMENT ON COLUMN "opautpag"."desord" IS 'null';

COMMENT ON COLUMN "opautpag"."mondes" IS 'null';

COMMENT ON COLUMN "opautpag"."monret" IS 'null';

COMMENT ON COLUMN "opautpag"."numche" IS 'null';

COMMENT ON COLUMN "opautpag"."ctaban" IS 'null';

COMMENT ON COLUMN "opautpag"."ctapag" IS 'null';

COMMENT ON COLUMN "opautpag"."numcom" IS 'null';

COMMENT ON COLUMN "opautpag"."status" IS 'null';

COMMENT ON COLUMN "opautpag"."coduni" IS 'null';

COMMENT ON COLUMN "opautpag"."fecenvcon" IS 'null';

COMMENT ON COLUMN "opautpag"."fecenvfin" IS 'null';

COMMENT ON COLUMN "opautpag"."ctapagfin" IS 'null';

COMMENT ON COLUMN "opautpag"."nombensus" IS 'null';

COMMENT ON COLUMN "opautpag"."fecanu" IS 'null';

COMMENT ON COLUMN "opautpag"."fecrecfin" IS 'null';

COMMENT ON COLUMN "opautpag"."anopre" IS 'null';

COMMENT ON COLUMN "opautpag"."fecpag" IS 'null';

COMMENT ON COLUMN "opautpag"."monpag" IS 'null';

COMMENT ON COLUMN "opautpag"."obsord" IS 'null';

COMMENT ON COLUMN "opautpag"."numtiq" IS 'null';

COMMENT ON COLUMN "opautpag"."peraut" IS 'null';

COMMENT ON COLUMN "opautpag"."desanu" IS 'null';

COMMENT ON COLUMN "opautpag"."cedaut" IS 'null';

COMMENT ON COLUMN "opautpag"."nomper1" IS 'null';

COMMENT ON COLUMN "opautpag"."nomper2" IS 'null';

COMMENT ON COLUMN "opautpag"."horcon" IS 'null';

COMMENT ON COLUMN "opautpag"."feccon" IS 'null';

COMMENT ON COLUMN "opautpag"."nomcat" IS 'null';

COMMENT ON COLUMN "opautpag"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opbenefi
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opbenefi" CASCADE;

CREATE TABLE "opbenefi"
(
    "cedrif" VARCHAR(15) NOT NULL,
    "nomben" VARCHAR(250) NOT NULL,
    "dirben" VARCHAR(100),
    "telben" VARCHAR(20),
    "codcta" VARCHAR(32),
    "nitben" VARCHAR(15),
    "codtipben" VARCHAR(3) NOT NULL,
    "tipper" VARCHAR(1),
    "nacionalidad" VARCHAR(1),
    "residente" VARCHAR(1),
    "constituida" VARCHAR(1),
    "codord" VARCHAR(32),
    "codpercon" VARCHAR(32),
    "codordadi" VARCHAR(32),
    "codperconadi" VARCHAR(32),
    "codordcontra" VARCHAR(32),
    "codpercontra" VARCHAR(32),
    "temcedrif" VARCHAR(15),
    "codctasec" VARCHAR(32),
    "codctacajchi" VARCHAR(32),
    "numcue" VARCHAR(20),
    "codctaant" VARCHAR(32),
    "codban" VARCHAR(4),
    "numcueb" VARCHAR(20),
    "banint" VARCHAR(100),
    "numcueint" VARCHAR(20),
    "dirbanint" VARCHAR(200),
    "codswiint" VARCHAR(20),
    "numabaint" VARCHAR(40),
    "numibaint" VARCHAR(40),
    "numrouint" VARCHAR(40),
    "banben" VARCHAR(100),
    "numcueben" VARCHAR(20),
    "dirbanben" VARCHAR(200),
    "codswiben" VARCHAR(20),
    "numababen" VARCHAR(40),
    "numibaben" VARCHAR(40),
    "numrouben" VARCHAR(40),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_opbenefi_cedrif" UNIQUE ("cedrif")
);

COMMENT ON TABLE "opbenefi" IS 'Tabla que contiene información referente a los beneficiarios';

COMMENT ON COLUMN "opbenefi"."cedrif" IS 'Cédula o RIF del beneficiario';

COMMENT ON COLUMN "opbenefi"."nomben" IS 'Nombre del Beneficiario';

COMMENT ON COLUMN "opbenefi"."dirben" IS 'Dirección del Beneficiario';

COMMENT ON COLUMN "opbenefi"."telben" IS 'Teléfono del beneficiario';

COMMENT ON COLUMN "opbenefi"."codcta" IS 'Cuenta Contable asociada al Beneficiario';

COMMENT ON COLUMN "opbenefi"."nitben" IS 'NIT del Beneficiario';

COMMENT ON COLUMN "opbenefi"."codtipben" IS 'Código del Tipo de Beneficiario';

COMMENT ON COLUMN "opbenefi"."tipper" IS 'Tipo de Persona (Natural o Jurídica)';

COMMENT ON COLUMN "opbenefi"."nacionalidad" IS 'Indica si es de Nacionalidad Venezolana o Extranjera';

COMMENT ON COLUMN "opbenefi"."residente" IS 'Indica si reside o no en el país';

COMMENT ON COLUMN "opbenefi"."constituida" IS 'Indica si la empresa fue constituida o no en el país';

COMMENT ON COLUMN "opbenefi"."codord" IS 'Cuenta Contable de Orden';

COMMENT ON COLUMN "opbenefi"."codpercon" IS 'Cuenta Contable de Percontra';

COMMENT ON COLUMN "opbenefi"."codordadi" IS 'Cuenta Contable de Orden Adicional para Caso de Servicios';

COMMENT ON COLUMN "opbenefi"."codperconadi" IS 'Cuenta Contable de Percontra Adicional para Caso de Servicios';

COMMENT ON COLUMN "opbenefi"."codordcontra" IS 'Cuenta Contable de Orden Asociada';

COMMENT ON COLUMN "opbenefi"."codpercontra" IS 'Cuenta Contable PerContra Asociada';

COMMENT ON COLUMN "opbenefi"."temcedrif" IS 'null';

COMMENT ON COLUMN "opbenefi"."codctasec" IS 'Cuenta Contable de la Actividad Secundaria del Beneficiario';

COMMENT ON COLUMN "opbenefi"."codctacajchi" IS 'Cuenta Contable de Manejo de Caja Chica';

COMMENT ON COLUMN "opbenefi"."numcue" IS 'Número de la Cuenta Bancaria';

COMMENT ON COLUMN "opbenefi"."codctaant" IS 'Cuenta Contable de Anticipos';

COMMENT ON COLUMN "opbenefi"."banint" IS 'Banco Intermediario';

COMMENT ON COLUMN "opbenefi"."numcueint" IS 'Número de Cuenta del Intermediario';

COMMENT ON COLUMN "opbenefi"."dirbanint" IS 'Dirección del Banco Intermediario';

COMMENT ON COLUMN "opbenefi"."codswiint" IS 'CODIGO SWIFT del Intermediario';

COMMENT ON COLUMN "opbenefi"."numabaint" IS 'NUMERO ABA del Intermediario';

COMMENT ON COLUMN "opbenefi"."numibaint" IS 'NUMERO IBAN del Intermediario';

COMMENT ON COLUMN "opbenefi"."numrouint" IS 'NUMERO ROUTING del Intermediario';

COMMENT ON COLUMN "opbenefi"."banben" IS 'Banco del Beneficiario';

COMMENT ON COLUMN "opbenefi"."numcueben" IS 'Número de Cuenta del Beneficiario';

COMMENT ON COLUMN "opbenefi"."dirbanben" IS 'Dirección del Banco del Beneficiario';

COMMENT ON COLUMN "opbenefi"."codswiben" IS 'CODIGO SWIFT del Beneficiario';

COMMENT ON COLUMN "opbenefi"."numababen" IS 'NUMERO ABA del Beneficiario';

COMMENT ON COLUMN "opbenefi"."numibaben" IS 'NUMERO IBAN del Beneficiario';

COMMENT ON COLUMN "opbenefi"."numrouben" IS 'NUMERO ROUTING del Beneficiario';

COMMENT ON COLUMN "opbenefi"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opbenefi2
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opbenefi2" CASCADE;

CREATE TABLE "opbenefi2"
(
    "cedrif" VARCHAR(15) NOT NULL,
    "nomben" VARCHAR(250) NOT NULL,
    "dirben" VARCHAR(100),
    "telben" VARCHAR(20),
    "codcta" VARCHAR(32),
    "nitben" VARCHAR(15),
    "codtipben" VARCHAR(3),
    "tipper" VARCHAR(1),
    "nacionalidad" VARCHAR(1),
    "residente" VARCHAR(1),
    "constituida" VARCHAR(1),
    "codord" VARCHAR(32),
    "codpercon" VARCHAR(32),
    "codordadi" VARCHAR(32),
    "codperconadi" VARCHAR(32),
    "codordcontra" VARCHAR(32),
    "codpercontra" VARCHAR(32),
    "temcedrif" VARCHAR(15),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opbenefi2" IS 'null';

COMMENT ON COLUMN "opbenefi2"."cedrif" IS 'null';

COMMENT ON COLUMN "opbenefi2"."nomben" IS 'null';

COMMENT ON COLUMN "opbenefi2"."dirben" IS 'null';

COMMENT ON COLUMN "opbenefi2"."telben" IS 'null';

COMMENT ON COLUMN "opbenefi2"."codcta" IS 'null';

COMMENT ON COLUMN "opbenefi2"."nitben" IS 'null';

COMMENT ON COLUMN "opbenefi2"."codtipben" IS 'null';

COMMENT ON COLUMN "opbenefi2"."tipper" IS 'null';

COMMENT ON COLUMN "opbenefi2"."nacionalidad" IS 'null';

COMMENT ON COLUMN "opbenefi2"."residente" IS 'null';

COMMENT ON COLUMN "opbenefi2"."constituida" IS 'null';

COMMENT ON COLUMN "opbenefi2"."codord" IS 'null';

COMMENT ON COLUMN "opbenefi2"."codpercon" IS 'null';

COMMENT ON COLUMN "opbenefi2"."codordadi" IS 'null';

COMMENT ON COLUMN "opbenefi2"."codperconadi" IS 'null';

COMMENT ON COLUMN "opbenefi2"."codordcontra" IS 'null';

COMMENT ON COLUMN "opbenefi2"."codpercontra" IS 'null';

COMMENT ON COLUMN "opbenefi2"."temcedrif" IS 'null';

COMMENT ON COLUMN "opbenefi2"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opbenfonava
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opbenfonava" CASCADE;

CREATE TABLE "opbenfonava"
(
    "cedrif" VARCHAR(15),
    "codcta" VARCHAR(32),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opbenfonava" IS 'null';

COMMENT ON COLUMN "opbenfonava"."cedrif" IS 'null';

COMMENT ON COLUMN "opbenfonava"."codcta" IS 'null';

COMMENT ON COLUMN "opbenfonava"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opcheper
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opcheper" CASCADE;

CREATE TABLE "opcheper"
(
    "refopp" VARCHAR(8),
    "refcuo" VARCHAR(8),
    "monpag" NUMERIC(14,2),
    "fecpag" DATE,
    "numord" VARCHAR(8),
    "ctaban" VARCHAR(20),
    "numche" VARCHAR(20),
    "tipmov" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opcheper" IS 'null';

COMMENT ON COLUMN "opcheper"."refopp" IS 'null';

COMMENT ON COLUMN "opcheper"."refcuo" IS 'null';

COMMENT ON COLUMN "opcheper"."monpag" IS 'null';

COMMENT ON COLUMN "opcheper"."fecpag" IS 'null';

COMMENT ON COLUMN "opcheper"."numord" IS 'null';

COMMENT ON COLUMN "opcheper"."ctaban" IS 'null';

COMMENT ON COLUMN "opcheper"."numche" IS 'null';

COMMENT ON COLUMN "opcheper"."tipmov" IS 'null';

COMMENT ON COLUMN "opcheper"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opcominc
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opcominc" CASCADE;

CREATE TABLE "opcominc"
(
    "refaju" VARCHAR(8),
    "numcom" VARCHAR(8),
    "fecaju" DATE,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opcominc" IS 'null';

COMMENT ON COLUMN "opcominc"."refaju" IS 'null';

COMMENT ON COLUMN "opcominc"."numcom" IS 'null';

COMMENT ON COLUMN "opcominc"."fecaju" IS 'null';

COMMENT ON COLUMN "opcominc"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opconinc
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opconinc" CASCADE;

CREATE TABLE "opconinc"
(
    "refaju" VARCHAR(8),
    "numcom" VARCHAR(8),
    "fecaju" DATE,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opconinc" IS 'null';

COMMENT ON COLUMN "opconinc"."refaju" IS 'null';

COMMENT ON COLUMN "opconinc"."numcom" IS 'null';

COMMENT ON COLUMN "opconinc"."fecaju" IS 'null';

COMMENT ON COLUMN "opconinc"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opconnom
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opconnom" CASCADE;

CREATE TABLE "opconnom"
(
    "codcat" VARCHAR(32) NOT NULL,
    "codnom" VARCHAR(3),
    "refprc" VARCHAR(8),
    "refcom" VARCHAR(8),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opconnom" IS 'null';

COMMENT ON COLUMN "opconnom"."codcat" IS 'null';

COMMENT ON COLUMN "opconnom"."codnom" IS 'null';

COMMENT ON COLUMN "opconnom"."refprc" IS 'null';

COMMENT ON COLUMN "opconnom"."refcom" IS 'null';

COMMENT ON COLUMN "opconnom"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opdefemp
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opdefemp" CASCADE;

CREATE TABLE "opdefemp"
(
    "codemp" VARCHAR(3) NOT NULL,
    "ctapag" VARCHAR(32) NOT NULL,
    "ctades" VARCHAR(32) NOT NULL,
    "numini" VARCHAR(8) NOT NULL,
    "ordnom" VARCHAR(4) NOT NULL,
    "ordobr" VARCHAR(4) NOT NULL,
    "unitri" NUMERIC(14,2),
    "vercomret" VARCHAR(1),
    "genctaord" VARCHAR(1),
    "forubi" VARCHAR(32),
    "tipaju" VARCHAR(4),
    "tipeje" VARCHAR(4),
    "numaut" VARCHAR(8),
    "tipmov" VARCHAR(4),
    "coriva" NUMERIC(14,0),
    "ctabono" VARCHAR(32),
    "ctavaca" VARCHAR(32),
    "gencaubon" VARCHAR(1),
    "gencomadi" VARCHAR(1),
    "unidad" VARCHAR(100),
    "ordliq" VARCHAR(4),
    "ordfid" VARCHAR(4),
    "ordval" VARCHAR(4),
    "genordret" VARCHAR(1),
    "emichepag" VARCHAR(1),
    "cuecajchi" VARCHAR(20),
    "tipcajchi" VARCHAR(4),
    "monapecajchi" NUMERIC(14,2),
    "porrepcajchi" NUMERIC(14,2),
    "tiprencajchi" VARCHAR(4),
    "numinicajchi" VARCHAR(8),
    "cedrifcajchi" VARCHAR(15),
    "codcatcajchi" VARCHAR(32),
    "gencomalc" VARCHAR(1),
    "reqaprord" VARCHAR(1),
    "ordcomptot" VARCHAR(1),
    "ordmotanu" VARCHAR(1),
    "manbloqban" VARCHAR(1),
    "ordret" VARCHAR(4) NOT NULL,
    "ordconpre" BOOLEAN DEFAULT 'f',
    "ordtna" VARCHAR(4),
    "ordtba" VARCHAR(4),
    "ordcre" VARCHAR(4),
    "ordsolpag" VARCHAR(4),
    "monche" NUMERIC(14,2),
    "codmon" VARCHAR(3) NOT NULL,
    "tippag" VARCHAR(4),
    "corpagele" VARCHAR(8),
    "tipagele" VARCHAR(1),
    "cormovele" VARCHAR(8),
    "rifemp" VARCHAR(15),
    "numneg" VARCHAR(8),
    "ordamo" VARCHAR(4),
    "ordant" VARCHAR(4),
    "ordantcom" VARCHAR(4),
    "reppreimpop" VARCHAR(50),
    "corciecaj" VARCHAR(8),
    "ordhcm" VARCHAR(4),
    "comiva" VARCHAR(50),
    "comislr" VARCHAR(50),
    "comltf" VARCHAR(50),
    "comrs" VARCHAR(50),
    "ordcarava" VARCHAR(4),
    "ordciecaj" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opdefemp" IS 'Tabla que contiene información referente a la definición de empresa';

COMMENT ON COLUMN "opdefemp"."codemp" IS 'Código de la Empresa';

COMMENT ON COLUMN "opdefemp"."ctapag" IS 'Cuenta por Pagar';

COMMENT ON COLUMN "opdefemp"."ctades" IS 'Cuenta de Descuento';

COMMENT ON COLUMN "opdefemp"."numini" IS 'Número Inicial para la Orden de Pago';

COMMENT ON COLUMN "opdefemp"."ordnom" IS 'Orden de pago para Cancelar Nomina';

COMMENT ON COLUMN "opdefemp"."ordobr" IS 'Orden de pago para Cancelar Obras';

COMMENT ON COLUMN "opdefemp"."unitri" IS 'Valor de la Unidad Tributaria';

COMMENT ON COLUMN "opdefemp"."vercomret" IS 'Indica si se permite ver Comprobantes y Retenciones';

COMMENT ON COLUMN "opdefemp"."genctaord" IS 'Indica si genera Comprobantes de Orden';

COMMENT ON COLUMN "opdefemp"."forubi" IS 'Formato de las Unidades de Origen';

COMMENT ON COLUMN "opdefemp"."tipaju" IS 'null';

COMMENT ON COLUMN "opdefemp"."tipeje" IS 'Tipo de Ejecución Presupuestaria';

COMMENT ON COLUMN "opdefemp"."numaut" IS 'Número inicial para la identificación de las Autorizaciones de Pago';

COMMENT ON COLUMN "opdefemp"."tipmov" IS 'Tipo de Movimiento según Libro para la Autorización de Pago';

COMMENT ON COLUMN "opdefemp"."coriva" IS 'Correlativo para Comprobante de Retención de IVA';

COMMENT ON COLUMN "opdefemp"."ctabono" IS 'null';

COMMENT ON COLUMN "opdefemp"."ctavaca" IS 'null';

COMMENT ON COLUMN "opdefemp"."gencaubon" IS 'null';

COMMENT ON COLUMN "opdefemp"."gencomadi" IS 'Indica si Genera Comprobante Contable Adicional al hacer una OP';

COMMENT ON COLUMN "opdefemp"."unidad" IS 'null';

COMMENT ON COLUMN "opdefemp"."ordliq" IS 'Tipo de Causado para Generar Ordenes de Pago de Liquidación';

COMMENT ON COLUMN "opdefemp"."ordfid" IS 'Tipo de Causado para Generar Ordenes de Pago de Fideicomiso';

COMMENT ON COLUMN "opdefemp"."ordval" IS 'null';

COMMENT ON COLUMN "opdefemp"."genordret" IS 'null';

COMMENT ON COLUMN "opdefemp"."emichepag" IS 'null';

COMMENT ON COLUMN "opdefemp"."cuecajchi" IS 'null';

COMMENT ON COLUMN "opdefemp"."tipcajchi" IS 'null';

COMMENT ON COLUMN "opdefemp"."monapecajchi" IS 'null';

COMMENT ON COLUMN "opdefemp"."porrepcajchi" IS 'null';

COMMENT ON COLUMN "opdefemp"."tiprencajchi" IS 'null';

COMMENT ON COLUMN "opdefemp"."numinicajchi" IS 'null';

COMMENT ON COLUMN "opdefemp"."cedrifcajchi" IS 'null';

COMMENT ON COLUMN "opdefemp"."codcatcajchi" IS 'null';

COMMENT ON COLUMN "opdefemp"."gencomalc" IS 'null';

COMMENT ON COLUMN "opdefemp"."reqaprord" IS 'null';

COMMENT ON COLUMN "opdefemp"."ordcomptot" IS 'null';

COMMENT ON COLUMN "opdefemp"."ordmotanu" IS 'null';

COMMENT ON COLUMN "opdefemp"."manbloqban" IS 'null';

COMMENT ON COLUMN "opdefemp"."ordret" IS 'null';

COMMENT ON COLUMN "opdefemp"."ordconpre" IS 'null';

COMMENT ON COLUMN "opdefemp"."ordtna" IS 'null';

COMMENT ON COLUMN "opdefemp"."ordtba" IS 'null';

COMMENT ON COLUMN "opdefemp"."ordcre" IS 'null';

COMMENT ON COLUMN "opdefemp"."ordsolpag" IS 'null';

COMMENT ON COLUMN "opdefemp"."monche" IS 'null';

COMMENT ON COLUMN "opdefemp"."codmon" IS 'null';

COMMENT ON COLUMN "opdefemp"."tippag" IS 'Código del Tipo de Documento';

COMMENT ON COLUMN "opdefemp"."corpagele" IS 'Correlativo para los Pagos Electronicos';

COMMENT ON COLUMN "opdefemp"."tipagele" IS 'Tipo de Pago electronico';

COMMENT ON COLUMN "opdefemp"."cormovele" IS 'Correlativo para los mov electronicos';

COMMENT ON COLUMN "opdefemp"."rifemp" IS 'RIF de la Empresa';

COMMENT ON COLUMN "opdefemp"."numneg" IS 'Número de Negociación de la Empresa';

COMMENT ON COLUMN "opdefemp"."ordamo" IS 'Tipo de Orden de Amortización';

COMMENT ON COLUMN "opdefemp"."ordant" IS 'Tipo de Orden de Pago de Deudas Anteriores';

COMMENT ON COLUMN "opdefemp"."ordantcom" IS 'Tipo de Orden de Pago de Deudas Anteriores que refieren a Compromiso';

COMMENT ON COLUMN "opdefemp"."corciecaj" IS 'Correlativo para Cierre de Caja Chica';

COMMENT ON COLUMN "opdefemp"."ordhcm" IS 'Tipo de Orden de Pago HCM';

COMMENT ON COLUMN "opdefemp"."comiva" IS 'nombre del reporte de comprobante de retencion de IVA';

COMMENT ON COLUMN "opdefemp"."comislr" IS 'nombre del reporte de comprobante de retencion de ISLR';

COMMENT ON COLUMN "opdefemp"."comltf" IS 'nombre del reporte de comprobante de retencion de LTF';

COMMENT ON COLUMN "opdefemp"."comrs" IS 'nombre del reporte de comprobante de retencion de RS';

COMMENT ON COLUMN "opdefemp"."ordcarava" IS 'Tipo de Causado para Generar Ordenes de Pago de Carta Aval';

COMMENT ON COLUMN "opdefemp"."ordciecaj" IS 'Tipo de Causado para Generar Ordenes de Pago de Cierre Caja Chica';

COMMENT ON COLUMN "opdefemp"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opdetaut
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opdetaut" CASCADE;

CREATE TABLE "opdetaut"
(
    "numord" VARCHAR(8) NOT NULL,
    "refcom" VARCHAR(8),
    "codpre" VARCHAR(50) NOT NULL,
    "moncau" NUMERIC(14,2) NOT NULL,
    "mondes" NUMERIC(14,2),
    "monret" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opdetaut" IS 'null';

COMMENT ON COLUMN "opdetaut"."numord" IS 'null';

COMMENT ON COLUMN "opdetaut"."refcom" IS 'null';

COMMENT ON COLUMN "opdetaut"."codpre" IS 'null';

COMMENT ON COLUMN "opdetaut"."moncau" IS 'null';

COMMENT ON COLUMN "opdetaut"."mondes" IS 'null';

COMMENT ON COLUMN "opdetaut"."monret" IS 'null';

COMMENT ON COLUMN "opdetaut"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opdetfac
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opdetfac" CASCADE;

CREATE TABLE "opdetfac"
(
    "numord" VARCHAR(8),
    "desdet" VARCHAR(255),
    "mondet" NUMERIC(20,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opdetfac" IS 'null';

COMMENT ON COLUMN "opdetfac"."numord" IS 'null';

COMMENT ON COLUMN "opdetfac"."desdet" IS 'null';

COMMENT ON COLUMN "opdetfac"."mondet" IS 'null';

COMMENT ON COLUMN "opdetfac"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opdetord
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opdetord" CASCADE;

CREATE TABLE "opdetord"
(
    "numord" VARCHAR(8) NOT NULL,
    "refcom" VARCHAR(8),
    "codpre" VARCHAR(50) NOT NULL,
    "moncau" NUMERIC(14,2) NOT NULL,
    "mondes" NUMERIC(14,2),
    "monret" NUMERIC(14,2),
    "refsal" VARCHAR(2000),
    "reffon" VARCHAR(1000),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opdetord" IS 'Tabla que contiene información referente al detalle de la orden de pago';

COMMENT ON COLUMN "opdetord"."numord" IS 'Número de la Orden de Pago';

COMMENT ON COLUMN "opdetord"."refcom" IS 'Referencia del Comprobante';

COMMENT ON COLUMN "opdetord"."codpre" IS 'Código Presupuestario';

COMMENT ON COLUMN "opdetord"."moncau" IS 'Monto a Causar';

COMMENT ON COLUMN "opdetord"."mondes" IS 'Monto del Descuento';

COMMENT ON COLUMN "opdetord"."monret" IS 'Monto del Descuento';

COMMENT ON COLUMN "opdetord"."refsal" IS 'null';

COMMENT ON COLUMN "opdetord"."reffon" IS 'null';

COMMENT ON COLUMN "opdetord"."id" IS 'Identificador Único del registro';

CREATE INDEX "i01opdetord" ON "opdetord" ("numord","codpre");

CREATE INDEX "i02opdetord" ON "opdetord" ("codpre");

-----------------------------------------------------------------------
-- opdetper
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opdetper" CASCADE;

CREATE TABLE "opdetper"
(
    "refopp" VARCHAR(8) NOT NULL,
    "refcuo" VARCHAR(8) NOT NULL,
    "fecinicuo" DATE NOT NULL,
    "fecfincuo" DATE NOT NULL,
    "moncuo" NUMERIC(14,2) NOT NULL,
    "fecpag" DATE,
    "numord" VARCHAR(8),
    "ctaban" VARCHAR(20),
    "numche" VARCHAR(20),
    "tipmov" VARCHAR(4),
    "monpag" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opdetper" IS 'Tabla que contiene información referente al detalle de la Orden de Pago Permanente.';

COMMENT ON COLUMN "opdetper"."refopp" IS 'Número de Referencia de la Orden de Pago Permanente';

COMMENT ON COLUMN "opdetper"."refcuo" IS 'Número de Referencia de    la Cuota.';

COMMENT ON COLUMN "opdetper"."fecinicuo" IS 'Fecha de Inicio de la Cuota';

COMMENT ON COLUMN "opdetper"."fecfincuo" IS 'Fecha de Fin de la Cuota';

COMMENT ON COLUMN "opdetper"."moncuo" IS 'Monto de la Cuota';

COMMENT ON COLUMN "opdetper"."fecpag" IS 'Fecha de Pago';

COMMENT ON COLUMN "opdetper"."numord" IS 'Número de la Orden de Pago';

COMMENT ON COLUMN "opdetper"."ctaban" IS 'Código de la Cuenta Bancaria';

COMMENT ON COLUMN "opdetper"."numche" IS 'Número del Cheque con que se pago la Orden de Pago';

COMMENT ON COLUMN "opdetper"."tipmov" IS 'Código del Tipo de Movimiento';

COMMENT ON COLUMN "opdetper"."monpag" IS 'Monto del Pago';

COMMENT ON COLUMN "opdetper"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opdisfue
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opdisfue" CASCADE;

CREATE TABLE "opdisfue"
(
    "numord" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(50) NOT NULL,
    "fuefin" VARCHAR(4),
    "monfue" NUMERIC(14,2),
    "correl" VARCHAR(10),
    "origen" VARCHAR(10),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opdisfue" IS 'null';

COMMENT ON COLUMN "opdisfue"."numord" IS 'null';

COMMENT ON COLUMN "opdisfue"."codpre" IS 'null';

COMMENT ON COLUMN "opdisfue"."fuefin" IS 'null';

COMMENT ON COLUMN "opdisfue"."monfue" IS 'null';

COMMENT ON COLUMN "opdisfue"."correl" IS 'null';

COMMENT ON COLUMN "opdisfue"."origen" IS 'null';

COMMENT ON COLUMN "opdisfue"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opfactur
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opfactur" CASCADE;

CREATE TABLE "opfactur"
(
    "numord" VARCHAR(8),
    "fecfac" DATE,
    "numfac" VARCHAR(50),
    "numctr" VARCHAR(50),
    "tiptra" VARCHAR(10),
    "totfac" NUMERIC(18,2),
    "exeiva" NUMERIC(18,2),
    "basimp" NUMERIC(18,2),
    "poriva" NUMERIC(18,2),
    "moniva" NUMERIC(18,2),
    "monret" NUMERIC(18,2),
    "basltf" NUMERIC(18,2),
    "porltf" NUMERIC(18,2),
    "monltf" NUMERIC(18,2),
    "basislr" NUMERIC(18,2),
    "porislr" NUMERIC(18,2),
    "monislr" NUMERIC(18,2),
    "codislr" VARCHAR(50),
    "rifalt" VARCHAR(20),
    "facafe" VARCHAR(50),
    "observacion" VARCHAR(250),
    "basirs" NUMERIC(18,2),
    "porirs" NUMERIC(18,2),
    "monirs" NUMERIC(18,2),
    "codirs" VARCHAR(50),
    "basimn" NUMERIC(18,2),
    "porimn" NUMERIC(18,2),
    "monimn" NUMERIC(18,2),
    "codimn" VARCHAR(50),
    "fecrecfac" DATE,
    "mnosuj" NUMERIC(18,2),
    "mdecre" NUMERIC(18,2),
    "monexo" NUMERIC(18,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opfactur" IS 'Tabla que contiene información referente a la factura canceladas por órdenes de pago.';

COMMENT ON COLUMN "opfactur"."numord" IS 'Número de la Orden';

COMMENT ON COLUMN "opfactur"."fecfac" IS 'Fecha de la Factura';

COMMENT ON COLUMN "opfactur"."numfac" IS 'Número de la Factura';

COMMENT ON COLUMN "opfactur"."numctr" IS 'Numero de Control de la Factura';

COMMENT ON COLUMN "opfactur"."tiptra" IS 'Tipo de Transacción';

COMMENT ON COLUMN "opfactur"."totfac" IS 'Total de de la Factura';

COMMENT ON COLUMN "opfactur"."exeiva" IS 'Monto exento de IVA';

COMMENT ON COLUMN "opfactur"."basimp" IS 'Base Imponible';

COMMENT ON COLUMN "opfactur"."poriva" IS 'Porcentaje del IVA';

COMMENT ON COLUMN "opfactur"."moniva" IS 'null';

COMMENT ON COLUMN "opfactur"."monret" IS 'Monto de Retención del IVA';

COMMENT ON COLUMN "opfactur"."basltf" IS 'Base Imponible para el Calculo';

COMMENT ON COLUMN "opfactur"."porltf" IS 'Porcentaje de Retención';

COMMENT ON COLUMN "opfactur"."monltf" IS 'Monto Retenido en la Factura';

COMMENT ON COLUMN "opfactur"."basislr" IS 'Base Imponible para el ISLR';

COMMENT ON COLUMN "opfactur"."porislr" IS 'Porcentaje del ISLR';

COMMENT ON COLUMN "opfactur"."monislr" IS 'Monto del ISLR';

COMMENT ON COLUMN "opfactur"."codislr" IS 'Código del ISLR';

COMMENT ON COLUMN "opfactur"."rifalt" IS 'RIF Alterno para Caso de Sucursales';

COMMENT ON COLUMN "opfactur"."facafe" IS 'null';

COMMENT ON COLUMN "opfactur"."observacion" IS 'null';

COMMENT ON COLUMN "opfactur"."basirs" IS 'null';

COMMENT ON COLUMN "opfactur"."porirs" IS 'null';

COMMENT ON COLUMN "opfactur"."monirs" IS 'null';

COMMENT ON COLUMN "opfactur"."codirs" IS 'null';

COMMENT ON COLUMN "opfactur"."basimn" IS 'null';

COMMENT ON COLUMN "opfactur"."porimn" IS 'null';

COMMENT ON COLUMN "opfactur"."monimn" IS 'null';

COMMENT ON COLUMN "opfactur"."codimn" IS 'null';

COMMENT ON COLUMN "opfactur"."fecrecfac" IS 'Fecha Recepción de la Factura';

COMMENT ON COLUMN "opfactur"."mnosuj" IS 'Monto No Sujeto';

COMMENT ON COLUMN "opfactur"."mdecre" IS 'Monto Sin Derecho a Crédito';

COMMENT ON COLUMN "opfactur"."monexo" IS 'Monto Exonerado';

COMMENT ON COLUMN "opfactur"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opordche
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opordche" CASCADE;

CREATE TABLE "opordche"
(
    "numord" VARCHAR(8) NOT NULL,
    "numche" VARCHAR(20) NOT NULL,
    "codcta" VARCHAR(32) NOT NULL,
    "monpag" NUMERIC(20,2),
    "tipmov" VARCHAR(4) NOT NULL,
    "compret" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opordche" IS 'Tabla que contiene información referente a los cheques usados para cancelar una determinada orden de pago';

COMMENT ON COLUMN "opordche"."numord" IS 'Número de la Orden';

COMMENT ON COLUMN "opordche"."numche" IS 'Número del Cheque';

COMMENT ON COLUMN "opordche"."codcta" IS 'Código de la Cuenta Contable';

COMMENT ON COLUMN "opordche"."monpag" IS 'Monto del pago';

COMMENT ON COLUMN "opordche"."tipmov" IS 'null';

COMMENT ON COLUMN "opordche"."compret" IS 'Si tiene el comprobante de Retención';

COMMENT ON COLUMN "opordche"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opordpag
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opordpag" CASCADE;

CREATE TABLE "opordpag"
(
    "numord" VARCHAR(8) NOT NULL,
    "tipcau" VARCHAR(4) NOT NULL,
    "fecemi" DATE NOT NULL,
    "cedrif" VARCHAR(15) NOT NULL,
    "nomben" VARCHAR(250) NOT NULL,
    "monord" NUMERIC(14,2) NOT NULL,
    "desord" VARCHAR(1000) NOT NULL,
    "mondes" NUMERIC(14,2),
    "monret" NUMERIC(14,2),
    "numche" VARCHAR(20),
    "ctaban" VARCHAR(20),
    "ctapag" VARCHAR(32),
    "numcom" VARCHAR(8),
    "status" VARCHAR(1) NOT NULL,
    "coduni" VARCHAR(30),
    "fecenvcon" DATE,
    "fecenvfin" DATE,
    "ctapagfin" VARCHAR(32),
    "obsord" VARCHAR(250),
    "fecven" DATE,
    "fecanu" DATE,
    "desanu" VARCHAR(250),
    "monpag" NUMERIC(16,2),
    "aproba" VARCHAR(1),
    "nombensus" VARCHAR(250),
    "fecrecfin" DATE,
    "anopre" VARCHAR(4),
    "fecpag" DATE,
    "numtiq" VARCHAR(8),
    "peraut" VARCHAR(500),
    "cedaut" VARCHAR(100),
    "nomper2" VARCHAR(50),
    "nomper1" VARCHAR(50),
    "horcon" VARCHAR(10),
    "feccon" DATE,
    "nomcat" VARCHAR(250),
    "numfac" VARCHAR(20),
    "numconfac" VARCHAR(20),
    "numcorfac" VARCHAR(20),
    "fechafac" DATE,
    "fecfac" DATE,
    "tipfin" VARCHAR(4),
    "comret" VARCHAR(20),
    "feccomret" DATE,
    "comretislr" VARCHAR(20),
    "feccomretislr" DATE,
    "comretltf" VARCHAR(20),
    "feccomretltf" DATE,
    "comretfiel" VARCHAR(20),
    "feccomretfiel" DATE,
    "comretlab" VARCHAR(20),
    "feccomretlab" DATE,
    "numsigecof" VARCHAR(8),
    "fecsigecof" DATE,
    "expsigecof" VARCHAR(8),
    "aprobadoord" VARCHAR(1),
    "codmotanu" VARCHAR(4),
    "usuanu" VARCHAR(250),
    "aprobadotes" VARCHAR(1),
    "fecret" DATE NOT NULL,
    "numcue" VARCHAR(20),
    "numcomapr" VARCHAR(8),
    "codconcepto" VARCHAR(4),
    "numforpre" VARCHAR(15),
    "motrecord" VARCHAR(500),
    "motrectes" VARCHAR(500),
    "aprorddir" VARCHAR(1),
    "codcajchi" VARCHAR(3),
    "numcta" VARCHAR(20),
    "tipdoc" VARCHAR(4),
    "loguse" VARCHAR(50),
    "codfonant" VARCHAR(3),
    "amortiza" NUMERIC(14,2),
    "codmon" VARCHAR(3) NOT NULL,
    "valmon" NUMERIC(14,6),
    "ordsnc" VARCHAR(1),
    "codsnc" VARCHAR(20),
    "fecini" DATE,
    "fecfin" DATE,
    "medcom" VARCHAR(1),
    "modcon" VARCHAR(1),
    "lugeje" VARCHAR(16),
    "numcon" VARCHAR(20),
    "mescon" VARCHAR(2),
    "staele" VARCHAR(1),
    "codtde" VARCHAR(3),
    "codadi" VARCHAR(2),
    "numordamo" VARCHAR(8),
    "codrgo" VARCHAR(4),
    "monrgo" NUMERIC(14,2),
    "codpro" VARCHAR(4),
    "coddirec" VARCHAR(4),
    "refava" VARCHAR(20),
    "fecava" DATE,
    "nompacava" VARCHAR(200),
    "cedpacava" VARCHAR(10),
    "motsolava" VARCHAR(500),
    "moncarava" NUMERIC(14,2),
    "usuaprord" VARCHAR(50),
    "fecaprord" DATE,
    "usuaprdir" VARCHAR(50),
    "fecaprdir" DATE,
    "usuaprtes" VARCHAR(50),
    "fecaprtes" DATE,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opordpag" IS 'Tabla que contiene información referente a la orden pago';

COMMENT ON COLUMN "opordpag"."numord" IS 'Número de la Orden';

COMMENT ON COLUMN "opordpag"."tipcau" IS 'Tipo de Causado';

COMMENT ON COLUMN "opordpag"."fecemi" IS 'Fecha de Emisión';

COMMENT ON COLUMN "opordpag"."cedrif" IS 'Cédula o RIF del Beneficiario';

COMMENT ON COLUMN "opordpag"."nomben" IS 'Nombre del Beneficiario';

COMMENT ON COLUMN "opordpag"."monord" IS 'Monto de la Orden';

COMMENT ON COLUMN "opordpag"."desord" IS 'Descripción de la Orden';

COMMENT ON COLUMN "opordpag"."mondes" IS 'Monto de Descuento';

COMMENT ON COLUMN "opordpag"."monret" IS 'Monto de Retención';

COMMENT ON COLUMN "opordpag"."numche" IS 'Número del Cheque';

COMMENT ON COLUMN "opordpag"."ctaban" IS 'Cuenta Bancaria';

COMMENT ON COLUMN "opordpag"."ctapag" IS 'Código de la Cuenta por Pagar';

COMMENT ON COLUMN "opordpag"."numcom" IS 'Número de Comprobante Contable';

COMMENT ON COLUMN "opordpag"."status" IS 'Etatus de la Orden';

COMMENT ON COLUMN "opordpag"."coduni" IS 'Código de la Unidad de Origen';

COMMENT ON COLUMN "opordpag"."fecenvcon" IS 'Fecha de Envió a Contraloría';

COMMENT ON COLUMN "opordpag"."fecenvfin" IS 'Fecha de Envió a  Finanzas';

COMMENT ON COLUMN "opordpag"."ctapagfin" IS 'Cuenta Contable de Finanzas';

COMMENT ON COLUMN "opordpag"."obsord" IS 'Observaciones';

COMMENT ON COLUMN "opordpag"."fecven" IS 'Fecha de Vencimiento de la Orden';

COMMENT ON COLUMN "opordpag"."fecanu" IS 'Fecha de Anulación';

COMMENT ON COLUMN "opordpag"."desanu" IS 'Descripción de la anulación';

COMMENT ON COLUMN "opordpag"."monpag" IS 'Monto del Cheque';

COMMENT ON COLUMN "opordpag"."aproba" IS 'Estatus de aprobación de la orden';

COMMENT ON COLUMN "opordpag"."nombensus" IS 'Beneficiario Sustituto';

COMMENT ON COLUMN "opordpag"."fecrecfin" IS 'Fecha de Recepción de Finanzas';

COMMENT ON COLUMN "opordpag"."anopre" IS 'Año Presupuestario';

COMMENT ON COLUMN "opordpag"."fecpag" IS 'Fecha del Pago';

COMMENT ON COLUMN "opordpag"."numtiq" IS 'Número de Ticket';

COMMENT ON COLUMN "opordpag"."peraut" IS 'Persona Autorizada por el Ticket';

COMMENT ON COLUMN "opordpag"."cedaut" IS 'Cédula de la Persona Autorizada por el Ticket';

COMMENT ON COLUMN "opordpag"."nomper2" IS 'Nombre de la Persona que recibió la Llamada de Gestión';

COMMENT ON COLUMN "opordpag"."nomper1" IS 'Nombre de la Persona que recibió la Llamada de Gestión';

COMMENT ON COLUMN "opordpag"."horcon" IS 'Hora de Conformación';

COMMENT ON COLUMN "opordpag"."feccon" IS 'Fecha de Conformación';

COMMENT ON COLUMN "opordpag"."nomcat" IS 'null';

COMMENT ON COLUMN "opordpag"."numfac" IS 'null';

COMMENT ON COLUMN "opordpag"."numconfac" IS 'Numero de Control de Factura';

COMMENT ON COLUMN "opordpag"."numcorfac" IS 'Numero de Correlativo de Factura';

COMMENT ON COLUMN "opordpag"."fechafac" IS 'Fecha de la Factura';

COMMENT ON COLUMN "opordpag"."fecfac" IS 'Fecha de la Factura';

COMMENT ON COLUMN "opordpag"."tipfin" IS 'Tipo de Financiamiento';

COMMENT ON COLUMN "opordpag"."comret" IS 'Numero de Comprobante de Retención de IVA';

COMMENT ON COLUMN "opordpag"."feccomret" IS 'Fecha de Comprobante de Retención de IVA';

COMMENT ON COLUMN "opordpag"."comretislr" IS 'Número de Comprobante de Retención de ISLR';

COMMENT ON COLUMN "opordpag"."feccomretislr" IS 'Fecha de Comprobante de Retención de ISLR';

COMMENT ON COLUMN "opordpag"."comretltf" IS 'Número de Comprobante de Retención de Ley de Timbre Fiscal';

COMMENT ON COLUMN "opordpag"."feccomretltf" IS 'Fecha de Comprobante de Retención de Ley de Timbre Fiscal';

COMMENT ON COLUMN "opordpag"."comretfiel" IS 'Número de Comprobante de Retención de Garantía de Fiel Cumplimiento';

COMMENT ON COLUMN "opordpag"."feccomretfiel" IS 'Fecha de Comprobante de Retención de Garantía de Fiel Cumplimiento';

COMMENT ON COLUMN "opordpag"."comretlab" IS 'Número de Comprobante de Retención Laboral';

COMMENT ON COLUMN "opordpag"."feccomretlab" IS 'Fecha de Comprobante de Retención Laboral';

COMMENT ON COLUMN "opordpag"."numsigecof" IS 'null';

COMMENT ON COLUMN "opordpag"."fecsigecof" IS 'null';

COMMENT ON COLUMN "opordpag"."expsigecof" IS 'null';

COMMENT ON COLUMN "opordpag"."aprobadoord" IS 'null';

COMMENT ON COLUMN "opordpag"."codmotanu" IS 'null';

COMMENT ON COLUMN "opordpag"."usuanu" IS 'null';

COMMENT ON COLUMN "opordpag"."fecret" IS 'null';

COMMENT ON COLUMN "opordpag"."numcue" IS 'null';

COMMENT ON COLUMN "opordpag"."numcomapr" IS 'null';

COMMENT ON COLUMN "opordpag"."codconcepto" IS 'null';

COMMENT ON COLUMN "opordpag"."numforpre" IS 'null';

COMMENT ON COLUMN "opordpag"."motrecord" IS 'null';

COMMENT ON COLUMN "opordpag"."motrectes" IS 'null';

COMMENT ON COLUMN "opordpag"."aprorddir" IS 'null';

COMMENT ON COLUMN "opordpag"."codcajchi" IS 'null';

COMMENT ON COLUMN "opordpag"."numcta" IS 'null';

COMMENT ON COLUMN "opordpag"."tipdoc" IS 'null';

COMMENT ON COLUMN "opordpag"."loguse" IS 'null';

COMMENT ON COLUMN "opordpag"."codfonant" IS 'null';

COMMENT ON COLUMN "opordpag"."amortiza" IS 'null';

COMMENT ON COLUMN "opordpag"."valmon" IS 'null';

COMMENT ON COLUMN "opordpag"."ordsnc" IS 'null';

COMMENT ON COLUMN "opordpag"."codsnc" IS 'null';

COMMENT ON COLUMN "opordpag"."fecini" IS 'null';

COMMENT ON COLUMN "opordpag"."fecfin" IS 'null';

COMMENT ON COLUMN "opordpag"."medcom" IS 'null';

COMMENT ON COLUMN "opordpag"."modcon" IS 'null';

COMMENT ON COLUMN "opordpag"."lugeje" IS 'null';

COMMENT ON COLUMN "opordpag"."numcon" IS 'null';

COMMENT ON COLUMN "opordpag"."mescon" IS 'null';

COMMENT ON COLUMN "opordpag"."staele" IS 'Estatus creado para indicar las OP que se haran por Pagos Electronicos';

COMMENT ON COLUMN "opordpag"."codtde" IS 'codigo del tipo de descuento';

COMMENT ON COLUMN "opordpag"."codadi" IS 'Código adicional del banco';

COMMENT ON COLUMN "opordpag"."numordamo" IS 'Número de la Orden de Amortización';

COMMENT ON COLUMN "opordpag"."codrgo" IS 'Código recargo';

COMMENT ON COLUMN "opordpag"."monrgo" IS 'Monto del Recargo';

COMMENT ON COLUMN "opordpag"."codpro" IS 'Código del Proyecto';

COMMENT ON COLUMN "opordpag"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "opordpag"."refava" IS 'Referencia de la Carta Aval';

COMMENT ON COLUMN "opordpag"."fecava" IS 'Fecha de la Carta Aval';

COMMENT ON COLUMN "opordpag"."nompacava" IS 'Nombre del Paciente de la Carta Aval';

COMMENT ON COLUMN "opordpag"."cedpacava" IS 'Cédula del Paciente de la Carta Aval';

COMMENT ON COLUMN "opordpag"."motsolava" IS 'Motivo de Solicitud de la Carta Aval';

COMMENT ON COLUMN "opordpag"."moncarava" IS 'Monto de la Carta Aval';

COMMENT ON COLUMN "opordpag"."usuaprord" IS 'usuario que realizo la aprobación en Ordenamiento de Pago';

COMMENT ON COLUMN "opordpag"."fecaprord" IS 'fecha en que realizo la aprobación en Ordenamiento de Pago';

COMMENT ON COLUMN "opordpag"."usuaprdir" IS 'usuario que realizo la aprobación de la Orden Directa';

COMMENT ON COLUMN "opordpag"."fecaprdir" IS 'fecha en que realizo la aprobación de la Orden Directa';

COMMENT ON COLUMN "opordpag"."usuaprtes" IS 'usuario que realizo la aprobación en Tesoreria';

COMMENT ON COLUMN "opordpag"."fecaprtes" IS 'fecha en que realizo la aprobación en Tesoreria';

COMMENT ON COLUMN "opordpag"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opordper
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opordper" CASCADE;

CREATE TABLE "opordper"
(
    "refopp" VARCHAR(8) NOT NULL,
    "desopp" VARCHAR(250) NOT NULL,
    "fecemi" DATE NOT NULL,
    "numcuo" NUMERIC(6,0) NOT NULL,
    "freopp" VARCHAR(1) NOT NULL,
    "cedrif" VARCHAR(15) NOT NULL,
    "monopp" NUMERIC(14,2) NOT NULL,
    "staord" VARCHAR(1) NOT NULL,
    "numcue" VARCHAR(20),
    "ctaban" VARCHAR(20),
    "tipdoc" VARCHAR(4),
    "refere" VARCHAR(8),
    "coduni" VARCHAR(30),
    "tippag" VARCHAR(1),
    "numtiq" VARCHAR(8),
    "peraut" VARCHAR(40),
    "cedaut" VARCHAR(15),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opordper" IS 'null';

COMMENT ON COLUMN "opordper"."refopp" IS 'null';

COMMENT ON COLUMN "opordper"."desopp" IS 'null';

COMMENT ON COLUMN "opordper"."fecemi" IS 'null';

COMMENT ON COLUMN "opordper"."numcuo" IS 'null';

COMMENT ON COLUMN "opordper"."freopp" IS 'null';

COMMENT ON COLUMN "opordper"."cedrif" IS 'null';

COMMENT ON COLUMN "opordper"."monopp" IS 'null';

COMMENT ON COLUMN "opordper"."numcue" IS 'null';

COMMENT ON COLUMN "opordper"."ctaban" IS 'null';

COMMENT ON COLUMN "opordper"."tipdoc" IS 'null';

COMMENT ON COLUMN "opordper"."refere" IS 'null';

COMMENT ON COLUMN "opordper"."coduni" IS 'null';

COMMENT ON COLUMN "opordper"."tippag" IS 'null';

COMMENT ON COLUMN "opordper"."numtiq" IS 'null';

COMMENT ON COLUMN "opordper"."peraut" IS 'null';

COMMENT ON COLUMN "opordper"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opretcon
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opretcon" CASCADE;

CREATE TABLE "opretcon"
(
    "codcon" VARCHAR(3) NOT NULL,
    "codtip" VARCHAR(3) NOT NULL,
    "codnom" VARCHAR(3) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opretcon" IS 'Tabla que contiene información referente a concepto de retenciones de nomina';

COMMENT ON COLUMN "opretcon"."codcon" IS 'Código del Concepto';

COMMENT ON COLUMN "opretcon"."codtip" IS 'Código del Tipo de la Retención';

COMMENT ON COLUMN "opretcon"."codnom" IS 'Código de la Nomina';

COMMENT ON COLUMN "opretcon"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opretord
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opretord" CASCADE;

CREATE TABLE "opretord"
(
    "numord" VARCHAR(8) NOT NULL,
    "codtip" VARCHAR(3) NOT NULL,
    "monret" NUMERIC(14,2) NOT NULL,
    "codpre" VARCHAR(50) NOT NULL,
    "numret" VARCHAR(8),
    "refere" VARCHAR(8),
    "correl" NUMERIC(3,0),
    "monbas" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opretord" IS 'Tabla que contiene información referente a la orden de pago de retención';

COMMENT ON COLUMN "opretord"."numord" IS 'Número de Orden';

COMMENT ON COLUMN "opretord"."codtip" IS 'Tipo de Retención';

COMMENT ON COLUMN "opretord"."monret" IS 'Monto de Retención';

COMMENT ON COLUMN "opretord"."codpre" IS 'Código Presupuestario';

COMMENT ON COLUMN "opretord"."numret" IS 'Numero de la Orden de Pago de la Retención';

COMMENT ON COLUMN "opretord"."refere" IS 'Referencia del Compromiso';

COMMENT ON COLUMN "opretord"."correl" IS 'Referencia del Compromiso';

COMMENT ON COLUMN "opretord"."monbas" IS 'null';

COMMENT ON COLUMN "opretord"."id" IS 'Identificador Único del registro';

CREATE INDEX "i01opretord" ON "opretord" ("numord","codtip");

-----------------------------------------------------------------------
-- optipben
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "optipben" CASCADE;

CREATE TABLE "optipben"
(
    "codtipben" VARCHAR(3) NOT NULL,
    "destipben" VARCHAR(50) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_optipben_codtipben" UNIQUE ("codtipben")
);

COMMENT ON TABLE "optipben" IS 'Tabla que contiene información referente al tipo de beneficiario';

COMMENT ON COLUMN "optipben"."codtipben" IS 'Código del Tipo de Beneficiario';

COMMENT ON COLUMN "optipben"."destipben" IS 'Descripción del Tipo de Beneficiario';

COMMENT ON COLUMN "optipben"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- optipret
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "optipret" CASCADE;

CREATE TABLE "optipret"
(
    "codtip" VARCHAR(3) NOT NULL,
    "destip" VARCHAR(250) NOT NULL,
    "codcon" VARCHAR(32) NOT NULL,
    "basimp" NUMERIC(14,2),
    "porret" NUMERIC(14,2),
    "unitri" NUMERIC(14,2),
    "porsus" NUMERIC(14,2),
    "factor" NUMERIC(16,6),
    "codtipsen" VARCHAR(3),
    "mbasmi" NUMERIC(14,2),
    "mbasma" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_optipret_codtip" UNIQUE ("codtip")
);

COMMENT ON TABLE "optipret" IS 'Tabla que contiene información referente al tipo de retención';

COMMENT ON COLUMN "optipret"."codtip" IS 'Código del Tipo de Beneficiario';

COMMENT ON COLUMN "optipret"."destip" IS 'Descripción del Tipo de Beneficiario';

COMMENT ON COLUMN "optipret"."codcon" IS 'Código Contable';

COMMENT ON COLUMN "optipret"."basimp" IS 'Base Imponible';

COMMENT ON COLUMN "optipret"."porret" IS 'Porcentaje de Retención';

COMMENT ON COLUMN "optipret"."unitri" IS 'Unidades Tributarias';

COMMENT ON COLUMN "optipret"."porsus" IS 'Porcentaje usado para el Calculo del Sustraendo';

COMMENT ON COLUMN "optipret"."factor" IS 'null';

COMMENT ON COLUMN "optipret"."codtipsen" IS 'null';

COMMENT ON COLUMN "optipret"."mbasmi" IS 'null';

COMMENT ON COLUMN "optipret"."mbasma" IS 'null';

COMMENT ON COLUMN "optipret"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsantici
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsantici" CASCADE;

CREATE TABLE "tsantici"
(
    "refant" VARCHAR(20) NOT NULL,
    "desant" VARCHAR(250),
    "cedrif" VARCHAR(15) NOT NULL,
    "refcom" VARCHAR(8) NOT NULL,
    "fecant" DATE NOT NULL,
    "monto" NUMERIC(14,2),
    "saldo" NUMERIC(14,2),
    "numcom" VARCHAR(8),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsantici" IS 'null';

COMMENT ON COLUMN "tsantici"."refant" IS 'null';

COMMENT ON COLUMN "tsantici"."desant" IS 'null';

COMMENT ON COLUMN "tsantici"."cedrif" IS 'null';

COMMENT ON COLUMN "tsantici"."refcom" IS 'null';

COMMENT ON COLUMN "tsantici"."fecant" IS 'null';

COMMENT ON COLUMN "tsantici"."monto" IS 'null';

COMMENT ON COLUMN "tsantici"."saldo" IS 'null';

COMMENT ON COLUMN "tsantici"."numcom" IS 'null';

COMMENT ON COLUMN "tsantici"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsantord
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsantord" CASCADE;

CREATE TABLE "tsantord"
(
    "numord" VARCHAR(8) NOT NULL,
    "numche" VARCHAR(20) NOT NULL,
    "monto" NUMERIC(14,2),
    "refant" VARCHAR(20) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsantord" IS 'null';

COMMENT ON COLUMN "tsantord"."numord" IS 'null';

COMMENT ON COLUMN "tsantord"."numche" IS 'null';

COMMENT ON COLUMN "tsantord"."monto" IS 'null';

COMMENT ON COLUMN "tsantord"."refant" IS 'null';

COMMENT ON COLUMN "tsantord"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsasifte
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsasifte" CASCADE;

CREATE TABLE "tsasifte"
(
    "numche" VARCHAR(20) NOT NULL,
    "codtipfte" VARCHAR(3) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsasifte" IS 'null';

COMMENT ON COLUMN "tsasifte"."numche" IS 'null';

COMMENT ON COLUMN "tsasifte"."codtipfte" IS 'null';

COMMENT ON COLUMN "tsasifte"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tscheemi
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tscheemi" CASCADE;

CREATE TABLE "tscheemi"
(
    "numche" VARCHAR(20) NOT NULL,
    "numcue" VARCHAR(20) NOT NULL,
    "cedrif" VARCHAR(15) NOT NULL,
    "fecemi" DATE NOT NULL,
    "monche" NUMERIC(14,2) NOT NULL,
    "status" VARCHAR(1),
    "codemi" VARCHAR(50),
    "fecent" DATE,
    "codent" VARCHAR(50),
    "obsent" VARCHAR(100),
    "fecanu" DATE,
    "cedrec" VARCHAR(15),
    "nomrec" VARCHAR(50),
    "tipdoc" VARCHAR(4),
    "fecing" DATE,
    "temporal" VARCHAR(100),
    "temporal2" VARCHAR(100),
    "nombensus" VARCHAR(250),
    "anopre" VARCHAR(4),
    "numtiq" VARCHAR(8),
    "cedaut" VARCHAR(15),
    "peraut" VARCHAR(40),
    "numcomegr" VARCHAR(8),
    "motdev" VARCHAR(100),
    "fecdev" DATE,
    "codmon" VARCHAR(3) NOT NULL,
    "valmon" NUMERIC(14,6),
    "codconcepto" VARCHAR(4),
    "loguse" VARCHAR(50),
    "conformado" VARCHAR(1),
    "nomfuncon" VARCHAR(500),
    "agenban" VARCHAR(500),
    "fecconfor" DATE,
    "horconfor" VARCHAR(50),
    "fotper" VARCHAR(250),
    "fotced" VARCHAR(250),
    "fotche" VARCHAR(250),
    "comentcon" VARCHAR(1),
    "coddirec" VARCHAR(4),
    "fecreg" TIMESTAMP,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tscheemi" IS 'Tabla que contiene información referente a los cheques emitidos.';

COMMENT ON COLUMN "tscheemi"."numche" IS 'Número del cheque.';

COMMENT ON COLUMN "tscheemi"."numcue" IS 'Número del cheque.';

COMMENT ON COLUMN "tscheemi"."cedrif" IS 'Cédula o RIF del Beneficiario.';

COMMENT ON COLUMN "tscheemi"."fecemi" IS 'Fecha de Emisión.';

COMMENT ON COLUMN "tscheemi"."monche" IS 'Monto Total del Cheque.';

COMMENT ON COLUMN "tscheemi"."status" IS 'Estatus del Cheque.';

COMMENT ON COLUMN "tscheemi"."codemi" IS 'Código del Usuario que emite el Cheque.';

COMMENT ON COLUMN "tscheemi"."fecent" IS 'Fecha en la cual se Entregó el Cheque.';

COMMENT ON COLUMN "tscheemi"."codent" IS 'Código de Usuario que Entrega.';

COMMENT ON COLUMN "tscheemi"."obsent" IS 'Observación del Cheque.';

COMMENT ON COLUMN "tscheemi"."fecanu" IS 'Fecha de Anulación.';

COMMENT ON COLUMN "tscheemi"."cedrec" IS 'Cedula de la Persona q Recibe el Cheque';

COMMENT ON COLUMN "tscheemi"."nomrec" IS 'Nombre de la persona q recibe el chq';

COMMENT ON COLUMN "tscheemi"."tipdoc" IS 'Tipo de Documento';

COMMENT ON COLUMN "tscheemi"."fecing" IS 'null';

COMMENT ON COLUMN "tscheemi"."temporal" IS 'Campos para almacenar Observaciones de la Entrega del cheque';

COMMENT ON COLUMN "tscheemi"."temporal2" IS 'Campos para almacenar Observaciones de la Entrega del cheque';

COMMENT ON COLUMN "tscheemi"."nombensus" IS 'Nombre del beneficiario sustituto';

COMMENT ON COLUMN "tscheemi"."anopre" IS 'Año Presupuestario';

COMMENT ON COLUMN "tscheemi"."numtiq" IS 'Número del Ticket de Pago';

COMMENT ON COLUMN "tscheemi"."cedaut" IS 'Cédula del Autorizado por el Ticket';

COMMENT ON COLUMN "tscheemi"."peraut" IS 'Nombre de la Persona Autorizada por el Ticket';

COMMENT ON COLUMN "tscheemi"."numcomegr" IS 'null';

COMMENT ON COLUMN "tscheemi"."motdev" IS 'null';

COMMENT ON COLUMN "tscheemi"."fecdev" IS 'null';

COMMENT ON COLUMN "tscheemi"."codmon" IS 'null';

COMMENT ON COLUMN "tscheemi"."valmon" IS 'null';

COMMENT ON COLUMN "tscheemi"."codconcepto" IS 'Concepto de Pago';

COMMENT ON COLUMN "tscheemi"."loguse" IS 'Usuario que hizo el cheque.';

COMMENT ON COLUMN "tscheemi"."conformado" IS '(S-N) Si esta conformado';

COMMENT ON COLUMN "tscheemi"."nomfuncon" IS 'Nombre del Funcionario que hizo la conformación';

COMMENT ON COLUMN "tscheemi"."agenban" IS 'Nombre de la agencia del banco.';

COMMENT ON COLUMN "tscheemi"."fecconfor" IS 'Fecha de la Conformación';

COMMENT ON COLUMN "tscheemi"."horconfor" IS 'Hora  de la Conformación';

COMMENT ON COLUMN "tscheemi"."fotper" IS 'Foto de la Persona que retira el cheque';

COMMENT ON COLUMN "tscheemi"."fotced" IS 'Foto de la Cedula de la Persona que retira el cheque';

COMMENT ON COLUMN "tscheemi"."fotche" IS 'Foto del Cheque a retirar';

COMMENT ON COLUMN "tscheemi"."comentcon" IS '(S-N) Si fue o No entregado el comprobante en Contabilidad';

COMMENT ON COLUMN "tscheemi"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "tscheemi"."fecreg" IS 'Fecha de registro';

COMMENT ON COLUMN "tscheemi"."id" IS 'Identificador Único del registro';

CREATE INDEX "itscheemi2" ON "tscheemi" ("numcue","cedrif");

CREATE INDEX "itscheemi3" ON "tscheemi" ("cedrif");

CREATE INDEX "itscheemi4" ON "tscheemi" ("fecemi");

-----------------------------------------------------------------------
-- tscomprobantes
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tscomprobantes" CASCADE;

CREATE TABLE "tscomprobantes"
(
    "tipo" VARCHAR(4),
    "cedrif" VARCHAR(15),
    "ano" VARCHAR(4),
    "mes" VARCHAR(2),
    "comprobante" VARCHAR(6),
    "numord" VARCHAR(8),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tscomprobantes" IS 'null';

COMMENT ON COLUMN "tscomprobantes"."tipo" IS 'null';

COMMENT ON COLUMN "tscomprobantes"."cedrif" IS 'null';

COMMENT ON COLUMN "tscomprobantes"."ano" IS 'null';

COMMENT ON COLUMN "tscomprobantes"."mes" IS 'null';

COMMENT ON COLUMN "tscomprobantes"."comprobante" IS 'null';

COMMENT ON COLUMN "tscomprobantes"."numord" IS 'null';

COMMENT ON COLUMN "tscomprobantes"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsconcil
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsconcil" CASCADE;

CREATE TABLE "tsconcil"
(
    "numcue" VARCHAR(20) NOT NULL,
    "mescon" VARCHAR(2) NOT NULL,
    "anocon" VARCHAR(4) NOT NULL,
    "refere" VARCHAR(20) NOT NULL,
    "movlib" VARCHAR(4),
    "movban" VARCHAR(4),
    "feclib" DATE,
    "fecban" DATE,
    "desref" VARCHAR(1000),
    "monlib" NUMERIC(14,2),
    "monban" NUMERIC(14,2),
    "result" VARCHAR(50),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsconcil" IS 'Tabla que contiene información referente a las conciliaciones bancarias realizadas.';

COMMENT ON COLUMN "tsconcil"."numcue" IS 'Número de la Cuenta.';

COMMENT ON COLUMN "tsconcil"."mescon" IS 'Número del Mes de la Conciliación.';

COMMENT ON COLUMN "tsconcil"."anocon" IS 'Año de la Conciliación.';

COMMENT ON COLUMN "tsconcil"."refere" IS 'Referencia del Movimiento.';

COMMENT ON COLUMN "tsconcil"."movlib" IS 'Tipo del Movimiento Libro';

COMMENT ON COLUMN "tsconcil"."movban" IS 'Tipo del Movimiento Banco.';

COMMENT ON COLUMN "tsconcil"."feclib" IS 'Fecha del Movimiento Libro.';

COMMENT ON COLUMN "tsconcil"."fecban" IS 'Fecha del Movimiento Banco.';

COMMENT ON COLUMN "tsconcil"."desref" IS 'Descripción del Movimiento.';

COMMENT ON COLUMN "tsconcil"."monlib" IS 'Monto Total en  Libro.';

COMMENT ON COLUMN "tsconcil"."monban" IS 'Monto Total en  Banco.';

COMMENT ON COLUMN "tsconcil"."result" IS 'Indica el Resultado de la Conciliación';

COMMENT ON COLUMN "tsconcil"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsconcilhis
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsconcilhis" CASCADE;

CREATE TABLE "tsconcilhis"
(
    "numcue" VARCHAR(20) NOT NULL,
    "mescon" VARCHAR(2) NOT NULL,
    "anocon" VARCHAR(4) NOT NULL,
    "refere" VARCHAR(20) NOT NULL,
    "movlib" VARCHAR(4),
    "movban" VARCHAR(4),
    "feclib" DATE,
    "fecban" DATE,
    "desref" VARCHAR(1000),
    "monlib" NUMERIC(14,2),
    "monban" NUMERIC(14,2),
    "result" VARCHAR(50),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsconcilhis" IS 'Tabla que contiene información referente al Histórico de las conciliaciones bancarias realizadas.';

COMMENT ON COLUMN "tsconcilhis"."numcue" IS 'Número de la Cuenta.';

COMMENT ON COLUMN "tsconcilhis"."mescon" IS 'Número del Mes de la Conciliación.';

COMMENT ON COLUMN "tsconcilhis"."anocon" IS 'Año de la Conciliación.';

COMMENT ON COLUMN "tsconcilhis"."refere" IS 'Referencia del Movimiento.';

COMMENT ON COLUMN "tsconcilhis"."movlib" IS 'Tipo del Movimiento Libro';

COMMENT ON COLUMN "tsconcilhis"."movban" IS 'Tipo del Movimiento Banco.';

COMMENT ON COLUMN "tsconcilhis"."feclib" IS 'Fecha del Movimiento Libro.';

COMMENT ON COLUMN "tsconcilhis"."fecban" IS 'Fecha del Movimiento Banco.';

COMMENT ON COLUMN "tsconcilhis"."desref" IS 'Descripción del Movimiento.';

COMMENT ON COLUMN "tsconcilhis"."monlib" IS 'Monto Total en  Libro.';

COMMENT ON COLUMN "tsconcilhis"."monban" IS 'Monto Total en  Banco.';

COMMENT ON COLUMN "tsconcilhis"."result" IS 'Indica el Resultado de la Conciliación';

COMMENT ON COLUMN "tsconcilhis"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsconuni
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsconuni" CASCADE;

CREATE TABLE "tsconuni"
(
    "codcta" VARCHAR(32),
    "codsus" VARCHAR(32),
    "nomsus" VARCHAR(255),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsconuni" IS 'null';

COMMENT ON COLUMN "tsconuni"."codcta" IS 'null';

COMMENT ON COLUMN "tsconuni"."codsus" IS 'null';

COMMENT ON COLUMN "tsconuni"."nomsus" IS 'null';

COMMENT ON COLUMN "tsconuni"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsdefban
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsdefban" CASCADE;

CREATE TABLE "tsdefban"
(
    "numcue" VARCHAR(20) NOT NULL,
    "nomcue" VARCHAR(40) NOT NULL,
    "tipcue" VARCHAR(3) NOT NULL,
    "codcta" VARCHAR(32),
    "fecreg" DATE NOT NULL,
    "fecven" DATE,
    "fecper" DATE,
    "renaut" VARCHAR(1),
    "porint" NUMERIC(5,2),
    "tipint" VARCHAR(1),
    "numche" VARCHAR(20),
    "antban" NUMERIC(14,2),
    "debban" NUMERIC(14,2),
    "creban" NUMERIC(14,2),
    "antlib" NUMERIC(14,2),
    "deblib" NUMERIC(14,2),
    "crelib" NUMERIC(14,2),
    "valche" NUMERIC(3,0),
    "concil" VARCHAR(1),
    "plazo" NUMERIC(3,0),
    "fecape" DATE,
    "usocue" VARCHAR(20),
    "tipren" VARCHAR(20) NOT NULL,
    "desenl" VARCHAR(250),
    "porsalmin" NUMERIC(5,2),
    "monsalmin" NUMERIC(14,2),
    "codctaprecoo" VARCHAR(32),
    "codctapreord" VARCHAR(32),
    "trasitoria" VARCHAR(1),
    "salact" NUMERIC(20,2),
    "fecaper" DATE,
    "temnumcue" VARCHAR(20),
    "cantdig" NUMERIC(2,0),
    "endosable" VARCHAR(1),
    "salmin" NUMERIC(20,2),
    "nomrep" VARCHAR(50),
    "codmon" VARCHAR(3),
    "codcom" VARCHAR(3),
    "codtiptra" VARCHAR(3),
    "codadi" VARCHAR(2),
    "codubi" VARCHAR(30),
    "coddirec" VARCHAR(4),
    "conformable" VARCHAR(1),
    "monconfor" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_tsdefban_numcue" UNIQUE ("numcue")
);

COMMENT ON TABLE "tsdefban" IS 'Tabla que contiene información referente a la definición de las Cuentas Bancarias.';

COMMENT ON COLUMN "tsdefban"."numcue" IS 'Número de la Cuenta Bancaria';

COMMENT ON COLUMN "tsdefban"."nomcue" IS 'Nombre de la Cuenta Bancaria.';

COMMENT ON COLUMN "tsdefban"."tipcue" IS 'Tipo de Cuenta.';

COMMENT ON COLUMN "tsdefban"."codcta" IS 'Código de la Cuenta Contable.';

COMMENT ON COLUMN "tsdefban"."fecreg" IS 'Fecha de Registro.';

COMMENT ON COLUMN "tsdefban"."fecven" IS 'Fecha de Vencimiento.';

COMMENT ON COLUMN "tsdefban"."fecper" IS 'Fecha Período Bancos.';

COMMENT ON COLUMN "tsdefban"."renaut" IS 'Indica si la Renovación es  Automática.';

COMMENT ON COLUMN "tsdefban"."porint" IS 'Porcentaje de Interés.';

COMMENT ON COLUMN "tsdefban"."tipint" IS 'Tipo de Interés.';

COMMENT ON COLUMN "tsdefban"."numche" IS 'Número del Cheque.';

COMMENT ON COLUMN "tsdefban"."antban" IS 'Saldo anterior en bancos.';

COMMENT ON COLUMN "tsdefban"."debban" IS 'Monto de débitos en bancos.';

COMMENT ON COLUMN "tsdefban"."creban" IS 'Monto de créditos en bancos.';

COMMENT ON COLUMN "tsdefban"."antlib" IS 'Saldo Anterior en Libros.';

COMMENT ON COLUMN "tsdefban"."deblib" IS 'Monto de Débitos en Libros.';

COMMENT ON COLUMN "tsdefban"."crelib" IS 'Monto de Créditos en Libros.';

COMMENT ON COLUMN "tsdefban"."valche" IS 'Validez en días del cheque.';

COMMENT ON COLUMN "tsdefban"."concil" IS 'Indica si es ó no Conciliable.';

COMMENT ON COLUMN "tsdefban"."plazo" IS 'Plazo de Interés.';

COMMENT ON COLUMN "tsdefban"."fecape" IS 'Fecha de Apertura';

COMMENT ON COLUMN "tsdefban"."usocue" IS 'Uso que se le da a la Cuenta';

COMMENT ON COLUMN "tsdefban"."tipren" IS 'Tipo de Rendimiento que da la Cuenta';

COMMENT ON COLUMN "tsdefban"."desenl" IS 'Nombre del Banco (SIGRE)';

COMMENT ON COLUMN "tsdefban"."porsalmin" IS 'Porcentaje Saldo Minimo';

COMMENT ON COLUMN "tsdefban"."monsalmin" IS 'Monto Saldo Minimo';

COMMENT ON COLUMN "tsdefban"."codctaprecoo" IS 'Cuenta Contable del Banco por Tipo de Presupuesto Ordinario';

COMMENT ON COLUMN "tsdefban"."codctapreord" IS 'Cuenta Contable del Banco por Tipo de Presupuesto Coordinado';

COMMENT ON COLUMN "tsdefban"."trasitoria" IS 'Cantidad de Dígitos Significativos de la Chequera';

COMMENT ON COLUMN "tsdefban"."salact" IS 'null';

COMMENT ON COLUMN "tsdefban"."fecaper" IS 'Fecha de Apertura de Cuenta';

COMMENT ON COLUMN "tsdefban"."temnumcue" IS 'null';

COMMENT ON COLUMN "tsdefban"."cantdig" IS 'Cantidad de Dígitos Significativos de la Chequera';

COMMENT ON COLUMN "tsdefban"."endosable" IS 'null';

COMMENT ON COLUMN "tsdefban"."salmin" IS 'null';

COMMENT ON COLUMN "tsdefban"."nomrep" IS 'null';

COMMENT ON COLUMN "tsdefban"."codmon" IS 'null';

COMMENT ON COLUMN "tsdefban"."codcom" IS 'null';

COMMENT ON COLUMN "tsdefban"."codtiptra" IS 'Tipo de Transacción.';

COMMENT ON COLUMN "tsdefban"."codadi" IS 'Código adicional del banco';

COMMENT ON COLUMN "tsdefban"."codubi" IS 'Código Ubuicación Administrativa';

COMMENT ON COLUMN "tsdefban"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "tsdefban"."conformable" IS '(S-N) Si los cheques son conformables';

COMMENT ON COLUMN "tsdefban"."monconfor" IS 'Monto de Conformación';

COMMENT ON COLUMN "tsdefban"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsdefchequera
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsdefchequera" CASCADE;

CREATE TABLE "tsdefchequera"
(
    "codchq" VARCHAR(8) NOT NULL,
    "numche" VARCHAR(20),
    "numcue" VARCHAR(20),
    "numchedes" VARCHAR(20),
    "numchehas" VARCHAR(20),
    "activa" VARCHAR(2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsdefchequera" IS 'null';

COMMENT ON COLUMN "tsdefchequera"."codchq" IS 'Código de la Chequera';

COMMENT ON COLUMN "tsdefchequera"."numche" IS 'Número de Chequera';

COMMENT ON COLUMN "tsdefchequera"."numcue" IS 'Número de la Cuenta Bancaria';

COMMENT ON COLUMN "tsdefchequera"."numchedes" IS 'Numero de Cheque que Inicia la Chequera';

COMMENT ON COLUMN "tsdefchequera"."numchehas" IS 'null';

COMMENT ON COLUMN "tsdefchequera"."activa" IS 'null';

COMMENT ON COLUMN "tsdefchequera"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsdefemp
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsdefemp" CASCADE;

CREATE TABLE "tsdefemp"
(
    "codemp" VARCHAR(3) NOT NULL,
    "feccon" DATE NOT NULL,
    "fecban" DATE NOT NULL,
    "tipmon" VARCHAR(20) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsdefemp" IS 'null';

COMMENT ON COLUMN "tsdefemp"."codemp" IS 'null';

COMMENT ON COLUMN "tsdefemp"."feccon" IS 'null';

COMMENT ON COLUMN "tsdefemp"."fecban" IS 'null';

COMMENT ON COLUMN "tsdefemp"."tipmon" IS 'null';

COMMENT ON COLUMN "tsdefemp"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsdefrengas
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsdefrengas" CASCADE;

CREATE TABLE "tsdefrengas"
(
    "pagrepcaj" VARCHAR(4),
    "ctarepcaj" VARCHAR(32),
    "pagcheant" VARCHAR(4),
    "ctacheant" VARCHAR(32),
    "movreicaj" VARCHAR(4),
    "ctareicaj" VARCHAR(32),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsdefrengas" IS 'Tabla que contiene información referente a las cuentas contables para rendición de gastos';

COMMENT ON COLUMN "tsdefrengas"."pagrepcaj" IS 'Tipo de Movimiento Causado para la OP de Apertura o Reposición de Caja Chica';

COMMENT ON COLUMN "tsdefrengas"."ctarepcaj" IS 'Cuenta Contable para la  Apertura o Reposición de Caja Chica';

COMMENT ON COLUMN "tsdefrengas"."pagcheant" IS 'Tipo de Causado para la OP de Rendición de Caja Chica';

COMMENT ON COLUMN "tsdefrengas"."ctacheant" IS 'null';

COMMENT ON COLUMN "tsdefrengas"."movreicaj" IS 'Tipo de Movimiento Según Libro para el Reintegro de Caja Chica';

COMMENT ON COLUMN "tsdefrengas"."ctareicaj" IS 'Cuenta Contable para el Reintegro de Caja Chica';

COMMENT ON COLUMN "tsdefrengas"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsdepfonpre
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsdepfonpre" CASCADE;

CREATE TABLE "tsdepfonpre"
(
    "numdep" VARCHAR(20) NOT NULL,
    "tipemp" VARCHAR(1) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "tsdepfonpre"."numdep" IS 'null';

COMMENT ON COLUMN "tsdepfonpre"."tipemp" IS 'null';

COMMENT ON COLUMN "tsdepfonpre"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsdesmon
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsdesmon" CASCADE;

CREATE TABLE "tsdesmon"
(
    "codmon" VARCHAR(3) NOT NULL,
    "nommon" VARCHAR(40) NOT NULL,
    "valmon" NUMERIC(12,6) NOT NULL,
    "fecmon" DATE NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsdesmon" IS 'Tabla que contiene información referente a la definición de la moneda.';

COMMENT ON COLUMN "tsdesmon"."codmon" IS 'Código de la Moneda.';

COMMENT ON COLUMN "tsdesmon"."nommon" IS 'Denominación de la Moneda.';

COMMENT ON COLUMN "tsdesmon"."valmon" IS 'Valor de la Moneda.';

COMMENT ON COLUMN "tsdesmon"."fecmon" IS 'Fecha de Cambio de la Moneda.';

COMMENT ON COLUMN "tsdesmon"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tscammon
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tscammon" CASCADE;

CREATE TABLE "tscammon"
(
    "codmon" VARCHAR(3) NOT NULL,
    "valmon" NUMERIC(12,6) NOT NULL,
    "fecmon" TIMESTAMP NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tscammon" IS 'null';

COMMENT ON COLUMN "tscammon"."codmon" IS 'null';

COMMENT ON COLUMN "tscammon"."valmon" IS 'null';

COMMENT ON COLUMN "tscammon"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsdettra
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsdettra" CASCADE;

CREATE TABLE "tsdettra"
(
    "reftra" VARCHAR(10) NOT NULL,
    "ctaori" VARCHAR(18) NOT NULL,
    "ctades" VARCHAR(18) NOT NULL,
    "aumdis" VARCHAR(1) NOT NULL,
    "montra" NUMERIC(14,2) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsdettra" IS 'null';

COMMENT ON COLUMN "tsdettra"."reftra" IS 'null';

COMMENT ON COLUMN "tsdettra"."ctaori" IS 'null';

COMMENT ON COLUMN "tsdettra"."ctades" IS 'null';

COMMENT ON COLUMN "tsdettra"."aumdis" IS 'null';

COMMENT ON COLUMN "tsdettra"."montra" IS 'null';

COMMENT ON COLUMN "tsdettra"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsentislr
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsentislr" CASCADE;

CREATE TABLE "tsentislr"
(
    "numdep" VARCHAR(20) NOT NULL,
    "fecha" DATE,
    "banco" VARCHAR(100),
    "numord" VARCHAR(8) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsentislr" IS 'Tabla que contiene información referente a los pagos de retenciones de ISLR';

COMMENT ON COLUMN "tsentislr"."numdep" IS 'Número del Deposito';

COMMENT ON COLUMN "tsentislr"."fecha" IS 'Fecha del Depósito';

COMMENT ON COLUMN "tsentislr"."banco" IS 'Nombre del Banco';

COMMENT ON COLUMN "tsentislr"."numord" IS 'Numero de la Orden de Pago de Retenciones';

COMMENT ON COLUMN "tsentislr"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsfonpre
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsfonpre" CASCADE;

CREATE TABLE "tsfonpre"
(
    "numche" VARCHAR(20) NOT NULL,
    "tipemp" VARCHAR(1) NOT NULL,
    "tippre" VARCHAR(1) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsfonpre" IS 'null';

COMMENT ON COLUMN "tsfonpre"."numche" IS 'null';

COMMENT ON COLUMN "tsfonpre"."tipemp" IS 'null';

COMMENT ON COLUMN "tsfonpre"."tippre" IS 'null';

COMMENT ON COLUMN "tsfonpre"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tslibcom
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tslibcom" CASCADE;

CREATE TABLE "tslibcom"
(
    "numlin" NUMERIC(10,0),
    "deslin" VARCHAR(1000),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tslibcom" IS 'null';

COMMENT ON COLUMN "tslibcom"."numlin" IS 'null';

COMMENT ON COLUMN "tslibcom"."deslin" IS 'null';

COMMENT ON COLUMN "tslibcom"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsmaetra
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsmaetra" CASCADE;

CREATE TABLE "tsmaetra"
(
    "reftra" VARCHAR(10) NOT NULL,
    "fectra" DATE NOT NULL,
    "destra" VARCHAR(100) NOT NULL,
    "tottra" NUMERIC(14,2) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsmaetra" IS 'null';

COMMENT ON COLUMN "tsmaetra"."reftra" IS 'null';

COMMENT ON COLUMN "tsmaetra"."fectra" IS 'null';

COMMENT ON COLUMN "tsmaetra"."destra" IS 'null';

COMMENT ON COLUMN "tsmaetra"."tottra" IS 'null';

COMMENT ON COLUMN "tsmaetra"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsmovban
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsmovban" CASCADE;

CREATE TABLE "tsmovban"
(
    "numcue" VARCHAR(20) NOT NULL,
    "codcta" VARCHAR(32),
    "refban" VARCHAR(20) NOT NULL,
    "fecban" DATE NOT NULL,
    "tipmov" VARCHAR(4) NOT NULL,
    "desban" VARCHAR(4000) NOT NULL,
    "monmov" NUMERIC(14,2) NOT NULL,
    "status" VARCHAR(1) NOT NULL,
    "stacon" VARCHAR(1) NOT NULL,
    "transito" VARCHAR(1),
    "stacon1" VARCHAR(1),
    "codmon" VARCHAR(3) NOT NULL,
    "valmon" NUMERIC(14,6),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsmovban" IS 'Tabla que contiene información referente a los movimientos en bancos.';

COMMENT ON COLUMN "tsmovban"."numcue" IS 'Número de la Cuenta Bancaria.';

COMMENT ON COLUMN "tsmovban"."codcta" IS 'Código de la Cuenta.';

COMMENT ON COLUMN "tsmovban"."refban" IS 'Referencia del Movimiento Bancario.';

COMMENT ON COLUMN "tsmovban"."fecban" IS 'Fecha del Movimiento Bancario';

COMMENT ON COLUMN "tsmovban"."tipmov" IS 'Código de Tipo de Movimiento.';

COMMENT ON COLUMN "tsmovban"."desban" IS 'Descripción del Movimiento Bancario.';

COMMENT ON COLUMN "tsmovban"."monmov" IS 'Monto Total del Movimiento.';

COMMENT ON COLUMN "tsmovban"."status" IS 'Estatus del Movimiento.';

COMMENT ON COLUMN "tsmovban"."stacon" IS 'Estatus de Conciliado.';

COMMENT ON COLUMN "tsmovban"."transito" IS 'Indica si el Movimiento  esta en Transito';

COMMENT ON COLUMN "tsmovban"."stacon1" IS 'Indica si el campo está Conciliado Manualmente';

COMMENT ON COLUMN "tsmovban"."codmon" IS 'Código de la Moneda';

COMMENT ON COLUMN "tsmovban"."valmon" IS 'Valor de Moneda';

COMMENT ON COLUMN "tsmovban"."id" IS 'Identificador Único del registro';

CREATE INDEX "itsmovban2" ON "tsmovban" ("numcue","tipmov","fecban");

-----------------------------------------------------------------------
-- tsmovlib
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsmovlib" CASCADE;

CREATE TABLE "tsmovlib"
(
    "numcue" VARCHAR(20) NOT NULL,
    "reflib" VARCHAR(20) NOT NULL,
    "feclib" DATE NOT NULL,
    "tipmov" VARCHAR(4) NOT NULL,
    "deslib" VARCHAR(4000) NOT NULL,
    "monmov" NUMERIC(14,2) NOT NULL,
    "codcta" VARCHAR(32),
    "numcom" VARCHAR(8),
    "feccom" DATE,
    "status" VARCHAR(1) NOT NULL,
    "stacon" VARCHAR(1) NOT NULL,
    "fecing" DATE,
    "fecanu" DATE,
    "tipmovpad" VARCHAR(4),
    "reflibpad" VARCHAR(20),
    "transito" VARCHAR(1),
    "numcomadi" VARCHAR(8),
    "feccomadi" DATE,
    "nombensus" VARCHAR(250),
    "orden" NUMERIC(14,0),
    "horing" VARCHAR(12),
    "stacon1" VARCHAR(1),
    "motanu" VARCHAR(250),
    "refpag" VARCHAR(8),
    "loguse" VARCHAR(50),
    "cedrif" VARCHAR(15),
    "codmon" VARCHAR(3) NOT NULL,
    "valmon" NUMERIC(14,6),
    "codconcepto" VARCHAR(4),
    "stadif" VARCHAR(1),
    "codpro" VARCHAR(4),
    "tipmovaju" VARCHAR(4),
    "reflibaju" VARCHAR(20),
    "coddirec" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsmovlib" IS 'Tabla que contiene información referente a los Movimientos en Libros.';

COMMENT ON COLUMN "tsmovlib"."numcue" IS 'Número de la Cuenta Bancaria.';

COMMENT ON COLUMN "tsmovlib"."reflib" IS 'Número del Movimiento en Libro.';

COMMENT ON COLUMN "tsmovlib"."feclib" IS 'Fecha del Movimiento en Libro.';

COMMENT ON COLUMN "tsmovlib"."tipmov" IS 'Descripción del Código de Tipo de Movimiento.';

COMMENT ON COLUMN "tsmovlib"."deslib" IS 'Descripción del Movimiento en Libro.';

COMMENT ON COLUMN "tsmovlib"."monmov" IS 'Monto Total del Movimiento.';

COMMENT ON COLUMN "tsmovlib"."codcta" IS 'Código de la Cuenta Contable.';

COMMENT ON COLUMN "tsmovlib"."numcom" IS 'Número del Comprobante Contable.';

COMMENT ON COLUMN "tsmovlib"."feccom" IS 'Fecha del Comprobante Contable.';

COMMENT ON COLUMN "tsmovlib"."status" IS 'Estatus del Movimiento en Libro.';

COMMENT ON COLUMN "tsmovlib"."stacon" IS 'Estatus del Comprobante.';

COMMENT ON COLUMN "tsmovlib"."fecing" IS 'Fecha de Ingreso del Movimiento en Libro.';

COMMENT ON COLUMN "tsmovlib"."fecanu" IS 'Fecha de Anulación';

COMMENT ON COLUMN "tsmovlib"."tipmovpad" IS 'Tipo de Movimiento Padre';

COMMENT ON COLUMN "tsmovlib"."reflibpad" IS 'Referencia de Libro Padre';

COMMENT ON COLUMN "tsmovlib"."transito" IS 'null';

COMMENT ON COLUMN "tsmovlib"."numcomadi" IS 'Numero de Comprobante de Adición';

COMMENT ON COLUMN "tsmovlib"."feccomadi" IS 'Fecha de Comprobante de la Adición';

COMMENT ON COLUMN "tsmovlib"."nombensus" IS 'null';

COMMENT ON COLUMN "tsmovlib"."orden" IS 'null';

COMMENT ON COLUMN "tsmovlib"."horing" IS 'null';

COMMENT ON COLUMN "tsmovlib"."stacon1" IS 'null';

COMMENT ON COLUMN "tsmovlib"."motanu" IS 'null';

COMMENT ON COLUMN "tsmovlib"."refpag" IS 'null';

COMMENT ON COLUMN "tsmovlib"."loguse" IS 'null';

COMMENT ON COLUMN "tsmovlib"."cedrif" IS 'null';

COMMENT ON COLUMN "tsmovlib"."codmon" IS 'null';

COMMENT ON COLUMN "tsmovlib"."valmon" IS 'null';

COMMENT ON COLUMN "tsmovlib"."codconcepto" IS 'Concepto de Pago';

COMMENT ON COLUMN "tsmovlib"."stadif" IS 'Estatus del Ajuste Diferencial';

COMMENT ON COLUMN "tsmovlib"."codpro" IS 'Código del Proyecto';

COMMENT ON COLUMN "tsmovlib"."tipmovaju" IS 'Tipo de Movimiento Padre Ajustes Diferencial';

COMMENT ON COLUMN "tsmovlib"."reflibaju" IS 'Referencia de Libro Padre Ajustes Diferencial';

COMMENT ON COLUMN "tsmovlib"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "tsmovlib"."id" IS 'Identificador Único del registro';

CREATE INDEX "i01tsmovlib" ON "tsmovlib" ("numcue");

CREATE INDEX "i02tsmovlib" ON "tsmovlib" ("tipmov","numcue");

CREATE INDEX "i03tsmovlib" ON "tsmovlib" ("tipmov");

CREATE INDEX "i04tsmovlib" ON "tsmovlib" ("numcue","feclib");

CREATE INDEX "i06movlib" ON "tsmovlib" ("reflib");

CREATE INDEX "i07movlib" ON "tsmovlib" ("feclib");

CREATE INDEX "itsmovlib2" ON "tsmovlib" ("numcue","tipmov","feclib");

-----------------------------------------------------------------------
-- tsmovtra
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsmovtra" CASCADE;

CREATE TABLE "tsmovtra"
(
    "reftra" VARCHAR(20) NOT NULL,
    "fectra" DATE NOT NULL,
    "destra" VARCHAR(250) NOT NULL,
    "ctaori" VARCHAR(20) NOT NULL,
    "ctades" VARCHAR(20) NOT NULL,
    "montra" NUMERIC(14,2) NOT NULL,
    "numcom" VARCHAR(8),
    "status" VARCHAR(1) NOT NULL,
    "fecing" DATE,
    "fecanu" DATE,
    "codmon" VARCHAR(3),
    "valmon" NUMERIC(12,6),
    "tipmovdesd" VARCHAR(4) NOT NULL,
    "tipmovhast" VARCHAR(4) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsmovtra" IS 'Tabla que contiene información referente a los movimientos de transferencias bancarias.';

COMMENT ON COLUMN "tsmovtra"."reftra" IS 'Número de Referencia del transferencia';

COMMENT ON COLUMN "tsmovtra"."fectra" IS 'Fecha del Transferencia';

COMMENT ON COLUMN "tsmovtra"."destra" IS 'Descripción de la Transferencia';

COMMENT ON COLUMN "tsmovtra"."ctaori" IS 'Cuenta Contable Origen';

COMMENT ON COLUMN "tsmovtra"."ctades" IS 'Cuenta Contable Destino';

COMMENT ON COLUMN "tsmovtra"."montra" IS 'Monto de la Transferencia';

COMMENT ON COLUMN "tsmovtra"."numcom" IS 'Número del Comprobante Contable';

COMMENT ON COLUMN "tsmovtra"."status" IS 'Estatus del Movimiento';

COMMENT ON COLUMN "tsmovtra"."fecing" IS 'Fecha de Ingreso del Movimiento en Libro';

COMMENT ON COLUMN "tsmovtra"."fecanu" IS 'Fecha de Anulación del Movimiento';

COMMENT ON COLUMN "tsmovtra"."codmon" IS 'null';

COMMENT ON COLUMN "tsmovtra"."valmon" IS 'null';

COMMENT ON COLUMN "tsmovtra"."tipmovdesd" IS 'Código del Tipo de Movimiento Origen.';

COMMENT ON COLUMN "tsmovtra"."tipmovhast" IS 'Código del Tipo de Movimiento Origen.';

COMMENT ON COLUMN "tsmovtra"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tspararc
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tspararc" CASCADE;

CREATE TABLE "tspararc"
(
    "numcue" VARCHAR(20) NOT NULL,
    "inicue" NUMERIC(3,0) NOT NULL,
    "fincue" NUMERIC(3,0) NOT NULL,
    "iniref" NUMERIC(3,0) NOT NULL,
    "finref" NUMERIC(3,0) NOT NULL,
    "digsigp" INTEGER,
    "digsign" INTEGER,
    "inifec" NUMERIC(3,0) NOT NULL,
    "finfec" NUMERIC(3,0) NOT NULL,
    "forfec" VARCHAR(10),
    "initip" NUMERIC(3,0) NOT NULL,
    "fintip" NUMERIC(3,0) NOT NULL,
    "valdefp" VARCHAR(4),
    "valdefn" VARCHAR(4),
    "inides" NUMERIC(3,0) NOT NULL,
    "findes" NUMERIC(3,0) NOT NULL,
    "valdefd" VARCHAR(250),
    "inimon" NUMERIC(3,0) NOT NULL,
    "finmon" NUMERIC(3,0) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tspararc" IS 'null';

COMMENT ON COLUMN "tspararc"."numcue" IS 'null';

COMMENT ON COLUMN "tspararc"."inicue" IS 'null';

COMMENT ON COLUMN "tspararc"."fincue" IS 'null';

COMMENT ON COLUMN "tspararc"."iniref" IS 'null';

COMMENT ON COLUMN "tspararc"."finref" IS 'null';

COMMENT ON COLUMN "tspararc"."digsigp" IS 'null';

COMMENT ON COLUMN "tspararc"."digsign" IS 'null';

COMMENT ON COLUMN "tspararc"."inifec" IS 'null';

COMMENT ON COLUMN "tspararc"."finfec" IS 'null';

COMMENT ON COLUMN "tspararc"."forfec" IS 'null';

COMMENT ON COLUMN "tspararc"."initip" IS 'null';

COMMENT ON COLUMN "tspararc"."fintip" IS 'null';

COMMENT ON COLUMN "tspararc"."valdefp" IS 'null';

COMMENT ON COLUMN "tspararc"."valdefn" IS 'null';

COMMENT ON COLUMN "tspararc"."inides" IS 'null';

COMMENT ON COLUMN "tspararc"."findes" IS 'null';

COMMENT ON COLUMN "tspararc"."valdefd" IS 'null';

COMMENT ON COLUMN "tspararc"."inimon" IS 'null';

COMMENT ON COLUMN "tspararc"."finmon" IS 'null';

COMMENT ON COLUMN "tspararc"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsrelfonvia
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsrelfonvia" CASCADE;

CREATE TABLE "tsrelfonvia"
(
    "numsol" VARCHAR(10) NOT NULL,
    "numche" VARCHAR(20) NOT NULL,
    "numcue" VARCHAR(20) NOT NULL,
    "cedrif" VARCHAR(15),
    "nomben" VARCHAR(100),
    "monche" NUMERIC(14,2),
    "codcat" VARCHAR(32),
    "fecemi" DATE,
    "codpre" VARCHAR(50),
    "numdep" VARCHAR(20),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsrelfonvia" IS 'null';

COMMENT ON COLUMN "tsrelfonvia"."numsol" IS 'null';

COMMENT ON COLUMN "tsrelfonvia"."numche" IS 'null';

COMMENT ON COLUMN "tsrelfonvia"."numcue" IS 'null';

COMMENT ON COLUMN "tsrelfonvia"."cedrif" IS 'null';

COMMENT ON COLUMN "tsrelfonvia"."nomben" IS 'null';

COMMENT ON COLUMN "tsrelfonvia"."monche" IS 'null';

COMMENT ON COLUMN "tsrelfonvia"."codcat" IS 'null';

COMMENT ON COLUMN "tsrelfonvia"."fecemi" IS 'null';

COMMENT ON COLUMN "tsrelfonvia"."codpre" IS 'null';

COMMENT ON COLUMN "tsrelfonvia"."numdep" IS 'null';

COMMENT ON COLUMN "tsrelfonvia"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsrepret
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsrepret" CASCADE;

CREATE TABLE "tsrepret"
(
    "codrep" VARCHAR(50) NOT NULL,
    "codret" VARCHAR(4) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsrepret" IS 'Tabla que contiene información referente a los reportes de comprobantes de retención';

COMMENT ON COLUMN "tsrepret"."codrep" IS 'Código del Tipo de Comprobante';

COMMENT ON COLUMN "tsrepret"."codret" IS 'Código de la Retención';

COMMENT ON COLUMN "tsrepret"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsretiva
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsretiva" CASCADE;

CREATE TABLE "tsretiva"
(
    "codret" VARCHAR(3) NOT NULL,
    "codrec" VARCHAR(4) NOT NULL,
    "codpar" VARCHAR(32) NOT NULL,
    "anoant" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsretiva" IS 'Tabla que contiene información referente a la retención del IVA';

COMMENT ON COLUMN "tsretiva"."codret" IS 'Código de la Retención';

COMMENT ON COLUMN "tsretiva"."codrec" IS 'Código del Recargo';

COMMENT ON COLUMN "tsretiva"."codpar" IS 'Código de la Partida';

COMMENT ON COLUMN "tsretiva"."anoant" IS 'null';

COMMENT ON COLUMN "tsretiva"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tssecofi
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tssecofi" CASCADE;

CREATE TABLE "tssecofi"
(
    "codsecofi" VARCHAR(10),
    "dessecofi" VARCHAR(100),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tssecofi" IS 'null';

COMMENT ON COLUMN "tssecofi"."codsecofi" IS 'null';

COMMENT ON COLUMN "tssecofi"."dessecofi" IS 'null';

COMMENT ON COLUMN "tssecofi"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tssolpag
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tssolpag" CASCADE;

CREATE TABLE "tssolpag"
(
    "numsol" VARCHAR(8) NOT NULL,
    "fecsol" DATE NOT NULL,
    "numaep" VARCHAR(8),
    "numopp" VARCHAR(8),
    "monsol" NUMERIC(14,2),
    "dessol" VARCHAR(255),
    "numfac" VARCHAR(255),
    "cedrif" VARCHAR(15),
    "cedsol" VARCHAR(15),
    "nomsol" VARCHAR(100),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tssolpag" IS 'null';

COMMENT ON COLUMN "tssolpag"."numsol" IS 'null';

COMMENT ON COLUMN "tssolpag"."fecsol" IS 'null';

COMMENT ON COLUMN "tssolpag"."numaep" IS 'null';

COMMENT ON COLUMN "tssolpag"."numopp" IS 'null';

COMMENT ON COLUMN "tssolpag"."monsol" IS 'null';

COMMENT ON COLUMN "tssolpag"."dessol" IS 'null';

COMMENT ON COLUMN "tssolpag"."numfac" IS 'null';

COMMENT ON COLUMN "tssolpag"."cedrif" IS 'null';

COMMENT ON COLUMN "tssolpag"."cedsol" IS 'null';

COMMENT ON COLUMN "tssolpag"."nomsol" IS 'null';

COMMENT ON COLUMN "tssolpag"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tstipcue
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tstipcue" CASCADE;

CREATE TABLE "tstipcue"
(
    "codtip" VARCHAR(3) NOT NULL,
    "destip" VARCHAR(40) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_tstipcue_codtip" UNIQUE ("codtip")
);

COMMENT ON TABLE "tstipcue" IS 'Tabla que contiene información referente a la definición de tipos de cuentas bancarias.';

COMMENT ON COLUMN "tstipcue"."codtip" IS 'Código del Tipo de Cuenta.';

COMMENT ON COLUMN "tstipcue"."destip" IS 'Descripción del Tipo de Cuenta.';

COMMENT ON COLUMN "tstipcue"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tstipfte
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tstipfte" CASCADE;

CREATE TABLE "tstipfte"
(
    "codtipfte" VARCHAR(3) NOT NULL,
    "nomtipfte" VARCHAR(100) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tstipfte" IS 'null';

COMMENT ON COLUMN "tstipfte"."codtipfte" IS 'null';

COMMENT ON COLUMN "tstipfte"."nomtipfte" IS 'null';

COMMENT ON COLUMN "tstipfte"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tstipmov
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tstipmov" CASCADE;

CREATE TABLE "tstipmov"
(
    "codtip" VARCHAR(4) NOT NULL,
    "destip" VARCHAR(40) NOT NULL,
    "debcre" VARCHAR(1) NOT NULL,
    "orden" VARCHAR(2),
    "escheque" BOOLEAN,
    "codcon" VARCHAR(32),
    "tipo" VARCHAR(1),
    "pagnom" BOOLEAN,
    "codtiptra" VARCHAR(3),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_tstipmov_codtip" UNIQUE ("codtip")
);

COMMENT ON TABLE "tstipmov" IS 'Tabla que contiene información referente a la definición de Tipos de Movimientos Bancarios.';

COMMENT ON COLUMN "tstipmov"."codtip" IS 'Código del Tipo de Movimiento.';

COMMENT ON COLUMN "tstipmov"."destip" IS 'Descripción del Tipo de Movimiento.';

COMMENT ON COLUMN "tstipmov"."debcre" IS 'Indica si el Movimiento afecta los Débitos ó Créditos de las cuentas bancarias.';

COMMENT ON COLUMN "tstipmov"."orden" IS 'Orden del Movimiento';

COMMENT ON COLUMN "tstipmov"."escheque" IS 'null';

COMMENT ON COLUMN "tstipmov"."codcon" IS 'null';

COMMENT ON COLUMN "tstipmov"."tipo" IS 'null';

COMMENT ON COLUMN "tstipmov"."pagnom" IS 'null';

COMMENT ON COLUMN "tstipmov"."codtiptra" IS 'Tipo de Transacción.';

COMMENT ON COLUMN "tstipmov"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tstipren
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tstipren" CASCADE;

CREATE TABLE "tstipren"
(
    "codtip" VARCHAR(3) NOT NULL,
    "destip" VARCHAR(50) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_tstipren_codtip" UNIQUE ("codtip")
);

COMMENT ON TABLE "tstipren" IS 'null';

COMMENT ON COLUMN "tstipren"."codtip" IS 'Código del Tipo de Rendimiento';

COMMENT ON COLUMN "tstipren"."destip" IS 'Descripción del Tipo de Rendimiento';

COMMENT ON COLUMN "tstipren"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tssalcaj
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tssalcaj" CASCADE;

CREATE TABLE "tssalcaj"
(
    "refsal" VARCHAR(20) NOT NULL,
    "fecsal" DATE NOT NULL,
    "cedrif" VARCHAR(15),
    "dessal" VARCHAR(1000),
    "monsal" NUMERIC(14,2),
    "stasal" VARCHAR(1),
    "codcaj" VARCHAR(3) NOT NULL,
    "coddirec" VARCHAR(4),
    "codeve" VARCHAR(6),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tssalcaj" IS 'null';

COMMENT ON COLUMN "tssalcaj"."refsal" IS 'null';

COMMENT ON COLUMN "tssalcaj"."fecsal" IS 'null';

COMMENT ON COLUMN "tssalcaj"."cedrif" IS 'null';

COMMENT ON COLUMN "tssalcaj"."dessal" IS 'null';

COMMENT ON COLUMN "tssalcaj"."monsal" IS 'null';

COMMENT ON COLUMN "tssalcaj"."stasal" IS 'null';

COMMENT ON COLUMN "tssalcaj"."codcaj" IS 'null';

COMMENT ON COLUMN "tssalcaj"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "tssalcaj"."codeve" IS 'Codigo del Evento';

COMMENT ON COLUMN "tssalcaj"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsdetsal
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsdetsal" CASCADE;

CREATE TABLE "tsdetsal"
(
    "refsal" VARCHAR(20) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "codcat" VARCHAR(50) NOT NULL,
    "monsal" NUMERIC(14,2),
    "monrec" NUMERIC(14,2),
    "totsal" NUMERIC(14,2),
    "stasal" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsdetsal" IS 'null';

COMMENT ON COLUMN "tsdetsal"."refsal" IS 'null';

COMMENT ON COLUMN "tsdetsal"."codart" IS 'null';

COMMENT ON COLUMN "tsdetsal"."codcat" IS 'null';

COMMENT ON COLUMN "tsdetsal"."monsal" IS 'null';

COMMENT ON COLUMN "tsdetsal"."monrec" IS 'null';

COMMENT ON COLUMN "tsdetsal"."totsal" IS 'null';

COMMENT ON COLUMN "tsdetsal"."stasal" IS 'null';

COMMENT ON COLUMN "tsdetsal"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsrelasiord
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsrelasiord" CASCADE;

CREATE TABLE "tsrelasiord"
(
    "ctagasxpag" VARCHAR(32),
    "ctaordxpag" VARCHAR(32),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsrelasiord" IS 'null';

COMMENT ON COLUMN "tsrelasiord"."ctagasxpag" IS 'null';

COMMENT ON COLUMN "tsrelasiord"."ctaordxpag" IS 'null';

COMMENT ON COLUMN "tsrelasiord"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsmotanu
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsmotanu" CASCADE;

CREATE TABLE "tsmotanu"
(
    "codmotanu" VARCHAR(4),
    "desmotanu" VARCHAR(250),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsmotanu" IS 'null';

COMMENT ON COLUMN "tsmotanu"."codmotanu" IS 'null';

COMMENT ON COLUMN "tsmotanu"."desmotanu" IS 'null';

COMMENT ON COLUMN "tsmotanu"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsbloqban
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsbloqban" CASCADE;

CREATE TABLE "tsbloqban"
(
    "numcue" VARCHAR(20),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsbloqban" IS 'null';

COMMENT ON COLUMN "tsbloqban"."numcue" IS 'null';

COMMENT ON COLUMN "tsbloqban"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opconpag
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opconpag" CASCADE;

CREATE TABLE "opconpag"
(
    "codconcepto" VARCHAR(4),
    "nomconcepto" VARCHAR(250),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opconpag" IS 'null';

COMMENT ON COLUMN "opconpag"."codconcepto" IS 'null';

COMMENT ON COLUMN "opconpag"."nomconcepto" IS 'null';

COMMENT ON COLUMN "opconpag"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tscomegrmes
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tscomegrmes" CASCADE;

CREATE TABLE "tscomegrmes"
(
    "mes1" VARCHAR(2) NOT NULL,
    "cormes1" INTEGER,
    "mes2" VARCHAR(2) NOT NULL,
    "cormes2" INTEGER,
    "mes3" VARCHAR(2) NOT NULL,
    "cormes3" INTEGER,
    "mes4" VARCHAR(2) NOT NULL,
    "cormes4" INTEGER,
    "mes5" VARCHAR(2) NOT NULL,
    "cormes5" INTEGER,
    "mes6" VARCHAR(2) NOT NULL,
    "cormes6" INTEGER,
    "mes7" VARCHAR(2) NOT NULL,
    "cormes7" INTEGER,
    "mes8" VARCHAR(2) NOT NULL,
    "cormes8" INTEGER,
    "mes9" VARCHAR(2) NOT NULL,
    "cormes9" INTEGER,
    "mes10" VARCHAR(2) NOT NULL,
    "cormes10" INTEGER,
    "mes11" VARCHAR(2) NOT NULL,
    "cormes11" INTEGER,
    "mes12" VARCHAR(2) NOT NULL,
    "cormes12" INTEGER,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tscomegrmes" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."mes1" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."mes2" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."mes3" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."cormes3" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."mes4" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."cormes4" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."mes5" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."cormes5" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."mes6" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."cormes6" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."mes7" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."cormes7" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."mes8" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."cormes8" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."mes9" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."cormes9" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."mes10" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."cormes10" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."mes11" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."cormes11" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."mes12" IS 'null';

COMMENT ON COLUMN "tscomegrmes"."cormes12" IS 'null';

-----------------------------------------------------------------------
-- tsdefcajchi
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsdefcajchi" CASCADE;

CREATE TABLE "tsdefcajchi"
(
    "codcaj" VARCHAR(3) NOT NULL,
    "descaj" VARCHAR(250) NOT NULL,
    "cedrif" VARCHAR(15) NOT NULL,
    "codcat" VARCHAR(32) NOT NULL,
    "numcue" VARCHAR(20),
    "codtip" VARCHAR(4) NOT NULL,
    "tipcau" VARCHAR(4) NOT NULL,
    "monape" NUMERIC(14,2),
    "porrep" NUMERIC(14,2),
    "numini" VARCHAR(8),
    "tipcom" VARCHAR(4) NOT NULL,
    "ctapag" VARCHAR(32),
    "sujren" VARCHAR(1),
    "tippag" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_tsdefcajchi_codcaj" UNIQUE ("codcaj")
);

COMMENT ON TABLE "tsdefcajchi" IS 'null';

COMMENT ON COLUMN "tsdefcajchi"."codcaj" IS 'null';

COMMENT ON COLUMN "tsdefcajchi"."descaj" IS 'null';

COMMENT ON COLUMN "tsdefcajchi"."cedrif" IS 'null';

COMMENT ON COLUMN "tsdefcajchi"."codcat" IS 'null';

COMMENT ON COLUMN "tsdefcajchi"."numcue" IS 'null';

COMMENT ON COLUMN "tsdefcajchi"."codtip" IS 'null';

COMMENT ON COLUMN "tsdefcajchi"."tipcau" IS 'null';

COMMENT ON COLUMN "tsdefcajchi"."monape" IS 'null';

COMMENT ON COLUMN "tsdefcajchi"."porrep" IS 'null';

COMMENT ON COLUMN "tsdefcajchi"."numini" IS 'null';

COMMENT ON COLUMN "tsdefcajchi"."tipcom" IS 'Tipo de Compromiso para el Cierre';

COMMENT ON COLUMN "tsdefcajchi"."ctapag" IS 'Código de la Cuenta por Pagar';

COMMENT ON COLUMN "tsdefcajchi"."sujren" IS 'Sujeta Rendición (SI/NO)';

COMMENT ON COLUMN "tsdefcajchi"."tippag" IS 'Código del Tipo de Documento';

COMMENT ON COLUMN "tsdefcajchi"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tscormestxt
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tscormestxt" CASCADE;

CREATE TABLE "tscormestxt"
(
    "numcue" VARCHAR(20) NOT NULL,
    "mes1" VARCHAR(2) NOT NULL,
    "cormes1" INTEGER,
    "mes2" VARCHAR(2) NOT NULL,
    "cormes2" INTEGER,
    "mes3" VARCHAR(2) NOT NULL,
    "cormes3" INTEGER,
    "mes4" VARCHAR(2) NOT NULL,
    "cormes4" INTEGER,
    "mes5" VARCHAR(2) NOT NULL,
    "cormes5" INTEGER,
    "mes6" VARCHAR(2) NOT NULL,
    "cormes6" INTEGER,
    "mes7" VARCHAR(2) NOT NULL,
    "cormes7" INTEGER,
    "mes8" VARCHAR(2) NOT NULL,
    "cormes8" INTEGER,
    "mes9" VARCHAR(2) NOT NULL,
    "cormes9" INTEGER,
    "mes10" VARCHAR(2) NOT NULL,
    "cormes10" INTEGER,
    "mes11" VARCHAR(2) NOT NULL,
    "cormes11" INTEGER,
    "mes12" VARCHAR(2) NOT NULL,
    "cormes12" INTEGER,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tscormestxt" IS 'null';

COMMENT ON COLUMN "tscormestxt"."numcue" IS 'null';

COMMENT ON COLUMN "tscormestxt"."mes1" IS 'null';

COMMENT ON COLUMN "tscormestxt"."mes2" IS 'null';

COMMENT ON COLUMN "tscormestxt"."cormes2" IS 'null';

COMMENT ON COLUMN "tscormestxt"."mes3" IS 'null';

COMMENT ON COLUMN "tscormestxt"."cormes3" IS 'null';

COMMENT ON COLUMN "tscormestxt"."mes4" IS 'null';

COMMENT ON COLUMN "tscormestxt"."cormes4" IS 'null';

COMMENT ON COLUMN "tscormestxt"."mes5" IS 'null';

COMMENT ON COLUMN "tscormestxt"."cormes5" IS 'null';

COMMENT ON COLUMN "tscormestxt"."mes6" IS 'null';

COMMENT ON COLUMN "tscormestxt"."cormes6" IS 'null';

COMMENT ON COLUMN "tscormestxt"."mes7" IS 'null';

COMMENT ON COLUMN "tscormestxt"."cormes7" IS 'null';

COMMENT ON COLUMN "tscormestxt"."mes8" IS 'null';

COMMENT ON COLUMN "tscormestxt"."cormes8" IS 'null';

COMMENT ON COLUMN "tscormestxt"."mes9" IS 'null';

COMMENT ON COLUMN "tscormestxt"."cormes9" IS 'null';

COMMENT ON COLUMN "tscormestxt"."mes10" IS 'null';

COMMENT ON COLUMN "tscormestxt"."cormes10" IS 'null';

COMMENT ON COLUMN "tscormestxt"."mes11" IS 'null';

COMMENT ON COLUMN "tscormestxt"."cormes11" IS 'null';

COMMENT ON COLUMN "tscormestxt"."mes12" IS 'null';

COMMENT ON COLUMN "tscormestxt"."cormes12" IS 'null';

-----------------------------------------------------------------------
-- tsuniadm
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsuniadm" CASCADE;

CREATE TABLE "tsuniadm"
(
    "coduniadm" VARCHAR(30),
    "desuniadm" VARCHAR(250),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsuniadm" IS 'null';

COMMENT ON COLUMN "tsuniadm"."coduniadm" IS 'Código Unidad Administradora';

COMMENT ON COLUMN "tsuniadm"."desuniadm" IS 'Descripcion Unidad Administradora';

COMMENT ON COLUMN "tsuniadm"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsdeffonant
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsdeffonant" CASCADE;

CREATE TABLE "tsdeffonant"
(
    "codfon" VARCHAR(3) NOT NULL,
    "desfon" VARCHAR(250) NOT NULL,
    "unieje" VARCHAR(15) NOT NULL,
    "coduniadm" VARCHAR(30) NOT NULL,
    "cedrif" VARCHAR(15) NOT NULL,
    "codcat" VARCHAR(32) NOT NULL,
    "numcue" VARCHAR(20) NOT NULL,
    "tipmovsal" VARCHAR(4) NOT NULL,
    "tipmovren" VARCHAR(4) NOT NULL,
    "monmincon" NUMERIC(14,2),
    "monmaxcon" NUMERIC(14,2),
    "porrep" NUMERIC(14,2),
    "numini" VARCHAR(8),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_tsdeffonant_codfon" UNIQUE ("codfon")
);

COMMENT ON TABLE "tsdeffonant" IS 'null';

COMMENT ON COLUMN "tsdeffonant"."codfon" IS 'Código del fondo de anticipo';

COMMENT ON COLUMN "tsdeffonant"."desfon" IS 'Descripcion del fondo de anticipo';

COMMENT ON COLUMN "tsdeffonant"."unieje" IS 'Unidad Ejecutora del fondo de anticipo';

COMMENT ON COLUMN "tsdeffonant"."coduniadm" IS 'Código de la unidad Administradora';

COMMENT ON COLUMN "tsdeffonant"."cedrif" IS 'Responsable de fonde de anticipo';

COMMENT ON COLUMN "tsdeffonant"."codcat" IS 'Código de la categoria programatica';

COMMENT ON COLUMN "tsdeffonant"."numcue" IS 'Numero de la cuenta de Bancaria';

COMMENT ON COLUMN "tsdeffonant"."tipmovsal" IS 'Tipo de Movimiento de Salida';

COMMENT ON COLUMN "tsdeffonant"."tipmovren" IS 'Tipo de movimiento de Rendición';

COMMENT ON COLUMN "tsdeffonant"."monmincon" IS 'Monto mínimo de Constitución (en U.T.)';

COMMENT ON COLUMN "tsdeffonant"."monmaxcon" IS 'Monto máximo de Constitución (en U.T.)';

COMMENT ON COLUMN "tsdeffonant"."porrep" IS 'Porcentaje de Reposición';

COMMENT ON COLUMN "tsdeffonant"."numini" IS 'Numero Incial';

COMMENT ON COLUMN "tsdeffonant"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsfonant
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsfonant" CASCADE;

CREATE TABLE "tsfonant"
(
    "reffon" VARCHAR(8) NOT NULL,
    "fecfon" DATE NOT NULL,
    "cedrif" VARCHAR(15),
    "desfon" VARCHAR(1000),
    "monfon" NUMERIC(14,2),
    "stafon" VARCHAR(1),
    "codfon" VARCHAR(3) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsfonant" IS 'null';

COMMENT ON COLUMN "tsfonant"."reffon" IS 'null';

COMMENT ON COLUMN "tsfonant"."fecfon" IS 'null';

COMMENT ON COLUMN "tsfonant"."cedrif" IS 'null';

COMMENT ON COLUMN "tsfonant"."desfon" IS 'null';

COMMENT ON COLUMN "tsfonant"."monfon" IS 'null';

COMMENT ON COLUMN "tsfonant"."stafon" IS 'null';

COMMENT ON COLUMN "tsfonant"."codfon" IS 'null';

COMMENT ON COLUMN "tsfonant"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsdetfon
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsdetfon" CASCADE;

CREATE TABLE "tsdetfon"
(
    "reffon" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "codcat" VARCHAR(50) NOT NULL,
    "monfon" NUMERIC(14,2),
    "monrec" NUMERIC(14,2),
    "totfon" NUMERIC(14,2),
    "stafon" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsdetfon" IS 'null';

COMMENT ON COLUMN "tsdetfon"."reffon" IS 'null';

COMMENT ON COLUMN "tsdetfon"."codart" IS 'null';

COMMENT ON COLUMN "tsdetfon"."codcat" IS 'null';

COMMENT ON COLUMN "tsdetfon"."monfon" IS 'null';

COMMENT ON COLUMN "tsdetfon"."monrec" IS 'null';

COMMENT ON COLUMN "tsdetfon"."totfon" IS 'null';

COMMENT ON COLUMN "tsdetfon"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsccilnmov
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsccilnmov" CASCADE;

CREATE TABLE "tsccilnmov"
(
    "numcue" VARCHAR(20) NOT NULL,
    "mescon" VARCHAR(2) NOT NULL,
    "anocon" VARCHAR(4) NOT NULL,
    "status" VARCHAR(1) DEFAULT 'A',
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsccilnmov" IS 'null';

COMMENT ON COLUMN "tsccilnmov"."numcue" IS 'null';

COMMENT ON COLUMN "tsccilnmov"."mescon" IS 'null';

COMMENT ON COLUMN "tsccilnmov"."anocon" IS 'null';

COMMENT ON COLUMN "tsccilnmov"."status" IS 'null';

COMMENT ON COLUMN "tsccilnmov"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opordemp
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opordemp" CASCADE;

CREATE TABLE "opordemp"
(
    "numord" VARCHAR(8) NOT NULL,
    "cedrif" VARCHAR(15) NOT NULL,
    "montot" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opordemp" IS 'null';

COMMENT ON COLUMN "opordemp"."numord" IS 'null';

COMMENT ON COLUMN "opordemp"."cedrif" IS 'null';

COMMENT ON COLUMN "opordemp"."montot" IS 'null';

COMMENT ON COLUMN "opordemp"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsdefmon
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsdefmon" CASCADE;

CREATE TABLE "tsdefmon"
(
    "codmon" VARCHAR(3) NOT NULL,
    "nommon" VARCHAR(40) NOT NULL,
    "aumdis" VARCHAR(1) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_tsdefmon_codmon" UNIQUE ("codmon")
);

COMMENT ON TABLE "tsdefmon" IS 'null';

COMMENT ON COLUMN "tsdefmon"."codmon" IS 'null';

COMMENT ON COLUMN "tsdefmon"."nommon" IS 'null';

COMMENT ON COLUMN "tsdefmon"."aumdis" IS 'null';

COMMENT ON COLUMN "tsdefmon"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tscomban
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tscomban" CASCADE;

CREATE TABLE "tscomban"
(
    "codcom" VARCHAR(3) NOT NULL,
    "descom" VARCHAR(100) NOT NULL,
    "moncom" NUMERIC(14,2),
    "codcta" VARCHAR(32),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "tscomban"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tspagele
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tspagele" CASCADE;

CREATE TABLE "tspagele"
(
    "refpag" VARCHAR(8) NOT NULL,
    "numcue" VARCHAR(20) NOT NULL,
    "fecpag" DATE NOT NULL,
    "monpag" NUMERIC(14,2) NOT NULL,
    "estpag" VARCHAR(1),
    "loguse" VARCHAR(50),
    "tipdoc" VARCHAR(4),
    "despag" VARCHAR(1000),
    "cedrif" VARCHAR(15),
    "tiptxt" VARCHAR(1),
    "fecanu" DATE,
    "desanu" VARCHAR(100),
    "fecpagado" DATE,
    "fecefepag" DATE,
    "codmon" VARCHAR(3) NOT NULL,
    "valmon" NUMERIC(14,6),
    "coddirec" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tspagele" IS 'Tabla que contiene información referente a los Pagos Electronicos.';

COMMENT ON COLUMN "tspagele"."refpag" IS 'Número de Referencia del Pago.';

COMMENT ON COLUMN "tspagele"."numcue" IS 'Número de la Cuenta Bancaria.';

COMMENT ON COLUMN "tspagele"."fecpag" IS 'Fecha de Emisión del Pago.';

COMMENT ON COLUMN "tspagele"."monpag" IS 'Monto Total del Pago.';

COMMENT ON COLUMN "tspagele"."estpag" IS 'Estatus del pagado.';

COMMENT ON COLUMN "tspagele"."loguse" IS 'logon del usuario que realizo el pago';

COMMENT ON COLUMN "tspagele"."tipdoc" IS 'Tipo de Documento';

COMMENT ON COLUMN "tspagele"."despag" IS 'Descripción del Pagado.';

COMMENT ON COLUMN "tspagele"."cedrif" IS 'Cédula o RIF del Beneficiario.';

COMMENT ON COLUMN "tspagele"."tiptxt" IS 'Tipo de TXT  a generar';

COMMENT ON COLUMN "tspagele"."fecanu" IS 'Fecha de Anulación.';

COMMENT ON COLUMN "tspagele"."desanu" IS 'Descripcion de la anulacion';

COMMENT ON COLUMN "tspagele"."fecpagado" IS 'Fecha de Aprobación del Pago.';

COMMENT ON COLUMN "tspagele"."fecefepag" IS 'Fecha Efectiva del Pago.';

COMMENT ON COLUMN "tspagele"."codmon" IS 'Código de la Moneda';

COMMENT ON COLUMN "tspagele"."valmon" IS 'Valor de Moneda';

COMMENT ON COLUMN "tspagele"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "tspagele"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- tsdetpagele
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "tsdetpagele" CASCADE;

CREATE TABLE "tsdetpagele"
(
    "refpag" VARCHAR(8) NOT NULL,
    "numord" VARCHAR(8) NOT NULL,
    "fecval" DATE NOT NULL,
    "estord" VARCHAR(1),
    "numcom" VARCHAR(8),
    "refmovlib" VARCHAR(8),
    "refpagpre" VARCHAR(8),
    "monord" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "tsdetpagele" IS 'Tabla que contiene información referente al detalle de los Pagos Electronicos.';

COMMENT ON COLUMN "tsdetpagele"."refpag" IS 'Número de Referencia del Pago.';

COMMENT ON COLUMN "tsdetpagele"."numord" IS 'Número de la Orden';

COMMENT ON COLUMN "tsdetpagele"."fecval" IS 'Fecha de valor.';

COMMENT ON COLUMN "tsdetpagele"."estord" IS 'Estatus de la Orden.';

COMMENT ON COLUMN "tsdetpagele"."numcom" IS 'Numero de Comprobante.';

COMMENT ON COLUMN "tsdetpagele"."refmovlib" IS 'Referencia Mov segun Libros';

COMMENT ON COLUMN "tsdetpagele"."refpagpre" IS 'Referencia pagado presupuestario';

COMMENT ON COLUMN "tsdetpagele"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opsolpag
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opsolpag" CASCADE;

CREATE TABLE "opsolpag"
(
    "refsol" VARCHAR(8) NOT NULL,
    "fecsol" DATE,
    "refcom" VARCHAR(8),
    "dessol" VARCHAR(1000),
    "monsol" NUMERIC(14,2),
    "stasol" VARCHAR(1),
    "cedrif" VARCHAR(15),
    "nomben" VARCHAR(1000),
    "numsolcre" VARCHAR(15),
    "numcre" VARCHAR(15),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "opsolpag"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opdetsolpag
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opdetsolpag" CASCADE;

CREATE TABLE "opdetsolpag"
(
    "refsol" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(50) NOT NULL,
    "monimp" NUMERIC(14,2),
    "staimp" VARCHAR(1),
    "reford" VARCHAR(8),
    "refere" VARCHAR(8),
    "refprc" VARCHAR(8),
    "refcom" VARCHAR(8),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "opdetsolpag"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opctaben
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opctaben" CASCADE;

CREATE TABLE "opctaben"
(
    "cedrif" VARCHAR(15) NOT NULL,
    "codcta" VARCHAR(32),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opctaben" IS 'Tabla que contiene información referente a cuentas contables de los beneficiarios';

COMMENT ON COLUMN "opctaben"."cedrif" IS 'Cédula o RIF del beneficiario';

COMMENT ON COLUMN "opctaben"."codcta" IS 'Cuenta Contable asociada al Beneficiario';

COMMENT ON COLUMN "opctaben"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- optipdes
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "optipdes" CASCADE;

CREATE TABLE "optipdes"
(
    "codtde" VARCHAR(3) NOT NULL,
    "destde" VARCHAR(250) NOT NULL,
    "codcta" VARCHAR(32) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "optipdes" IS 'Tabla que contiene información referente a tipos de descuentos';

COMMENT ON COLUMN "optipdes"."codtde" IS 'codigo del tipo de descuento';

COMMENT ON COLUMN "optipdes"."destde" IS 'Descripción del tipo de descuento';

COMMENT ON COLUMN "optipdes"."codcta" IS 'Cuenta Contable asociada al tipo de descuento';

COMMENT ON COLUMN "optipdes"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opvenfacmen
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opvenfacmen" CASCADE;

CREATE TABLE "opvenfacmen"
(
    "mes" VARCHAR(2) NOT NULL,
    "mongra" NUMERIC(14,2) NOT NULL,
    "mosgra" NUMERIC(14,2) NOT NULL,
    "totmes" NUMERIC(14,2) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opvenfacmen" IS 'Tabla que contiene información referente al Registro de Ventas por Facturación Mensual';

COMMENT ON COLUMN "opvenfacmen"."mes" IS 'Mes';

COMMENT ON COLUMN "opvenfacmen"."mongra" IS 'Monto de Venta Gravadas';

COMMENT ON COLUMN "opvenfacmen"."mosgra" IS 'Monto de Venta sin Gravadas';

COMMENT ON COLUMN "opvenfacmen"."totmes" IS 'Total';

COMMENT ON COLUMN "opvenfacmen"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opciecaj
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opciecaj" CASCADE;

CREATE TABLE "opciecaj"
(
    "numref" VARCHAR(8) NOT NULL,
    "feccie" DATE NOT NULL,
    "descon" VARCHAR(1000) NOT NULL,
    "codubi" VARCHAR(30),
    "codfin" VARCHAR(4),
    "codcajchi" VARCHAR(3),
    "loguse" VARCHAR(50),
    "moncie" NUMERIC(14,2) NOT NULL,
    "refcom" VARCHAR(8),
    "refpag" VARCHAR(8),
    "numcom" VARCHAR(8),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opciecaj" IS 'Tabla que contiene información referente al cierre de caja chica';

COMMENT ON COLUMN "opciecaj"."numref" IS 'Referencia';

COMMENT ON COLUMN "opciecaj"."feccie" IS 'Fecha de Cierre';

COMMENT ON COLUMN "opciecaj"."descon" IS 'Descripción del Concepto';

COMMENT ON COLUMN "opciecaj"."codubi" IS 'Código de la Unidad de Origen';

COMMENT ON COLUMN "opciecaj"."codfin" IS 'Código del Tipo de Financiamiento';

COMMENT ON COLUMN "opciecaj"."codcajchi" IS 'Código de la Caja Chica';

COMMENT ON COLUMN "opciecaj"."loguse" IS 'Usuario que realizo el cierre';

COMMENT ON COLUMN "opciecaj"."moncie" IS 'Monto Total del Cierre';

COMMENT ON COLUMN "opciecaj"."refcom" IS 'Referencia del Compromiso';

COMMENT ON COLUMN "opciecaj"."refpag" IS 'Número de Referencia del Pagado.';

COMMENT ON COLUMN "opciecaj"."numcom" IS 'Número de Comprobante.';

COMMENT ON COLUMN "opciecaj"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- opdetciecaj
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "opdetciecaj" CASCADE;

CREATE TABLE "opdetciecaj"
(
    "numref" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(50) NOT NULL,
    "moncom" NUMERIC(14,2) NOT NULL,
    "refsal" VARCHAR(2000),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "opdetciecaj" IS 'Tabla que contiene información referente al detalle del Cierre de Caja Chica';

COMMENT ON COLUMN "opdetciecaj"."numref" IS 'Referencia';

COMMENT ON COLUMN "opdetciecaj"."codpre" IS 'Código Presupuestario';

COMMENT ON COLUMN "opdetciecaj"."moncom" IS 'Monto a Comprometer';

COMMENT ON COLUMN "opdetciecaj"."refsal" IS 'Referencia de la Salida de Caja Chica';

COMMENT ON COLUMN "opdetciecaj"."id" IS 'Identificador Único del registro';

ALTER TABLE "opbenefi" ADD CONSTRAINT "opbenefi_FK_1"
    FOREIGN KEY ("codtipben")
    REFERENCES "optipben" ("codtipben");

ALTER TABLE "opordpag" ADD CONSTRAINT "opordpag_FK_1"
    FOREIGN KEY ("cedrif")
    REFERENCES "opbenefi" ("cedrif");

ALTER TABLE "opordpag" ADD CONSTRAINT "opordpag_FK_2"
    FOREIGN KEY ("codmon")
    REFERENCES "tsdefmon" ("codmon");

ALTER TABLE "opretcon" ADD CONSTRAINT "opretcon_FK_1"
    FOREIGN KEY ("codtip")
    REFERENCES "optipret" ("codtip");

ALTER TABLE "opretord" ADD CONSTRAINT "opretord_FK_1"
    FOREIGN KEY ("codtip")
    REFERENCES "optipret" ("codtip");

ALTER TABLE "tscheemi" ADD CONSTRAINT "tscheemi_FK_1"
    FOREIGN KEY ("codmon")
    REFERENCES "tsdefmon" ("codmon");

ALTER TABLE "tsdefban" ADD CONSTRAINT "tsdefban_FK_1"
    FOREIGN KEY ("tipcue")
    REFERENCES "tstipcue" ("codtip");

ALTER TABLE "tsdefban" ADD CONSTRAINT "tsdefban_FK_2"
    FOREIGN KEY ("tipren")
    REFERENCES "tstipren" ("codtip");

ALTER TABLE "tsmovban" ADD CONSTRAINT "tsmovban_FK_1"
    FOREIGN KEY ("numcue")
    REFERENCES "tsdefban" ("numcue");

ALTER TABLE "tsmovban" ADD CONSTRAINT "tsmovban_FK_2"
    FOREIGN KEY ("tipmov")
    REFERENCES "tstipmov" ("codtip");

ALTER TABLE "tsmovban" ADD CONSTRAINT "tsmovban_FK_3"
    FOREIGN KEY ("codmon")
    REFERENCES "tsdefmon" ("codmon");

ALTER TABLE "tsmovlib" ADD CONSTRAINT "tsmovlib_FK_1"
    FOREIGN KEY ("numcue")
    REFERENCES "tsdefban" ("numcue");

ALTER TABLE "tsmovlib" ADD CONSTRAINT "tsmovlib_FK_2"
    FOREIGN KEY ("tipmov")
    REFERENCES "tstipmov" ("codtip");

ALTER TABLE "tsmovlib" ADD CONSTRAINT "tsmovlib_FK_3"
    FOREIGN KEY ("codmon")
    REFERENCES "tsdefmon" ("codmon");

ALTER TABLE "tsrepret" ADD CONSTRAINT "tsrepret_FK_1"
    FOREIGN KEY ("codret")
    REFERENCES "optipret" ("codtip");

ALTER TABLE "tsretiva" ADD CONSTRAINT "tsretiva_FK_1"
    FOREIGN KEY ("codret")
    REFERENCES "optipret" ("codtip");

ALTER TABLE "tsdeffonant" ADD CONSTRAINT "tsdeffonant_FK_1"
    FOREIGN KEY ("coduniadm")
    REFERENCES "tsuniadm" ("coduniadm");

ALTER TABLE "tsdeffonant" ADD CONSTRAINT "tsdeffonant_FK_2"
    FOREIGN KEY ("cedrif")
    REFERENCES "opbenefi" ("cedrif");

ALTER TABLE "tsdeffonant" ADD CONSTRAINT "tsdeffonant_FK_3"
    FOREIGN KEY ("numcue")
    REFERENCES "tsdefban" ("numcue");

ALTER TABLE "tspagele" ADD CONSTRAINT "tspagele_FK_1"
    FOREIGN KEY ("codmon")
    REFERENCES "tsdefmon" ("codmon");

ALTER TABLE "opdetsolpag" ADD CONSTRAINT "opdetsolpag_FK_1"
    FOREIGN KEY ("refsol")
    REFERENCES "opsolpag" ("refsol");

ALTER TABLE "opdetsolpag" ADD CONSTRAINT "opdetsolpag_FK_2"
    FOREIGN KEY ("reford")
    REFERENCES "opordpag" ("numord");
