
-----------------------------------------------------------------------
-- cpadidis
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpadidis" CASCADE;

CREATE TABLE "cpadidis"
(
    "refadi" VARCHAR(8) NOT NULL,
    "fecadi" DATE NOT NULL,
    "anoadi" VARCHAR(4),
    "desadi" VARCHAR(1000),
    "desanu" VARCHAR(1000),
    "adidis" VARCHAR(1),
    "totadi" NUMERIC(14,2),
    "staadi" VARCHAR(1),
    "numcom" VARCHAR(8),
    "fecanu" DATE,
    "peradi" VARCHAR(2),
    "tipgas" VARCHAR(250),
    "loguse" VARCHAR(50),
    "fecreg" TIMESTAMP DEFAULT 'now',
    "clasifica" VARCHAR(1),
    "coddirec" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpadidis" IS 'Tabla que contiene información referente a las Adiciones y Disminuciones efectuadas a las partidas presupuestarias.';

COMMENT ON COLUMN "cpadidis"."refadi" IS 'Referencia de la adición o disminución.';

COMMENT ON COLUMN "cpadidis"."fecadi" IS 'Fecha en que se realiza la adición o disminución.';

COMMENT ON COLUMN "cpadidis"."anoadi" IS 'Año de adición.';

COMMENT ON COLUMN "cpadidis"."desadi" IS 'Descripción de la adición o disminución.';

COMMENT ON COLUMN "cpadidis"."desanu" IS 'Descripción de la anulación, en caso de que el movimiento esté anulado.';

COMMENT ON COLUMN "cpadidis"."adidis" IS 'Indica si el movimiento es de adición o disminución.';

COMMENT ON COLUMN "cpadidis"."totadi" IS 'Monto total de las adiciones o disminuciones.';

COMMENT ON COLUMN "cpadidis"."staadi" IS 'Estatus de las adiciones.';

COMMENT ON COLUMN "cpadidis"."numcom" IS 'Numero del comprobante contable';

COMMENT ON COLUMN "cpadidis"."fecanu" IS 'Fecha en la cual se anulo el movimiento.';

COMMENT ON COLUMN "cpadidis"."peradi" IS 'Fecha en la cual se anulo el movimiento.';

COMMENT ON COLUMN "cpadidis"."tipgas" IS 'Tipo de Gasto';

COMMENT ON COLUMN "cpadidis"."loguse" IS 'null';

COMMENT ON COLUMN "cpadidis"."fecreg" IS 'Fecha de registro';

COMMENT ON COLUMN "cpadidis"."clasifica" IS 'Tipo de Clasificación Crédito ó Reformulación';

COMMENT ON COLUMN "cpadidis"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "cpadidis"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpajuste
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpajuste" CASCADE;

CREATE TABLE "cpajuste"
(
    "refaju" VARCHAR(8) NOT NULL,
    "tipaju" VARCHAR(4) NOT NULL,
    "fecaju" DATE NOT NULL,
    "anoaju" VARCHAR(4) NOT NULL,
    "refere" VARCHAR(8) NOT NULL,
    "desaju" VARCHAR(1000),
    "desanu" VARCHAR(1000),
    "totaju" NUMERIC(14,2),
    "staaju" VARCHAR(1),
    "fecanu" DATE,
    "numcom" VARCHAR(8),
    "cuoanu" NUMERIC(6,0),
    "fecanudes" DATE,
    "fecanuhas" DATE,
    "ordpag" VARCHAR(1),
    "fecenvcon" DATE,
    "fecenvfin" DATE,
    "fecreg" TIMESTAMP DEFAULT 'now',
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpajuste" IS 'Tabla que contiene información referente a los Ajustes realizados durante la ejecución del ejercicio.';

COMMENT ON COLUMN "cpajuste"."refaju" IS 'Referencia del ajuste.';

COMMENT ON COLUMN "cpajuste"."tipaju" IS 'Código del Tipo de ajuste.';

COMMENT ON COLUMN "cpajuste"."fecaju" IS 'Fecha de emisión ó Registro del ajuste.';

COMMENT ON COLUMN "cpajuste"."anoaju" IS 'Año del Ajuste.';

COMMENT ON COLUMN "cpajuste"."refere" IS 'Indica el Número del Movimiento a quien hace referencia el ajuste.';

COMMENT ON COLUMN "cpajuste"."desaju" IS 'Descripción del Ajuste.';

COMMENT ON COLUMN "cpajuste"."desanu" IS 'Descripción de la  Anulación.';

COMMENT ON COLUMN "cpajuste"."totaju" IS 'Monto Total del Ajuste.';

COMMENT ON COLUMN "cpajuste"."staaju" IS 'Estatus del Ajuste.';

COMMENT ON COLUMN "cpajuste"."fecanu" IS 'Fecha en la que se realizó la anulación';

COMMENT ON COLUMN "cpajuste"."numcom" IS 'Número de Comprobante de Anulación de una Orden de Pago Permanente';

COMMENT ON COLUMN "cpajuste"."cuoanu" IS 'Número de Cuotas a anular de la Orden de Pago Permanente';

COMMENT ON COLUMN "cpajuste"."fecanudes" IS 'Fecha de Anulación Desde';

COMMENT ON COLUMN "cpajuste"."fecanuhas" IS 'Fecha de Anulación Hasta';

COMMENT ON COLUMN "cpajuste"."ordpag" IS 'Número de Orden de Pago';

COMMENT ON COLUMN "cpajuste"."fecenvcon" IS 'Fecha de Envío a Contraloría';

COMMENT ON COLUMN "cpajuste"."fecenvfin" IS 'Fecha de Envío a Finanzas';

COMMENT ON COLUMN "cpajuste"."fecreg" IS 'Fecha de registro';

COMMENT ON COLUMN "cpajuste"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpasiini
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpasiini" CASCADE;

CREATE TABLE "cpasiini"
(
    "codpre" VARCHAR(50) NOT NULL,
    "nompre" VARCHAR(500),
    "perpre" VARCHAR(2) NOT NULL,
    "anopre" VARCHAR(4) NOT NULL,
    "monasi" NUMERIC(14,2),
    "monprc" NUMERIC(14,2),
    "moncom" NUMERIC(14,2),
    "moncau" NUMERIC(14,2),
    "monpag" NUMERIC(14,2),
    "montra" NUMERIC(14,2),
    "montrn" NUMERIC(14,2),
    "monadi" NUMERIC(14,2),
    "mondim" NUMERIC(14,2),
    "monaju" NUMERIC(14,2),
    "mondis" NUMERIC(14,2),
    "difere" NUMERIC(14,2),
    "status" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpasiini" IS 'Tabla que contiene información referente a las asignaciones iniciales de cada una de las partidas presupuestarias registradas al inicio del ejercicio.';

COMMENT ON COLUMN "cpasiini"."codpre" IS 'Código de la Partida Presupuestario.';

COMMENT ON COLUMN "cpasiini"."nompre" IS 'Nombre del Título Presupuestario.';

COMMENT ON COLUMN "cpasiini"."perpre" IS 'Periodo Presupuestario.';

COMMENT ON COLUMN "cpasiini"."anopre" IS 'Año Presupuestario.';

COMMENT ON COLUMN "cpasiini"."monasi" IS 'Monto Asignado.';

COMMENT ON COLUMN "cpasiini"."monprc" IS 'Monto Precomprometido.';

COMMENT ON COLUMN "cpasiini"."moncom" IS 'Monto Comprometido.';

COMMENT ON COLUMN "cpasiini"."moncau" IS 'Monto Causado.';

COMMENT ON COLUMN "cpasiini"."monpag" IS 'Monto Pagado.';

COMMENT ON COLUMN "cpasiini"."montra" IS 'Monto Trasladado.';

COMMENT ON COLUMN "cpasiini"."montrn" IS 'Monto Trasladado (-).';

COMMENT ON COLUMN "cpasiini"."monadi" IS 'Monto Adicional.';

COMMENT ON COLUMN "cpasiini"."mondim" IS 'Monto Disminuciones.';

COMMENT ON COLUMN "cpasiini"."monaju" IS 'Monto de Ajustes.';

COMMENT ON COLUMN "cpasiini"."mondis" IS 'Monto de Ajustes.';

COMMENT ON COLUMN "cpasiini"."difere" IS 'Corresponde a Diferencia.';

COMMENT ON COLUMN "cpasiini"."status" IS 'Estatus de la Asignación.';

-----------------------------------------------------------------------
-- cpcompro
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpcompro" CASCADE;

CREATE TABLE "cpcompro"
(
    "refcom" VARCHAR(8) NOT NULL,
    "tipcom" VARCHAR(4) NOT NULL,
    "feccom" DATE NOT NULL,
    "anocom" VARCHAR(4),
    "refprc" VARCHAR(8),
    "tipprc" VARCHAR(4),
    "descom" VARCHAR(1000),
    "desanu" VARCHAR(1000),
    "moncom" NUMERIC(14,2),
    "salcau" NUMERIC(14,2),
    "salpag" NUMERIC(14,2),
    "salaju" NUMERIC(14,2),
    "stacom" VARCHAR(1),
    "fecanu" DATE,
    "cedrif" VARCHAR(15),
    "tipo" VARCHAR(1),
    "aprcom" VARCHAR(1),
    "codubi" VARCHAR(30),
    "motrec" VARCHAR(1000),
    "loguse" VARCHAR(50),
    "sercon" BOOLEAN,
    "fecser" DATE,
    "tesore" BOOLEAN,
    "fectes" DATE,
    "admini" BOOLEAN,
    "fecadm" DATE,
    "fecreg" TIMESTAMP DEFAULT 'now',
    "coddirec" VARCHAR(4),
    "usuapr" VARCHAR(50),
    "fecapr" DATE,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpcompro" IS 'Tabla que contiene información referente a las operaciones que comprometen la disponibilidad de las partidas presupuestarias.';

COMMENT ON COLUMN "cpcompro"."refcom" IS 'Referencia del Compromiso.';

COMMENT ON COLUMN "cpcompro"."tipcom" IS 'Tipo de Compromiso.';

COMMENT ON COLUMN "cpcompro"."feccom" IS 'Fecha de Emisión del Compromiso.';

COMMENT ON COLUMN "cpcompro"."anocom" IS 'Año del Compromiso.';

COMMENT ON COLUMN "cpcompro"."refprc" IS 'Referencia del Precompromiso al cual hace Referencia.';

COMMENT ON COLUMN "cpcompro"."tipprc" IS 'Tipo Precompromiso.';

COMMENT ON COLUMN "cpcompro"."descom" IS 'Descripción del Compromiso.';

COMMENT ON COLUMN "cpcompro"."desanu" IS 'Descripción de Anulado.';

COMMENT ON COLUMN "cpcompro"."moncom" IS 'Monto del Compromiso.';

COMMENT ON COLUMN "cpcompro"."salcau" IS 'Saldo Causado.';

COMMENT ON COLUMN "cpcompro"."salpag" IS 'Saldo Pagado.';

COMMENT ON COLUMN "cpcompro"."salaju" IS 'Corresponde a Saldo Ajustado.';

COMMENT ON COLUMN "cpcompro"."stacom" IS 'Estatus del Compromiso.';

COMMENT ON COLUMN "cpcompro"."fecanu" IS 'Fecha de Anulación.';

COMMENT ON COLUMN "cpcompro"."cedrif" IS 'Cédula ó Rif. del Beneficiario.';

COMMENT ON COLUMN "cpcompro"."tipo" IS 'Indica si la Orden es de tipo Adjudicación Directa, Licitación, Compra Directa, Compra Eventual';

COMMENT ON COLUMN "cpcompro"."aprcom" IS 'null';

COMMENT ON COLUMN "cpcompro"."codubi" IS 'null';

COMMENT ON COLUMN "cpcompro"."motrec" IS 'null';

COMMENT ON COLUMN "cpcompro"."loguse" IS 'null';

COMMENT ON COLUMN "cpcompro"."fecser" IS 'null';

COMMENT ON COLUMN "cpcompro"."tesore" IS 'null';

COMMENT ON COLUMN "cpcompro"."fectes" IS 'null';

COMMENT ON COLUMN "cpcompro"."admini" IS 'null';

COMMENT ON COLUMN "cpcompro"."fecadm" IS 'null';

COMMENT ON COLUMN "cpcompro"."fecreg" IS 'Fecha de registro';

COMMENT ON COLUMN "cpcompro"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "cpcompro"."usuapr" IS 'usuario que realizo la aprobación';

COMMENT ON COLUMN "cpcompro"."fecapr" IS 'fecha en que realizo la aprobación';

COMMENT ON COLUMN "cpcompro"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpdefniv
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpdefniv" CASCADE;

CREATE TABLE "cpdefniv"
(
    "codemp" VARCHAR(3) NOT NULL,
    "loncod" NUMERIC(2,0) NOT NULL,
    "rupcat" NUMERIC(2,0) NOT NULL,
    "ruppar" NUMERIC(2,0) NOT NULL,
    "nivdis" NUMERIC(2,0) NOT NULL,
    "forpre" VARCHAR(32) NOT NULL,
    "asiper" VARCHAR(1) NOT NULL,
    "numper" NUMERIC(2,0) NOT NULL,
    "peract" VARCHAR(2) NOT NULL,
    "fecper" DATE,
    "fecini" DATE,
    "feccie" DATE,
    "etadef" VARCHAR(1),
    "staprc" VARCHAR(1),
    "coraep" INTEGER(8),
    "gencom" VARCHAR(1),
    "numcom" VARCHAR(8),
    "caraep" VARCHAR(8),
    "tiptraprc" VARCHAR(4),
    "fueord" VARCHAR(4),
    "fuecre" VARCHAR(4),
    "fuetra" VARCHAR(4),
    "nomgob" VARCHAR(100),
    "nomsec" VARCHAR(100),
    "unidad" VARCHAR(100),
    "cortrasla" INTEGER,
    "coradidis" INTEGER,
    "corprc" INTEGER,
    "corcom" INTEGER,
    "corcau" INTEGER,
    "corpag" INTEGER,
    "corsoladidis" INTEGER,
    "corsoltra" INTEGER,
    "coraju" INTEGER,
    "corfue" INTEGER,
    "btnanu" BOOLEAN DEFAULT 't',
    "btneli" BOOLEAN DEFAULT 't',
    "conpar" NUMERIC(2,0),
    "tipcau" VARCHAR(4),
    "cedrif" VARCHAR(15),
    "tipcom" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpdefniv" IS 'Tabla que contiene información referente a la definición de niveles presupuestarios.';

COMMENT ON COLUMN "cpdefniv"."codemp" IS 'Código de la Empresa.';

COMMENT ON COLUMN "cpdefniv"."loncod" IS 'Longitud del Título Presupuestario.';

COMMENT ON COLUMN "cpdefniv"."rupcat" IS 'Cantidad de Rupturas de la categoría.';

COMMENT ON COLUMN "cpdefniv"."ruppar" IS 'Cantidad de Rupturas de la partida.';

COMMENT ON COLUMN "cpdefniv"."nivdis" IS 'Nivel de Disponibilidad de la Partida Presupuestaria.';

COMMENT ON COLUMN "cpdefniv"."forpre" IS 'Formato de Presupuesto.';

COMMENT ON COLUMN "cpdefniv"."asiper" IS 'Asignación del Periodo.';

COMMENT ON COLUMN "cpdefniv"."numper" IS 'Numero del Periodo.';

COMMENT ON COLUMN "cpdefniv"."peract" IS 'Periodo del Activo.';

COMMENT ON COLUMN "cpdefniv"."fecper" IS 'Fecha del Periodo.';

COMMENT ON COLUMN "cpdefniv"."fecini" IS 'Fecha de Inicio.';

COMMENT ON COLUMN "cpdefniv"."feccie" IS 'Fecha de Inicio.';

COMMENT ON COLUMN "cpdefniv"."etadef" IS 'Indica la Etapa de la Definición.';

COMMENT ON COLUMN "cpdefniv"."staprc" IS 'Estatus del Precompromiso.';

COMMENT ON COLUMN "cpdefniv"."coraep" IS 'Numero Inicial del Correlativo para el Registro de la Certificación Presupuestaria.';

COMMENT ON COLUMN "cpdefniv"."gencom" IS 'null';

COMMENT ON COLUMN "cpdefniv"."numcom" IS 'null';

COMMENT ON COLUMN "cpdefniv"."caraep" IS 'null';

COMMENT ON COLUMN "cpdefniv"."tiptraprc" IS 'Correlativo de Precompromiso';

COMMENT ON COLUMN "cpdefniv"."fueord" IS 'Fuente de Financiamiento Presupuesto Ordinario';

COMMENT ON COLUMN "cpdefniv"."fuecre" IS 'Fuente de Financiamiento Crédito Adicional';

COMMENT ON COLUMN "cpdefniv"."fuetra" IS 'Fuente de Financiamiento Traslado';

COMMENT ON COLUMN "cpdefniv"."nomgob" IS 'Nombre del Gobernador';

COMMENT ON COLUMN "cpdefniv"."nomsec" IS 'Nombre de la Secretaría General de Gobierno';

COMMENT ON COLUMN "cpdefniv"."unidad" IS 'Unidad Ejecutora';

COMMENT ON COLUMN "cpdefniv"."cortrasla" IS 'null';

COMMENT ON COLUMN "cpdefniv"."coradidis" IS 'null';

COMMENT ON COLUMN "cpdefniv"."corprc" IS 'null';

COMMENT ON COLUMN "cpdefniv"."corcom" IS 'null';

COMMENT ON COLUMN "cpdefniv"."corcau" IS 'null';

COMMENT ON COLUMN "cpdefniv"."corpag" IS 'null';

COMMENT ON COLUMN "cpdefniv"."corsoladidis" IS 'null';

COMMENT ON COLUMN "cpdefniv"."corsoltra" IS 'null';

COMMENT ON COLUMN "cpdefniv"."coraju" IS 'null';

COMMENT ON COLUMN "cpdefniv"."corfue" IS 'Correlativo Fuente de Financiamiento';

COMMENT ON COLUMN "cpdefniv"."btnanu" IS 'null';

COMMENT ON COLUMN "cpdefniv"."btneli" IS 'null';

COMMENT ON COLUMN "cpdefniv"."conpar" IS 'Consecutivo de partidas genericas';

COMMENT ON COLUMN "cpdefniv"."tipcau" IS 'Tipo de Causado Anticipo';

COMMENT ON COLUMN "cpdefniv"."cedrif" IS 'Cédula o RIF del beneficiario del Compromiso de Nómina';

COMMENT ON COLUMN "cpdefniv"."tipcom" IS 'Código tipo de documento del Compromiso de Nómina.';

COMMENT ON COLUMN "cpdefniv"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpdeftit
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpdeftit" CASCADE;

CREATE TABLE "cpdeftit"
(
    "codpre" VARCHAR(50) NOT NULL,
    "nompre" VARCHAR(500) NOT NULL,
    "codcta" VARCHAR(32),
    "stacod" VARCHAR(1),
    "coduni" VARCHAR(4),
    "estatus" VARCHAR(1),
    "codtip" VARCHAR(32),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpdeftit" IS 'Tabla que contiene información referente a definición de títulos presupuestarios.';

COMMENT ON COLUMN "cpdeftit"."codpre" IS 'Código Presupuestario.';

COMMENT ON COLUMN "cpdeftit"."nompre" IS 'Nombre del Código Presupuestario.';

COMMENT ON COLUMN "cpdeftit"."codcta" IS 'Código de Cuenta Contable.';

COMMENT ON COLUMN "cpdeftit"."stacod" IS 'Estatus del Código Presupuestario.';

COMMENT ON COLUMN "cpdeftit"."coduni" IS 'Código Unitario.';

COMMENT ON COLUMN "cpdeftit"."estatus" IS 'Tipo de Gasto (Inversión, Funcionamiento, Ninguno)';

COMMENT ON COLUMN "cpdeftit"."codtip" IS 'Tipo de Fuente de Financiamiento';

COMMENT ON COLUMN "cpdeftit"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpdiscre
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpdiscre" CASCADE;

CREATE TABLE "cpdiscre"
(
    "sector" VARCHAR(2),
    "partida" VARCHAR(3),
    "monto" NUMERIC(16,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpdiscre" IS 'null';

COMMENT ON COLUMN "cpdiscre"."sector" IS 'null';

COMMENT ON COLUMN "cpdiscre"."partida" IS 'null';

COMMENT ON COLUMN "cpdiscre"."monto" IS 'null';

COMMENT ON COLUMN "cpdiscre"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpdisfuefin
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpdisfuefin" CASCADE;

CREATE TABLE "cpdisfuefin"
(
    "correl" VARCHAR(10),
    "origen" VARCHAR(250),
    "fuefin" VARCHAR(4),
    "fecdis" DATE,
    "codpre" VARCHAR(50),
    "monasi" NUMERIC(20,2),
    "refdis" VARCHAR(8),
    "status" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpdisfuefin" IS 'null';

COMMENT ON COLUMN "cpdisfuefin"."correl" IS 'null';

COMMENT ON COLUMN "cpdisfuefin"."origen" IS 'null';

COMMENT ON COLUMN "cpdisfuefin"."fuefin" IS 'null';

COMMENT ON COLUMN "cpdisfuefin"."fecdis" IS 'null';

COMMENT ON COLUMN "cpdisfuefin"."codpre" IS 'null';

COMMENT ON COLUMN "cpdisfuefin"."monasi" IS 'null';

COMMENT ON COLUMN "cpdisfuefin"."refdis" IS 'null';

COMMENT ON COLUMN "cpdisfuefin"."status" IS 'null';

COMMENT ON COLUMN "cpdisfuefin"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpdocaju
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpdocaju" CASCADE;

CREATE TABLE "cpdocaju"
(
    "tipaju" VARCHAR(4) NOT NULL,
    "nomext" VARCHAR(100) NOT NULL,
    "nomabr" VARCHAR(4) NOT NULL,
    "refier" VARCHAR(1) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpdocaju" IS 'Tabla que contiene información referente a los documentos que ajustan.';

COMMENT ON COLUMN "cpdocaju"."tipaju" IS 'Código tipo de Documento.';

COMMENT ON COLUMN "cpdocaju"."nomext" IS 'Nombre Extendido.';

COMMENT ON COLUMN "cpdocaju"."nomabr" IS 'Nombre Abreviado.';

COMMENT ON COLUMN "cpdocaju"."refier" IS 'Indica si el Documento que se registra  refiere a un Precompromiso, Compromiso, Causado o Pagado.';

COMMENT ON COLUMN "cpdocaju"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpdoccau
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpdoccau" CASCADE;

CREATE TABLE "cpdoccau"
(
    "tipcau" VARCHAR(4) NOT NULL,
    "nomext" VARCHAR(100) NOT NULL,
    "nomabr" VARCHAR(4) NOT NULL,
    "refier" VARCHAR(1) NOT NULL,
    "afeprc" VARCHAR(1) NOT NULL,
    "afecom" VARCHAR(1) NOT NULL,
    "afecau" VARCHAR(1) NOT NULL,
    "afedis" VARCHAR(1) NOT NULL,
    "codcta" VARCHAR(32),
    "codtiptra" VARCHAR(3),
    "genlibcom" BOOLEAN,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpdoccau" IS 'Tabla que contiene información referente a los documentos que registran un causado.';

COMMENT ON COLUMN "cpdoccau"."tipcau" IS 'Código tipo de documento.';

COMMENT ON COLUMN "cpdoccau"."nomext" IS 'Nombre extendido.';

COMMENT ON COLUMN "cpdoccau"."nomabr" IS 'Nombre abreviado.';

COMMENT ON COLUMN "cpdoccau"."refier" IS 'Indica si el Documento que se registra  refiere a un Precompromiso, Compromiso o Ninguno.';

COMMENT ON COLUMN "cpdoccau"."afeprc" IS 'Indica si afecta al Precompromiso.';

COMMENT ON COLUMN "cpdoccau"."afecom" IS 'Indica si afecta al Compromiso.';

COMMENT ON COLUMN "cpdoccau"."afecau" IS 'Indica si afecta al Causado.';

COMMENT ON COLUMN "cpdoccau"."afedis" IS 'Indica si afecta la Disponibilidad.';

COMMENT ON COLUMN "cpdoccau"."codcta" IS 'Código de Cuenta Contable.';

COMMENT ON COLUMN "cpdoccau"."codtiptra" IS 'Tipo de Transacción.';

COMMENT ON COLUMN "cpdoccau"."genlibcom" IS 'Genera Libro de Compras';

COMMENT ON COLUMN "cpdoccau"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpdoccom
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpdoccom" CASCADE;

CREATE TABLE "cpdoccom"
(
    "tipcom" VARCHAR(4) NOT NULL,
    "nomext" VARCHAR(100) NOT NULL,
    "nomabr" VARCHAR(4) NOT NULL,
    "refprc" VARCHAR(1) NOT NULL,
    "afeprc" VARCHAR(1) NOT NULL,
    "afecom" VARCHAR(1) NOT NULL,
    "afedis" VARCHAR(1) NOT NULL,
    "reqaut" VARCHAR(1) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpdoccom" IS 'Tabla que contiene información referente a los documentos que registran un compromiso.';

COMMENT ON COLUMN "cpdoccom"."tipcom" IS 'Código tipo de documento.';

COMMENT ON COLUMN "cpdoccom"."nomext" IS 'Nombre extendido.';

COMMENT ON COLUMN "cpdoccom"."nomabr" IS 'Nombre abreviado.';

COMMENT ON COLUMN "cpdoccom"."refprc" IS 'Indica si el documento refiere a un precompromiso.';

COMMENT ON COLUMN "cpdoccom"."afeprc" IS 'Indica si afecta al precompromiso.';

COMMENT ON COLUMN "cpdoccom"."afecom" IS 'Indica si afecta al compromiso.';

COMMENT ON COLUMN "cpdoccom"."afedis" IS 'Indica si afecta la disponibilidad.';

COMMENT ON COLUMN "cpdoccom"."reqaut" IS 'Autorizacion';

COMMENT ON COLUMN "cpdoccom"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpdocpag
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpdocpag" CASCADE;

CREATE TABLE "cpdocpag"
(
    "tippag" VARCHAR(4) NOT NULL,
    "nomext" VARCHAR(100) NOT NULL,
    "nomabr" VARCHAR(4) NOT NULL,
    "refier" VARCHAR(1) NOT NULL,
    "afeprc" VARCHAR(1) NOT NULL,
    "afecom" VARCHAR(1) NOT NULL,
    "afecau" VARCHAR(1) NOT NULL,
    "afepag" VARCHAR(1) NOT NULL,
    "afedis" VARCHAR(1) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpdocpag" IS 'Tabla que contiene información referente a los documentos que registran un pagado';

COMMENT ON COLUMN "cpdocpag"."tippag" IS 'Código del Tipo de Documento';

COMMENT ON COLUMN "cpdocpag"."nomext" IS 'Nombre Extendido';

COMMENT ON COLUMN "cpdocpag"."nomabr" IS 'Nombre Abreviado';

COMMENT ON COLUMN "cpdocpag"."refier" IS 'Indica si el documento que se registra  refiere a un Precompromiso, Compromiso, Causado ó Ninguno';

COMMENT ON COLUMN "cpdocpag"."afeprc" IS 'Indica si afecta al Precompromiso.';

COMMENT ON COLUMN "cpdocpag"."afecom" IS 'Indica si afecta al Compromiso.';

COMMENT ON COLUMN "cpdocpag"."afecau" IS 'Indica si afecta al Compromiso.';

COMMENT ON COLUMN "cpdocpag"."afepag" IS 'Indica si afecta al Pagado';

COMMENT ON COLUMN "cpdocpag"."afedis" IS 'Indica si afecta la Disponibilidad.';

COMMENT ON COLUMN "cpdocpag"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpdocprc
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpdocprc" CASCADE;

CREATE TABLE "cpdocprc"
(
    "tipprc" VARCHAR(4) NOT NULL,
    "nomext" VARCHAR(100) NOT NULL,
    "nomabr" VARCHAR(4) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpdocprc" IS 'Tabla que contiene información referente a los documentos que registran un precompromiso.';

COMMENT ON COLUMN "cpdocprc"."tipprc" IS 'Código Tipo de Documento.';

COMMENT ON COLUMN "cpdocprc"."nomext" IS 'Nombre Extendido.';

COMMENT ON COLUMN "cpdocprc"."nomabr" IS 'Nombre Abreviado.';

COMMENT ON COLUMN "cpdocprc"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpimpcau
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpimpcau" CASCADE;

CREATE TABLE "cpimpcau"
(
    "refcau" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(50) NOT NULL,
    "monimp" NUMERIC(14,2),
    "monpag" NUMERIC(14,2),
    "monaju" NUMERIC(14,2),
    "staimp" VARCHAR(1),
    "refere" VARCHAR(8),
    "refprc" VARCHAR(8),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpimpcau" IS 'Tabla que contiene información referente a las imputaciones de causados.';

COMMENT ON COLUMN "cpimpcau"."refcau" IS 'Numero de Referencia del Causado.';

COMMENT ON COLUMN "cpimpcau"."codpre" IS 'Código del Presupuesto.';

COMMENT ON COLUMN "cpimpcau"."monimp" IS 'Monto de la Imputación.';

COMMENT ON COLUMN "cpimpcau"."monpag" IS 'Monto Pagado.';

COMMENT ON COLUMN "cpimpcau"."monaju" IS 'Monto Ajustado.';

COMMENT ON COLUMN "cpimpcau"."staimp" IS 'Estatus de la Imputación.';

COMMENT ON COLUMN "cpimpcau"."refere" IS 'Movimiento que hace referencia al Causado.';

COMMENT ON COLUMN "cpimpcau"."refprc" IS 'Numero de Referencia del Precompromiso.';

COMMENT ON COLUMN "cpimpcau"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpimpcom
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpimpcom" CASCADE;

CREATE TABLE "cpimpcom"
(
    "refcom" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(50) NOT NULL,
    "monimp" NUMERIC(14,2),
    "moncau" NUMERIC(14,2),
    "monpag" NUMERIC(14,2),
    "monaju" NUMERIC(14,2),
    "staimp" VARCHAR(1),
    "refere" VARCHAR(8),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpimpcom" IS 'Tabla que contiene información referente a las imputaciones de compromiso.';

COMMENT ON COLUMN "cpimpcom"."refcom" IS 'Numero de Referencia del Compromiso.';

COMMENT ON COLUMN "cpimpcom"."codpre" IS 'Código Presupuestario.';

COMMENT ON COLUMN "cpimpcom"."monimp" IS 'Monto de la Imputación.';

COMMENT ON COLUMN "cpimpcom"."moncau" IS 'Monto del Causado.';

COMMENT ON COLUMN "cpimpcom"."monpag" IS 'Monto del Causado.';

COMMENT ON COLUMN "cpimpcom"."monaju" IS 'Monto Ajustado.';

COMMENT ON COLUMN "cpimpcom"."staimp" IS 'Estatus de la Imputación.';

COMMENT ON COLUMN "cpimpcom"."refere" IS 'Movimiento que hace referencia al Compromiso.';

COMMENT ON COLUMN "cpimpcom"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpimppag
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpimppag" CASCADE;

CREATE TABLE "cpimppag"
(
    "refpag" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(50) NOT NULL,
    "monimp" NUMERIC(14,2),
    "monaju" NUMERIC(14,2),
    "staimp" VARCHAR(1),
    "refere" VARCHAR(8),
    "refprc" VARCHAR(8),
    "refcom" VARCHAR(8),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpimppag" IS 'Tabla que contiene información referente a la imputación de pagados.';

COMMENT ON COLUMN "cpimppag"."refpag" IS 'Número de Referencia del Pagado.';

COMMENT ON COLUMN "cpimppag"."codpre" IS 'Código Presupuestario.';

COMMENT ON COLUMN "cpimppag"."monimp" IS 'Monto de la Imputación.';

COMMENT ON COLUMN "cpimppag"."monaju" IS 'Monto del Ajuste.';

COMMENT ON COLUMN "cpimppag"."staimp" IS 'Estatus de la Imputación.';

COMMENT ON COLUMN "cpimppag"."refere" IS 'Movimiento que hace Referencia al Pagado.';

COMMENT ON COLUMN "cpimppag"."refprc" IS 'Número de Referencia del Precompromiso.';

COMMENT ON COLUMN "cpimppag"."refcom" IS 'Número de Referencia del Compromiso.';

COMMENT ON COLUMN "cpimppag"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpimpprc
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpimpprc" CASCADE;

CREATE TABLE "cpimpprc"
(
    "refprc" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(50) NOT NULL,
    "monimp" NUMERIC(14,2),
    "moncom" NUMERIC(14,2),
    "moncau" NUMERIC(14,2),
    "monpag" NUMERIC(14,2),
    "monaju" NUMERIC(14,2),
    "staimp" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpimpprc" IS 'Tabla que contiene información referente a la imputación de precompromiso.';

COMMENT ON COLUMN "cpimpprc"."refprc" IS 'Corresponde a Referencia Precompromiso (código).';

COMMENT ON COLUMN "cpimpprc"."codpre" IS 'Código Presupuestario.';

COMMENT ON COLUMN "cpimpprc"."monimp" IS 'Monto de la Imputación.';

COMMENT ON COLUMN "cpimpprc"."moncom" IS 'Monto del Compromiso.';

COMMENT ON COLUMN "cpimpprc"."moncau" IS 'Monto Causado.';

COMMENT ON COLUMN "cpimpprc"."monpag" IS 'Monto Pagado.';

COMMENT ON COLUMN "cpimpprc"."monaju" IS 'Monto Ajustado.';

COMMENT ON COLUMN "cpimpprc"."staimp" IS 'Estatus de la Imputación.';

COMMENT ON COLUMN "cpimpprc"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpmovadi
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpmovadi" CASCADE;

CREATE TABLE "cpmovadi"
(
    "refadi" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(50) NOT NULL,
    "perpre" VARCHAR(2) NOT NULL,
    "monmov" NUMERIC(14,2),
    "stamov" VARCHAR(1),
    "tipo" VARCHAR(1),
    "monto" NUMERIC(14,2),
    "iva" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpmovadi" IS 'Tabla que contiene información referente a los movimientos de las adiciones.';

COMMENT ON COLUMN "cpmovadi"."refadi" IS 'Referencia de la Adición';

COMMENT ON COLUMN "cpmovadi"."codpre" IS 'Código Presupuestario Asociado a la Adición.';

COMMENT ON COLUMN "cpmovadi"."perpre" IS 'Periodo Presupuestario Asociado a la Adición.';

COMMENT ON COLUMN "cpmovadi"."monmov" IS 'Monto del Movimiento Asociado a la Adición.';

COMMENT ON COLUMN "cpmovadi"."stamov" IS 'Estatus del Movimiento Asociado a la Adición.';

COMMENT ON COLUMN "cpmovadi"."tipo" IS 'null';

COMMENT ON COLUMN "cpmovadi"."monto" IS 'null';

COMMENT ON COLUMN "cpmovadi"."iva" IS 'null';

COMMENT ON COLUMN "cpmovadi"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpmovaju
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpmovaju" CASCADE;

CREATE TABLE "cpmovaju"
(
    "refaju" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(50) NOT NULL,
    "monaju" NUMERIC(14,2),
    "stamov" VARCHAR(1),
    "refprc" VARCHAR(8) NOT NULL,
    "refcom" VARCHAR(8) NOT NULL,
    "refcau" VARCHAR(8) NOT NULL,
    "refpag" VARCHAR(8),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpmovaju" IS 'Tabla que contiene información referente a los movimientos de los ajustes.';

COMMENT ON COLUMN "cpmovaju"."refaju" IS 'Numero de Referencia del Ajuste.';

COMMENT ON COLUMN "cpmovaju"."codpre" IS 'Código Presupuestario.';

COMMENT ON COLUMN "cpmovaju"."monaju" IS 'Monto Ajustado.';

COMMENT ON COLUMN "cpmovaju"."stamov" IS 'Estatus del Movimiento.';

COMMENT ON COLUMN "cpmovaju"."refprc" IS 'Numero de Referencia del Ajuste.';

COMMENT ON COLUMN "cpmovaju"."refcom" IS 'Numero de Referencia del Compromiso.';

COMMENT ON COLUMN "cpmovaju"."refcau" IS 'Numero de Referencia del causado.';

COMMENT ON COLUMN "cpmovaju"."refpag" IS 'Numero de Referencia del Pagado.';

COMMENT ON COLUMN "cpmovaju"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cppagos
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cppagos" CASCADE;

CREATE TABLE "cppagos"
(
    "refpag" VARCHAR(8) NOT NULL,
    "tippag" VARCHAR(4) NOT NULL,
    "fecpag" DATE NOT NULL,
    "anopag" VARCHAR(4),
    "refcau" VARCHAR(8),
    "tipcau" VARCHAR(4),
    "despag" VARCHAR(1000),
    "desanu" VARCHAR(1000),
    "monpag" NUMERIC(14,2),
    "salaju" NUMERIC(14,2),
    "stapag" VARCHAR(1),
    "fecanu" DATE,
    "cedrif" VARCHAR(15),
    "fecreg" TIMESTAMP DEFAULT 'now',
    "coddirec" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cppagos" IS 'Tabla que contiene información referente a los pagados.';

COMMENT ON COLUMN "cppagos"."refpag" IS 'Número de Referencia del Pagado.';

COMMENT ON COLUMN "cppagos"."tippag" IS 'Tipo de Pagado.';

COMMENT ON COLUMN "cppagos"."fecpag" IS 'Fecha del Pago.';

COMMENT ON COLUMN "cppagos"."anopag" IS 'Corresponde al año del Pagado.';

COMMENT ON COLUMN "cppagos"."refcau" IS 'Número de Referencia del causado.';

COMMENT ON COLUMN "cppagos"."tipcau" IS 'Tipo de Causado.';

COMMENT ON COLUMN "cppagos"."despag" IS 'Descripción del Pagado.';

COMMENT ON COLUMN "cppagos"."desanu" IS 'Descripción de la Anulación del Pagado.';

COMMENT ON COLUMN "cppagos"."monpag" IS 'Descripción de la Anulación del Pagado.';

COMMENT ON COLUMN "cppagos"."salaju" IS 'Monto del Ajuste correspondiente al pagado.';

COMMENT ON COLUMN "cppagos"."stapag" IS 'Estatus del Pagado.';

COMMENT ON COLUMN "cppagos"."fecanu" IS 'Estatus del Pagado.';

COMMENT ON COLUMN "cppagos"."cedrif" IS 'Cedula o RIF';

COMMENT ON COLUMN "cppagos"."fecreg" IS 'Fecha de registro';

COMMENT ON COLUMN "cppagos"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "cppagos"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpprecom
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpprecom" CASCADE;

CREATE TABLE "cpprecom"
(
    "refprc" VARCHAR(8) NOT NULL,
    "tipprc" VARCHAR(4) NOT NULL,
    "fecprc" DATE NOT NULL,
    "anoprc" VARCHAR(4),
    "desprc" VARCHAR(1000),
    "desanu" VARCHAR(1000),
    "monprc" NUMERIC(14,2),
    "salcom" NUMERIC(14,2),
    "salcau" NUMERIC(14,2),
    "salpag" NUMERIC(14,2),
    "salaju" NUMERIC(14,2),
    "staprc" VARCHAR(1),
    "fecanu" DATE,
    "cedrif" VARCHAR(15),
    "refsol" VARCHAR(8),
    "fecreg" TIMESTAMP DEFAULT 'now',
    "coddirec" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpprecom" IS 'Tabla que contiene información referente a los precompromisos.';

COMMENT ON COLUMN "cpprecom"."refprc" IS 'Numero de Referencia del Precompromiso.';

COMMENT ON COLUMN "cpprecom"."tipprc" IS 'Tipo de Precompromiso.';

COMMENT ON COLUMN "cpprecom"."fecprc" IS 'Fecha del Precompromiso.';

COMMENT ON COLUMN "cpprecom"."anoprc" IS 'Año del Precompromiso.';

COMMENT ON COLUMN "cpprecom"."desprc" IS 'Descripción del Precompromiso.';

COMMENT ON COLUMN "cpprecom"."desanu" IS 'Descripción de la Anulación del Precompromiso.';

COMMENT ON COLUMN "cpprecom"."monprc" IS 'Monto del Precompromiso.';

COMMENT ON COLUMN "cpprecom"."salcom" IS 'Saldo del Compromiso.';

COMMENT ON COLUMN "cpprecom"."salcau" IS 'Saldo del Causado.';

COMMENT ON COLUMN "cpprecom"."salpag" IS 'Saldo del Pagado.';

COMMENT ON COLUMN "cpprecom"."salaju" IS 'Saldo del Ajuste.';

COMMENT ON COLUMN "cpprecom"."staprc" IS 'Estatus del Precompromiso.';

COMMENT ON COLUMN "cpprecom"."fecanu" IS 'Fecha en la cual se anulo el Movimiento.';

COMMENT ON COLUMN "cpprecom"."cedrif" IS 'Cédula o RIF.';

COMMENT ON COLUMN "cpprecom"."refsol" IS 'Referencia de Solicitud de Traslado que originó el Precompromiso';

COMMENT ON COLUMN "cpprecom"."fecreg" IS 'Fecha de registro';

COMMENT ON COLUMN "cpprecom"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "cpprecom"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpsoltrasla
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpsoltrasla" CASCADE;

CREATE TABLE "cpsoltrasla"
(
    "reftra" VARCHAR(8) NOT NULL,
    "fectra" DATE,
    "anotra" VARCHAR(4),
    "pertra" VARCHAR(2) NOT NULL,
    "destra" VARCHAR(1000),
    "desanu" VARCHAR(1000),
    "tottra" NUMERIC(14,2),
    "statra" VARCHAR(1),
    "codart" VARCHAR(3),
    "stacon" VARCHAR(1),
    "stagob" VARCHAR(1),
    "stapre" VARCHAR(1),
    "staniv4" VARCHAR(1),
    "staniv5" VARCHAR(1),
    "staniv6" VARCHAR(1),
    "fecpre" DATE,
    "despre" VARCHAR(250),
    "feccon" DATE,
    "descon" VARCHAR(250),
    "fecgob" DATE,
    "desgob" VARCHAR(250),
    "fecniv4" DATE,
    "desniv4" VARCHAR(250),
    "fecniv5" DATE,
    "desniv5" VARCHAR(250),
    "fecniv6" DATE,
    "desniv6" VARCHAR(250),
    "justificacion" VARCHAR(4000),
    "feccont" DATE,
    "justificacion1" VARCHAR(4000),
    "justificacion2" VARCHAR(4000),
    "justificacion3" VARCHAR(4000),
    "justificacion4" VARCHAR(4000),
    "justificacion5" VARCHAR(4000),
    "justificacion6" VARCHAR(4000),
    "justificacion7" VARCHAR(4000),
    "justificacion8" VARCHAR(4000),
    "justificacion9" VARCHAR(4000),
    "tipo" VARCHAR(100),
    "tipgas" VARCHAR(250),
    "fecanu" DATE,
    "coddirec" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpsoltrasla" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."reftra" IS 'Referencia del Traslado.';

COMMENT ON COLUMN "cpsoltrasla"."fectra" IS 'Referencia del Traslado.';

COMMENT ON COLUMN "cpsoltrasla"."anotra" IS 'Año del traslado.';

COMMENT ON COLUMN "cpsoltrasla"."pertra" IS 'Periodo del traslado.';

COMMENT ON COLUMN "cpsoltrasla"."destra" IS 'Descripción del traslado.';

COMMENT ON COLUMN "cpsoltrasla"."desanu" IS 'Descripción de la anulación del traslado.';

COMMENT ON COLUMN "cpsoltrasla"."tottra" IS 'Total del traslado.';

COMMENT ON COLUMN "cpsoltrasla"."statra" IS 'Status del traslado.';

COMMENT ON COLUMN "cpsoltrasla"."codart" IS 'Corresponde al código del artículo';

COMMENT ON COLUMN "cpsoltrasla"."stacon" IS 'Estatus de Aprobación de contraloría';

COMMENT ON COLUMN "cpsoltrasla"."stagob" IS 'Estatus de Aprobación de Gobernación';

COMMENT ON COLUMN "cpsoltrasla"."stapre" IS 'Estatus de Aprobación de presupuesto';

COMMENT ON COLUMN "cpsoltrasla"."staniv4" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."staniv5" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."staniv6" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."fecpre" IS 'Corresponde al código del artículo';

COMMENT ON COLUMN "cpsoltrasla"."despre" IS 'Descripción si no fue aprobado por parte de presupuesto';

COMMENT ON COLUMN "cpsoltrasla"."feccon" IS 'Fecha de aprobación de contraloría';

COMMENT ON COLUMN "cpsoltrasla"."descon" IS 'Descripción si no fue aprobado por parte de contraloría';

COMMENT ON COLUMN "cpsoltrasla"."fecgob" IS 'Fecha de aprobación de gobernador';

COMMENT ON COLUMN "cpsoltrasla"."desgob" IS 'Descripción si no fue aprobado por parte de Gobernación';

COMMENT ON COLUMN "cpsoltrasla"."fecniv4" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."desniv4" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."fecniv5" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."desniv5" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."fecniv6" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."desniv6" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."justificacion" IS 'Justificación del Traslado';

COMMENT ON COLUMN "cpsoltrasla"."feccont" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."justificacion1" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."justificacion2" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."justificacion3" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."justificacion4" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."justificacion5" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."justificacion6" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."justificacion7" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."justificacion8" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."justificacion9" IS 'null';

COMMENT ON COLUMN "cpsoltrasla"."tipo" IS 'Tipo de Traslado';

COMMENT ON COLUMN "cpsoltrasla"."tipgas" IS 'Tipo de Gasto';

COMMENT ON COLUMN "cpsoltrasla"."fecanu" IS 'Fecha en la que se anuló el Movimiento';

COMMENT ON COLUMN "cpsoltrasla"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "cpsoltrasla"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpartley
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpartley" CASCADE;

CREATE TABLE "cpartley"
(
    "codart" VARCHAR(3) NOT NULL,
    "desart" VARCHAR(250),
    "nomabr" VARCHAR(4),
    "stacon" VARCHAR(1),
    "stagob" VARCHAR(1),
    "stapre" VARCHAR(1),
    "staniv4" VARCHAR(1),
    "staniv5" VARCHAR(1),
    "staniv6" VARCHAR(1),
    "portra" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "u_cpartley" UNIQUE ("codart")
);

COMMENT ON TABLE "cpartley" IS 'Tabla es para niveles de autorización de traslados';

COMMENT ON COLUMN "cpartley"."codart" IS 'Código del Artículo';

COMMENT ON COLUMN "cpartley"."desart" IS 'Descripción del Artículo';

COMMENT ON COLUMN "cpartley"."nomabr" IS 'Nombre Abreviado';

COMMENT ON COLUMN "cpartley"."stacon" IS 'Estatus para Firmar del Contralor';

COMMENT ON COLUMN "cpartley"."stagob" IS 'Estatus para Firmar del Gobernador';

COMMENT ON COLUMN "cpartley"."stapre" IS 'Estatus para Firmar del Director de Presupuesto';

COMMENT ON COLUMN "cpartley"."staniv4" IS 'null';

COMMENT ON COLUMN "cpartley"."staniv5" IS 'null';

COMMENT ON COLUMN "cpartley"."staniv6" IS 'null';

COMMENT ON COLUMN "cpartley"."portra" IS 'null';

COMMENT ON COLUMN "cpartley"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpcausad
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpcausad" CASCADE;

CREATE TABLE "cpcausad"
(
    "refcau" VARCHAR(8) NOT NULL,
    "tipcau" VARCHAR(4) NOT NULL,
    "feccau" DATE NOT NULL,
    "anocau" VARCHAR(4),
    "refcom" VARCHAR(8),
    "tipcom" VARCHAR(4),
    "descau" VARCHAR(1000),
    "desanu" VARCHAR(1000),
    "moncau" NUMERIC(14,2),
    "salpag" NUMERIC(14,2),
    "salaju" NUMERIC(14,2),
    "stacau" VARCHAR(1),
    "fecanu" DATE,
    "cedrif" VARCHAR(15),
    "fecreg" TIMESTAMP DEFAULT 'now',
    "numcom" VARCHAR(8),
    "coddirec" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpcausad" IS 'Tabla que contiene información referente a los movimientos que causan presupuestariamente.';

COMMENT ON COLUMN "cpcausad"."refcau" IS 'Referencia del causado.';

COMMENT ON COLUMN "cpcausad"."tipcau" IS 'Tipo de causado.';

COMMENT ON COLUMN "cpcausad"."feccau" IS 'Fecha del causado.';

COMMENT ON COLUMN "cpcausad"."anocau" IS 'Año del causado.';

COMMENT ON COLUMN "cpcausad"."refcom" IS 'Referencia del comprometido.';

COMMENT ON COLUMN "cpcausad"."tipcom" IS 'Referencia del comprometido.';

COMMENT ON COLUMN "cpcausad"."descau" IS 'Descripción del causado.';

COMMENT ON COLUMN "cpcausad"."desanu" IS 'Descripción del causado.';

COMMENT ON COLUMN "cpcausad"."moncau" IS 'Monto causado.';

COMMENT ON COLUMN "cpcausad"."salpag" IS 'Saldo pagado.';

COMMENT ON COLUMN "cpcausad"."salaju" IS 'Saldo pagado.';

COMMENT ON COLUMN "cpcausad"."stacau" IS 'Estatus del causado.';

COMMENT ON COLUMN "cpcausad"."fecanu" IS 'Cédula ó Rif. del Beneficiario.';

COMMENT ON COLUMN "cpcausad"."cedrif" IS 'Cédula ó Rif. del Beneficiario.';

COMMENT ON COLUMN "cpcausad"."fecreg" IS 'Fecha de registro';

COMMENT ON COLUMN "cpcausad"."numcom" IS 'Número de Comprobante';

COMMENT ON COLUMN "cpcausad"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "cpcausad"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpsolmovadi
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpsolmovadi" CASCADE;

CREATE TABLE "cpsolmovadi"
(
    "refadi" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(50),
    "perpre" VARCHAR(2),
    "monmov" NUMERIC(14,2),
    "stamov" VARCHAR(1),
    "nrores" VARCHAR(20),
    "fecres" DATE,
    "tipo" VARCHAR(1),
    "monto" NUMERIC(14,2),
    "iva" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpsolmovadi" IS 'Tabla que refiere al detalle  solicitud de ejecutar una adición o disminuciones presupuestaria';

COMMENT ON COLUMN "cpsolmovadi"."refadi" IS 'Referencia de la Adición o Disminución.';

COMMENT ON COLUMN "cpsolmovadi"."codpre" IS 'Código Presupuestario';

COMMENT ON COLUMN "cpsolmovadi"."perpre" IS 'Periodo Presupuestario.';

COMMENT ON COLUMN "cpsolmovadi"."monmov" IS 'Monto del Movimiento';

COMMENT ON COLUMN "cpsolmovadi"."stamov" IS 'Estatus del Movimiento';

COMMENT ON COLUMN "cpsolmovadi"."nrores" IS 'null';

COMMENT ON COLUMN "cpsolmovadi"."fecres" IS 'null';

COMMENT ON COLUMN "cpsolmovadi"."tipo" IS 'null';

COMMENT ON COLUMN "cpsolmovadi"."monto" IS 'null';

COMMENT ON COLUMN "cpsolmovadi"."iva" IS 'null';

COMMENT ON COLUMN "cpsolmovadi"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpsolmovtra
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpsolmovtra" CASCADE;

CREATE TABLE "cpsolmovtra"
(
    "reftra" VARCHAR(8) NOT NULL,
    "codori" VARCHAR(50),
    "coddes" VARCHAR(50),
    "monmov" NUMERIC(14,2),
    "stamov" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpsolmovtra" IS 'Tabla que refiere al detalle de la solicitud de traslado presupuestario.';

COMMENT ON COLUMN "cpsolmovtra"."reftra" IS 'Corresponde a la Referencia del Traslado';

COMMENT ON COLUMN "cpsolmovtra"."codori" IS 'Código Presupuestario Origen';

COMMENT ON COLUMN "cpsolmovtra"."coddes" IS 'Código Presupuestario Destino';

COMMENT ON COLUMN "cpsolmovtra"."monmov" IS 'Monto del Movimiento';

COMMENT ON COLUMN "cpsolmovtra"."stamov" IS 'Estatus del Movimiento';

COMMENT ON COLUMN "cpsolmovtra"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpsoladidis
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpsoladidis" CASCADE;

CREATE TABLE "cpsoladidis"
(
    "despre" VARCHAR(250),
    "justificacion" VARCHAR(4000),
    "enunciado" VARCHAR(1000),
    "refadi" VARCHAR(8) NOT NULL,
    "fecadi" DATE,
    "anoadi" VARCHAR(4),
    "desadi" VARCHAR(1000),
    "desanu" VARCHAR(1000),
    "fecanu" DATE,
    "totadi" NUMERIC(14,2),
    "staadi" VARCHAR(1),
    "adidis" VARCHAR(1),
    "codart" VARCHAR(3),
    "stacon" VARCHAR(1),
    "stagob" VARCHAR(1),
    "stapre" VARCHAR(1),
    "staniv4" VARCHAR(1),
    "staniv5" VARCHAR(1),
    "staniv6" VARCHAR(1),
    "fecpre" DATE,
    "feccon" DATE,
    "descon" VARCHAR(250),
    "fecgob" DATE,
    "desgob" VARCHAR(250),
    "fecniv4" DATE,
    "desniv4" VARCHAR(250),
    "fecniv5" DATE,
    "desniv5" VARCHAR(250),
    "fecniv6" DATE,
    "desniv6" VARCHAR(250),
    "numofi" VARCHAR(10),
    "fecofi" DATE,
    "coddirec" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpsoladidis" IS 'Tabla que refiere a la  solicitud de ejecutar una adición o disminuciones presupuestaria';

COMMENT ON COLUMN "cpsoladidis"."despre" IS 'null';

COMMENT ON COLUMN "cpsoladidis"."justificacion" IS 'Justificación del Crédito adicional';

COMMENT ON COLUMN "cpsoladidis"."enunciado" IS 'Justificación del Crédito adicional';

COMMENT ON COLUMN "cpsoladidis"."refadi" IS 'Referencia de la adición o disminución.';

COMMENT ON COLUMN "cpsoladidis"."fecadi" IS 'Fecha en que se realiza la adición o disminución.';

COMMENT ON COLUMN "cpsoladidis"."anoadi" IS 'Año de adición.';

COMMENT ON COLUMN "cpsoladidis"."desadi" IS 'Descripción de la adición o disminución.';

COMMENT ON COLUMN "cpsoladidis"."desanu" IS 'Descripción de la anulación, en caso de que el movimiento esté anulado.';

COMMENT ON COLUMN "cpsoladidis"."fecanu" IS 'Fecha en la cual se anulo el movimiento.';

COMMENT ON COLUMN "cpsoladidis"."totadi" IS 'Monto total de las adiciones o disminuciones.';

COMMENT ON COLUMN "cpsoladidis"."staadi" IS 'Estatus de las adiciones.';

COMMENT ON COLUMN "cpsoladidis"."adidis" IS 'Indica si el movimiento es de adición o disminución.';

COMMENT ON COLUMN "cpsoladidis"."codart" IS 'Corresponde al código del artículo';

COMMENT ON COLUMN "cpsoladidis"."stacon" IS 'Estatus de Aprobación de contraloría';

COMMENT ON COLUMN "cpsoladidis"."stagob" IS 'Estatus de Aprobación de Gobernación';

COMMENT ON COLUMN "cpsoladidis"."stapre" IS 'Estatus de Aprobación de presupuesto';

COMMENT ON COLUMN "cpsoladidis"."staniv4" IS 'null';

COMMENT ON COLUMN "cpsoladidis"."staniv5" IS 'null';

COMMENT ON COLUMN "cpsoladidis"."staniv6" IS 'null';

COMMENT ON COLUMN "cpsoladidis"."fecpre" IS 'Fecha de aprobación de presupuesto';

COMMENT ON COLUMN "cpsoladidis"."feccon" IS 'Fecha de aprobación de contraloría';

COMMENT ON COLUMN "cpsoladidis"."descon" IS 'Descripción si no fue aprobado por parte de contraloría';

COMMENT ON COLUMN "cpsoladidis"."fecgob" IS 'Fecha de aprobación de gobernador';

COMMENT ON COLUMN "cpsoladidis"."desgob" IS 'Fecha de aprobación de gobernador';

COMMENT ON COLUMN "cpsoladidis"."fecniv4" IS 'null';

COMMENT ON COLUMN "cpsoladidis"."desniv4" IS 'null';

COMMENT ON COLUMN "cpsoladidis"."fecniv5" IS 'null';

COMMENT ON COLUMN "cpsoladidis"."desniv5" IS 'null';

COMMENT ON COLUMN "cpsoladidis"."fecniv6" IS 'null';

COMMENT ON COLUMN "cpsoladidis"."desniv6" IS 'null';

COMMENT ON COLUMN "cpsoladidis"."numofi" IS 'null';

COMMENT ON COLUMN "cpsoladidis"."fecofi" IS 'null';

COMMENT ON COLUMN "cpsoladidis"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "cpsoladidis"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpmovfuefin
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpmovfuefin" CASCADE;

CREATE TABLE "cpmovfuefin"
(
    "correl" VARCHAR(10),
    "refmov" VARCHAR(8),
    "tipmov" VARCHAR(20),
    "monmov" NUMERIC(20,2),
    "fecmov" DATE,
    "codpre" VARCHAR(50),
    "stamov" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpmovfuefin" IS 'null';

COMMENT ON COLUMN "cpmovfuefin"."correl" IS 'null';

COMMENT ON COLUMN "cpmovfuefin"."refmov" IS 'null';

COMMENT ON COLUMN "cpmovfuefin"."tipmov" IS 'null';

COMMENT ON COLUMN "cpmovfuefin"."monmov" IS 'null';

COMMENT ON COLUMN "cpmovfuefin"."fecmov" IS 'null';

COMMENT ON COLUMN "cpmovfuefin"."codpre" IS 'null';

COMMENT ON COLUMN "cpmovfuefin"."stamov" IS 'null';

COMMENT ON COLUMN "cpmovfuefin"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpniveles
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpniveles" CASCADE;

CREATE TABLE "cpniveles"
(
    "catpar" VARCHAR(1) NOT NULL,
    "consec" NUMERIC(2) NOT NULL,
    "nomabr" VARCHAR(10) NOT NULL,
    "nomext" VARCHAR(50) NOT NULL,
    "lonniv" NUMERIC(2),
    "staniv" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpniveles" IS 'Tabla que contiene información referente a el detalle del código presupuestario.';

COMMENT ON COLUMN "cpniveles"."catpar" IS 'Indica si la Ruptura del Código es de Tipo Categoría o Partida.';

COMMENT ON COLUMN "cpniveles"."consec" IS 'Numero Consecutivo del Nivel.';

COMMENT ON COLUMN "cpniveles"."nomabr" IS 'Nombre Abreviado del Nivel Presupuestario';

COMMENT ON COLUMN "cpniveles"."nomext" IS 'Nombre Extendido del Nivel Presupuestario';

COMMENT ON COLUMN "cpniveles"."lonniv" IS 'Longitud del Nivel Presupuestario';

COMMENT ON COLUMN "cpniveles"."staniv" IS 'Estatus del Nivel Presupuestario';

COMMENT ON COLUMN "cpniveles"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cppereje
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cppereje" CASCADE;

CREATE TABLE "cppereje"
(
    "fecini" DATE NOT NULL,
    "feccie" DATE NOT NULL,
    "pereje" VARCHAR(2) NOT NULL,
    "fecdes" DATE NOT NULL,
    "fechas" DATE NOT NULL,
    "estper" VARCHAR(1),
    "cerrado" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cppereje" IS 'Tabla que contiene información referente al periodo de ejecución.';

COMMENT ON COLUMN "cppereje"."fecini" IS 'Fecha de Inicio del Ejercicio.';

COMMENT ON COLUMN "cppereje"."feccie" IS 'Fecha del Cierre del Ejercicio.';

COMMENT ON COLUMN "cppereje"."pereje" IS 'Fecha del Cierre del Ejercicio.';

COMMENT ON COLUMN "cppereje"."fecdes" IS 'Fecha de Inicio del Periodo de Ejecución.';

COMMENT ON COLUMN "cppereje"."fechas" IS 'Fecha de Finalización del Periodo de Ejecución.';

COMMENT ON COLUMN "cppereje"."estper" IS 'Estado del Periodo';

COMMENT ON COLUMN "cppereje"."cerrado" IS 'indica Si esta cerrado el periodo';

COMMENT ON COLUMN "cppereje"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cptrasla
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cptrasla" CASCADE;

CREATE TABLE "cptrasla"
(
    "reftra" VARCHAR(8) NOT NULL,
    "fectra" DATE NOT NULL,
    "anotra" VARCHAR(4),
    "pertra" VARCHAR(2) NOT NULL,
    "destra" VARCHAR(1000),
    "desanu" VARCHAR(1000),
    "tottra" NUMERIC(14,2),
    "statra" VARCHAR(1),
    "fecanu" DATE,
    "nrodec" VARCHAR(8),
    "loguse" VARCHAR(50),
    "fecreg" TIMESTAMP DEFAULT 'now',
    "coddirec" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cptrasla" IS 'Tabla que contiene información referente a los traslados.';

COMMENT ON COLUMN "cptrasla"."reftra" IS 'Referencia del traslado.';

COMMENT ON COLUMN "cptrasla"."fectra" IS 'Fecha del traslado.';

COMMENT ON COLUMN "cptrasla"."anotra" IS 'Fecha del traslado.';

COMMENT ON COLUMN "cptrasla"."pertra" IS 'Periodo del traslado.';

COMMENT ON COLUMN "cptrasla"."destra" IS 'Descripción del traslado.';

COMMENT ON COLUMN "cptrasla"."desanu" IS 'Descripción de la anulación del traslado.';

COMMENT ON COLUMN "cptrasla"."tottra" IS 'Total del traslado.';

COMMENT ON COLUMN "cptrasla"."statra" IS 'Status del traslado.';

COMMENT ON COLUMN "cptrasla"."fecanu" IS 'Fecha en la cual se anulo el movimiento.';

COMMENT ON COLUMN "cptrasla"."nrodec" IS 'Número de Decreto';

COMMENT ON COLUMN "cptrasla"."loguse" IS 'null';

COMMENT ON COLUMN "cptrasla"."fecreg" IS 'Fecha de registro';

COMMENT ON COLUMN "cptrasla"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "cptrasla"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpimpapa
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpimpapa" CASCADE;

CREATE TABLE "cpimpapa"
(
    "refapa" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(32) NOT NULL,
    "monimp" NUMERIC(14,2),
    "moncom" NUMERIC(14,2),
    "moncau" NUMERIC(14,2),
    "monpag" NUMERIC(14,2),
    "monaju" NUMERIC(14,2),
    "staimp" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpimpapa" IS 'null';

COMMENT ON COLUMN "cpimpapa"."refapa" IS 'null';

COMMENT ON COLUMN "cpimpapa"."codpre" IS 'null';

COMMENT ON COLUMN "cpimpapa"."monimp" IS 'null';

COMMENT ON COLUMN "cpimpapa"."moncom" IS 'null';

COMMENT ON COLUMN "cpimpapa"."moncau" IS 'null';

COMMENT ON COLUMN "cpimpapa"."monpag" IS 'null';

COMMENT ON COLUMN "cpimpapa"."monaju" IS 'null';

COMMENT ON COLUMN "cpimpapa"."staimp" IS 'null';

COMMENT ON COLUMN "cpimpapa"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpimprel
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpimprel" CASCADE;

CREATE TABLE "cpimprel"
(
    "refrel" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(32) NOT NULL,
    "monrel" NUMERIC(14,2),
    "monaju" NUMERIC(14,2),
    "starel" VARCHAR(1),
    "refere" VARCHAR(8),
    "refprc" VARCHAR(8),
    "refcom" VARCHAR(8),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpimprel" IS 'null';

COMMENT ON COLUMN "cpimprel"."refrel" IS 'null';

COMMENT ON COLUMN "cpimprel"."codpre" IS 'null';

COMMENT ON COLUMN "cpimprel"."monrel" IS 'null';

COMMENT ON COLUMN "cpimprel"."monaju" IS 'null';

COMMENT ON COLUMN "cpimprel"."starel" IS 'null';

COMMENT ON COLUMN "cpimprel"."refere" IS 'null';

COMMENT ON COLUMN "cpimprel"."refprc" IS 'null';

COMMENT ON COLUMN "cpimprel"."refcom" IS 'null';

COMMENT ON COLUMN "cpimprel"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpmovtra
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpmovtra" CASCADE;

CREATE TABLE "cpmovtra"
(
    "reftra" VARCHAR(8) NOT NULL,
    "codori" VARCHAR(50) NOT NULL,
    "coddes" VARCHAR(50) NOT NULL,
    "monmov" NUMERIC(14,2),
    "stamov" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpmovtra" IS 'Tabla que contiene información referente a los movimientos asociados al traslado.';

COMMENT ON COLUMN "cpmovtra"."reftra" IS 'Numero de Referencia del  traslado.';

COMMENT ON COLUMN "cpmovtra"."codori" IS 'Código Presupuestario Origen.';

COMMENT ON COLUMN "cpmovtra"."coddes" IS 'Código Presupuestario Destino.';

COMMENT ON COLUMN "cpmovtra"."monmov" IS 'Monto del Movimiento asociado al Traslado.';

COMMENT ON COLUMN "cpmovtra"."stamov" IS 'Monto del Movimiento asociado al Traslado.';

COMMENT ON COLUMN "cpmovtra"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpmovadifin
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpmovadifin" CASCADE;

CREATE TABLE "cpmovadifin"
(
    "refadi" VARCHAR(8) NOT NULL,
    "codfin" VARCHAR(4),
    "monfin" NUMERIC(20,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpmovadifin" IS 'null';

COMMENT ON COLUMN "cpmovadifin"."refadi" IS 'null';

COMMENT ON COLUMN "cpmovadifin"."codfin" IS 'null';

COMMENT ON COLUMN "cpmovadifin"."monfin" IS 'null';

COMMENT ON COLUMN "cpmovadifin"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cprelapa
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cprelapa" CASCADE;

CREATE TABLE "cprelapa"
(
    "refrel" VARCHAR(8) NOT NULL,
    "tiprel" VARCHAR(4) NOT NULL,
    "fecrel" DATE NOT NULL,
    "refapa" VARCHAR(8),
    "desrel" VARCHAR(250),
    "desanu" VARCHAR(100),
    "monrel" NUMERIC(14,2),
    "salaju" NUMERIC(14,2),
    "starel" VARCHAR(1),
    "fecanu" DATE,
    "cedrif" VARCHAR(15),
    "numcom" VARCHAR(8),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cprelapa" IS 'null';

COMMENT ON COLUMN "cprelapa"."refrel" IS 'null';

COMMENT ON COLUMN "cprelapa"."tiprel" IS 'null';

COMMENT ON COLUMN "cprelapa"."fecrel" IS 'null';

COMMENT ON COLUMN "cprelapa"."refapa" IS 'null';

COMMENT ON COLUMN "cprelapa"."desrel" IS 'null';

COMMENT ON COLUMN "cprelapa"."desanu" IS 'null';

COMMENT ON COLUMN "cprelapa"."monrel" IS 'null';

COMMENT ON COLUMN "cprelapa"."salaju" IS 'null';

COMMENT ON COLUMN "cprelapa"."starel" IS 'null';

COMMENT ON COLUMN "cprelapa"."fecanu" IS 'null';

COMMENT ON COLUMN "cprelapa"."cedrif" IS 'null';

COMMENT ON COLUMN "cprelapa"."numcom" IS 'null';

COMMENT ON COLUMN "cprelapa"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpdefapr
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpdefapr" CASCADE;

CREATE TABLE "cpdefapr"
(
    "stacon" VARCHAR(50),
    "abrstacon" VARCHAR(5),
    "stagob" VARCHAR(50),
    "abrstagob" VARCHAR(5),
    "stapre" VARCHAR(50),
    "abrstapre" VARCHAR(5),
    "staniv4" VARCHAR(50),
    "abrstaniv4" VARCHAR(5),
    "staniv5" VARCHAR(50),
    "abrstaniv5" VARCHAR(5),
    "staniv6" VARCHAR(50),
    "abrstaniv6" VARCHAR(5),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpdefapr" IS 'null';

COMMENT ON COLUMN "cpdefapr"."stacon" IS 'null';

COMMENT ON COLUMN "cpdefapr"."abrstacon" IS 'null';

COMMENT ON COLUMN "cpdefapr"."stagob" IS 'null';

COMMENT ON COLUMN "cpdefapr"."abrstagob" IS 'null';

COMMENT ON COLUMN "cpdefapr"."stapre" IS 'null';

COMMENT ON COLUMN "cpdefapr"."abrstapre" IS 'null';

COMMENT ON COLUMN "cpdefapr"."staniv4" IS 'null';

COMMENT ON COLUMN "cpdefapr"."abrstaniv4" IS 'null';

COMMENT ON COLUMN "cpdefapr"."staniv5" IS 'null';

COMMENT ON COLUMN "cpdefapr"."abrstaniv5" IS 'null';

COMMENT ON COLUMN "cpdefapr"."staniv6" IS 'null';

COMMENT ON COLUMN "cpdefapr"."abrstaniv6" IS 'null';

COMMENT ON COLUMN "cpdefapr"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- hisconb
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "hisconb" CASCADE;

CREATE TABLE "hisconb"
(
    "codcta" VARCHAR(18) NOT NULL,
    "descta" VARCHAR(40) NOT NULL,
    "salant" NUMERIC(12,2),
    "debcre" VARCHAR(1) NOT NULL,
    "cargab" VARCHAR(1) NOT NULL,
    "fecini" DATE NOT NULL,
    "feccie" DATE NOT NULL,
    "salprgper" NUMERIC(14,2),
    "salacuper" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "hisconb" IS 'null';

COMMENT ON COLUMN "hisconb"."codcta" IS 'null';

COMMENT ON COLUMN "hisconb"."descta" IS 'null';

COMMENT ON COLUMN "hisconb"."salant" IS 'null';

COMMENT ON COLUMN "hisconb"."debcre" IS 'null';

COMMENT ON COLUMN "hisconb"."cargab" IS 'null';

COMMENT ON COLUMN "hisconb"."fecini" IS 'null';

COMMENT ON COLUMN "hisconb"."feccie" IS 'null';

COMMENT ON COLUMN "hisconb"."salprgper" IS 'null';

COMMENT ON COLUMN "hisconb"."salacuper" IS 'null';

COMMENT ON COLUMN "hisconb"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- hisconb1
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "hisconb1" CASCADE;

CREATE TABLE "hisconb1"
(
    "codcta" VARCHAR(18) NOT NULL,
    "fecini" DATE NOT NULL,
    "feccie" DATE NOT NULL,
    "pereje" VARCHAR(2) NOT NULL,
    "totdeb" NUMERIC(14,2),
    "totcre" NUMERIC(14,2),
    "salact" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "hisconb1" IS 'null';

COMMENT ON COLUMN "hisconb1"."codcta" IS 'null';

COMMENT ON COLUMN "hisconb1"."fecini" IS 'null';

COMMENT ON COLUMN "hisconb1"."feccie" IS 'null';

COMMENT ON COLUMN "hisconb1"."pereje" IS 'null';

COMMENT ON COLUMN "hisconb1"."totdeb" IS 'null';

COMMENT ON COLUMN "hisconb1"."totcre" IS 'null';

COMMENT ON COLUMN "hisconb1"."salact" IS 'null';

COMMENT ON COLUMN "hisconb1"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- hisconc
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "hisconc" CASCADE;

CREATE TABLE "hisconc"
(
    "numcom" VARCHAR(8) NOT NULL,
    "feccom" DATE NOT NULL,
    "descom" VARCHAR(50),
    "moncom" NUMERIC(14,2),
    "stacom" VARCHAR(1) NOT NULL,
    "tipcom" VARCHAR(3),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "hisconc" IS 'null';

COMMENT ON COLUMN "hisconc"."numcom" IS 'null';

COMMENT ON COLUMN "hisconc"."feccom" IS 'null';

COMMENT ON COLUMN "hisconc"."descom" IS 'null';

COMMENT ON COLUMN "hisconc"."moncom" IS 'null';

COMMENT ON COLUMN "hisconc"."stacom" IS 'null';

COMMENT ON COLUMN "hisconc"."tipcom" IS 'null';

COMMENT ON COLUMN "hisconc"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- hisconc1
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "hisconc1" CASCADE;

CREATE TABLE "hisconc1"
(
    "numcom" VARCHAR(8) NOT NULL,
    "feccom" DATE NOT NULL,
    "debcre" VARCHAR(1) NOT NULL,
    "codcta" VARCHAR(18) NOT NULL,
    "numasi" NUMERIC(3,0) NOT NULL,
    "refasi" VARCHAR(8),
    "desasi" VARCHAR(30),
    "monasi" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "hisconc1" IS 'null';

COMMENT ON COLUMN "hisconc1"."numcom" IS 'null';

COMMENT ON COLUMN "hisconc1"."feccom" IS 'null';

COMMENT ON COLUMN "hisconc1"."debcre" IS 'null';

COMMENT ON COLUMN "hisconc1"."codcta" IS 'null';

COMMENT ON COLUMN "hisconc1"."numasi" IS 'null';

COMMENT ON COLUMN "hisconc1"."refasi" IS 'null';

COMMENT ON COLUMN "hisconc1"."desasi" IS 'null';

COMMENT ON COLUMN "hisconc1"."monasi" IS 'null';

COMMENT ON COLUMN "hisconc1"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpcontra
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpcontra" CASCADE;

CREATE TABLE "cpcontra"
(
    "codparma" VARCHAR(16) NOT NULL,
    "codparde" VARCHAR(16) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpcontra" IS 'null';

COMMENT ON COLUMN "cpcontra"."codparma" IS 'null';

COMMENT ON COLUMN "cpcontra"."codparde" IS 'null';

COMMENT ON COLUMN "cpcontra"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpcomext
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpcomext" CASCADE;

CREATE TABLE "cpcomext"
(
    "refcomext" VARCHAR(8) NOT NULL,
    "tipcom" VARCHAR(4) NOT NULL,
    "feccom" DATE NOT NULL,
    "anocom" VARCHAR(4),
    "refcom" VARCHAR(8),
    "descom" VARCHAR(1000),
    "desanu" VARCHAR(1000),
    "moncom" NUMERIC(14,2),
    "stacom" VARCHAR(1),
    "fecanu" DATE,
    "cedrif" VARCHAR(15),
    "loguse" VARCHAR(50),
    "fecreg" TIMESTAMP DEFAULT 'now',
    "codmon" VARCHAR(3) NOT NULL,
    "valmon" NUMERIC(14,6),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpcomext" IS 'Tabla que contiene información referente a las operaciones que comprometen la disponibilidad de las partidas presupuestarias.';

COMMENT ON COLUMN "cpcomext"."refcomext" IS 'Referencia del Compromiso.';

COMMENT ON COLUMN "cpcomext"."tipcom" IS 'Tipo de Compromiso.';

COMMENT ON COLUMN "cpcomext"."feccom" IS 'Fecha de Emisión del Compromiso.';

COMMENT ON COLUMN "cpcomext"."anocom" IS 'Año del Compromiso.';

COMMENT ON COLUMN "cpcomext"."refcom" IS 'Referencia del Compromiso al cual hace Referencia.';

COMMENT ON COLUMN "cpcomext"."descom" IS 'Descripción del Compromiso.';

COMMENT ON COLUMN "cpcomext"."desanu" IS 'Descripción de Anulado.';

COMMENT ON COLUMN "cpcomext"."moncom" IS 'Monto del Compromiso.';

COMMENT ON COLUMN "cpcomext"."stacom" IS 'Estatus del Compromiso.';

COMMENT ON COLUMN "cpcomext"."fecanu" IS 'Fecha de Anulación.';

COMMENT ON COLUMN "cpcomext"."cedrif" IS 'Cédula ó Rif. del Beneficiario.';

COMMENT ON COLUMN "cpcomext"."loguse" IS 'null';

COMMENT ON COLUMN "cpcomext"."fecreg" IS 'Fecha de registro';

COMMENT ON COLUMN "cpcomext"."valmon" IS 'null';

COMMENT ON COLUMN "cpcomext"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpimpcomext
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpimpcomext" CASCADE;

CREATE TABLE "cpimpcomext"
(
    "refcomext" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(50) NOT NULL,
    "monimp" NUMERIC(14,2),
    "moncau" NUMERIC(14,2),
    "monpag" NUMERIC(14,2),
    "monaju" NUMERIC(14,2),
    "staimp" VARCHAR(1),
    "refere" VARCHAR(8),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpimpcomext" IS 'Tabla que contiene información referente a las imputaciones de compromiso en el extranjero';

COMMENT ON COLUMN "cpimpcomext"."refcomext" IS 'Numero de Referencia del Compromiso.';

COMMENT ON COLUMN "cpimpcomext"."codpre" IS 'Código Presupuestario.';

COMMENT ON COLUMN "cpimpcomext"."monimp" IS 'Monto de la Imputación.';

COMMENT ON COLUMN "cpimpcomext"."moncau" IS 'Monto del Causado.';

COMMENT ON COLUMN "cpimpcomext"."monpag" IS 'Monto del Causado.';

COMMENT ON COLUMN "cpimpcomext"."monaju" IS 'Monto Ajustado.';

COMMENT ON COLUMN "cpimpcomext"."staimp" IS 'Estatus de la Imputación.';

COMMENT ON COLUMN "cpimpcomext"."refere" IS 'Movimiento que hace referencia al Compromiso.';

COMMENT ON COLUMN "cpimpcomext"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpevepre
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpevepre" CASCADE;

CREATE TABLE "cpevepre"
(
    "codeve" VARCHAR(6) NOT NULL,
    "deseve" VARCHAR(1000) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpevepre" IS 'null';

COMMENT ON COLUMN "cpevepre"."codeve" IS 'Codigo del Evento';

COMMENT ON COLUMN "cpevepre"."deseve" IS 'Descripción del Evento';

COMMENT ON COLUMN "cpevepre"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpdiseve
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpdiseve" CASCADE;

CREATE TABLE "cpdiseve"
(
    "refdoc" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(50) NOT NULL,
    "codeve" VARCHAR(6) NOT NULL,
    "moneve" NUMERIC(14,2),
    "tipdoc" VARCHAR(4),
    "tipmov" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cpdiseve"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpptocta
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpptocta" CASCADE;

CREATE TABLE "cpptocta"
(
    "numpta" VARCHAR(20) NOT NULL,
    "fecpta" DATE NOT NULL,
    "codubiori" VARCHAR(30) NOT NULL,
    "codubides" VARCHAR(30) NOT NULL,
    "asunto" VARCHAR(2000),
    "motivo" VARCHAR(10000),
    "reccon" VARCHAR(2000),
    "loguse" VARCHAR(50),
    "aprpto" VARCHAR(1),
    "usuapr" VARCHAR(50),
    "fecapr" DATE,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpptocta" IS 'Tabla para registrar los Puntos de Cuenta';

COMMENT ON COLUMN "cpptocta"."numpta" IS 'Número de Punto de Cuenta';

COMMENT ON COLUMN "cpptocta"."fecpta" IS 'Fecha de Punto de Cuenta';

COMMENT ON COLUMN "cpptocta"."codubiori" IS 'Unidad de Origen';

COMMENT ON COLUMN "cpptocta"."codubides" IS 'Unidad de Destino';

COMMENT ON COLUMN "cpptocta"."asunto" IS 'Asunto';

COMMENT ON COLUMN "cpptocta"."motivo" IS 'Exposición de Motivos';

COMMENT ON COLUMN "cpptocta"."reccon" IS 'Recomendación/Conclusiones';

COMMENT ON COLUMN "cpptocta"."loguse" IS 'Usuario que genero el punto de cuenta.';

COMMENT ON COLUMN "cpptocta"."aprpto" IS 'Estatus de Aprobación del Punto de Cuenta';

COMMENT ON COLUMN "cpptocta"."usuapr" IS 'usuario que realizo la aprobación';

COMMENT ON COLUMN "cpptocta"."fecapr" IS 'fecha en que realizo la aprobación';

COMMENT ON COLUMN "cpptocta"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpdetptocta
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpdetptocta" CASCADE;

CREATE TABLE "cpdetptocta"
(
    "numpta" VARCHAR(20) NOT NULL,
    "codpre" VARCHAR(50) NOT NULL,
    "moncod" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpdetptocta" IS 'Tabla para registrar el detalle de los Puntos de Cuenta';

COMMENT ON COLUMN "cpdetptocta"."numpta" IS 'Número de Punto de Cuenta';

COMMENT ON COLUMN "cpdetptocta"."codpre" IS 'Código Presupuestario.';

COMMENT ON COLUMN "cpdetptocta"."moncod" IS 'Monto Código Presupuestario.';

COMMENT ON COLUMN "cpdetptocta"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cpdefparpre
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cpdefparpre" CASCADE;

CREATE TABLE "cpdefparpre"
(
    "codparpre" VARCHAR(32) NOT NULL,
    "nomparpre" VARCHAR(500) NOT NULL,
    "stagen" VARCHAR(100),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cpdefparpre" IS 'Tabla que Graba el registro de las Partidas Presupuestarias';

COMMENT ON COLUMN "cpdefparpre"."codparpre" IS 'Código de la Partida Presupuestaria';

COMMENT ON COLUMN "cpdefparpre"."nomparpre" IS 'Nombre de la Partida Presupuestaria';

COMMENT ON COLUMN "cpdefparpre"."stagen" IS 'Status que indica si fue generada en Cpdeftit';

COMMENT ON COLUMN "cpdefparpre"."id" IS 'Identificador Único del registro';

ALTER TABLE "cpajuste" ADD CONSTRAINT "cpajuste_FK_1"
    FOREIGN KEY ("tipaju")
    REFERENCES "cpdocaju" ("tipaju");

ALTER TABLE "cpasiini" ADD CONSTRAINT "cpasiini_FK_1"
    FOREIGN KEY ("codpre")
    REFERENCES "cpdeftit" ("codpre");

ALTER TABLE "cpcompro" ADD CONSTRAINT "cpcompro_FK_1"
    FOREIGN KEY ("tipcom")
    REFERENCES "cpdoccom" ("tipcom");

ALTER TABLE "cpcompro" ADD CONSTRAINT "cpcompro_FK_2"
    FOREIGN KEY ("refprc")
    REFERENCES "cpprecom" ("refprc");

ALTER TABLE "cpimpcau" ADD CONSTRAINT "cpimpcau_FK_1"
    FOREIGN KEY ("refcau")
    REFERENCES "cpcausad" ("refcau");

ALTER TABLE "cpimpcom" ADD CONSTRAINT "cpimpcom_FK_1"
    FOREIGN KEY ("refcom")
    REFERENCES "cpcompro" ("refcom");

ALTER TABLE "cpimpcom" ADD CONSTRAINT "cpimpcom_FK_2"
    FOREIGN KEY ("codpre")
    REFERENCES "cpdeftit" ("codpre");

ALTER TABLE "cpimppag" ADD CONSTRAINT "cpimppag_FK_1"
    FOREIGN KEY ("refpag")
    REFERENCES "cppagos" ("refpag");

ALTER TABLE "cpimpprc" ADD CONSTRAINT "cpimpprc_FK_1"
    FOREIGN KEY ("refprc")
    REFERENCES "cpprecom" ("refprc");

ALTER TABLE "cpimpprc" ADD CONSTRAINT "cpimpprc_FK_2"
    FOREIGN KEY ("codpre")
    REFERENCES "cpdeftit" ("codpre");

ALTER TABLE "cpmovadi" ADD CONSTRAINT "cpmovadi_FK_1"
    FOREIGN KEY ("refadi")
    REFERENCES "cpadidis" ("refadi");

ALTER TABLE "cpmovadi" ADD CONSTRAINT "cpmovadi_FK_2"
    FOREIGN KEY ("codpre")
    REFERENCES "cpdeftit" ("codpre");

ALTER TABLE "cpmovaju" ADD CONSTRAINT "cpmovaju_FK_1"
    FOREIGN KEY ("refaju")
    REFERENCES "cpajuste" ("refaju");

ALTER TABLE "cpmovaju" ADD CONSTRAINT "cpmovaju_FK_2"
    FOREIGN KEY ("codpre")
    REFERENCES "cpdeftit" ("codpre");

ALTER TABLE "cppagos" ADD CONSTRAINT "cppagos_FK_1"
    FOREIGN KEY ("tippag")
    REFERENCES "cpdocpag" ("tippag");

ALTER TABLE "cpprecom" ADD CONSTRAINT "cpprecom_FK_1"
    FOREIGN KEY ("tipprc")
    REFERENCES "cpdocprc" ("tipprc");

ALTER TABLE "cpsoltrasla" ADD CONSTRAINT "cpsoltrasla_FK_1"
    FOREIGN KEY ("codart")
    REFERENCES "cpartley" ("codart");

ALTER TABLE "cpcausad" ADD CONSTRAINT "cpcausad_FK_1"
    FOREIGN KEY ("tipcau")
    REFERENCES "cpdoccau" ("tipcau");

ALTER TABLE "cpcausad" ADD CONSTRAINT "cpcausad_FK_2"
    FOREIGN KEY ("refcom")
    REFERENCES "cpcompro" ("refcom");

ALTER TABLE "cpsolmovadi" ADD CONSTRAINT "cpsolmovadi_FK_1"
    FOREIGN KEY ("refadi")
    REFERENCES "cpsoladidis" ("refadi");

ALTER TABLE "cpsolmovtra" ADD CONSTRAINT "cpsolmovtra_FK_1"
    FOREIGN KEY ("reftra")
    REFERENCES "cpsoltrasla" ("reftra");

ALTER TABLE "cpsoladidis" ADD CONSTRAINT "cpsoladidis_FK_1"
    FOREIGN KEY ("codart")
    REFERENCES "cpartley" ("codart");

ALTER TABLE "cpmovtra" ADD CONSTRAINT "cpmovtra_FK_1"
    FOREIGN KEY ("reftra")
    REFERENCES "cptrasla" ("reftra");

ALTER TABLE "cpmovtra" ADD CONSTRAINT "cpmovtra_FK_2"
    FOREIGN KEY ("codori")
    REFERENCES "cpdeftit" ("codpre");

ALTER TABLE "cpmovtra" ADD CONSTRAINT "cpmovtra_FK_3"
    FOREIGN KEY ("coddes")
    REFERENCES "cpdeftit" ("codpre");

ALTER TABLE "cpcomext" ADD CONSTRAINT "cpcomext_FK_1"
    FOREIGN KEY ("tipcom")
    REFERENCES "cpdoccom" ("tipcom");

ALTER TABLE "cpimpcomext" ADD CONSTRAINT "cpimpcomext_FK_1"
    FOREIGN KEY ("refcomext")
    REFERENCES "cpcomext" ("refcomext");

ALTER TABLE "cpimpcomext" ADD CONSTRAINT "cpimpcomext_FK_2"
    FOREIGN KEY ("codpre")
    REFERENCES "cpdeftit" ("codpre");

ALTER TABLE "cpdetptocta" ADD CONSTRAINT "cpdetptocta_FK_1"
    FOREIGN KEY ("codpre")
    REFERENCES "cpdeftit" ("codpre");
