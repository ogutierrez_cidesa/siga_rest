<?php
namespace Controllers\Licitaciones;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Controllers\Licitaciones;
use \Utils\H;
use \Business\Licitaciones\Licitacion;
use \LiprebasQuery;

class PresupuestoBaseControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];;

        // Create Presupuesto Base
        $controllers->post('/presupuesto_base', 'Controllers\Licitaciones\PresupuestoBaseControllerProvider::presupuesto_base_create');
        $controllers->get('/presupuesto_base', 'Controllers\Licitaciones\PresupuestoBaseControllerProvider::presupuesto_base_find');
        $controllers->get('/presupuesto_base/{numpre}', 'Controllers\Licitaciones\PresupuestoBaseControllerProvider::presupuesto_base_find');
        $controllers->delete('/presupuesto_base/{numpre}', 'Controllers\Licitaciones\PresupuestoBaseControllerProvider::presupuesto_base_delete');

        return $controllers;
    }

    public function presupuesto_base_delete(Application $app, Request $request){
      $numpre = $request->get('numpre');
      if($numpre!=null){
        $liprebas = LiprebasQuery::create()->findOneByNumpre($numpre);
        if($liprebas==null){
          $resp = array("response" => H::obtenerMensajeError("0", true));
          return new Response(json_encode($resp), Response::HTTP_NOT_FOUND);
        }else{
          Licitacion::EliminarPrebas($liprebas);
          $liprebas->delete();
          $resp = array("response" => array('cod' => '-1'), 'data' => array($liprebas));
          return new Response(json_encode($resp), Response::HTTP_OK);          
        }
      }else{
        $resp = array("response" => H::obtenerMensajeError("0", true));
        return new Response(json_encode($resp), Response::HTTP_NOT_FOUND);
      } 
    }

    public function presupuesto_base_find(Application $app, Request $request){
      $numpre = $request->get('numpre');
      if($numpre==null){
        $liprebas = LiprebasQuery::create()->joinWith('Liprebas.Liprebasdet')->find()->toArray();
      }else{
        $liprebas = LiprebasQuery::create()->joinWith('Liprebas.Liprebasdet')->findOneByNumpre($numpre);
        if($liprebas) $liprebas = $liprebas->toArray();
      }
      $resp = array("response" => array('cod' => '-1'), 'data' => $liprebas);
      return new Response(json_encode($resp), Response::HTTP_OK);
    }

    public function presupuesto_base_create(Application $app, Request $request) {

      $liprebas = $request->get('liprebas');

      $liprebasdet = $request->get('liprebasdet');

      if(Licitacion::ValidarPrebasDesdeArray($liprebas, $liprebasdet)){

        $liprebas_obj = new \Liprebas();
        $liprebas_obj->fromArray($liprebas, \BasePeer::TYPE_FIELDNAME);

        $liprebasdet_array = array();

        foreach ($liprebasdet as $v) {
          $liprebasdet_obj = new \Liprebasdet();
          $liprebasdet_obj->fromArray($v, \BasePeer::TYPE_FIELDNAME);
          $liprebasdet_array[] = $liprebasdet_obj;
        }

        $error = Licitacion::ValidarPrebas($liprebas_obj, array($liprebasdet_array), array(), "A");
        if($error == '-1'){
          try{
            $error = Licitacion::SalvarPrebas($liprebas_obj, array($liprebasdet_array, array()), array());
            if($error == '-1'){
              if($liprebas_obj->save()) $error == '-1';
              else $error == '0';
            }
            if($error==-1){
              $resp = array("response" => array('cod' => '-1', 'msj' => "Presupuesto Base Creado"), "data" => array("numpre" => $liprebas_obj->getNumpre()));
            }else{
              $resp = array("response" => H::obtenerMensajeError($error, true), "data" => array($liprebas_obj->toArray()));
            }
            
          }catch (Exception $e){
            $resp = array("response" => H::obtenerMensajeError("0", true), "data" => array("Exception"));
          }
        }else{
          $resp = array("response" => H::obtenerMensajeError($error, true), "data" => array($liprebas_obj->toArray()));
        }
      }else $resp = array("response" => H::obtenerMensajeError("0", true), "data" => array("Validacion por Array"));

      return new Response(json_encode($resp), Response::HTTP_CREATED);
    }

}
?>