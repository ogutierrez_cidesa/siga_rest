<?php
namespace Controllers\Facturacion;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Controllers\Facturacion;
use \Utils\H;
use \Business\Facturacion\Facturacionv2;
use \Business\Facturacion\Clientev2;
use \Fapedido;
use \FapedidoQuery;
use \Faartped;
use \FaartpedQuery;
use \Faclientev2;
use \Facliente;
use \FaclienteQuery;

class PedidoBaseControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];;

        // Create Presupuesto Base
        $controllers->post('/pedido', 'Controllers\Facturacion\PedidoBaseControllerProvider::pedido_create');
        $controllers->get('/pedido', 'Controllers\Facturacion\PedidoBaseControllerProvider::pedido_find');
        $controllers->get('/pedido/{nroped}', 'Controllers\Facturacion\PedidoBaseControllerProvider::pedido_find');
        $controllers->delete('/pedido/{nroped}', 'Controllers\Facturacion\PedidoBaseControllerProvider::pedido_delete');
        $controllers->put('/pedido/{nroped}', 'Controllers\Facturacion\PedidoBaseControllerProvider::pedido_anular');

        return $controllers;
    }

    public function pedido_delete(Application $app, Request $request){
      $nroped = $request->get('nroped');
      if($nroped!=null){
        $fapedido = FapedidoQuery::create()->findOneByNroped($nroped);
        if($fapedido==null){
          $resp = array("response" => H::obtenerMensajeError("0", true));
          return new Response(json_encode($resp), Response::HTTP_NOT_FOUND);
        }else{
          FaartpedQuery::create()->filterByNroped($nroped)->delete();
          $fapedido->delete();

          $resp = array("response" => array('cod' => '-1'), 'data' => array());
          return new Response(json_encode($resp), Response::HTTP_OK);          
        }
      }else{
        $resp = array("response" => H::obtenerMensajeError("0", true));
        return new Response(json_encode($resp), Response::HTTP_NOT_FOUND);
      } 
    }

    public function pedido_anular(Application $app, Request $request){
      $nroped = $request->get('nroped');
      $fecanu = $fecanu = date('d/m/Y',strtotime('now'));
      if($nroped!=null){
        $fapedido = FapedidoQuery::create()->findOneByNroped($nroped);
        if($fapedido==null){
          $resp = array("response" => H::obtenerMensajeError("0", true));
          return new Response(json_encode($resp), Response::HTTP_NOT_FOUND);
        }else{
          $fapedido->anular($fecanu);

          $resp = array("response" => array('cod' => '-1'), 'data' => array());
          return new Response(json_encode($resp), Response::HTTP_OK);          
        }
      }else{
        $resp = array("response" => H::obtenerMensajeError("0", true));
        return new Response(json_encode($resp), Response::HTTP_NOT_FOUND);
      } 
    }


    public function pedido_find(Application $app, Request $request){
      $nroped = $request->get('nroped');
      if($nroped==null){
        $fapedido = FapedidoQuery::create()->joinWith('Fapedido.Faartped')->find()->toJson();
      }else{
        $fapedido = FapedidoQuery::create()->joinWith('Fapedido.Faartped')->findByNroped($nroped)->toJson();
      }
      $resp = array("response" => array('cod' => '-1'), 'data' => array($fapedido));
      return new Response(json_encode($resp), Response::HTTP_OK);
    }

    public function pedido_create(Application $app, Request $request) {

      $fapedido = $request->get('fapedido');

      $faartped = $request->get('faartped');

      $facliente = $request->get('facliente');

      if(Facturacionv2::ValidarPedidoDesdeArray($fapedido, $faartped)){

        $fapedido_obj = new \Fapedido();
        $fapedido_obj->fromArray($fapedido, \BasePeer::TYPE_FIELDNAME);

        $faartped_array = array();

        foreach ($faartped as $v) {
          $faartped_obj = new \Faartped();
          $faartped_obj->fromArray($v, \BasePeer::TYPE_FIELDNAME);
          $faartped_array[] = $faartped_obj;
        }

        $error = Facturacionv2::ValidarPedido($fapedido_obj, $faartped_array);

        if($error==1198 && count($facliente)>=4){
          $valid = Clientev2::ValidarClienteDesdeArray($facliente);
          if($valid==-1){

            $facliente_obj = new \Facliente();
            $facliente_obj->fromArray($facliente, \BasePeer::TYPE_FIELDNAME);

            try{
              $error = Clientev2::salvarFacliente($facliente_obj);
              if($error == -1){
                $error = '-1';
              }else{
                $resp = array("response" => H::obtenerMensajeError($error, true), "data" => array($facliente_obj->toArray()));
                return new Response(json_encode($resp), Response::HTTP_CREATED);
              }            
            }catch (Exception $e){
              $resp = array("response" => H::obtenerMensajeError("0", true));
              return new Response(json_encode($resp), Response::HTTP_CREATED);
            }
          }else{
            $resp = array("response" => H::obtenerMensajeError($valid, true), "data" => $facliente);
            return new Response(json_encode($resp), Response::HTTP_CREATED);
          } 
        }

        if($error == '-1'){
          try{
            $fapedido_obj->setTipref('PE');
            $error = Facturacionv2::salvarPedidos($fapedido_obj, array($faartped_array, array()), array(array(), array()));
            if($error == -1){
              $resp = array("response" => array("cod" => '-1', "msj" => "Pedido Generado"), "data" => array("nroped" => $fapedido_obj->getNroped()));
            }else{
              $resp = array("response" => H::obtenerMensajeError($error, true), "data" => array($fapedido_obj->toArray()));
            }            
          }catch (Exception $e){
            $resp = array("response" => H::obtenerMensajeError("0", true), 'data' => array('Exception'));
          }
        }else{
          $resp = array("response" => H::obtenerMensajeError($error, true), "data" => array('Error Validacion Negocio'));
        }
      }else $resp = array("response" => H::obtenerMensajeError("0", true), "data" => array('Error Validacion Datos', $fapedido, $faartped));

      return new Response(json_encode($resp), Response::HTTP_CREATED);
    }

}
?>
