<?php
namespace Controllers\Facturacion;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Controllers\Facturacion;
use \Utils\H;
use \Business\Facturacion\Facturacionv2;
use \Business\Facturacion\Clientev2;
use \Fapedido;
use \Faclientev2;
use \FapedidoQuery;
use \Faartped;
use \FaartpedQuery;
use \Facliente;
use \FaclienteQuery;

class ClienteControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];;

        // Create Presupuesto Base
        $controllers->post('/cliente', 'Controllers\Facturacion\ClienteControllerProvider::cliente_create');
        $controllers->get('/cliente/{codpro}', 'Controllers\Facturacion\ClienteControllerProvider::cliente_find');
        $controllers->get('/cliente', 'Controllers\Facturacion\ClienteControllerProvider::cliente_find');

        return $controllers;
    }

    public function cliente_find(Application $app, Request $request){
      $codpro = $request->get('codpro');
      if($codpro==null){
        $facliente = FaclienteQuery::create()->find()->toJson();
      }else{
        $facliente = FaclienteQuery::create()->findByCodpro($codpro)->toJson();
      }
      $resp = array("response" => array('cod' => '-1'), 'data' => array($facliente));
      return new Response(json_encode($resp), Response::HTTP_OK);
    }

    public function cliente_create(Application $app, Request $request) {

      $facliente = $request->get('facliente');

      $valid = Clientev2::ValidarClienteDesdeArray($facliente);
      if($valid==-1){

        $facliente_obj = new \Facliente();
        $facliente_obj->fromArray($facliente, \BasePeer::TYPE_FIELDNAME);

        try{
          $error = Clientev2::salvarFacliente($facliente_obj);
          if($error == -1){
            $resp = array("response" => array("cod" => '-1', "msj" => "Cliente Generado"), "data" => array("corpro" => $facliente_obj->getCodpro()));
          }else{
            $resp = array("response" => H::obtenerMensajeError($error, true), "data" => array($facliente_obj->toArray()));
          }            
        }catch (Exception $e){
          $resp = array("response" => H::obtenerMensajeError("0", true));
        }
      }else $resp = array("response" => H::obtenerMensajeError($valid, true), "data" => $facliente);

      return new Response(json_encode($resp), Response::HTTP_CREATED);
    }

}
?>