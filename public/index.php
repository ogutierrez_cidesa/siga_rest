<?php
require_once __DIR__.'/../vendor/autoload.php';

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Propel\Silex\PropelServiceProvider;

$app = new Application();
$app['debug'] = true;

$app['propel.config_file'] = __DIR__.'/../config/database.php';
$app['propel.model_path'] = __DIR__."/../";
$app->register(new Propel\Silex\PropelServiceProvider());
$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../log/development.log',
));


$con = Propel::getConnection(CaregartPeer::DATABASE_NAME);
$con->useDebug(true);

$app->mount('/licitacion', new Controllers\Licitaciones\PresupuestoBaseControllerProvider());
$app->mount('/facturacion', new Controllers\Facturacion\PedidoBaseControllerProvider());
$app->mount('/facturacion', new Controllers\Facturacion\ClienteControllerProvider());
$app->mount('/compras', new Controllers\Compras\ComprasControllerProvider());

$app->get('/{a}/{b}', function(Application $app, Request $request) {

  $a = $request->get('a');
  $b = $request->get('b');

  return json_encode(array('result' => $a+$b));
});

$app->get('/', function(Application $app, Request $request) {

  return json_encode(array("Sin Datos Para la Suma"));
});


$app->run();
?>
