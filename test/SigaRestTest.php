<?php 
  include 'restclient.php';

  $api = new RestClient(array(
      'base_url' => "http://sigaws.dev.suvinca.gob.ve", 
      // 'format' => "json", 
      // 'headers' => array('Authorization' => 'Bearer '.OAUTH_BEARER), 
  ));


  // Ejemplo 1: Sumar sin parámetros
  $result = $api->get("#");
  echo "GET ".$result->info->url;
  echo "\n";
  echo "Response: ".$result->response;
  echo "\n";
  echo "\n";
  echo "\n";

  // Ejemplo 2: Sumar con parámetros
  $result = $api->get("10/20");
  echo "GET ".$result->info->url;
  echo "\n";
  print_r((array)$result->response);
  echo "\n";

  // Ejemplo 3: Listado de presupuestos bases
  $result = $api->get("licitacion/presupuesto_base");
  echo "GET ".$result->info->url;
  echo "\n";
  print_r((array)$result->response);
  echo "\n";

  // Ejemplo 4: Creando Presupuesto Base.
  // No se pasan todos los parámetros y devuelve un error.
  $result = $api->post("licitacion/presupuesto_base", array("liprebas[numpre]" => "########", "liprebas[fecreg]" => "10/10/2014"));
  echo "POST ".$result->info->url;
  echo "\n";
  print_r((array)$result->response);
  echo "\n";
  print_r((array)$result->headers);
  echo "\n";

  // NOTAS:
  // Todas las peticiones POST (creación de registros) devuelven un parámetro "response"
  // con un "cod" de resultado. Este "cod" será >= -1, donde -1 indica que fue satisfactoria
  // la petición y >=0 es el código de error devuelto. Cuando sea >=0 devolverá en otro paŕametro
  // "msj" el mensaje que corresponde con el código de error.

 ?>
