Instalacion
========

git clone https://ogutierrez_cidesa@bitbucket.org/ogutierrez_cidesa/siga_rest.git

cd siga_rest

apt-get install curl

curl -sS https://getcomposer.org/installer | php

./composer.phar update

----Para ejecutar sin usar apache-------------------
cd siga_rest
php -S localhost:8000 -t public



Configurar En Apache
==================

Crear en public el archivo .htaccess y agregar...

<IfModule mod_rewrite.c>
    Options -MultiViews

    RewriteEngine On
    #RewriteBase /path/to/app
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [QSA,L]
</IfModule>

habilitar el modulo rewrite en apache

a2enmod rewrite

reiniciar apache


Listo!!!!!!