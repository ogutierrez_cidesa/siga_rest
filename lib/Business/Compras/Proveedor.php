<?php
namespace Business\Compras;

use \Criteria;
use \Utils\H;
use \Utils\Herramientas;
use \CarcpartQuery;
use \Caartrcp;
use \CaregartPeer;
use \CaartalmubiPeer;
use \CaartalmPeer;
use \CaartordPeer;
use \ContabbPeer;
use \ContabcPeer;
use \Contabc;
use \Contabc1;
use \Contabc1Peer;
use \CpdeftitPeer;
use \Business\Contabilidad\Comprobantev2;
use \Business\Facturacion\Facturav2;


// use \FapedidoQuery;
// use \Facorrelat;

/**
 * Recepción: Clase estática para el manejo de las recepciones de almacén
 *
 * @package    Roraima
 * @subpackage compras
 * @author     $Author$ <desarrollo@cidesa.com.ve>
 * @version SVN: $Id$
 *
 * @copyright  Copyright 2007, Cide S.A.
 * @license    http://opensource.org/licenses/gpl-2.0.php GPLv2
 */
class Proveedor
{

#############no me quiere reconocer la Clase

  public static function ValidarProveedorDesdeArray($proveedor)
  {

    $valid_array_key_proveedor = array("codpro", "rifpro", "nompro", "telpro", "emapro");
    if( count($proveedor) > 0  ){
        if(!\Utils\H::keys_in_array($valid_array_key_proveedor, $proveedor)) {
          return false;
        }
    }else
      return false;

    return true;
  }

  /**
   * Función para registrar artículos
   *
   * @static
   * @param $proveedor Object Artículo a guardar
   * @param $grid Array de Objects Almacen.
   * @return void
   */
    public static function salvarProveedor($proveedor, &$resultado = array() )
    {
      $msj=-1;
      //Valores por defecto
      $tipoper = strtoupper(substr($proveedor['rifpro'],0,1));
      $rif=strtoupper($proveedor['rifpro']);
      $codigo=strtoupper($proveedor['codpro']);
      $nombre=strtoupper($proveedor['nompro']);
      $telpro=strtoupper($proveedor['telpro']);
      $emapro=strtoupper($proveedor['emapro']);
      $stapro = "A";
      //Valores del array


      $pos = strrpos("NVGJ",$tipoper);
      if ($pos === false)
        return 6100;//El primer Caracter del rif no es Valido

      $sql = "select * from caprovee where codpro = '".$codigo."'";
      if (Herramientas::BuscarDatos($sql,$tabla)) //Verifico que el Proveedor no existe
         if ($tabla)
          return 6101; //El codigo de proveedor ya existe no se puede incluir el proveedor

      $sql = "select * from opbenefi where cedrif = '".$rif."'";
      if (Herramientas::BuscarDatos($sql,$tabla)) //Verifico que el Proveedor no existe
         if ($tabla)
          return 6102; //El codigo de proveedor ya existe no se puede incluir el proveedor
     
      $sql = "select codctaws from cadefart";
      if (Herramientas::BuscarDatos($sql,$tabla)) //Verifico Codigo Generico para cuentas contables
         if ($tabla)
          $cuenta=$tabla[codctaws];



      //print $codigoart;

        
         
      //Aqui ya deberiamos estar listos para insertar
      $sql="INSERT INTO CAPROVEE (CODPRO, NOMPRO, RIFPRO, NITPRO, DIRPRO, TELPRO, EMAIL, ESTPRO, codcta)
            VALUES ('".$codigo."', '".$nombre."', '".$rif."', '".$tipoper."','' ,'".$telpro."' ,'".$emapro."' ,'".$stapro."','".$cuenta."' )";
            //print($sql);exit;
      if (Herramientas::insertarRegistros($sql))

      $sql="INSERT INTO opbenefi (cedrif, nomben, nitben, dirben, telben, email)
            VALUES ('".$codigo."', '".$nombre."', '".$tipoper."','' ,'".$telpro."' ,'".$emapro."')";
            //print($sql);exit;
      if (Herramientas::insertarRegistros($sql))  

      //retornamos el resultado como un array por valor
      $sql="Select CODPRO, NOMPRO, RIFPRO, NITPRO, DIRPRO, TELPRO, EMAIL, ESTPRO from caprovee where codpro='$codigo' ";
      Herramientas::BuscarDatos($sql, $resultado) ;
      return $msj;
    }



}
?>
