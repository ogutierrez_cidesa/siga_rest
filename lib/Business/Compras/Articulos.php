<?php
namespace Business\Compras;

use \Criteria;
use \Utils\H;
use \Utils\Herramientas;
use \CarcpartQuery;
use \Caartrcp;
use \CaregartPeer;
use \CaartalmubiPeer;
use \CaartalmPeer;
use \CaartordPeer;
use \ContabbPeer;
use \ContabcPeer;
use \Contabc;
use \Contabc1;
use \Contabc1Peer;
use \CpdeftitPeer;
use \Business\Contabilidad\Comprobantev2;
use \Business\Facturacion\Facturav2;


// use \FapedidoQuery;
// use \Facorrelat;

/**
 * Recepción: Clase estática para el manejo de las recepciones de almacén
 *
 * @package    Roraima
 * @subpackage compras
 * @author     $Author$ <desarrollo@cidesa.com.ve>
 * @version SVN: $Id$
 *
 * @copyright  Copyright 2007, Cide S.A.
 * @license    http://opensource.org/licenses/gpl-2.0.php GPLv2
 */
class Articulos
{

#############no me quiere reconocer la Clase

  public static function ValidarArticuloDesdeArray($articulo)
  {

    $valid_array_key_articulo = array("rubro", "codbar", "desart", "codpar", "cosult");
    if( count($articulo) > 0  ){
        if(!\Utils\H::keys_in_array($valid_array_key_articulo, $articulo)) {
          return false;
        }
    }else
      return false;

    return true;
  }

  /**
   * Función para registrar artículos
   *
   * @static
   * @param $articulo Object Artículo a guardar
   * @param $grid Array de Objects Almacen.
   * @return void
   */
    public static function salvarArticulo($articulo, &$resultado = array() )
    {
      $msj=-1;
      //Valores por defecto
      $tipoart = "A";
      $cospro = 0;
      $exitot = 0;
      $unimed = "UNIDAD";
      $staart = "A";
      //Valores del array
      $descrip = $articulo['desart'];
      $codigobar = $articulo['codbar'];
      $codigopar = $articulo['codpar'];
      $costoult = $articulo['cosult'];


      $sql = "select * from caregart where codbar = '".$codigobar."'";

      if (Herramientas::BuscarDatos($sql,$tabla)) //Verifico que el codigo de barras no exista
         if ($tabla)
          return 6003; //El codigo de Barras ya existe no se puede incluir el articulo


      $sql = "select a.rupfalws,b.coding,b.ramart  from cadefart a, caregart b where b.codart='".$articulo['rubro']."'";
      if (Herramientas::BuscarDatos($sql,$tabladef)){
         if ($tabladef){
             $rupfaltan  = $tabladef['rupfalws'];
             $codigoing = $tabladef['coding'];
             $ramoart = $tabladef['ramart'];
             $codigoaux = $articulo['rubro'] . "-" . $rupfaltan . "-";
         }
         else{
          return 6000; //hubo error al obtener las Definiciones Verifique el Rubro
         }

      }
      else{
         return 6001; //hubo error al obtener las Definiciones
      }


      // buscamos Correlativo

      $sql = "select lpad((coalesce(max(right(codart,4)),'0000')::int+1)::varchar, 4,'0') as correl from caregart where codart like '".$codigoaux."%'";

      if (Herramientas::BuscarDatos($sql,$tablacorr))
         $codigoart  = $codigoaux.$tablacorr['correl'];
      else
         return 6002; //No Fue posible obtener el correlativo

      //print $codigoart;

      //Aqui ya deberiamos estar listos para insertar
      $sql="INSERT INTO CAREGART (CODART, DESART, CODPAR, RAMART, COSULT, COSPRO, EXITOT, UNIMED, INVINI, CODBAR, TIPO, CODING, STAART, FECULT, COSUNIPRI)
            VALUES ('".$codigoart."', '".$descrip."', '".$codigopar."', '".$ramoart."',".$costoult." ,".$cospro." ,".$exitot." ,'".$unimed."',0 ,'".$codigobar."', '".$tipoart."', '".$codigoing."', '".$staart."', now()::date, ".$costoult." )";
      if (Herramientas::insertarRegistros($sql))

      //retornamos el resultado como un array por valor
      $sql="Select codart,desart,id from caregart where codart='$codigoart' ";
      Herramientas::BuscarDatos($sql, $resultado) ;
      return $msj;
    }



}
?>
