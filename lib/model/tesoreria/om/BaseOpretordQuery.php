<?php


/**
 * Base class that represents a query for the 'opretord' table.
 *
 * Tabla que contiene información referente a la orden de pago de retención
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:41 2015
 *
 * @method OpretordQuery orderByNumord($order = Criteria::ASC) Order by the numord column
 * @method OpretordQuery orderByCodtip($order = Criteria::ASC) Order by the codtip column
 * @method OpretordQuery orderByMonret($order = Criteria::ASC) Order by the monret column
 * @method OpretordQuery orderByCodpre($order = Criteria::ASC) Order by the codpre column
 * @method OpretordQuery orderByNumret($order = Criteria::ASC) Order by the numret column
 * @method OpretordQuery orderByRefere($order = Criteria::ASC) Order by the refere column
 * @method OpretordQuery orderByCorrel($order = Criteria::ASC) Order by the correl column
 * @method OpretordQuery orderByMonbas($order = Criteria::ASC) Order by the monbas column
 * @method OpretordQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method OpretordQuery groupByNumord() Group by the numord column
 * @method OpretordQuery groupByCodtip() Group by the codtip column
 * @method OpretordQuery groupByMonret() Group by the monret column
 * @method OpretordQuery groupByCodpre() Group by the codpre column
 * @method OpretordQuery groupByNumret() Group by the numret column
 * @method OpretordQuery groupByRefere() Group by the refere column
 * @method OpretordQuery groupByCorrel() Group by the correl column
 * @method OpretordQuery groupByMonbas() Group by the monbas column
 * @method OpretordQuery groupById() Group by the id column
 *
 * @method OpretordQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method OpretordQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method OpretordQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method OpretordQuery leftJoinOptipret($relationAlias = null) Adds a LEFT JOIN clause to the query using the Optipret relation
 * @method OpretordQuery rightJoinOptipret($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Optipret relation
 * @method OpretordQuery innerJoinOptipret($relationAlias = null) Adds a INNER JOIN clause to the query using the Optipret relation
 *
 * @method Opretord findOne(PropelPDO $con = null) Return the first Opretord matching the query
 * @method Opretord findOneOrCreate(PropelPDO $con = null) Return the first Opretord matching the query, or a new Opretord object populated from the query conditions when no match is found
 *
 * @method Opretord findOneByNumord(string $numord) Return the first Opretord filtered by the numord column
 * @method Opretord findOneByCodtip(string $codtip) Return the first Opretord filtered by the codtip column
 * @method Opretord findOneByMonret(string $monret) Return the first Opretord filtered by the monret column
 * @method Opretord findOneByCodpre(string $codpre) Return the first Opretord filtered by the codpre column
 * @method Opretord findOneByNumret(string $numret) Return the first Opretord filtered by the numret column
 * @method Opretord findOneByRefere(string $refere) Return the first Opretord filtered by the refere column
 * @method Opretord findOneByCorrel(string $correl) Return the first Opretord filtered by the correl column
 * @method Opretord findOneByMonbas(string $monbas) Return the first Opretord filtered by the monbas column
 *
 * @method array findByNumord(string $numord) Return Opretord objects filtered by the numord column
 * @method array findByCodtip(string $codtip) Return Opretord objects filtered by the codtip column
 * @method array findByMonret(string $monret) Return Opretord objects filtered by the monret column
 * @method array findByCodpre(string $codpre) Return Opretord objects filtered by the codpre column
 * @method array findByNumret(string $numret) Return Opretord objects filtered by the numret column
 * @method array findByRefere(string $refere) Return Opretord objects filtered by the refere column
 * @method array findByCorrel(string $correl) Return Opretord objects filtered by the correl column
 * @method array findByMonbas(string $monbas) Return Opretord objects filtered by the monbas column
 * @method array findById(int $id) Return Opretord objects filtered by the id column
 *
 * @package    propel.generator.lib.model.tesoreria.om
 */
abstract class BaseOpretordQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseOpretordQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Opretord', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new OpretordQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   OpretordQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return OpretordQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof OpretordQuery) {
            return $criteria;
        }
        $query = new OpretordQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Opretord|Opretord[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = OpretordPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(OpretordPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Opretord A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Opretord A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "numord", "codtip", "monret", "codpre", "numret", "refere", "correl", "monbas", "id" FROM "opretord" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Opretord();
            $obj->hydrate($row);
            OpretordPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Opretord|Opretord[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Opretord[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return OpretordQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OpretordPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return OpretordQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OpretordPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the numord column
     *
     * Example usage:
     * <code>
     * $query->filterByNumord('fooValue');   // WHERE numord = 'fooValue'
     * $query->filterByNumord('%fooValue%'); // WHERE numord LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numord The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpretordQuery The current query, for fluid interface
     */
    public function filterByNumord($numord = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numord)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numord)) {
                $numord = str_replace('*', '%', $numord);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OpretordPeer::NUMORD, $numord, $comparison);
    }

    /**
     * Filter the query on the codtip column
     *
     * Example usage:
     * <code>
     * $query->filterByCodtip('fooValue');   // WHERE codtip = 'fooValue'
     * $query->filterByCodtip('%fooValue%'); // WHERE codtip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codtip The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpretordQuery The current query, for fluid interface
     */
    public function filterByCodtip($codtip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codtip)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codtip)) {
                $codtip = str_replace('*', '%', $codtip);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OpretordPeer::CODTIP, $codtip, $comparison);
    }

    /**
     * Filter the query on the monret column
     *
     * Example usage:
     * <code>
     * $query->filterByMonret(1234); // WHERE monret = 1234
     * $query->filterByMonret(array(12, 34)); // WHERE monret IN (12, 34)
     * $query->filterByMonret(array('min' => 12)); // WHERE monret >= 12
     * $query->filterByMonret(array('max' => 12)); // WHERE monret <= 12
     * </code>
     *
     * @param     mixed $monret The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpretordQuery The current query, for fluid interface
     */
    public function filterByMonret($monret = null, $comparison = null)
    {
        if (is_array($monret)) {
            $useMinMax = false;
            if (isset($monret['min'])) {
                $this->addUsingAlias(OpretordPeer::MONRET, $monret['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monret['max'])) {
                $this->addUsingAlias(OpretordPeer::MONRET, $monret['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OpretordPeer::MONRET, $monret, $comparison);
    }

    /**
     * Filter the query on the codpre column
     *
     * Example usage:
     * <code>
     * $query->filterByCodpre('fooValue');   // WHERE codpre = 'fooValue'
     * $query->filterByCodpre('%fooValue%'); // WHERE codpre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codpre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpretordQuery The current query, for fluid interface
     */
    public function filterByCodpre($codpre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codpre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codpre)) {
                $codpre = str_replace('*', '%', $codpre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OpretordPeer::CODPRE, $codpre, $comparison);
    }

    /**
     * Filter the query on the numret column
     *
     * Example usage:
     * <code>
     * $query->filterByNumret('fooValue');   // WHERE numret = 'fooValue'
     * $query->filterByNumret('%fooValue%'); // WHERE numret LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numret The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpretordQuery The current query, for fluid interface
     */
    public function filterByNumret($numret = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numret)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numret)) {
                $numret = str_replace('*', '%', $numret);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OpretordPeer::NUMRET, $numret, $comparison);
    }

    /**
     * Filter the query on the refere column
     *
     * Example usage:
     * <code>
     * $query->filterByRefere('fooValue');   // WHERE refere = 'fooValue'
     * $query->filterByRefere('%fooValue%'); // WHERE refere LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refere The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpretordQuery The current query, for fluid interface
     */
    public function filterByRefere($refere = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refere)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refere)) {
                $refere = str_replace('*', '%', $refere);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OpretordPeer::REFERE, $refere, $comparison);
    }

    /**
     * Filter the query on the correl column
     *
     * Example usage:
     * <code>
     * $query->filterByCorrel(1234); // WHERE correl = 1234
     * $query->filterByCorrel(array(12, 34)); // WHERE correl IN (12, 34)
     * $query->filterByCorrel(array('min' => 12)); // WHERE correl >= 12
     * $query->filterByCorrel(array('max' => 12)); // WHERE correl <= 12
     * </code>
     *
     * @param     mixed $correl The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpretordQuery The current query, for fluid interface
     */
    public function filterByCorrel($correl = null, $comparison = null)
    {
        if (is_array($correl)) {
            $useMinMax = false;
            if (isset($correl['min'])) {
                $this->addUsingAlias(OpretordPeer::CORREL, $correl['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($correl['max'])) {
                $this->addUsingAlias(OpretordPeer::CORREL, $correl['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OpretordPeer::CORREL, $correl, $comparison);
    }

    /**
     * Filter the query on the monbas column
     *
     * Example usage:
     * <code>
     * $query->filterByMonbas(1234); // WHERE monbas = 1234
     * $query->filterByMonbas(array(12, 34)); // WHERE monbas IN (12, 34)
     * $query->filterByMonbas(array('min' => 12)); // WHERE monbas >= 12
     * $query->filterByMonbas(array('max' => 12)); // WHERE monbas <= 12
     * </code>
     *
     * @param     mixed $monbas The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpretordQuery The current query, for fluid interface
     */
    public function filterByMonbas($monbas = null, $comparison = null)
    {
        if (is_array($monbas)) {
            $useMinMax = false;
            if (isset($monbas['min'])) {
                $this->addUsingAlias(OpretordPeer::MONBAS, $monbas['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monbas['max'])) {
                $this->addUsingAlias(OpretordPeer::MONBAS, $monbas['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OpretordPeer::MONBAS, $monbas, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpretordQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(OpretordPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(OpretordPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OpretordPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query by a related Optipret object
     *
     * @param   Optipret|PropelObjectCollection $optipret The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 OpretordQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByOptipret($optipret, $comparison = null)
    {
        if ($optipret instanceof Optipret) {
            return $this
                ->addUsingAlias(OpretordPeer::CODTIP, $optipret->getCodtip(), $comparison);
        } elseif ($optipret instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OpretordPeer::CODTIP, $optipret->toKeyValue('PrimaryKey', 'Codtip'), $comparison);
        } else {
            throw new PropelException('filterByOptipret() only accepts arguments of type Optipret or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Optipret relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return OpretordQuery The current query, for fluid interface
     */
    public function joinOptipret($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Optipret');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Optipret');
        }

        return $this;
    }

    /**
     * Use the Optipret relation Optipret object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   OptipretQuery A secondary query class using the current class as primary query
     */
    public function useOptipretQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOptipret($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Optipret', 'OptipretQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Opretord $opretord Object to remove from the list of results
     *
     * @return OpretordQuery The current query, for fluid interface
     */
    public function prune($opretord = null)
    {
        if ($opretord) {
            $this->addUsingAlias(OpretordPeer::ID, $opretord->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
