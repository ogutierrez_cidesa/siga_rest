<?php


/**
 * Base class that represents a query for the 'tsfonpre' table.
 *
 * null
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:41 2015
 *
 * @method TsfonpreQuery orderByNumche($order = Criteria::ASC) Order by the numche column
 * @method TsfonpreQuery orderByTipemp($order = Criteria::ASC) Order by the tipemp column
 * @method TsfonpreQuery orderByTippre($order = Criteria::ASC) Order by the tippre column
 * @method TsfonpreQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method TsfonpreQuery groupByNumche() Group by the numche column
 * @method TsfonpreQuery groupByTipemp() Group by the tipemp column
 * @method TsfonpreQuery groupByTippre() Group by the tippre column
 * @method TsfonpreQuery groupById() Group by the id column
 *
 * @method TsfonpreQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method TsfonpreQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method TsfonpreQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Tsfonpre findOne(PropelPDO $con = null) Return the first Tsfonpre matching the query
 * @method Tsfonpre findOneOrCreate(PropelPDO $con = null) Return the first Tsfonpre matching the query, or a new Tsfonpre object populated from the query conditions when no match is found
 *
 * @method Tsfonpre findOneByNumche(string $numche) Return the first Tsfonpre filtered by the numche column
 * @method Tsfonpre findOneByTipemp(string $tipemp) Return the first Tsfonpre filtered by the tipemp column
 * @method Tsfonpre findOneByTippre(string $tippre) Return the first Tsfonpre filtered by the tippre column
 *
 * @method array findByNumche(string $numche) Return Tsfonpre objects filtered by the numche column
 * @method array findByTipemp(string $tipemp) Return Tsfonpre objects filtered by the tipemp column
 * @method array findByTippre(string $tippre) Return Tsfonpre objects filtered by the tippre column
 * @method array findById(int $id) Return Tsfonpre objects filtered by the id column
 *
 * @package    propel.generator.lib.model.tesoreria.om
 */
abstract class BaseTsfonpreQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseTsfonpreQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Tsfonpre', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new TsfonpreQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   TsfonpreQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return TsfonpreQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof TsfonpreQuery) {
            return $criteria;
        }
        $query = new TsfonpreQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Tsfonpre|Tsfonpre[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = TsfonprePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(TsfonprePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Tsfonpre A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Tsfonpre A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "numche", "tipemp", "tippre", "id" FROM "tsfonpre" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Tsfonpre();
            $obj->hydrate($row);
            TsfonprePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Tsfonpre|Tsfonpre[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Tsfonpre[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return TsfonpreQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TsfonprePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return TsfonpreQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TsfonprePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the numche column
     *
     * Example usage:
     * <code>
     * $query->filterByNumche('fooValue');   // WHERE numche = 'fooValue'
     * $query->filterByNumche('%fooValue%'); // WHERE numche LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numche The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsfonpreQuery The current query, for fluid interface
     */
    public function filterByNumche($numche = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numche)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numche)) {
                $numche = str_replace('*', '%', $numche);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TsfonprePeer::NUMCHE, $numche, $comparison);
    }

    /**
     * Filter the query on the tipemp column
     *
     * Example usage:
     * <code>
     * $query->filterByTipemp('fooValue');   // WHERE tipemp = 'fooValue'
     * $query->filterByTipemp('%fooValue%'); // WHERE tipemp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipemp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsfonpreQuery The current query, for fluid interface
     */
    public function filterByTipemp($tipemp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipemp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tipemp)) {
                $tipemp = str_replace('*', '%', $tipemp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TsfonprePeer::TIPEMP, $tipemp, $comparison);
    }

    /**
     * Filter the query on the tippre column
     *
     * Example usage:
     * <code>
     * $query->filterByTippre('fooValue');   // WHERE tippre = 'fooValue'
     * $query->filterByTippre('%fooValue%'); // WHERE tippre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tippre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsfonpreQuery The current query, for fluid interface
     */
    public function filterByTippre($tippre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tippre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tippre)) {
                $tippre = str_replace('*', '%', $tippre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TsfonprePeer::TIPPRE, $tippre, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsfonpreQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TsfonprePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TsfonprePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TsfonprePeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Tsfonpre $tsfonpre Object to remove from the list of results
     *
     * @return TsfonpreQuery The current query, for fluid interface
     */
    public function prune($tsfonpre = null)
    {
        if ($tsfonpre) {
            $this->addUsingAlias(TsfonprePeer::ID, $tsfonpre->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
