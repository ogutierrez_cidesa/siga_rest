<?php


/**
 * Base class that represents a query for the 'opconnom' table.
 *
 * null
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:40 2015
 *
 * @method OpconnomQuery orderByCodcat($order = Criteria::ASC) Order by the codcat column
 * @method OpconnomQuery orderByCodnom($order = Criteria::ASC) Order by the codnom column
 * @method OpconnomQuery orderByRefprc($order = Criteria::ASC) Order by the refprc column
 * @method OpconnomQuery orderByRefcom($order = Criteria::ASC) Order by the refcom column
 * @method OpconnomQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method OpconnomQuery groupByCodcat() Group by the codcat column
 * @method OpconnomQuery groupByCodnom() Group by the codnom column
 * @method OpconnomQuery groupByRefprc() Group by the refprc column
 * @method OpconnomQuery groupByRefcom() Group by the refcom column
 * @method OpconnomQuery groupById() Group by the id column
 *
 * @method OpconnomQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method OpconnomQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method OpconnomQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Opconnom findOne(PropelPDO $con = null) Return the first Opconnom matching the query
 * @method Opconnom findOneOrCreate(PropelPDO $con = null) Return the first Opconnom matching the query, or a new Opconnom object populated from the query conditions when no match is found
 *
 * @method Opconnom findOneByCodcat(string $codcat) Return the first Opconnom filtered by the codcat column
 * @method Opconnom findOneByCodnom(string $codnom) Return the first Opconnom filtered by the codnom column
 * @method Opconnom findOneByRefprc(string $refprc) Return the first Opconnom filtered by the refprc column
 * @method Opconnom findOneByRefcom(string $refcom) Return the first Opconnom filtered by the refcom column
 *
 * @method array findByCodcat(string $codcat) Return Opconnom objects filtered by the codcat column
 * @method array findByCodnom(string $codnom) Return Opconnom objects filtered by the codnom column
 * @method array findByRefprc(string $refprc) Return Opconnom objects filtered by the refprc column
 * @method array findByRefcom(string $refcom) Return Opconnom objects filtered by the refcom column
 * @method array findById(int $id) Return Opconnom objects filtered by the id column
 *
 * @package    propel.generator.lib.model.tesoreria.om
 */
abstract class BaseOpconnomQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseOpconnomQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Opconnom', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new OpconnomQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   OpconnomQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return OpconnomQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof OpconnomQuery) {
            return $criteria;
        }
        $query = new OpconnomQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Opconnom|Opconnom[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = OpconnomPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(OpconnomPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Opconnom A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Opconnom A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "codcat", "codnom", "refprc", "refcom", "id" FROM "opconnom" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Opconnom();
            $obj->hydrate($row);
            OpconnomPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Opconnom|Opconnom[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Opconnom[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return OpconnomQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OpconnomPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return OpconnomQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OpconnomPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the codcat column
     *
     * Example usage:
     * <code>
     * $query->filterByCodcat('fooValue');   // WHERE codcat = 'fooValue'
     * $query->filterByCodcat('%fooValue%'); // WHERE codcat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codcat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpconnomQuery The current query, for fluid interface
     */
    public function filterByCodcat($codcat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codcat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codcat)) {
                $codcat = str_replace('*', '%', $codcat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OpconnomPeer::CODCAT, $codcat, $comparison);
    }

    /**
     * Filter the query on the codnom column
     *
     * Example usage:
     * <code>
     * $query->filterByCodnom('fooValue');   // WHERE codnom = 'fooValue'
     * $query->filterByCodnom('%fooValue%'); // WHERE codnom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codnom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpconnomQuery The current query, for fluid interface
     */
    public function filterByCodnom($codnom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codnom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codnom)) {
                $codnom = str_replace('*', '%', $codnom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OpconnomPeer::CODNOM, $codnom, $comparison);
    }

    /**
     * Filter the query on the refprc column
     *
     * Example usage:
     * <code>
     * $query->filterByRefprc('fooValue');   // WHERE refprc = 'fooValue'
     * $query->filterByRefprc('%fooValue%'); // WHERE refprc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refprc The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpconnomQuery The current query, for fluid interface
     */
    public function filterByRefprc($refprc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refprc)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refprc)) {
                $refprc = str_replace('*', '%', $refprc);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OpconnomPeer::REFPRC, $refprc, $comparison);
    }

    /**
     * Filter the query on the refcom column
     *
     * Example usage:
     * <code>
     * $query->filterByRefcom('fooValue');   // WHERE refcom = 'fooValue'
     * $query->filterByRefcom('%fooValue%'); // WHERE refcom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refcom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpconnomQuery The current query, for fluid interface
     */
    public function filterByRefcom($refcom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refcom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refcom)) {
                $refcom = str_replace('*', '%', $refcom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OpconnomPeer::REFCOM, $refcom, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpconnomQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(OpconnomPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(OpconnomPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OpconnomPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Opconnom $opconnom Object to remove from the list of results
     *
     * @return OpconnomQuery The current query, for fluid interface
     */
    public function prune($opconnom = null)
    {
        if ($opconnom) {
            $this->addUsingAlias(OpconnomPeer::ID, $opconnom->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
