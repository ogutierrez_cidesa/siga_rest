<?php


/**
 * Base static class for performing query and update operations on the 'opbenefi2' table.
 *
 * null
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:40 2015
 *
 * @package propel.generator.lib.model.tesoreria.om
 */
abstract class BaseOpbenefi2Peer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'simaxxx';

    /** the table name for this class */
    const TABLE_NAME = 'opbenefi2';

    /** the related Propel class for this table */
    const OM_CLASS = 'Opbenefi2';

    /** the related TableMap class for this table */
    const TM_CLASS = 'Opbenefi2TableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 19;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 19;

    /** the column name for the cedrif field */
    const CEDRIF = 'opbenefi2.cedrif';

    /** the column name for the nomben field */
    const NOMBEN = 'opbenefi2.nomben';

    /** the column name for the dirben field */
    const DIRBEN = 'opbenefi2.dirben';

    /** the column name for the telben field */
    const TELBEN = 'opbenefi2.telben';

    /** the column name for the codcta field */
    const CODCTA = 'opbenefi2.codcta';

    /** the column name for the nitben field */
    const NITBEN = 'opbenefi2.nitben';

    /** the column name for the codtipben field */
    const CODTIPBEN = 'opbenefi2.codtipben';

    /** the column name for the tipper field */
    const TIPPER = 'opbenefi2.tipper';

    /** the column name for the nacionalidad field */
    const NACIONALIDAD = 'opbenefi2.nacionalidad';

    /** the column name for the residente field */
    const RESIDENTE = 'opbenefi2.residente';

    /** the column name for the constituida field */
    const CONSTITUIDA = 'opbenefi2.constituida';

    /** the column name for the codord field */
    const CODORD = 'opbenefi2.codord';

    /** the column name for the codpercon field */
    const CODPERCON = 'opbenefi2.codpercon';

    /** the column name for the codordadi field */
    const CODORDADI = 'opbenefi2.codordadi';

    /** the column name for the codperconadi field */
    const CODPERCONADI = 'opbenefi2.codperconadi';

    /** the column name for the codordcontra field */
    const CODORDCONTRA = 'opbenefi2.codordcontra';

    /** the column name for the codpercontra field */
    const CODPERCONTRA = 'opbenefi2.codpercontra';

    /** the column name for the temcedrif field */
    const TEMCEDRIF = 'opbenefi2.temcedrif';

    /** the column name for the id field */
    const ID = 'opbenefi2.id';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of Opbenefi2 objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array Opbenefi2[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. Opbenefi2Peer::$fieldNames[Opbenefi2Peer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Cedrif', 'Nomben', 'Dirben', 'Telben', 'Codcta', 'Nitben', 'Codtipben', 'Tipper', 'Nacionalidad', 'Residente', 'Constituida', 'Codord', 'Codpercon', 'Codordadi', 'Codperconadi', 'Codordcontra', 'Codpercontra', 'Temcedrif', 'Id', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('cedrif', 'nomben', 'dirben', 'telben', 'codcta', 'nitben', 'codtipben', 'tipper', 'nacionalidad', 'residente', 'constituida', 'codord', 'codpercon', 'codordadi', 'codperconadi', 'codordcontra', 'codpercontra', 'temcedrif', 'id', ),
        BasePeer::TYPE_COLNAME => array (Opbenefi2Peer::CEDRIF, Opbenefi2Peer::NOMBEN, Opbenefi2Peer::DIRBEN, Opbenefi2Peer::TELBEN, Opbenefi2Peer::CODCTA, Opbenefi2Peer::NITBEN, Opbenefi2Peer::CODTIPBEN, Opbenefi2Peer::TIPPER, Opbenefi2Peer::NACIONALIDAD, Opbenefi2Peer::RESIDENTE, Opbenefi2Peer::CONSTITUIDA, Opbenefi2Peer::CODORD, Opbenefi2Peer::CODPERCON, Opbenefi2Peer::CODORDADI, Opbenefi2Peer::CODPERCONADI, Opbenefi2Peer::CODORDCONTRA, Opbenefi2Peer::CODPERCONTRA, Opbenefi2Peer::TEMCEDRIF, Opbenefi2Peer::ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('CEDRIF', 'NOMBEN', 'DIRBEN', 'TELBEN', 'CODCTA', 'NITBEN', 'CODTIPBEN', 'TIPPER', 'NACIONALIDAD', 'RESIDENTE', 'CONSTITUIDA', 'CODORD', 'CODPERCON', 'CODORDADI', 'CODPERCONADI', 'CODORDCONTRA', 'CODPERCONTRA', 'TEMCEDRIF', 'ID', ),
        BasePeer::TYPE_FIELDNAME => array ('cedrif', 'nomben', 'dirben', 'telben', 'codcta', 'nitben', 'codtipben', 'tipper', 'nacionalidad', 'residente', 'constituida', 'codord', 'codpercon', 'codordadi', 'codperconadi', 'codordcontra', 'codpercontra', 'temcedrif', 'id', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. Opbenefi2Peer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Cedrif' => 0, 'Nomben' => 1, 'Dirben' => 2, 'Telben' => 3, 'Codcta' => 4, 'Nitben' => 5, 'Codtipben' => 6, 'Tipper' => 7, 'Nacionalidad' => 8, 'Residente' => 9, 'Constituida' => 10, 'Codord' => 11, 'Codpercon' => 12, 'Codordadi' => 13, 'Codperconadi' => 14, 'Codordcontra' => 15, 'Codpercontra' => 16, 'Temcedrif' => 17, 'Id' => 18, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('cedrif' => 0, 'nomben' => 1, 'dirben' => 2, 'telben' => 3, 'codcta' => 4, 'nitben' => 5, 'codtipben' => 6, 'tipper' => 7, 'nacionalidad' => 8, 'residente' => 9, 'constituida' => 10, 'codord' => 11, 'codpercon' => 12, 'codordadi' => 13, 'codperconadi' => 14, 'codordcontra' => 15, 'codpercontra' => 16, 'temcedrif' => 17, 'id' => 18, ),
        BasePeer::TYPE_COLNAME => array (Opbenefi2Peer::CEDRIF => 0, Opbenefi2Peer::NOMBEN => 1, Opbenefi2Peer::DIRBEN => 2, Opbenefi2Peer::TELBEN => 3, Opbenefi2Peer::CODCTA => 4, Opbenefi2Peer::NITBEN => 5, Opbenefi2Peer::CODTIPBEN => 6, Opbenefi2Peer::TIPPER => 7, Opbenefi2Peer::NACIONALIDAD => 8, Opbenefi2Peer::RESIDENTE => 9, Opbenefi2Peer::CONSTITUIDA => 10, Opbenefi2Peer::CODORD => 11, Opbenefi2Peer::CODPERCON => 12, Opbenefi2Peer::CODORDADI => 13, Opbenefi2Peer::CODPERCONADI => 14, Opbenefi2Peer::CODORDCONTRA => 15, Opbenefi2Peer::CODPERCONTRA => 16, Opbenefi2Peer::TEMCEDRIF => 17, Opbenefi2Peer::ID => 18, ),
        BasePeer::TYPE_RAW_COLNAME => array ('CEDRIF' => 0, 'NOMBEN' => 1, 'DIRBEN' => 2, 'TELBEN' => 3, 'CODCTA' => 4, 'NITBEN' => 5, 'CODTIPBEN' => 6, 'TIPPER' => 7, 'NACIONALIDAD' => 8, 'RESIDENTE' => 9, 'CONSTITUIDA' => 10, 'CODORD' => 11, 'CODPERCON' => 12, 'CODORDADI' => 13, 'CODPERCONADI' => 14, 'CODORDCONTRA' => 15, 'CODPERCONTRA' => 16, 'TEMCEDRIF' => 17, 'ID' => 18, ),
        BasePeer::TYPE_FIELDNAME => array ('cedrif' => 0, 'nomben' => 1, 'dirben' => 2, 'telben' => 3, 'codcta' => 4, 'nitben' => 5, 'codtipben' => 6, 'tipper' => 7, 'nacionalidad' => 8, 'residente' => 9, 'constituida' => 10, 'codord' => 11, 'codpercon' => 12, 'codordadi' => 13, 'codperconadi' => 14, 'codordcontra' => 15, 'codpercontra' => 16, 'temcedrif' => 17, 'id' => 18, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = Opbenefi2Peer::getFieldNames($toType);
        $key = isset(Opbenefi2Peer::$fieldKeys[$fromType][$name]) ? Opbenefi2Peer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(Opbenefi2Peer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, Opbenefi2Peer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return Opbenefi2Peer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. Opbenefi2Peer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(Opbenefi2Peer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(Opbenefi2Peer::CEDRIF);
            $criteria->addSelectColumn(Opbenefi2Peer::NOMBEN);
            $criteria->addSelectColumn(Opbenefi2Peer::DIRBEN);
            $criteria->addSelectColumn(Opbenefi2Peer::TELBEN);
            $criteria->addSelectColumn(Opbenefi2Peer::CODCTA);
            $criteria->addSelectColumn(Opbenefi2Peer::NITBEN);
            $criteria->addSelectColumn(Opbenefi2Peer::CODTIPBEN);
            $criteria->addSelectColumn(Opbenefi2Peer::TIPPER);
            $criteria->addSelectColumn(Opbenefi2Peer::NACIONALIDAD);
            $criteria->addSelectColumn(Opbenefi2Peer::RESIDENTE);
            $criteria->addSelectColumn(Opbenefi2Peer::CONSTITUIDA);
            $criteria->addSelectColumn(Opbenefi2Peer::CODORD);
            $criteria->addSelectColumn(Opbenefi2Peer::CODPERCON);
            $criteria->addSelectColumn(Opbenefi2Peer::CODORDADI);
            $criteria->addSelectColumn(Opbenefi2Peer::CODPERCONADI);
            $criteria->addSelectColumn(Opbenefi2Peer::CODORDCONTRA);
            $criteria->addSelectColumn(Opbenefi2Peer::CODPERCONTRA);
            $criteria->addSelectColumn(Opbenefi2Peer::TEMCEDRIF);
            $criteria->addSelectColumn(Opbenefi2Peer::ID);
        } else {
            $criteria->addSelectColumn($alias . '.cedrif');
            $criteria->addSelectColumn($alias . '.nomben');
            $criteria->addSelectColumn($alias . '.dirben');
            $criteria->addSelectColumn($alias . '.telben');
            $criteria->addSelectColumn($alias . '.codcta');
            $criteria->addSelectColumn($alias . '.nitben');
            $criteria->addSelectColumn($alias . '.codtipben');
            $criteria->addSelectColumn($alias . '.tipper');
            $criteria->addSelectColumn($alias . '.nacionalidad');
            $criteria->addSelectColumn($alias . '.residente');
            $criteria->addSelectColumn($alias . '.constituida');
            $criteria->addSelectColumn($alias . '.codord');
            $criteria->addSelectColumn($alias . '.codpercon');
            $criteria->addSelectColumn($alias . '.codordadi');
            $criteria->addSelectColumn($alias . '.codperconadi');
            $criteria->addSelectColumn($alias . '.codordcontra');
            $criteria->addSelectColumn($alias . '.codpercontra');
            $criteria->addSelectColumn($alias . '.temcedrif');
            $criteria->addSelectColumn($alias . '.id');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(Opbenefi2Peer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            Opbenefi2Peer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(Opbenefi2Peer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(Opbenefi2Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 Opbenefi2
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = Opbenefi2Peer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return Opbenefi2Peer::populateObjects(Opbenefi2Peer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(Opbenefi2Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            Opbenefi2Peer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(Opbenefi2Peer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      Opbenefi2 $obj A Opbenefi2 object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            Opbenefi2Peer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A Opbenefi2 object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof Opbenefi2) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Opbenefi2 object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(Opbenefi2Peer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   Opbenefi2 Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(Opbenefi2Peer::$instances[$key])) {
                return Opbenefi2Peer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (Opbenefi2Peer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        Opbenefi2Peer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to opbenefi2
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol + 18] === null) {
            return null;
        }

        return (string) $row[$startcol + 18];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol + 18];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = Opbenefi2Peer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = Opbenefi2Peer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = Opbenefi2Peer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                Opbenefi2Peer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (Opbenefi2 object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = Opbenefi2Peer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = Opbenefi2Peer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + Opbenefi2Peer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = Opbenefi2Peer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            Opbenefi2Peer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(Opbenefi2Peer::DATABASE_NAME)->getTable(Opbenefi2Peer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseOpbenefi2Peer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseOpbenefi2Peer::TABLE_NAME)) {
        $dbMap->addTableObject(new Opbenefi2TableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return Opbenefi2Peer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a Opbenefi2 or Criteria object.
     *
     * @param      mixed $values Criteria or Opbenefi2 object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(Opbenefi2Peer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from Opbenefi2 object
        }


        // Set the correct dbName
        $criteria->setDbName(Opbenefi2Peer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a Opbenefi2 or Criteria object.
     *
     * @param      mixed $values Criteria or Opbenefi2 object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(Opbenefi2Peer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(Opbenefi2Peer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(Opbenefi2Peer::ID);
            $value = $criteria->remove(Opbenefi2Peer::ID);
            if ($value) {
                $selectCriteria->add(Opbenefi2Peer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(Opbenefi2Peer::TABLE_NAME);
            }

        } else { // $values is Opbenefi2 object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(Opbenefi2Peer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the opbenefi2 table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(Opbenefi2Peer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(Opbenefi2Peer::TABLE_NAME, $con, Opbenefi2Peer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            Opbenefi2Peer::clearInstancePool();
            Opbenefi2Peer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a Opbenefi2 or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or Opbenefi2 object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(Opbenefi2Peer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            Opbenefi2Peer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof Opbenefi2) { // it's a model object
            // invalidate the cache for this single object
            Opbenefi2Peer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(Opbenefi2Peer::DATABASE_NAME);
            $criteria->add(Opbenefi2Peer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                Opbenefi2Peer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(Opbenefi2Peer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            Opbenefi2Peer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given Opbenefi2 object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      Opbenefi2 $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(Opbenefi2Peer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(Opbenefi2Peer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(Opbenefi2Peer::DATABASE_NAME, Opbenefi2Peer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return Opbenefi2
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = Opbenefi2Peer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(Opbenefi2Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(Opbenefi2Peer::DATABASE_NAME);
        $criteria->add(Opbenefi2Peer::ID, $pk);

        $v = Opbenefi2Peer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return Opbenefi2[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(Opbenefi2Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(Opbenefi2Peer::DATABASE_NAME);
            $criteria->add(Opbenefi2Peer::ID, $pks, Criteria::IN);
            $objs = Opbenefi2Peer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseOpbenefi2Peer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseOpbenefi2Peer::buildTableMap();

