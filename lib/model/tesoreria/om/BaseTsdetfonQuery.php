<?php


/**
 * Base class that represents a query for the 'tsdetfon' table.
 *
 * null
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:42 2015
 *
 * @method TsdetfonQuery orderByReffon($order = Criteria::ASC) Order by the reffon column
 * @method TsdetfonQuery orderByCodart($order = Criteria::ASC) Order by the codart column
 * @method TsdetfonQuery orderByCodcat($order = Criteria::ASC) Order by the codcat column
 * @method TsdetfonQuery orderByMonfon($order = Criteria::ASC) Order by the monfon column
 * @method TsdetfonQuery orderByMonrec($order = Criteria::ASC) Order by the monrec column
 * @method TsdetfonQuery orderByTotfon($order = Criteria::ASC) Order by the totfon column
 * @method TsdetfonQuery orderByStafon($order = Criteria::ASC) Order by the stafon column
 * @method TsdetfonQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method TsdetfonQuery groupByReffon() Group by the reffon column
 * @method TsdetfonQuery groupByCodart() Group by the codart column
 * @method TsdetfonQuery groupByCodcat() Group by the codcat column
 * @method TsdetfonQuery groupByMonfon() Group by the monfon column
 * @method TsdetfonQuery groupByMonrec() Group by the monrec column
 * @method TsdetfonQuery groupByTotfon() Group by the totfon column
 * @method TsdetfonQuery groupByStafon() Group by the stafon column
 * @method TsdetfonQuery groupById() Group by the id column
 *
 * @method TsdetfonQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method TsdetfonQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method TsdetfonQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Tsdetfon findOne(PropelPDO $con = null) Return the first Tsdetfon matching the query
 * @method Tsdetfon findOneOrCreate(PropelPDO $con = null) Return the first Tsdetfon matching the query, or a new Tsdetfon object populated from the query conditions when no match is found
 *
 * @method Tsdetfon findOneByReffon(string $reffon) Return the first Tsdetfon filtered by the reffon column
 * @method Tsdetfon findOneByCodart(string $codart) Return the first Tsdetfon filtered by the codart column
 * @method Tsdetfon findOneByCodcat(string $codcat) Return the first Tsdetfon filtered by the codcat column
 * @method Tsdetfon findOneByMonfon(string $monfon) Return the first Tsdetfon filtered by the monfon column
 * @method Tsdetfon findOneByMonrec(string $monrec) Return the first Tsdetfon filtered by the monrec column
 * @method Tsdetfon findOneByTotfon(string $totfon) Return the first Tsdetfon filtered by the totfon column
 * @method Tsdetfon findOneByStafon(string $stafon) Return the first Tsdetfon filtered by the stafon column
 *
 * @method array findByReffon(string $reffon) Return Tsdetfon objects filtered by the reffon column
 * @method array findByCodart(string $codart) Return Tsdetfon objects filtered by the codart column
 * @method array findByCodcat(string $codcat) Return Tsdetfon objects filtered by the codcat column
 * @method array findByMonfon(string $monfon) Return Tsdetfon objects filtered by the monfon column
 * @method array findByMonrec(string $monrec) Return Tsdetfon objects filtered by the monrec column
 * @method array findByTotfon(string $totfon) Return Tsdetfon objects filtered by the totfon column
 * @method array findByStafon(string $stafon) Return Tsdetfon objects filtered by the stafon column
 * @method array findById(int $id) Return Tsdetfon objects filtered by the id column
 *
 * @package    propel.generator.lib.model.tesoreria.om
 */
abstract class BaseTsdetfonQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseTsdetfonQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Tsdetfon', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new TsdetfonQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   TsdetfonQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return TsdetfonQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof TsdetfonQuery) {
            return $criteria;
        }
        $query = new TsdetfonQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Tsdetfon|Tsdetfon[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = TsdetfonPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(TsdetfonPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Tsdetfon A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Tsdetfon A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "reffon", "codart", "codcat", "monfon", "monrec", "totfon", "stafon", "id" FROM "tsdetfon" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Tsdetfon();
            $obj->hydrate($row);
            TsdetfonPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Tsdetfon|Tsdetfon[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Tsdetfon[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return TsdetfonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TsdetfonPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return TsdetfonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TsdetfonPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the reffon column
     *
     * Example usage:
     * <code>
     * $query->filterByReffon('fooValue');   // WHERE reffon = 'fooValue'
     * $query->filterByReffon('%fooValue%'); // WHERE reffon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $reffon The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsdetfonQuery The current query, for fluid interface
     */
    public function filterByReffon($reffon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($reffon)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $reffon)) {
                $reffon = str_replace('*', '%', $reffon);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TsdetfonPeer::REFFON, $reffon, $comparison);
    }

    /**
     * Filter the query on the codart column
     *
     * Example usage:
     * <code>
     * $query->filterByCodart('fooValue');   // WHERE codart = 'fooValue'
     * $query->filterByCodart('%fooValue%'); // WHERE codart LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codart The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsdetfonQuery The current query, for fluid interface
     */
    public function filterByCodart($codart = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codart)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codart)) {
                $codart = str_replace('*', '%', $codart);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TsdetfonPeer::CODART, $codart, $comparison);
    }

    /**
     * Filter the query on the codcat column
     *
     * Example usage:
     * <code>
     * $query->filterByCodcat('fooValue');   // WHERE codcat = 'fooValue'
     * $query->filterByCodcat('%fooValue%'); // WHERE codcat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codcat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsdetfonQuery The current query, for fluid interface
     */
    public function filterByCodcat($codcat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codcat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codcat)) {
                $codcat = str_replace('*', '%', $codcat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TsdetfonPeer::CODCAT, $codcat, $comparison);
    }

    /**
     * Filter the query on the monfon column
     *
     * Example usage:
     * <code>
     * $query->filterByMonfon(1234); // WHERE monfon = 1234
     * $query->filterByMonfon(array(12, 34)); // WHERE monfon IN (12, 34)
     * $query->filterByMonfon(array('min' => 12)); // WHERE monfon >= 12
     * $query->filterByMonfon(array('max' => 12)); // WHERE monfon <= 12
     * </code>
     *
     * @param     mixed $monfon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsdetfonQuery The current query, for fluid interface
     */
    public function filterByMonfon($monfon = null, $comparison = null)
    {
        if (is_array($monfon)) {
            $useMinMax = false;
            if (isset($monfon['min'])) {
                $this->addUsingAlias(TsdetfonPeer::MONFON, $monfon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monfon['max'])) {
                $this->addUsingAlias(TsdetfonPeer::MONFON, $monfon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TsdetfonPeer::MONFON, $monfon, $comparison);
    }

    /**
     * Filter the query on the monrec column
     *
     * Example usage:
     * <code>
     * $query->filterByMonrec(1234); // WHERE monrec = 1234
     * $query->filterByMonrec(array(12, 34)); // WHERE monrec IN (12, 34)
     * $query->filterByMonrec(array('min' => 12)); // WHERE monrec >= 12
     * $query->filterByMonrec(array('max' => 12)); // WHERE monrec <= 12
     * </code>
     *
     * @param     mixed $monrec The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsdetfonQuery The current query, for fluid interface
     */
    public function filterByMonrec($monrec = null, $comparison = null)
    {
        if (is_array($monrec)) {
            $useMinMax = false;
            if (isset($monrec['min'])) {
                $this->addUsingAlias(TsdetfonPeer::MONREC, $monrec['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monrec['max'])) {
                $this->addUsingAlias(TsdetfonPeer::MONREC, $monrec['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TsdetfonPeer::MONREC, $monrec, $comparison);
    }

    /**
     * Filter the query on the totfon column
     *
     * Example usage:
     * <code>
     * $query->filterByTotfon(1234); // WHERE totfon = 1234
     * $query->filterByTotfon(array(12, 34)); // WHERE totfon IN (12, 34)
     * $query->filterByTotfon(array('min' => 12)); // WHERE totfon >= 12
     * $query->filterByTotfon(array('max' => 12)); // WHERE totfon <= 12
     * </code>
     *
     * @param     mixed $totfon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsdetfonQuery The current query, for fluid interface
     */
    public function filterByTotfon($totfon = null, $comparison = null)
    {
        if (is_array($totfon)) {
            $useMinMax = false;
            if (isset($totfon['min'])) {
                $this->addUsingAlias(TsdetfonPeer::TOTFON, $totfon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totfon['max'])) {
                $this->addUsingAlias(TsdetfonPeer::TOTFON, $totfon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TsdetfonPeer::TOTFON, $totfon, $comparison);
    }

    /**
     * Filter the query on the stafon column
     *
     * Example usage:
     * <code>
     * $query->filterByStafon('fooValue');   // WHERE stafon = 'fooValue'
     * $query->filterByStafon('%fooValue%'); // WHERE stafon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $stafon The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsdetfonQuery The current query, for fluid interface
     */
    public function filterByStafon($stafon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($stafon)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $stafon)) {
                $stafon = str_replace('*', '%', $stafon);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TsdetfonPeer::STAFON, $stafon, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsdetfonQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TsdetfonPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TsdetfonPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TsdetfonPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Tsdetfon $tsdetfon Object to remove from the list of results
     *
     * @return TsdetfonQuery The current query, for fluid interface
     */
    public function prune($tsdetfon = null)
    {
        if ($tsdetfon) {
            $this->addUsingAlias(TsdetfonPeer::ID, $tsdetfon->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
