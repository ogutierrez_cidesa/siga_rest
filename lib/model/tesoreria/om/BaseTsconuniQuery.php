<?php


/**
 * Base class that represents a query for the 'tsconuni' table.
 *
 * null
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:41 2015
 *
 * @method TsconuniQuery orderByCodcta($order = Criteria::ASC) Order by the codcta column
 * @method TsconuniQuery orderByCodsus($order = Criteria::ASC) Order by the codsus column
 * @method TsconuniQuery orderByNomsus($order = Criteria::ASC) Order by the nomsus column
 * @method TsconuniQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method TsconuniQuery groupByCodcta() Group by the codcta column
 * @method TsconuniQuery groupByCodsus() Group by the codsus column
 * @method TsconuniQuery groupByNomsus() Group by the nomsus column
 * @method TsconuniQuery groupById() Group by the id column
 *
 * @method TsconuniQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method TsconuniQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method TsconuniQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Tsconuni findOne(PropelPDO $con = null) Return the first Tsconuni matching the query
 * @method Tsconuni findOneOrCreate(PropelPDO $con = null) Return the first Tsconuni matching the query, or a new Tsconuni object populated from the query conditions when no match is found
 *
 * @method Tsconuni findOneByCodcta(string $codcta) Return the first Tsconuni filtered by the codcta column
 * @method Tsconuni findOneByCodsus(string $codsus) Return the first Tsconuni filtered by the codsus column
 * @method Tsconuni findOneByNomsus(string $nomsus) Return the first Tsconuni filtered by the nomsus column
 *
 * @method array findByCodcta(string $codcta) Return Tsconuni objects filtered by the codcta column
 * @method array findByCodsus(string $codsus) Return Tsconuni objects filtered by the codsus column
 * @method array findByNomsus(string $nomsus) Return Tsconuni objects filtered by the nomsus column
 * @method array findById(int $id) Return Tsconuni objects filtered by the id column
 *
 * @package    propel.generator.lib.model.tesoreria.om
 */
abstract class BaseTsconuniQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseTsconuniQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Tsconuni', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new TsconuniQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   TsconuniQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return TsconuniQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof TsconuniQuery) {
            return $criteria;
        }
        $query = new TsconuniQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Tsconuni|Tsconuni[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = TsconuniPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(TsconuniPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Tsconuni A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Tsconuni A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "codcta", "codsus", "nomsus", "id" FROM "tsconuni" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Tsconuni();
            $obj->hydrate($row);
            TsconuniPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Tsconuni|Tsconuni[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Tsconuni[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return TsconuniQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TsconuniPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return TsconuniQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TsconuniPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the codcta column
     *
     * Example usage:
     * <code>
     * $query->filterByCodcta('fooValue');   // WHERE codcta = 'fooValue'
     * $query->filterByCodcta('%fooValue%'); // WHERE codcta LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codcta The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsconuniQuery The current query, for fluid interface
     */
    public function filterByCodcta($codcta = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codcta)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codcta)) {
                $codcta = str_replace('*', '%', $codcta);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TsconuniPeer::CODCTA, $codcta, $comparison);
    }

    /**
     * Filter the query on the codsus column
     *
     * Example usage:
     * <code>
     * $query->filterByCodsus('fooValue');   // WHERE codsus = 'fooValue'
     * $query->filterByCodsus('%fooValue%'); // WHERE codsus LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codsus The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsconuniQuery The current query, for fluid interface
     */
    public function filterByCodsus($codsus = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codsus)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codsus)) {
                $codsus = str_replace('*', '%', $codsus);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TsconuniPeer::CODSUS, $codsus, $comparison);
    }

    /**
     * Filter the query on the nomsus column
     *
     * Example usage:
     * <code>
     * $query->filterByNomsus('fooValue');   // WHERE nomsus = 'fooValue'
     * $query->filterByNomsus('%fooValue%'); // WHERE nomsus LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomsus The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsconuniQuery The current query, for fluid interface
     */
    public function filterByNomsus($nomsus = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomsus)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomsus)) {
                $nomsus = str_replace('*', '%', $nomsus);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TsconuniPeer::NOMSUS, $nomsus, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TsconuniQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TsconuniPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TsconuniPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TsconuniPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Tsconuni $tsconuni Object to remove from the list of results
     *
     * @return TsconuniQuery The current query, for fluid interface
     */
    public function prune($tsconuni = null)
    {
        if ($tsconuni) {
            $this->addUsingAlias(TsconuniPeer::ID, $tsconuni->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
