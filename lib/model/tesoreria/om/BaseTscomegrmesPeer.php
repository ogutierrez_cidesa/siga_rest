<?php


/**
 * Base static class for performing query and update operations on the 'tscomegrmes' table.
 *
 * null
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:42 2015
 *
 * @package propel.generator.lib.model.tesoreria.om
 */
abstract class BaseTscomegrmesPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'simaxxx';

    /** the table name for this class */
    const TABLE_NAME = 'tscomegrmes';

    /** the related Propel class for this table */
    const OM_CLASS = 'Tscomegrmes';

    /** the related TableMap class for this table */
    const TM_CLASS = 'TscomegrmesTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 25;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 25;

    /** the column name for the mes1 field */
    const MES1 = 'tscomegrmes.mes1';

    /** the column name for the cormes1 field */
    const CORMES1 = 'tscomegrmes.cormes1';

    /** the column name for the mes2 field */
    const MES2 = 'tscomegrmes.mes2';

    /** the column name for the cormes2 field */
    const CORMES2 = 'tscomegrmes.cormes2';

    /** the column name for the mes3 field */
    const MES3 = 'tscomegrmes.mes3';

    /** the column name for the cormes3 field */
    const CORMES3 = 'tscomegrmes.cormes3';

    /** the column name for the mes4 field */
    const MES4 = 'tscomegrmes.mes4';

    /** the column name for the cormes4 field */
    const CORMES4 = 'tscomegrmes.cormes4';

    /** the column name for the mes5 field */
    const MES5 = 'tscomegrmes.mes5';

    /** the column name for the cormes5 field */
    const CORMES5 = 'tscomegrmes.cormes5';

    /** the column name for the mes6 field */
    const MES6 = 'tscomegrmes.mes6';

    /** the column name for the cormes6 field */
    const CORMES6 = 'tscomegrmes.cormes6';

    /** the column name for the mes7 field */
    const MES7 = 'tscomegrmes.mes7';

    /** the column name for the cormes7 field */
    const CORMES7 = 'tscomegrmes.cormes7';

    /** the column name for the mes8 field */
    const MES8 = 'tscomegrmes.mes8';

    /** the column name for the cormes8 field */
    const CORMES8 = 'tscomegrmes.cormes8';

    /** the column name for the mes9 field */
    const MES9 = 'tscomegrmes.mes9';

    /** the column name for the cormes9 field */
    const CORMES9 = 'tscomegrmes.cormes9';

    /** the column name for the mes10 field */
    const MES10 = 'tscomegrmes.mes10';

    /** the column name for the cormes10 field */
    const CORMES10 = 'tscomegrmes.cormes10';

    /** the column name for the mes11 field */
    const MES11 = 'tscomegrmes.mes11';

    /** the column name for the cormes11 field */
    const CORMES11 = 'tscomegrmes.cormes11';

    /** the column name for the mes12 field */
    const MES12 = 'tscomegrmes.mes12';

    /** the column name for the cormes12 field */
    const CORMES12 = 'tscomegrmes.cormes12';

    /** the column name for the id field */
    const ID = 'tscomegrmes.id';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of Tscomegrmes objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array Tscomegrmes[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. TscomegrmesPeer::$fieldNames[TscomegrmesPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Mes1', 'Cormes1', 'Mes2', 'Cormes2', 'Mes3', 'Cormes3', 'Mes4', 'Cormes4', 'Mes5', 'Cormes5', 'Mes6', 'Cormes6', 'Mes7', 'Cormes7', 'Mes8', 'Cormes8', 'Mes9', 'Cormes9', 'Mes10', 'Cormes10', 'Mes11', 'Cormes11', 'Mes12', 'Cormes12', 'Id', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('mes1', 'cormes1', 'mes2', 'cormes2', 'mes3', 'cormes3', 'mes4', 'cormes4', 'mes5', 'cormes5', 'mes6', 'cormes6', 'mes7', 'cormes7', 'mes8', 'cormes8', 'mes9', 'cormes9', 'mes10', 'cormes10', 'mes11', 'cormes11', 'mes12', 'cormes12', 'id', ),
        BasePeer::TYPE_COLNAME => array (TscomegrmesPeer::MES1, TscomegrmesPeer::CORMES1, TscomegrmesPeer::MES2, TscomegrmesPeer::CORMES2, TscomegrmesPeer::MES3, TscomegrmesPeer::CORMES3, TscomegrmesPeer::MES4, TscomegrmesPeer::CORMES4, TscomegrmesPeer::MES5, TscomegrmesPeer::CORMES5, TscomegrmesPeer::MES6, TscomegrmesPeer::CORMES6, TscomegrmesPeer::MES7, TscomegrmesPeer::CORMES7, TscomegrmesPeer::MES8, TscomegrmesPeer::CORMES8, TscomegrmesPeer::MES9, TscomegrmesPeer::CORMES9, TscomegrmesPeer::MES10, TscomegrmesPeer::CORMES10, TscomegrmesPeer::MES11, TscomegrmesPeer::CORMES11, TscomegrmesPeer::MES12, TscomegrmesPeer::CORMES12, TscomegrmesPeer::ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('MES1', 'CORMES1', 'MES2', 'CORMES2', 'MES3', 'CORMES3', 'MES4', 'CORMES4', 'MES5', 'CORMES5', 'MES6', 'CORMES6', 'MES7', 'CORMES7', 'MES8', 'CORMES8', 'MES9', 'CORMES9', 'MES10', 'CORMES10', 'MES11', 'CORMES11', 'MES12', 'CORMES12', 'ID', ),
        BasePeer::TYPE_FIELDNAME => array ('mes1', 'cormes1', 'mes2', 'cormes2', 'mes3', 'cormes3', 'mes4', 'cormes4', 'mes5', 'cormes5', 'mes6', 'cormes6', 'mes7', 'cormes7', 'mes8', 'cormes8', 'mes9', 'cormes9', 'mes10', 'cormes10', 'mes11', 'cormes11', 'mes12', 'cormes12', 'id', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. TscomegrmesPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Mes1' => 0, 'Cormes1' => 1, 'Mes2' => 2, 'Cormes2' => 3, 'Mes3' => 4, 'Cormes3' => 5, 'Mes4' => 6, 'Cormes4' => 7, 'Mes5' => 8, 'Cormes5' => 9, 'Mes6' => 10, 'Cormes6' => 11, 'Mes7' => 12, 'Cormes7' => 13, 'Mes8' => 14, 'Cormes8' => 15, 'Mes9' => 16, 'Cormes9' => 17, 'Mes10' => 18, 'Cormes10' => 19, 'Mes11' => 20, 'Cormes11' => 21, 'Mes12' => 22, 'Cormes12' => 23, 'Id' => 24, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('mes1' => 0, 'cormes1' => 1, 'mes2' => 2, 'cormes2' => 3, 'mes3' => 4, 'cormes3' => 5, 'mes4' => 6, 'cormes4' => 7, 'mes5' => 8, 'cormes5' => 9, 'mes6' => 10, 'cormes6' => 11, 'mes7' => 12, 'cormes7' => 13, 'mes8' => 14, 'cormes8' => 15, 'mes9' => 16, 'cormes9' => 17, 'mes10' => 18, 'cormes10' => 19, 'mes11' => 20, 'cormes11' => 21, 'mes12' => 22, 'cormes12' => 23, 'id' => 24, ),
        BasePeer::TYPE_COLNAME => array (TscomegrmesPeer::MES1 => 0, TscomegrmesPeer::CORMES1 => 1, TscomegrmesPeer::MES2 => 2, TscomegrmesPeer::CORMES2 => 3, TscomegrmesPeer::MES3 => 4, TscomegrmesPeer::CORMES3 => 5, TscomegrmesPeer::MES4 => 6, TscomegrmesPeer::CORMES4 => 7, TscomegrmesPeer::MES5 => 8, TscomegrmesPeer::CORMES5 => 9, TscomegrmesPeer::MES6 => 10, TscomegrmesPeer::CORMES6 => 11, TscomegrmesPeer::MES7 => 12, TscomegrmesPeer::CORMES7 => 13, TscomegrmesPeer::MES8 => 14, TscomegrmesPeer::CORMES8 => 15, TscomegrmesPeer::MES9 => 16, TscomegrmesPeer::CORMES9 => 17, TscomegrmesPeer::MES10 => 18, TscomegrmesPeer::CORMES10 => 19, TscomegrmesPeer::MES11 => 20, TscomegrmesPeer::CORMES11 => 21, TscomegrmesPeer::MES12 => 22, TscomegrmesPeer::CORMES12 => 23, TscomegrmesPeer::ID => 24, ),
        BasePeer::TYPE_RAW_COLNAME => array ('MES1' => 0, 'CORMES1' => 1, 'MES2' => 2, 'CORMES2' => 3, 'MES3' => 4, 'CORMES3' => 5, 'MES4' => 6, 'CORMES4' => 7, 'MES5' => 8, 'CORMES5' => 9, 'MES6' => 10, 'CORMES6' => 11, 'MES7' => 12, 'CORMES7' => 13, 'MES8' => 14, 'CORMES8' => 15, 'MES9' => 16, 'CORMES9' => 17, 'MES10' => 18, 'CORMES10' => 19, 'MES11' => 20, 'CORMES11' => 21, 'MES12' => 22, 'CORMES12' => 23, 'ID' => 24, ),
        BasePeer::TYPE_FIELDNAME => array ('mes1' => 0, 'cormes1' => 1, 'mes2' => 2, 'cormes2' => 3, 'mes3' => 4, 'cormes3' => 5, 'mes4' => 6, 'cormes4' => 7, 'mes5' => 8, 'cormes5' => 9, 'mes6' => 10, 'cormes6' => 11, 'mes7' => 12, 'cormes7' => 13, 'mes8' => 14, 'cormes8' => 15, 'mes9' => 16, 'cormes9' => 17, 'mes10' => 18, 'cormes10' => 19, 'mes11' => 20, 'cormes11' => 21, 'mes12' => 22, 'cormes12' => 23, 'id' => 24, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = TscomegrmesPeer::getFieldNames($toType);
        $key = isset(TscomegrmesPeer::$fieldKeys[$fromType][$name]) ? TscomegrmesPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(TscomegrmesPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, TscomegrmesPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return TscomegrmesPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. TscomegrmesPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(TscomegrmesPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(TscomegrmesPeer::MES1);
            $criteria->addSelectColumn(TscomegrmesPeer::CORMES1);
            $criteria->addSelectColumn(TscomegrmesPeer::MES2);
            $criteria->addSelectColumn(TscomegrmesPeer::CORMES2);
            $criteria->addSelectColumn(TscomegrmesPeer::MES3);
            $criteria->addSelectColumn(TscomegrmesPeer::CORMES3);
            $criteria->addSelectColumn(TscomegrmesPeer::MES4);
            $criteria->addSelectColumn(TscomegrmesPeer::CORMES4);
            $criteria->addSelectColumn(TscomegrmesPeer::MES5);
            $criteria->addSelectColumn(TscomegrmesPeer::CORMES5);
            $criteria->addSelectColumn(TscomegrmesPeer::MES6);
            $criteria->addSelectColumn(TscomegrmesPeer::CORMES6);
            $criteria->addSelectColumn(TscomegrmesPeer::MES7);
            $criteria->addSelectColumn(TscomegrmesPeer::CORMES7);
            $criteria->addSelectColumn(TscomegrmesPeer::MES8);
            $criteria->addSelectColumn(TscomegrmesPeer::CORMES8);
            $criteria->addSelectColumn(TscomegrmesPeer::MES9);
            $criteria->addSelectColumn(TscomegrmesPeer::CORMES9);
            $criteria->addSelectColumn(TscomegrmesPeer::MES10);
            $criteria->addSelectColumn(TscomegrmesPeer::CORMES10);
            $criteria->addSelectColumn(TscomegrmesPeer::MES11);
            $criteria->addSelectColumn(TscomegrmesPeer::CORMES11);
            $criteria->addSelectColumn(TscomegrmesPeer::MES12);
            $criteria->addSelectColumn(TscomegrmesPeer::CORMES12);
            $criteria->addSelectColumn(TscomegrmesPeer::ID);
        } else {
            $criteria->addSelectColumn($alias . '.mes1');
            $criteria->addSelectColumn($alias . '.cormes1');
            $criteria->addSelectColumn($alias . '.mes2');
            $criteria->addSelectColumn($alias . '.cormes2');
            $criteria->addSelectColumn($alias . '.mes3');
            $criteria->addSelectColumn($alias . '.cormes3');
            $criteria->addSelectColumn($alias . '.mes4');
            $criteria->addSelectColumn($alias . '.cormes4');
            $criteria->addSelectColumn($alias . '.mes5');
            $criteria->addSelectColumn($alias . '.cormes5');
            $criteria->addSelectColumn($alias . '.mes6');
            $criteria->addSelectColumn($alias . '.cormes6');
            $criteria->addSelectColumn($alias . '.mes7');
            $criteria->addSelectColumn($alias . '.cormes7');
            $criteria->addSelectColumn($alias . '.mes8');
            $criteria->addSelectColumn($alias . '.cormes8');
            $criteria->addSelectColumn($alias . '.mes9');
            $criteria->addSelectColumn($alias . '.cormes9');
            $criteria->addSelectColumn($alias . '.mes10');
            $criteria->addSelectColumn($alias . '.cormes10');
            $criteria->addSelectColumn($alias . '.mes11');
            $criteria->addSelectColumn($alias . '.cormes11');
            $criteria->addSelectColumn($alias . '.mes12');
            $criteria->addSelectColumn($alias . '.cormes12');
            $criteria->addSelectColumn($alias . '.id');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(TscomegrmesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            TscomegrmesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(TscomegrmesPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(TscomegrmesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 Tscomegrmes
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = TscomegrmesPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return TscomegrmesPeer::populateObjects(TscomegrmesPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(TscomegrmesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            TscomegrmesPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(TscomegrmesPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      Tscomegrmes $obj A Tscomegrmes object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            TscomegrmesPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A Tscomegrmes object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof Tscomegrmes) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Tscomegrmes object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(TscomegrmesPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   Tscomegrmes Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(TscomegrmesPeer::$instances[$key])) {
                return TscomegrmesPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (TscomegrmesPeer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        TscomegrmesPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to tscomegrmes
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol + 24] === null) {
            return null;
        }

        return (string) $row[$startcol + 24];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol + 24];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = TscomegrmesPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = TscomegrmesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = TscomegrmesPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                TscomegrmesPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (Tscomegrmes object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = TscomegrmesPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = TscomegrmesPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + TscomegrmesPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = TscomegrmesPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            TscomegrmesPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(TscomegrmesPeer::DATABASE_NAME)->getTable(TscomegrmesPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseTscomegrmesPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseTscomegrmesPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new TscomegrmesTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return TscomegrmesPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a Tscomegrmes or Criteria object.
     *
     * @param      mixed $values Criteria or Tscomegrmes object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(TscomegrmesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from Tscomegrmes object
        }


        // Set the correct dbName
        $criteria->setDbName(TscomegrmesPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a Tscomegrmes or Criteria object.
     *
     * @param      mixed $values Criteria or Tscomegrmes object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(TscomegrmesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(TscomegrmesPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(TscomegrmesPeer::ID);
            $value = $criteria->remove(TscomegrmesPeer::ID);
            if ($value) {
                $selectCriteria->add(TscomegrmesPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(TscomegrmesPeer::TABLE_NAME);
            }

        } else { // $values is Tscomegrmes object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(TscomegrmesPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the tscomegrmes table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(TscomegrmesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(TscomegrmesPeer::TABLE_NAME, $con, TscomegrmesPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TscomegrmesPeer::clearInstancePool();
            TscomegrmesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a Tscomegrmes or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or Tscomegrmes object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(TscomegrmesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            TscomegrmesPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof Tscomegrmes) { // it's a model object
            // invalidate the cache for this single object
            TscomegrmesPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(TscomegrmesPeer::DATABASE_NAME);
            $criteria->add(TscomegrmesPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                TscomegrmesPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(TscomegrmesPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            TscomegrmesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given Tscomegrmes object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      Tscomegrmes $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(TscomegrmesPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(TscomegrmesPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(TscomegrmesPeer::DATABASE_NAME, TscomegrmesPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return Tscomegrmes
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = TscomegrmesPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(TscomegrmesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(TscomegrmesPeer::DATABASE_NAME);
        $criteria->add(TscomegrmesPeer::ID, $pk);

        $v = TscomegrmesPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return Tscomegrmes[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(TscomegrmesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(TscomegrmesPeer::DATABASE_NAME);
            $criteria->add(TscomegrmesPeer::ID, $pks, Criteria::IN);
            $objs = TscomegrmesPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseTscomegrmesPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseTscomegrmesPeer::buildTableMap();

