<?php



/**
 * This class defines the structure of the 'tspagele' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:43 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.tesoreria.map
 */
class TspageleTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.tesoreria.map.TspageleTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('tspagele');
        $this->setPhpName('Tspagele');
        $this->setClassname('Tspagele');
        $this->setPackage('lib.model.tesoreria');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('refpag', 'Refpag', 'VARCHAR', true, 8, null);
        $this->addColumn('numcue', 'Numcue', 'VARCHAR', true, 20, null);
        $this->addColumn('fecpag', 'Fecpag', 'DATE', true, null, null);
        $this->addColumn('monpag', 'Monpag', 'NUMERIC', true, 14, null);
        $this->addColumn('estpag', 'Estpag', 'VARCHAR', false, 1, null);
        $this->addColumn('loguse', 'Loguse', 'VARCHAR', false, 50, null);
        $this->addColumn('tipdoc', 'Tipdoc', 'VARCHAR', false, 4, null);
        $this->addColumn('despag', 'Despag', 'VARCHAR', false, 1000, null);
        $this->addColumn('cedrif', 'Cedrif', 'VARCHAR', false, 15, null);
        $this->addColumn('tiptxt', 'Tiptxt', 'VARCHAR', false, 1, null);
        $this->addColumn('fecanu', 'Fecanu', 'DATE', false, null, null);
        $this->addColumn('desanu', 'Desanu', 'VARCHAR', false, 100, null);
        $this->addColumn('fecpagado', 'Fecpagado', 'DATE', false, null, null);
        $this->addColumn('fecefepag', 'Fecefepag', 'DATE', false, null, null);
        $this->addForeignKey('codmon', 'Codmon', 'VARCHAR', 'tsdefmon', 'codmon', true, 3, null);
        $this->addColumn('valmon', 'Valmon', 'NUMERIC', false, 14, null);
        $this->addColumn('coddirec', 'Coddirec', 'VARCHAR', false, 4, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Tsdefmon', 'Tsdefmon', RelationMap::MANY_TO_ONE, array('codmon' => 'codmon', ), null, null);
    } // buildRelations()

} // TspageleTableMap
