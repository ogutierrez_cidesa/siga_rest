<?php


/**
 * Base class that represents a query for the 'carampro' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:46 2015
 *
 * @method CaramproQuery orderByCodpro($order = Criteria::ASC) Order by the codpro column
 * @method CaramproQuery orderByRamart($order = Criteria::ASC) Order by the ramart column
 * @method CaramproQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method CaramproQuery groupByCodpro() Group by the codpro column
 * @method CaramproQuery groupByRamart() Group by the ramart column
 * @method CaramproQuery groupById() Group by the id column
 *
 * @method CaramproQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CaramproQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CaramproQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Carampro findOne(PropelPDO $con = null) Return the first Carampro matching the query
 * @method Carampro findOneOrCreate(PropelPDO $con = null) Return the first Carampro matching the query, or a new Carampro object populated from the query conditions when no match is found
 *
 * @method Carampro findOneByCodpro(string $codpro) Return the first Carampro filtered by the codpro column
 * @method Carampro findOneByRamart(string $ramart) Return the first Carampro filtered by the ramart column
 *
 * @method array findByCodpro(string $codpro) Return Carampro objects filtered by the codpro column
 * @method array findByRamart(string $ramart) Return Carampro objects filtered by the ramart column
 * @method array findById(int $id) Return Carampro objects filtered by the id column
 *
 * @package    propel.generator.lib.model.compras.om
 */
abstract class BaseCaramproQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCaramproQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Carampro', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CaramproQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CaramproQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CaramproQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CaramproQuery) {
            return $criteria;
        }
        $query = new CaramproQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Carampro|Carampro[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CaramproPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CaramproPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Carampro A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Carampro A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "codpro", "ramart", "id" FROM "carampro" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Carampro();
            $obj->hydrate($row);
            CaramproPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Carampro|Carampro[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Carampro[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CaramproQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CaramproPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CaramproQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CaramproPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the codpro column
     *
     * Example usage:
     * <code>
     * $query->filterByCodpro('fooValue');   // WHERE codpro = 'fooValue'
     * $query->filterByCodpro('%fooValue%'); // WHERE codpro LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codpro The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CaramproQuery The current query, for fluid interface
     */
    public function filterByCodpro($codpro = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codpro)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codpro)) {
                $codpro = str_replace('*', '%', $codpro);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CaramproPeer::CODPRO, $codpro, $comparison);
    }

    /**
     * Filter the query on the ramart column
     *
     * Example usage:
     * <code>
     * $query->filterByRamart('fooValue');   // WHERE ramart = 'fooValue'
     * $query->filterByRamart('%fooValue%'); // WHERE ramart LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ramart The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CaramproQuery The current query, for fluid interface
     */
    public function filterByRamart($ramart = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ramart)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ramart)) {
                $ramart = str_replace('*', '%', $ramart);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CaramproPeer::RAMART, $ramart, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CaramproQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CaramproPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CaramproPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaramproPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Carampro $carampro Object to remove from the list of results
     *
     * @return CaramproQuery The current query, for fluid interface
     */
    public function prune($carampro = null)
    {
        if ($carampro) {
            $this->addUsingAlias(CaramproPeer::ID, $carampro->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
