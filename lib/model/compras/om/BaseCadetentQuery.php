<?php


/**
 * Base class that represents a query for the 'cadetent' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:45 2015
 *
 * @method CadetentQuery orderByRcpart($order = Criteria::ASC) Order by the rcpart column
 * @method CadetentQuery orderByCodart($order = Criteria::ASC) Order by the codart column
 * @method CadetentQuery orderByCanrec($order = Criteria::ASC) Order by the canrec column
 * @method CadetentQuery orderByMontot($order = Criteria::ASC) Order by the montot column
 * @method CadetentQuery orderByCosart($order = Criteria::ASC) Order by the cosart column
 * @method CadetentQuery orderByCodalm($order = Criteria::ASC) Order by the codalm column
 * @method CadetentQuery orderByCodubi($order = Criteria::ASC) Order by the codubi column
 * @method CadetentQuery orderByFecven($order = Criteria::ASC) Order by the fecven column
 * @method CadetentQuery orderByNumjau($order = Criteria::ASC) Order by the numjau column
 * @method CadetentQuery orderByTammet($order = Criteria::ASC) Order by the tammet column
 * @method CadetentQuery orderByNumlot($order = Criteria::ASC) Order by the numlot column
 * @method CadetentQuery orderByFecela($order = Criteria::ASC) Order by the fecela column
 * @method CadetentQuery orderByCodalt($order = Criteria::ASC) Order by the codalt column
 * @method CadetentQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method CadetentQuery groupByRcpart() Group by the rcpart column
 * @method CadetentQuery groupByCodart() Group by the codart column
 * @method CadetentQuery groupByCanrec() Group by the canrec column
 * @method CadetentQuery groupByMontot() Group by the montot column
 * @method CadetentQuery groupByCosart() Group by the cosart column
 * @method CadetentQuery groupByCodalm() Group by the codalm column
 * @method CadetentQuery groupByCodubi() Group by the codubi column
 * @method CadetentQuery groupByFecven() Group by the fecven column
 * @method CadetentQuery groupByNumjau() Group by the numjau column
 * @method CadetentQuery groupByTammet() Group by the tammet column
 * @method CadetentQuery groupByNumlot() Group by the numlot column
 * @method CadetentQuery groupByFecela() Group by the fecela column
 * @method CadetentQuery groupByCodalt() Group by the codalt column
 * @method CadetentQuery groupById() Group by the id column
 *
 * @method CadetentQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CadetentQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CadetentQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Cadetent findOne(PropelPDO $con = null) Return the first Cadetent matching the query
 * @method Cadetent findOneOrCreate(PropelPDO $con = null) Return the first Cadetent matching the query, or a new Cadetent object populated from the query conditions when no match is found
 *
 * @method Cadetent findOneByRcpart(string $rcpart) Return the first Cadetent filtered by the rcpart column
 * @method Cadetent findOneByCodart(string $codart) Return the first Cadetent filtered by the codart column
 * @method Cadetent findOneByCanrec(string $canrec) Return the first Cadetent filtered by the canrec column
 * @method Cadetent findOneByMontot(string $montot) Return the first Cadetent filtered by the montot column
 * @method Cadetent findOneByCosart(string $cosart) Return the first Cadetent filtered by the cosart column
 * @method Cadetent findOneByCodalm(string $codalm) Return the first Cadetent filtered by the codalm column
 * @method Cadetent findOneByCodubi(string $codubi) Return the first Cadetent filtered by the codubi column
 * @method Cadetent findOneByFecven(string $fecven) Return the first Cadetent filtered by the fecven column
 * @method Cadetent findOneByNumjau(string $numjau) Return the first Cadetent filtered by the numjau column
 * @method Cadetent findOneByTammet(string $tammet) Return the first Cadetent filtered by the tammet column
 * @method Cadetent findOneByNumlot(string $numlot) Return the first Cadetent filtered by the numlot column
 * @method Cadetent findOneByFecela(string $fecela) Return the first Cadetent filtered by the fecela column
 * @method Cadetent findOneByCodalt(string $codalt) Return the first Cadetent filtered by the codalt column
 *
 * @method array findByRcpart(string $rcpart) Return Cadetent objects filtered by the rcpart column
 * @method array findByCodart(string $codart) Return Cadetent objects filtered by the codart column
 * @method array findByCanrec(string $canrec) Return Cadetent objects filtered by the canrec column
 * @method array findByMontot(string $montot) Return Cadetent objects filtered by the montot column
 * @method array findByCosart(string $cosart) Return Cadetent objects filtered by the cosart column
 * @method array findByCodalm(string $codalm) Return Cadetent objects filtered by the codalm column
 * @method array findByCodubi(string $codubi) Return Cadetent objects filtered by the codubi column
 * @method array findByFecven(string $fecven) Return Cadetent objects filtered by the fecven column
 * @method array findByNumjau(string $numjau) Return Cadetent objects filtered by the numjau column
 * @method array findByTammet(string $tammet) Return Cadetent objects filtered by the tammet column
 * @method array findByNumlot(string $numlot) Return Cadetent objects filtered by the numlot column
 * @method array findByFecela(string $fecela) Return Cadetent objects filtered by the fecela column
 * @method array findByCodalt(string $codalt) Return Cadetent objects filtered by the codalt column
 * @method array findById(int $id) Return Cadetent objects filtered by the id column
 *
 * @package    propel.generator.lib.model.compras.om
 */
abstract class BaseCadetentQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCadetentQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Cadetent', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CadetentQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CadetentQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CadetentQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CadetentQuery) {
            return $criteria;
        }
        $query = new CadetentQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Cadetent|Cadetent[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CadetentPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CadetentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cadetent A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cadetent A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "rcpart", "codart", "canrec", "montot", "cosart", "codalm", "codubi", "fecven", "numjau", "tammet", "numlot", "fecela", "codalt", "id" FROM "cadetent" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Cadetent();
            $obj->hydrate($row);
            CadetentPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Cadetent|Cadetent[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Cadetent[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CadetentPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CadetentPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the rcpart column
     *
     * Example usage:
     * <code>
     * $query->filterByRcpart('fooValue');   // WHERE rcpart = 'fooValue'
     * $query->filterByRcpart('%fooValue%'); // WHERE rcpart LIKE '%fooValue%'
     * </code>
     *
     * @param     string $rcpart The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterByRcpart($rcpart = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($rcpart)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $rcpart)) {
                $rcpart = str_replace('*', '%', $rcpart);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CadetentPeer::RCPART, $rcpart, $comparison);
    }

    /**
     * Filter the query on the codart column
     *
     * Example usage:
     * <code>
     * $query->filterByCodart('fooValue');   // WHERE codart = 'fooValue'
     * $query->filterByCodart('%fooValue%'); // WHERE codart LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codart The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterByCodart($codart = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codart)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codart)) {
                $codart = str_replace('*', '%', $codart);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CadetentPeer::CODART, $codart, $comparison);
    }

    /**
     * Filter the query on the canrec column
     *
     * Example usage:
     * <code>
     * $query->filterByCanrec(1234); // WHERE canrec = 1234
     * $query->filterByCanrec(array(12, 34)); // WHERE canrec IN (12, 34)
     * $query->filterByCanrec(array('min' => 12)); // WHERE canrec >= 12
     * $query->filterByCanrec(array('max' => 12)); // WHERE canrec <= 12
     * </code>
     *
     * @param     mixed $canrec The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterByCanrec($canrec = null, $comparison = null)
    {
        if (is_array($canrec)) {
            $useMinMax = false;
            if (isset($canrec['min'])) {
                $this->addUsingAlias(CadetentPeer::CANREC, $canrec['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($canrec['max'])) {
                $this->addUsingAlias(CadetentPeer::CANREC, $canrec['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CadetentPeer::CANREC, $canrec, $comparison);
    }

    /**
     * Filter the query on the montot column
     *
     * Example usage:
     * <code>
     * $query->filterByMontot(1234); // WHERE montot = 1234
     * $query->filterByMontot(array(12, 34)); // WHERE montot IN (12, 34)
     * $query->filterByMontot(array('min' => 12)); // WHERE montot >= 12
     * $query->filterByMontot(array('max' => 12)); // WHERE montot <= 12
     * </code>
     *
     * @param     mixed $montot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterByMontot($montot = null, $comparison = null)
    {
        if (is_array($montot)) {
            $useMinMax = false;
            if (isset($montot['min'])) {
                $this->addUsingAlias(CadetentPeer::MONTOT, $montot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montot['max'])) {
                $this->addUsingAlias(CadetentPeer::MONTOT, $montot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CadetentPeer::MONTOT, $montot, $comparison);
    }

    /**
     * Filter the query on the cosart column
     *
     * Example usage:
     * <code>
     * $query->filterByCosart(1234); // WHERE cosart = 1234
     * $query->filterByCosart(array(12, 34)); // WHERE cosart IN (12, 34)
     * $query->filterByCosart(array('min' => 12)); // WHERE cosart >= 12
     * $query->filterByCosart(array('max' => 12)); // WHERE cosart <= 12
     * </code>
     *
     * @param     mixed $cosart The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterByCosart($cosart = null, $comparison = null)
    {
        if (is_array($cosart)) {
            $useMinMax = false;
            if (isset($cosart['min'])) {
                $this->addUsingAlias(CadetentPeer::COSART, $cosart['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cosart['max'])) {
                $this->addUsingAlias(CadetentPeer::COSART, $cosart['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CadetentPeer::COSART, $cosart, $comparison);
    }

    /**
     * Filter the query on the codalm column
     *
     * Example usage:
     * <code>
     * $query->filterByCodalm('fooValue');   // WHERE codalm = 'fooValue'
     * $query->filterByCodalm('%fooValue%'); // WHERE codalm LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codalm The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterByCodalm($codalm = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codalm)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codalm)) {
                $codalm = str_replace('*', '%', $codalm);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CadetentPeer::CODALM, $codalm, $comparison);
    }

    /**
     * Filter the query on the codubi column
     *
     * Example usage:
     * <code>
     * $query->filterByCodubi('fooValue');   // WHERE codubi = 'fooValue'
     * $query->filterByCodubi('%fooValue%'); // WHERE codubi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codubi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterByCodubi($codubi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codubi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codubi)) {
                $codubi = str_replace('*', '%', $codubi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CadetentPeer::CODUBI, $codubi, $comparison);
    }

    /**
     * Filter the query on the fecven column
     *
     * Example usage:
     * <code>
     * $query->filterByFecven('2011-03-14'); // WHERE fecven = '2011-03-14'
     * $query->filterByFecven('now'); // WHERE fecven = '2011-03-14'
     * $query->filterByFecven(array('max' => 'yesterday')); // WHERE fecven > '2011-03-13'
     * </code>
     *
     * @param     mixed $fecven The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterByFecven($fecven = null, $comparison = null)
    {
        if (is_array($fecven)) {
            $useMinMax = false;
            if (isset($fecven['min'])) {
                $this->addUsingAlias(CadetentPeer::FECVEN, $fecven['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fecven['max'])) {
                $this->addUsingAlias(CadetentPeer::FECVEN, $fecven['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CadetentPeer::FECVEN, $fecven, $comparison);
    }

    /**
     * Filter the query on the numjau column
     *
     * Example usage:
     * <code>
     * $query->filterByNumjau(1234); // WHERE numjau = 1234
     * $query->filterByNumjau(array(12, 34)); // WHERE numjau IN (12, 34)
     * $query->filterByNumjau(array('min' => 12)); // WHERE numjau >= 12
     * $query->filterByNumjau(array('max' => 12)); // WHERE numjau <= 12
     * </code>
     *
     * @param     mixed $numjau The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterByNumjau($numjau = null, $comparison = null)
    {
        if (is_array($numjau)) {
            $useMinMax = false;
            if (isset($numjau['min'])) {
                $this->addUsingAlias(CadetentPeer::NUMJAU, $numjau['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numjau['max'])) {
                $this->addUsingAlias(CadetentPeer::NUMJAU, $numjau['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CadetentPeer::NUMJAU, $numjau, $comparison);
    }

    /**
     * Filter the query on the tammet column
     *
     * Example usage:
     * <code>
     * $query->filterByTammet(1234); // WHERE tammet = 1234
     * $query->filterByTammet(array(12, 34)); // WHERE tammet IN (12, 34)
     * $query->filterByTammet(array('min' => 12)); // WHERE tammet >= 12
     * $query->filterByTammet(array('max' => 12)); // WHERE tammet <= 12
     * </code>
     *
     * @param     mixed $tammet The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterByTammet($tammet = null, $comparison = null)
    {
        if (is_array($tammet)) {
            $useMinMax = false;
            if (isset($tammet['min'])) {
                $this->addUsingAlias(CadetentPeer::TAMMET, $tammet['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tammet['max'])) {
                $this->addUsingAlias(CadetentPeer::TAMMET, $tammet['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CadetentPeer::TAMMET, $tammet, $comparison);
    }

    /**
     * Filter the query on the numlot column
     *
     * Example usage:
     * <code>
     * $query->filterByNumlot('fooValue');   // WHERE numlot = 'fooValue'
     * $query->filterByNumlot('%fooValue%'); // WHERE numlot LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numlot The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterByNumlot($numlot = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numlot)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numlot)) {
                $numlot = str_replace('*', '%', $numlot);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CadetentPeer::NUMLOT, $numlot, $comparison);
    }

    /**
     * Filter the query on the fecela column
     *
     * Example usage:
     * <code>
     * $query->filterByFecela('2011-03-14'); // WHERE fecela = '2011-03-14'
     * $query->filterByFecela('now'); // WHERE fecela = '2011-03-14'
     * $query->filterByFecela(array('max' => 'yesterday')); // WHERE fecela > '2011-03-13'
     * </code>
     *
     * @param     mixed $fecela The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterByFecela($fecela = null, $comparison = null)
    {
        if (is_array($fecela)) {
            $useMinMax = false;
            if (isset($fecela['min'])) {
                $this->addUsingAlias(CadetentPeer::FECELA, $fecela['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fecela['max'])) {
                $this->addUsingAlias(CadetentPeer::FECELA, $fecela['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CadetentPeer::FECELA, $fecela, $comparison);
    }

    /**
     * Filter the query on the codalt column
     *
     * Example usage:
     * <code>
     * $query->filterByCodalt('fooValue');   // WHERE codalt = 'fooValue'
     * $query->filterByCodalt('%fooValue%'); // WHERE codalt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codalt The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterByCodalt($codalt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codalt)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codalt)) {
                $codalt = str_replace('*', '%', $codalt);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CadetentPeer::CODALT, $codalt, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CadetentPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CadetentPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CadetentPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Cadetent $cadetent Object to remove from the list of results
     *
     * @return CadetentQuery The current query, for fluid interface
     */
    public function prune($cadetent = null)
    {
        if ($cadetent) {
            $this->addUsingAlias(CadetentPeer::ID, $cadetent->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
