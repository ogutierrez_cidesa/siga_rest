<?php


/**
 * Base class that represents a query for the 'cadefcla' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:46 2015
 *
 * @method CadefclaQuery orderByCodcla($order = Criteria::ASC) Order by the codcla column
 * @method CadefclaQuery orderByDescla($order = Criteria::ASC) Order by the descla column
 * @method CadefclaQuery orderByTipcla($order = Criteria::ASC) Order by the tipcla column
 * @method CadefclaQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method CadefclaQuery groupByCodcla() Group by the codcla column
 * @method CadefclaQuery groupByDescla() Group by the descla column
 * @method CadefclaQuery groupByTipcla() Group by the tipcla column
 * @method CadefclaQuery groupById() Group by the id column
 *
 * @method CadefclaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CadefclaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CadefclaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Cadefcla findOne(PropelPDO $con = null) Return the first Cadefcla matching the query
 * @method Cadefcla findOneOrCreate(PropelPDO $con = null) Return the first Cadefcla matching the query, or a new Cadefcla object populated from the query conditions when no match is found
 *
 * @method Cadefcla findOneByCodcla(string $codcla) Return the first Cadefcla filtered by the codcla column
 * @method Cadefcla findOneByDescla(string $descla) Return the first Cadefcla filtered by the descla column
 * @method Cadefcla findOneByTipcla(string $tipcla) Return the first Cadefcla filtered by the tipcla column
 *
 * @method array findByCodcla(string $codcla) Return Cadefcla objects filtered by the codcla column
 * @method array findByDescla(string $descla) Return Cadefcla objects filtered by the descla column
 * @method array findByTipcla(string $tipcla) Return Cadefcla objects filtered by the tipcla column
 * @method array findById(int $id) Return Cadefcla objects filtered by the id column
 *
 * @package    propel.generator.lib.model.compras.om
 */
abstract class BaseCadefclaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCadefclaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Cadefcla', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CadefclaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CadefclaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CadefclaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CadefclaQuery) {
            return $criteria;
        }
        $query = new CadefclaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Cadefcla|Cadefcla[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CadefclaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CadefclaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cadefcla A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cadefcla A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "codcla", "descla", "tipcla", "id" FROM "cadefcla" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Cadefcla();
            $obj->hydrate($row);
            CadefclaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Cadefcla|Cadefcla[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Cadefcla[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CadefclaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CadefclaPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CadefclaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CadefclaPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the codcla column
     *
     * Example usage:
     * <code>
     * $query->filterByCodcla('fooValue');   // WHERE codcla = 'fooValue'
     * $query->filterByCodcla('%fooValue%'); // WHERE codcla LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codcla The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadefclaQuery The current query, for fluid interface
     */
    public function filterByCodcla($codcla = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codcla)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codcla)) {
                $codcla = str_replace('*', '%', $codcla);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CadefclaPeer::CODCLA, $codcla, $comparison);
    }

    /**
     * Filter the query on the descla column
     *
     * Example usage:
     * <code>
     * $query->filterByDescla('fooValue');   // WHERE descla = 'fooValue'
     * $query->filterByDescla('%fooValue%'); // WHERE descla LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descla The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadefclaQuery The current query, for fluid interface
     */
    public function filterByDescla($descla = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descla)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $descla)) {
                $descla = str_replace('*', '%', $descla);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CadefclaPeer::DESCLA, $descla, $comparison);
    }

    /**
     * Filter the query on the tipcla column
     *
     * Example usage:
     * <code>
     * $query->filterByTipcla('fooValue');   // WHERE tipcla = 'fooValue'
     * $query->filterByTipcla('%fooValue%'); // WHERE tipcla LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipcla The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadefclaQuery The current query, for fluid interface
     */
    public function filterByTipcla($tipcla = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipcla)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tipcla)) {
                $tipcla = str_replace('*', '%', $tipcla);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CadefclaPeer::TIPCLA, $tipcla, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadefclaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CadefclaPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CadefclaPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CadefclaPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Cadefcla $cadefcla Object to remove from the list of results
     *
     * @return CadefclaQuery The current query, for fluid interface
     */
    public function prune($cadefcla = null)
    {
        if ($cadefcla) {
            $this->addUsingAlias(CadefclaPeer::ID, $cadefcla->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
