<?php


/**
 * Base class that represents a query for the 'cadetfirdocpre' table.
 *
 * Tabla para el detalle de  firmas para  documentos preimpresos
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:47 2015
 *
 * @method CadetfirdocpreQuery orderByCoddirec($order = Criteria::ASC) Order by the coddirec column
 * @method CadetfirdocpreQuery orderByTipdoc($order = Criteria::ASC) Order by the tipdoc column
 * @method CadetfirdocpreQuery orderByNumfir($order = Criteria::ASC) Order by the numfir column
 * @method CadetfirdocpreQuery orderByNomfir($order = Criteria::ASC) Order by the nomfir column
 * @method CadetfirdocpreQuery orderByCarfir($order = Criteria::ASC) Order by the carfir column
 * @method CadetfirdocpreQuery orderByObsfir($order = Criteria::ASC) Order by the obsfir column
 * @method CadetfirdocpreQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method CadetfirdocpreQuery groupByCoddirec() Group by the coddirec column
 * @method CadetfirdocpreQuery groupByTipdoc() Group by the tipdoc column
 * @method CadetfirdocpreQuery groupByNumfir() Group by the numfir column
 * @method CadetfirdocpreQuery groupByNomfir() Group by the nomfir column
 * @method CadetfirdocpreQuery groupByCarfir() Group by the carfir column
 * @method CadetfirdocpreQuery groupByObsfir() Group by the obsfir column
 * @method CadetfirdocpreQuery groupById() Group by the id column
 *
 * @method CadetfirdocpreQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CadetfirdocpreQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CadetfirdocpreQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Cadetfirdocpre findOne(PropelPDO $con = null) Return the first Cadetfirdocpre matching the query
 * @method Cadetfirdocpre findOneOrCreate(PropelPDO $con = null) Return the first Cadetfirdocpre matching the query, or a new Cadetfirdocpre object populated from the query conditions when no match is found
 *
 * @method Cadetfirdocpre findOneByCoddirec(string $coddirec) Return the first Cadetfirdocpre filtered by the coddirec column
 * @method Cadetfirdocpre findOneByTipdoc(string $tipdoc) Return the first Cadetfirdocpre filtered by the tipdoc column
 * @method Cadetfirdocpre findOneByNumfir(int $numfir) Return the first Cadetfirdocpre filtered by the numfir column
 * @method Cadetfirdocpre findOneByNomfir(string $nomfir) Return the first Cadetfirdocpre filtered by the nomfir column
 * @method Cadetfirdocpre findOneByCarfir(string $carfir) Return the first Cadetfirdocpre filtered by the carfir column
 * @method Cadetfirdocpre findOneByObsfir(string $obsfir) Return the first Cadetfirdocpre filtered by the obsfir column
 *
 * @method array findByCoddirec(string $coddirec) Return Cadetfirdocpre objects filtered by the coddirec column
 * @method array findByTipdoc(string $tipdoc) Return Cadetfirdocpre objects filtered by the tipdoc column
 * @method array findByNumfir(int $numfir) Return Cadetfirdocpre objects filtered by the numfir column
 * @method array findByNomfir(string $nomfir) Return Cadetfirdocpre objects filtered by the nomfir column
 * @method array findByCarfir(string $carfir) Return Cadetfirdocpre objects filtered by the carfir column
 * @method array findByObsfir(string $obsfir) Return Cadetfirdocpre objects filtered by the obsfir column
 * @method array findById(int $id) Return Cadetfirdocpre objects filtered by the id column
 *
 * @package    propel.generator.lib.model.compras.om
 */
abstract class BaseCadetfirdocpreQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCadetfirdocpreQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Cadetfirdocpre', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CadetfirdocpreQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CadetfirdocpreQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CadetfirdocpreQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CadetfirdocpreQuery) {
            return $criteria;
        }
        $query = new CadetfirdocpreQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Cadetfirdocpre|Cadetfirdocpre[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CadetfirdocprePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CadetfirdocprePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cadetfirdocpre A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cadetfirdocpre A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "coddirec", "tipdoc", "numfir", "nomfir", "carfir", "obsfir", "id" FROM "cadetfirdocpre" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Cadetfirdocpre();
            $obj->hydrate($row);
            CadetfirdocprePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Cadetfirdocpre|Cadetfirdocpre[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Cadetfirdocpre[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CadetfirdocpreQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CadetfirdocprePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CadetfirdocpreQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CadetfirdocprePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the coddirec column
     *
     * Example usage:
     * <code>
     * $query->filterByCoddirec('fooValue');   // WHERE coddirec = 'fooValue'
     * $query->filterByCoddirec('%fooValue%'); // WHERE coddirec LIKE '%fooValue%'
     * </code>
     *
     * @param     string $coddirec The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetfirdocpreQuery The current query, for fluid interface
     */
    public function filterByCoddirec($coddirec = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($coddirec)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $coddirec)) {
                $coddirec = str_replace('*', '%', $coddirec);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CadetfirdocprePeer::CODDIREC, $coddirec, $comparison);
    }

    /**
     * Filter the query on the tipdoc column
     *
     * Example usage:
     * <code>
     * $query->filterByTipdoc('fooValue');   // WHERE tipdoc = 'fooValue'
     * $query->filterByTipdoc('%fooValue%'); // WHERE tipdoc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipdoc The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetfirdocpreQuery The current query, for fluid interface
     */
    public function filterByTipdoc($tipdoc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipdoc)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tipdoc)) {
                $tipdoc = str_replace('*', '%', $tipdoc);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CadetfirdocprePeer::TIPDOC, $tipdoc, $comparison);
    }

    /**
     * Filter the query on the numfir column
     *
     * Example usage:
     * <code>
     * $query->filterByNumfir(1234); // WHERE numfir = 1234
     * $query->filterByNumfir(array(12, 34)); // WHERE numfir IN (12, 34)
     * $query->filterByNumfir(array('min' => 12)); // WHERE numfir >= 12
     * $query->filterByNumfir(array('max' => 12)); // WHERE numfir <= 12
     * </code>
     *
     * @param     mixed $numfir The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetfirdocpreQuery The current query, for fluid interface
     */
    public function filterByNumfir($numfir = null, $comparison = null)
    {
        if (is_array($numfir)) {
            $useMinMax = false;
            if (isset($numfir['min'])) {
                $this->addUsingAlias(CadetfirdocprePeer::NUMFIR, $numfir['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numfir['max'])) {
                $this->addUsingAlias(CadetfirdocprePeer::NUMFIR, $numfir['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CadetfirdocprePeer::NUMFIR, $numfir, $comparison);
    }

    /**
     * Filter the query on the nomfir column
     *
     * Example usage:
     * <code>
     * $query->filterByNomfir('fooValue');   // WHERE nomfir = 'fooValue'
     * $query->filterByNomfir('%fooValue%'); // WHERE nomfir LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomfir The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetfirdocpreQuery The current query, for fluid interface
     */
    public function filterByNomfir($nomfir = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomfir)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomfir)) {
                $nomfir = str_replace('*', '%', $nomfir);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CadetfirdocprePeer::NOMFIR, $nomfir, $comparison);
    }

    /**
     * Filter the query on the carfir column
     *
     * Example usage:
     * <code>
     * $query->filterByCarfir('fooValue');   // WHERE carfir = 'fooValue'
     * $query->filterByCarfir('%fooValue%'); // WHERE carfir LIKE '%fooValue%'
     * </code>
     *
     * @param     string $carfir The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetfirdocpreQuery The current query, for fluid interface
     */
    public function filterByCarfir($carfir = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($carfir)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $carfir)) {
                $carfir = str_replace('*', '%', $carfir);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CadetfirdocprePeer::CARFIR, $carfir, $comparison);
    }

    /**
     * Filter the query on the obsfir column
     *
     * Example usage:
     * <code>
     * $query->filterByObsfir('fooValue');   // WHERE obsfir = 'fooValue'
     * $query->filterByObsfir('%fooValue%'); // WHERE obsfir LIKE '%fooValue%'
     * </code>
     *
     * @param     string $obsfir The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetfirdocpreQuery The current query, for fluid interface
     */
    public function filterByObsfir($obsfir = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($obsfir)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $obsfir)) {
                $obsfir = str_replace('*', '%', $obsfir);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CadetfirdocprePeer::OBSFIR, $obsfir, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CadetfirdocpreQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CadetfirdocprePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CadetfirdocprePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CadetfirdocprePeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Cadetfirdocpre $cadetfirdocpre Object to remove from the list of results
     *
     * @return CadetfirdocpreQuery The current query, for fluid interface
     */
    public function prune($cadetfirdocpre = null)
    {
        if ($cadetfirdocpre) {
            $this->addUsingAlias(CadetfirdocprePeer::ID, $cadetfirdocpre->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
