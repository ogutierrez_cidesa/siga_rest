<?php


/**
 * Base class that represents a query for the 'caregiones' table.
 *
 * Tabla que contiene la información de las regiones
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:46 2015
 *
 * @method CaregionesQuery orderByCodreg($order = Criteria::ASC) Order by the codreg column
 * @method CaregionesQuery orderByNomreg($order = Criteria::ASC) Order by the nomreg column
 * @method CaregionesQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method CaregionesQuery groupByCodreg() Group by the codreg column
 * @method CaregionesQuery groupByNomreg() Group by the nomreg column
 * @method CaregionesQuery groupById() Group by the id column
 *
 * @method CaregionesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CaregionesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CaregionesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Caregiones findOne(PropelPDO $con = null) Return the first Caregiones matching the query
 * @method Caregiones findOneOrCreate(PropelPDO $con = null) Return the first Caregiones matching the query, or a new Caregiones object populated from the query conditions when no match is found
 *
 * @method Caregiones findOneByCodreg(string $codreg) Return the first Caregiones filtered by the codreg column
 * @method Caregiones findOneByNomreg(string $nomreg) Return the first Caregiones filtered by the nomreg column
 *
 * @method array findByCodreg(string $codreg) Return Caregiones objects filtered by the codreg column
 * @method array findByNomreg(string $nomreg) Return Caregiones objects filtered by the nomreg column
 * @method array findById(int $id) Return Caregiones objects filtered by the id column
 *
 * @package    propel.generator.lib.model.compras.om
 */
abstract class BaseCaregionesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCaregionesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Caregiones', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CaregionesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CaregionesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CaregionesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CaregionesQuery) {
            return $criteria;
        }
        $query = new CaregionesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Caregiones|Caregiones[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CaregionesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CaregionesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Caregiones A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Caregiones A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "codreg", "nomreg", "id" FROM "caregiones" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Caregiones();
            $obj->hydrate($row);
            CaregionesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Caregiones|Caregiones[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Caregiones[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CaregionesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CaregionesPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CaregionesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CaregionesPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the codreg column
     *
     * Example usage:
     * <code>
     * $query->filterByCodreg('fooValue');   // WHERE codreg = 'fooValue'
     * $query->filterByCodreg('%fooValue%'); // WHERE codreg LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codreg The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CaregionesQuery The current query, for fluid interface
     */
    public function filterByCodreg($codreg = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codreg)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codreg)) {
                $codreg = str_replace('*', '%', $codreg);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CaregionesPeer::CODREG, $codreg, $comparison);
    }

    /**
     * Filter the query on the nomreg column
     *
     * Example usage:
     * <code>
     * $query->filterByNomreg('fooValue');   // WHERE nomreg = 'fooValue'
     * $query->filterByNomreg('%fooValue%'); // WHERE nomreg LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomreg The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CaregionesQuery The current query, for fluid interface
     */
    public function filterByNomreg($nomreg = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomreg)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomreg)) {
                $nomreg = str_replace('*', '%', $nomreg);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CaregionesPeer::NOMREG, $nomreg, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CaregionesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CaregionesPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CaregionesPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaregionesPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Caregiones $caregiones Object to remove from the list of results
     *
     * @return CaregionesQuery The current query, for fluid interface
     */
    public function prune($caregiones = null)
    {
        if ($caregiones) {
            $this->addUsingAlias(CaregionesPeer::ID, $caregiones->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
