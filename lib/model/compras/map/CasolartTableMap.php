<?php



/**
 * This class defines the structure of the 'casolart' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:44 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.compras.map
 */
class CasolartTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.compras.map.CasolartTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('casolart');
        $this->setPhpName('Casolart');
        $this->setClassname('Casolart');
        $this->setPackage('lib.model.compras');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('reqart', 'Reqart', 'VARCHAR', true, 8, null);
        $this->addColumn('fecreq', 'Fecreq', 'DATE', true, null, null);
        $this->addColumn('desreq', 'Desreq', 'VARCHAR', false, 1000, null);
        $this->addColumn('monreq', 'Monreq', 'NUMERIC', false, 14, null);
        $this->addColumn('stareq', 'Stareq', 'VARCHAR', false, 1, null);
        $this->addColumn('motreq', 'Motreq', 'VARCHAR', false, 1000, null);
        $this->addColumn('benreq', 'Benreq', 'VARCHAR', false, 1000, null);
        $this->addColumn('mondes', 'Mondes', 'NUMERIC', false, 14, null);
        $this->addColumn('obsreq', 'Obsreq', 'VARCHAR', false, 5000, null);
        $this->addColumn('unires', 'Unires', 'VARCHAR', false, 32, null);
        $this->addForeignKey('tipmon', 'Tipmon', 'VARCHAR', 'tsdefmon', 'codmon', true, 3, null);
        $this->addColumn('valmon', 'Valmon', 'NUMERIC', false, 14, null);
        $this->addColumn('fecanu', 'Fecanu', 'DATE', false, null, null);
        $this->addColumn('codpro', 'Codpro', 'VARCHAR', false, 15, null);
        $this->addColumn('reqcom', 'Reqcom', 'VARCHAR', false, 8, null);
        $this->addColumn('tipfin', 'Tipfin', 'VARCHAR', false, 4, null);
        $this->addColumn('tipreq', 'Tipreq', 'CHAR', false, 1, null);
        $this->addColumn('aprreq', 'Aprreq', 'CHAR', false, 1, null);
        $this->addColumn('usuapr', 'Usuapr', 'CHAR', false, 1, null);
        $this->addColumn('fecapr', 'Fecapr', 'DATE', false, null, null);
        $this->addColumn('codemp', 'Codemp', 'VARCHAR', false, 16, null);
        $this->addColumn('codcen', 'Codcen', 'VARCHAR', false, 4, null);
        $this->addColumn('numproc', 'Numproc', 'VARCHAR', false, 30, null);
        $this->addColumn('codeve', 'Codeve', 'VARCHAR', false, 6, null);
        $this->addColumn('coddirec', 'Coddirec', 'VARCHAR', false, 4, null);
        $this->addColumn('coddivi', 'Coddivi', 'VARCHAR', false, 6, null);
        $this->addColumn('loguse', 'Loguse', 'VARCHAR', false, 50, null);
        $this->addColumn('fecana', 'Fecana', 'DATE', false, null, null);
        $this->addColumn('codubi', 'Codubi', 'VARCHAR', false, 30, null);
        $this->addColumn('nomben', 'Nomben', 'VARCHAR', false, 250, null);
        $this->addColumn('cedrif', 'Cedrif', 'VARCHAR', false, 15, null);
        $this->addColumn('fecsal', 'Fecsal', 'DATE', false, null, null);
        $this->addColumn('horsal', 'Horsal', 'VARCHAR', false, 10, null);
        $this->addColumn('fecreg', 'Fecreg', 'DATE', false, null, null);
        $this->addColumn('horreg', 'Horreg', 'VARCHAR', false, 10, null);
        $this->addColumn('codreg', 'Codreg', 'VARCHAR', false, 4, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Tsdefmon', 'Tsdefmon', RelationMap::MANY_TO_ONE, array('tipmon' => 'codmon', ), null, null);
    } // buildRelations()

} // CasolartTableMap
