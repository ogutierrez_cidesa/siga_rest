<?php



/**
 * This class defines the structure of the 'cargosol' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:44 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.compras.map
 */
class CargosolTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.compras.map.CargosolTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cargosol');
        $this->setPhpName('Cargosol');
        $this->setClassname('Cargosol');
        $this->setPackage('lib.model.compras');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('reqart', 'Reqart', 'VARCHAR', true, 8, null);
        $this->addForeignKey('codrgo', 'Codrgo', 'VARCHAR', 'carecarg', 'codrgo', true, 4, null);
        $this->addColumn('monrgo', 'Monrgo', 'NUMERIC', true, 14, null);
        $this->addColumn('tipdoc', 'Tipdoc', 'VARCHAR', true, 4, null);
        $this->addColumn('tipo', 'Tipo', 'VARCHAR', false, 1, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Carecarg', 'Carecarg', RelationMap::MANY_TO_ONE, array('codrgo' => 'codrgo', ), null, null);
    } // buildRelations()

} // CargosolTableMap
