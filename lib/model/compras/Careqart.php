<?php

/**
 * Subclass for representing a row from the 'careqart'.
 *
 *
 *
 * @package    Roraima
 * @subpackage lib.model
 * @author     $Author$ <desarrollo@cidesa.com.ve>
 * @version SVN: $Id$
 * 
 * @copyright  Copyright 2007, Cide S.A.
 * @license    http://opensource.org/licenses/gpl-2.0.php GPLv2
 */
class Careqart extends BaseCareqart
{
	protected $obj = array();
	protected $check = '';
	protected $check2 = '';
	protected $check3 = '';
	protected $totfil = '';

	public function getDesubi()
	{
            $catubibnu=H::getConfApp2('catubibnu', 'compras', 'almreq');
            if ($catubibnu=='S')
		return  Herramientas::getX('codubi','bnubica','desubi',self::getCodcatreq());
            else
                return  Herramientas::getX('codubi','bnubibie','desubi',self::getCodcatreq());
	}

  public function getDescen()
  {
	return Herramientas::getX('CODCEN','Cadefcen','Descen',self::getCodcen());
  }
  
    public function getNomalm()
  {
	return Herramientas::getX('CODALM','Cadefalm','Nomalm',$this->getCodalm());
  }

  public function getNomubi()
	{
		return Herramientas::getX('CODUBI','Cadefubi','Nomubi',$this->getCodubi());
	}

}
