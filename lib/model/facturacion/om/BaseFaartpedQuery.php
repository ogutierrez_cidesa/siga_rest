<?php


/**
 * Base class that represents a query for the 'faartped' table.
 *
 * null
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:47 2015
 *
 * @method FaartpedQuery orderByNroped($order = Criteria::ASC) Order by the nroped column
 * @method FaartpedQuery orderByCodart($order = Criteria::ASC) Order by the codart column
 * @method FaartpedQuery orderByCanord($order = Criteria::ASC) Order by the canord column
 * @method FaartpedQuery orderByCanaju($order = Criteria::ASC) Order by the canaju column
 * @method FaartpedQuery orderByCandes($order = Criteria::ASC) Order by the candes column
 * @method FaartpedQuery orderByCantot($order = Criteria::ASC) Order by the cantot column
 * @method FaartpedQuery orderByPreart($order = Criteria::ASC) Order by the preart column
 * @method FaartpedQuery orderByMondesc($order = Criteria::ASC) Order by the mondesc column
 * @method FaartpedQuery orderByMonrgo($order = Criteria::ASC) Order by the monrgo column
 * @method FaartpedQuery orderByTotart($order = Criteria::ASC) Order by the totart column
 * @method FaartpedQuery orderByBtucon($order = Criteria::ASC) Order by the btucon column
 * @method FaartpedQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method FaartpedQuery groupByNroped() Group by the nroped column
 * @method FaartpedQuery groupByCodart() Group by the codart column
 * @method FaartpedQuery groupByCanord() Group by the canord column
 * @method FaartpedQuery groupByCanaju() Group by the canaju column
 * @method FaartpedQuery groupByCandes() Group by the candes column
 * @method FaartpedQuery groupByCantot() Group by the cantot column
 * @method FaartpedQuery groupByPreart() Group by the preart column
 * @method FaartpedQuery groupByMondesc() Group by the mondesc column
 * @method FaartpedQuery groupByMonrgo() Group by the monrgo column
 * @method FaartpedQuery groupByTotart() Group by the totart column
 * @method FaartpedQuery groupByBtucon() Group by the btucon column
 * @method FaartpedQuery groupById() Group by the id column
 *
 * @method FaartpedQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FaartpedQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FaartpedQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method FaartpedQuery leftJoinFapedido($relationAlias = null) Adds a LEFT JOIN clause to the query using the Fapedido relation
 * @method FaartpedQuery rightJoinFapedido($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Fapedido relation
 * @method FaartpedQuery innerJoinFapedido($relationAlias = null) Adds a INNER JOIN clause to the query using the Fapedido relation
 *
 * @method Faartped findOne(PropelPDO $con = null) Return the first Faartped matching the query
 * @method Faartped findOneOrCreate(PropelPDO $con = null) Return the first Faartped matching the query, or a new Faartped object populated from the query conditions when no match is found
 *
 * @method Faartped findOneByNroped(string $nroped) Return the first Faartped filtered by the nroped column
 * @method Faartped findOneByCodart(string $codart) Return the first Faartped filtered by the codart column
 * @method Faartped findOneByCanord(string $canord) Return the first Faartped filtered by the canord column
 * @method Faartped findOneByCanaju(string $canaju) Return the first Faartped filtered by the canaju column
 * @method Faartped findOneByCandes(string $candes) Return the first Faartped filtered by the candes column
 * @method Faartped findOneByCantot(string $cantot) Return the first Faartped filtered by the cantot column
 * @method Faartped findOneByPreart(string $preart) Return the first Faartped filtered by the preart column
 * @method Faartped findOneByMondesc(string $mondesc) Return the first Faartped filtered by the mondesc column
 * @method Faartped findOneByMonrgo(string $monrgo) Return the first Faartped filtered by the monrgo column
 * @method Faartped findOneByTotart(string $totart) Return the first Faartped filtered by the totart column
 * @method Faartped findOneByBtucon(string $btucon) Return the first Faartped filtered by the btucon column
 *
 * @method array findByNroped(string $nroped) Return Faartped objects filtered by the nroped column
 * @method array findByCodart(string $codart) Return Faartped objects filtered by the codart column
 * @method array findByCanord(string $canord) Return Faartped objects filtered by the canord column
 * @method array findByCanaju(string $canaju) Return Faartped objects filtered by the canaju column
 * @method array findByCandes(string $candes) Return Faartped objects filtered by the candes column
 * @method array findByCantot(string $cantot) Return Faartped objects filtered by the cantot column
 * @method array findByPreart(string $preart) Return Faartped objects filtered by the preart column
 * @method array findByMondesc(string $mondesc) Return Faartped objects filtered by the mondesc column
 * @method array findByMonrgo(string $monrgo) Return Faartped objects filtered by the monrgo column
 * @method array findByTotart(string $totart) Return Faartped objects filtered by the totart column
 * @method array findByBtucon(string $btucon) Return Faartped objects filtered by the btucon column
 * @method array findById(int $id) Return Faartped objects filtered by the id column
 *
 * @package    propel.generator.lib.model.facturacion.om
 */
abstract class BaseFaartpedQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseFaartpedQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Faartped', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FaartpedQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FaartpedQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FaartpedQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FaartpedQuery) {
            return $criteria;
        }
        $query = new FaartpedQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Faartped|Faartped[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FaartpedPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FaartpedPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Faartped A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Faartped A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "nroped", "codart", "canord", "canaju", "candes", "cantot", "preart", "mondesc", "monrgo", "totart", "btucon", "id" FROM "faartped" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Faartped();
            $obj->hydrate($row);
            FaartpedPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Faartped|Faartped[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Faartped[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FaartpedPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FaartpedPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the nroped column
     *
     * Example usage:
     * <code>
     * $query->filterByNroped('fooValue');   // WHERE nroped = 'fooValue'
     * $query->filterByNroped('%fooValue%'); // WHERE nroped LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nroped The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function filterByNroped($nroped = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nroped)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nroped)) {
                $nroped = str_replace('*', '%', $nroped);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FaartpedPeer::NROPED, $nroped, $comparison);
    }

    /**
     * Filter the query on the codart column
     *
     * Example usage:
     * <code>
     * $query->filterByCodart('fooValue');   // WHERE codart = 'fooValue'
     * $query->filterByCodart('%fooValue%'); // WHERE codart LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codart The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function filterByCodart($codart = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codart)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codart)) {
                $codart = str_replace('*', '%', $codart);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FaartpedPeer::CODART, $codart, $comparison);
    }

    /**
     * Filter the query on the canord column
     *
     * Example usage:
     * <code>
     * $query->filterByCanord(1234); // WHERE canord = 1234
     * $query->filterByCanord(array(12, 34)); // WHERE canord IN (12, 34)
     * $query->filterByCanord(array('min' => 12)); // WHERE canord >= 12
     * $query->filterByCanord(array('max' => 12)); // WHERE canord <= 12
     * </code>
     *
     * @param     mixed $canord The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function filterByCanord($canord = null, $comparison = null)
    {
        if (is_array($canord)) {
            $useMinMax = false;
            if (isset($canord['min'])) {
                $this->addUsingAlias(FaartpedPeer::CANORD, $canord['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($canord['max'])) {
                $this->addUsingAlias(FaartpedPeer::CANORD, $canord['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaartpedPeer::CANORD, $canord, $comparison);
    }

    /**
     * Filter the query on the canaju column
     *
     * Example usage:
     * <code>
     * $query->filterByCanaju(1234); // WHERE canaju = 1234
     * $query->filterByCanaju(array(12, 34)); // WHERE canaju IN (12, 34)
     * $query->filterByCanaju(array('min' => 12)); // WHERE canaju >= 12
     * $query->filterByCanaju(array('max' => 12)); // WHERE canaju <= 12
     * </code>
     *
     * @param     mixed $canaju The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function filterByCanaju($canaju = null, $comparison = null)
    {
        if (is_array($canaju)) {
            $useMinMax = false;
            if (isset($canaju['min'])) {
                $this->addUsingAlias(FaartpedPeer::CANAJU, $canaju['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($canaju['max'])) {
                $this->addUsingAlias(FaartpedPeer::CANAJU, $canaju['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaartpedPeer::CANAJU, $canaju, $comparison);
    }

    /**
     * Filter the query on the candes column
     *
     * Example usage:
     * <code>
     * $query->filterByCandes(1234); // WHERE candes = 1234
     * $query->filterByCandes(array(12, 34)); // WHERE candes IN (12, 34)
     * $query->filterByCandes(array('min' => 12)); // WHERE candes >= 12
     * $query->filterByCandes(array('max' => 12)); // WHERE candes <= 12
     * </code>
     *
     * @param     mixed $candes The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function filterByCandes($candes = null, $comparison = null)
    {
        if (is_array($candes)) {
            $useMinMax = false;
            if (isset($candes['min'])) {
                $this->addUsingAlias(FaartpedPeer::CANDES, $candes['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($candes['max'])) {
                $this->addUsingAlias(FaartpedPeer::CANDES, $candes['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaartpedPeer::CANDES, $candes, $comparison);
    }

    /**
     * Filter the query on the cantot column
     *
     * Example usage:
     * <code>
     * $query->filterByCantot(1234); // WHERE cantot = 1234
     * $query->filterByCantot(array(12, 34)); // WHERE cantot IN (12, 34)
     * $query->filterByCantot(array('min' => 12)); // WHERE cantot >= 12
     * $query->filterByCantot(array('max' => 12)); // WHERE cantot <= 12
     * </code>
     *
     * @param     mixed $cantot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function filterByCantot($cantot = null, $comparison = null)
    {
        if (is_array($cantot)) {
            $useMinMax = false;
            if (isset($cantot['min'])) {
                $this->addUsingAlias(FaartpedPeer::CANTOT, $cantot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cantot['max'])) {
                $this->addUsingAlias(FaartpedPeer::CANTOT, $cantot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaartpedPeer::CANTOT, $cantot, $comparison);
    }

    /**
     * Filter the query on the preart column
     *
     * Example usage:
     * <code>
     * $query->filterByPreart(1234); // WHERE preart = 1234
     * $query->filterByPreart(array(12, 34)); // WHERE preart IN (12, 34)
     * $query->filterByPreart(array('min' => 12)); // WHERE preart >= 12
     * $query->filterByPreart(array('max' => 12)); // WHERE preart <= 12
     * </code>
     *
     * @param     mixed $preart The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function filterByPreart($preart = null, $comparison = null)
    {
        if (is_array($preart)) {
            $useMinMax = false;
            if (isset($preart['min'])) {
                $this->addUsingAlias(FaartpedPeer::PREART, $preart['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($preart['max'])) {
                $this->addUsingAlias(FaartpedPeer::PREART, $preart['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaartpedPeer::PREART, $preart, $comparison);
    }

    /**
     * Filter the query on the mondesc column
     *
     * Example usage:
     * <code>
     * $query->filterByMondesc(1234); // WHERE mondesc = 1234
     * $query->filterByMondesc(array(12, 34)); // WHERE mondesc IN (12, 34)
     * $query->filterByMondesc(array('min' => 12)); // WHERE mondesc >= 12
     * $query->filterByMondesc(array('max' => 12)); // WHERE mondesc <= 12
     * </code>
     *
     * @param     mixed $mondesc The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function filterByMondesc($mondesc = null, $comparison = null)
    {
        if (is_array($mondesc)) {
            $useMinMax = false;
            if (isset($mondesc['min'])) {
                $this->addUsingAlias(FaartpedPeer::MONDESC, $mondesc['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mondesc['max'])) {
                $this->addUsingAlias(FaartpedPeer::MONDESC, $mondesc['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaartpedPeer::MONDESC, $mondesc, $comparison);
    }

    /**
     * Filter the query on the monrgo column
     *
     * Example usage:
     * <code>
     * $query->filterByMonrgo(1234); // WHERE monrgo = 1234
     * $query->filterByMonrgo(array(12, 34)); // WHERE monrgo IN (12, 34)
     * $query->filterByMonrgo(array('min' => 12)); // WHERE monrgo >= 12
     * $query->filterByMonrgo(array('max' => 12)); // WHERE monrgo <= 12
     * </code>
     *
     * @param     mixed $monrgo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function filterByMonrgo($monrgo = null, $comparison = null)
    {
        if (is_array($monrgo)) {
            $useMinMax = false;
            if (isset($monrgo['min'])) {
                $this->addUsingAlias(FaartpedPeer::MONRGO, $monrgo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monrgo['max'])) {
                $this->addUsingAlias(FaartpedPeer::MONRGO, $monrgo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaartpedPeer::MONRGO, $monrgo, $comparison);
    }

    /**
     * Filter the query on the totart column
     *
     * Example usage:
     * <code>
     * $query->filterByTotart(1234); // WHERE totart = 1234
     * $query->filterByTotart(array(12, 34)); // WHERE totart IN (12, 34)
     * $query->filterByTotart(array('min' => 12)); // WHERE totart >= 12
     * $query->filterByTotart(array('max' => 12)); // WHERE totart <= 12
     * </code>
     *
     * @param     mixed $totart The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function filterByTotart($totart = null, $comparison = null)
    {
        if (is_array($totart)) {
            $useMinMax = false;
            if (isset($totart['min'])) {
                $this->addUsingAlias(FaartpedPeer::TOTART, $totart['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totart['max'])) {
                $this->addUsingAlias(FaartpedPeer::TOTART, $totart['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaartpedPeer::TOTART, $totart, $comparison);
    }

    /**
     * Filter the query on the btucon column
     *
     * Example usage:
     * <code>
     * $query->filterByBtucon(1234); // WHERE btucon = 1234
     * $query->filterByBtucon(array(12, 34)); // WHERE btucon IN (12, 34)
     * $query->filterByBtucon(array('min' => 12)); // WHERE btucon >= 12
     * $query->filterByBtucon(array('max' => 12)); // WHERE btucon <= 12
     * </code>
     *
     * @param     mixed $btucon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function filterByBtucon($btucon = null, $comparison = null)
    {
        if (is_array($btucon)) {
            $useMinMax = false;
            if (isset($btucon['min'])) {
                $this->addUsingAlias(FaartpedPeer::BTUCON, $btucon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($btucon['max'])) {
                $this->addUsingAlias(FaartpedPeer::BTUCON, $btucon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaartpedPeer::BTUCON, $btucon, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FaartpedPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FaartpedPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaartpedPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query by a related Fapedido object
     *
     * @param   Fapedido|PropelObjectCollection $fapedido The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FaartpedQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFapedido($fapedido, $comparison = null)
    {
        if ($fapedido instanceof Fapedido) {
            return $this
                ->addUsingAlias(FaartpedPeer::NROPED, $fapedido->getNroped(), $comparison);
        } elseif ($fapedido instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FaartpedPeer::NROPED, $fapedido->toKeyValue('PrimaryKey', 'Nroped'), $comparison);
        } else {
            throw new PropelException('filterByFapedido() only accepts arguments of type Fapedido or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Fapedido relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function joinFapedido($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Fapedido');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Fapedido');
        }

        return $this;
    }

    /**
     * Use the Fapedido relation Fapedido object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   FapedidoQuery A secondary query class using the current class as primary query
     */
    public function useFapedidoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFapedido($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Fapedido', 'FapedidoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Faartped $faartped Object to remove from the list of results
     *
     * @return FaartpedQuery The current query, for fluid interface
     */
    public function prune($faartped = null)
    {
        if ($faartped) {
            $this->addUsingAlias(FaartpedPeer::ID, $faartped->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
