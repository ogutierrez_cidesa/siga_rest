<?php


/**
 * Base class that represents a query for the 'fasubsistema' table.
 *
 * Contiene los registros de los Subsistemas
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:50 2015
 *
 * @method FasubsistemaQuery orderByCodsub($order = Criteria::ASC) Order by the codsub column
 * @method FasubsistemaQuery orderByNomsub($order = Criteria::ASC) Order by the nomsub column
 * @method FasubsistemaQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method FasubsistemaQuery groupByCodsub() Group by the codsub column
 * @method FasubsistemaQuery groupByNomsub() Group by the nomsub column
 * @method FasubsistemaQuery groupById() Group by the id column
 *
 * @method FasubsistemaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FasubsistemaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FasubsistemaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method FasubsistemaQuery leftJoinFainstedu($relationAlias = null) Adds a LEFT JOIN clause to the query using the Fainstedu relation
 * @method FasubsistemaQuery rightJoinFainstedu($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Fainstedu relation
 * @method FasubsistemaQuery innerJoinFainstedu($relationAlias = null) Adds a INNER JOIN clause to the query using the Fainstedu relation
 *
 * @method Fasubsistema findOne(PropelPDO $con = null) Return the first Fasubsistema matching the query
 * @method Fasubsistema findOneOrCreate(PropelPDO $con = null) Return the first Fasubsistema matching the query, or a new Fasubsistema object populated from the query conditions when no match is found
 *
 * @method Fasubsistema findOneByCodsub(string $codsub) Return the first Fasubsistema filtered by the codsub column
 * @method Fasubsistema findOneByNomsub(string $nomsub) Return the first Fasubsistema filtered by the nomsub column
 *
 * @method array findByCodsub(string $codsub) Return Fasubsistema objects filtered by the codsub column
 * @method array findByNomsub(string $nomsub) Return Fasubsistema objects filtered by the nomsub column
 * @method array findById(int $id) Return Fasubsistema objects filtered by the id column
 *
 * @package    propel.generator.lib.model.facturacion.om
 */
abstract class BaseFasubsistemaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseFasubsistemaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Fasubsistema', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FasubsistemaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FasubsistemaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FasubsistemaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FasubsistemaQuery) {
            return $criteria;
        }
        $query = new FasubsistemaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Fasubsistema|Fasubsistema[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FasubsistemaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FasubsistemaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Fasubsistema A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Fasubsistema A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "codsub", "nomsub", "id" FROM "fasubsistema" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Fasubsistema();
            $obj->hydrate($row);
            FasubsistemaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Fasubsistema|Fasubsistema[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Fasubsistema[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FasubsistemaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FasubsistemaPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FasubsistemaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FasubsistemaPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the codsub column
     *
     * Example usage:
     * <code>
     * $query->filterByCodsub('fooValue');   // WHERE codsub = 'fooValue'
     * $query->filterByCodsub('%fooValue%'); // WHERE codsub LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codsub The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FasubsistemaQuery The current query, for fluid interface
     */
    public function filterByCodsub($codsub = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codsub)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codsub)) {
                $codsub = str_replace('*', '%', $codsub);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FasubsistemaPeer::CODSUB, $codsub, $comparison);
    }

    /**
     * Filter the query on the nomsub column
     *
     * Example usage:
     * <code>
     * $query->filterByNomsub('fooValue');   // WHERE nomsub = 'fooValue'
     * $query->filterByNomsub('%fooValue%'); // WHERE nomsub LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomsub The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FasubsistemaQuery The current query, for fluid interface
     */
    public function filterByNomsub($nomsub = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomsub)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomsub)) {
                $nomsub = str_replace('*', '%', $nomsub);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FasubsistemaPeer::NOMSUB, $nomsub, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FasubsistemaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FasubsistemaPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FasubsistemaPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FasubsistemaPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query by a related Fainstedu object
     *
     * @param   Fainstedu|PropelObjectCollection $fainstedu  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FasubsistemaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFainstedu($fainstedu, $comparison = null)
    {
        if ($fainstedu instanceof Fainstedu) {
            return $this
                ->addUsingAlias(FasubsistemaPeer::CODSUB, $fainstedu->getCodsub(), $comparison);
        } elseif ($fainstedu instanceof PropelObjectCollection) {
            return $this
                ->useFainsteduQuery()
                ->filterByPrimaryKeys($fainstedu->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFainstedu() only accepts arguments of type Fainstedu or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Fainstedu relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FasubsistemaQuery The current query, for fluid interface
     */
    public function joinFainstedu($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Fainstedu');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Fainstedu');
        }

        return $this;
    }

    /**
     * Use the Fainstedu relation Fainstedu object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   FainsteduQuery A secondary query class using the current class as primary query
     */
    public function useFainsteduQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFainstedu($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Fainstedu', 'FainsteduQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Fasubsistema $fasubsistema Object to remove from the list of results
     *
     * @return FasubsistemaQuery The current query, for fluid interface
     */
    public function prune($fasubsistema = null)
    {
        if ($fasubsistema) {
            $this->addUsingAlias(FasubsistemaPeer::ID, $fasubsistema->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
