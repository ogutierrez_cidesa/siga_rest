<?php


/**
 * Base class that represents a query for the 'facajcorrelat' table.
 *
 * null
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:47 2015
 *
 * @method FacajcorrelatQuery orderByCodcaj($order = Criteria::ASC) Order by the codcaj column
 * @method FacajcorrelatQuery orderByCodfac($order = Criteria::ASC) Order by the codfac column
 * @method FacajcorrelatQuery orderByTipo($order = Criteria::ASC) Order by the tipo column
 * @method FacajcorrelatQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method FacajcorrelatQuery groupByCodcaj() Group by the codcaj column
 * @method FacajcorrelatQuery groupByCodfac() Group by the codfac column
 * @method FacajcorrelatQuery groupByTipo() Group by the tipo column
 * @method FacajcorrelatQuery groupById() Group by the id column
 *
 * @method FacajcorrelatQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FacajcorrelatQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FacajcorrelatQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Facajcorrelat findOne(PropelPDO $con = null) Return the first Facajcorrelat matching the query
 * @method Facajcorrelat findOneOrCreate(PropelPDO $con = null) Return the first Facajcorrelat matching the query, or a new Facajcorrelat object populated from the query conditions when no match is found
 *
 * @method Facajcorrelat findOneByCodcaj(string $codcaj) Return the first Facajcorrelat filtered by the codcaj column
 * @method Facajcorrelat findOneByCodfac(string $codfac) Return the first Facajcorrelat filtered by the codfac column
 * @method Facajcorrelat findOneByTipo(string $tipo) Return the first Facajcorrelat filtered by the tipo column
 *
 * @method array findByCodcaj(string $codcaj) Return Facajcorrelat objects filtered by the codcaj column
 * @method array findByCodfac(string $codfac) Return Facajcorrelat objects filtered by the codfac column
 * @method array findByTipo(string $tipo) Return Facajcorrelat objects filtered by the tipo column
 * @method array findById(int $id) Return Facajcorrelat objects filtered by the id column
 *
 * @package    propel.generator.lib.model.facturacion.om
 */
abstract class BaseFacajcorrelatQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseFacajcorrelatQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Facajcorrelat', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FacajcorrelatQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FacajcorrelatQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FacajcorrelatQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FacajcorrelatQuery) {
            return $criteria;
        }
        $query = new FacajcorrelatQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Facajcorrelat|Facajcorrelat[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FacajcorrelatPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FacajcorrelatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Facajcorrelat A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Facajcorrelat A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "codcaj", "codfac", "tipo", "id" FROM "facajcorrelat" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Facajcorrelat();
            $obj->hydrate($row);
            FacajcorrelatPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Facajcorrelat|Facajcorrelat[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Facajcorrelat[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FacajcorrelatQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FacajcorrelatPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FacajcorrelatQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FacajcorrelatPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the codcaj column
     *
     * Example usage:
     * <code>
     * $query->filterByCodcaj('fooValue');   // WHERE codcaj = 'fooValue'
     * $query->filterByCodcaj('%fooValue%'); // WHERE codcaj LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codcaj The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FacajcorrelatQuery The current query, for fluid interface
     */
    public function filterByCodcaj($codcaj = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codcaj)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codcaj)) {
                $codcaj = str_replace('*', '%', $codcaj);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacajcorrelatPeer::CODCAJ, $codcaj, $comparison);
    }

    /**
     * Filter the query on the codfac column
     *
     * Example usage:
     * <code>
     * $query->filterByCodfac('fooValue');   // WHERE codfac = 'fooValue'
     * $query->filterByCodfac('%fooValue%'); // WHERE codfac LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codfac The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FacajcorrelatQuery The current query, for fluid interface
     */
    public function filterByCodfac($codfac = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codfac)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codfac)) {
                $codfac = str_replace('*', '%', $codfac);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacajcorrelatPeer::CODFAC, $codfac, $comparison);
    }

    /**
     * Filter the query on the tipo column
     *
     * Example usage:
     * <code>
     * $query->filterByTipo('fooValue');   // WHERE tipo = 'fooValue'
     * $query->filterByTipo('%fooValue%'); // WHERE tipo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FacajcorrelatQuery The current query, for fluid interface
     */
    public function filterByTipo($tipo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tipo)) {
                $tipo = str_replace('*', '%', $tipo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacajcorrelatPeer::TIPO, $tipo, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FacajcorrelatQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FacajcorrelatPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FacajcorrelatPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacajcorrelatPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Facajcorrelat $facajcorrelat Object to remove from the list of results
     *
     * @return FacajcorrelatQuery The current query, for fluid interface
     */
    public function prune($facajcorrelat = null)
    {
        if ($facajcorrelat) {
            $this->addUsingAlias(FacajcorrelatPeer::ID, $facajcorrelat->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
