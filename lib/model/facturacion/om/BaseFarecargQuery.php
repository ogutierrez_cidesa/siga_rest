<?php


/**
 * Base class that represents a query for the 'farecarg' table.
 *
 * null
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:48 2015
 *
 * @method FarecargQuery orderByCodrgo($order = Criteria::ASC) Order by the codrgo column
 * @method FarecargQuery orderByNomrgo($order = Criteria::ASC) Order by the nomrgo column
 * @method FarecargQuery orderByCodpre($order = Criteria::ASC) Order by the codpre column
 * @method FarecargQuery orderByTiprgo($order = Criteria::ASC) Order by the tiprgo column
 * @method FarecargQuery orderByMonrgo($order = Criteria::ASC) Order by the monrgo column
 * @method FarecargQuery orderByCalcul($order = Criteria::ASC) Order by the calcul column
 * @method FarecargQuery orderByCodcta($order = Criteria::ASC) Order by the codcta column
 * @method FarecargQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method FarecargQuery groupByCodrgo() Group by the codrgo column
 * @method FarecargQuery groupByNomrgo() Group by the nomrgo column
 * @method FarecargQuery groupByCodpre() Group by the codpre column
 * @method FarecargQuery groupByTiprgo() Group by the tiprgo column
 * @method FarecargQuery groupByMonrgo() Group by the monrgo column
 * @method FarecargQuery groupByCalcul() Group by the calcul column
 * @method FarecargQuery groupByCodcta() Group by the codcta column
 * @method FarecargQuery groupById() Group by the id column
 *
 * @method FarecargQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FarecargQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FarecargQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Farecarg findOne(PropelPDO $con = null) Return the first Farecarg matching the query
 * @method Farecarg findOneOrCreate(PropelPDO $con = null) Return the first Farecarg matching the query, or a new Farecarg object populated from the query conditions when no match is found
 *
 * @method Farecarg findOneByCodrgo(string $codrgo) Return the first Farecarg filtered by the codrgo column
 * @method Farecarg findOneByNomrgo(string $nomrgo) Return the first Farecarg filtered by the nomrgo column
 * @method Farecarg findOneByCodpre(string $codpre) Return the first Farecarg filtered by the codpre column
 * @method Farecarg findOneByTiprgo(string $tiprgo) Return the first Farecarg filtered by the tiprgo column
 * @method Farecarg findOneByMonrgo(string $monrgo) Return the first Farecarg filtered by the monrgo column
 * @method Farecarg findOneByCalcul(string $calcul) Return the first Farecarg filtered by the calcul column
 * @method Farecarg findOneByCodcta(string $codcta) Return the first Farecarg filtered by the codcta column
 *
 * @method array findByCodrgo(string $codrgo) Return Farecarg objects filtered by the codrgo column
 * @method array findByNomrgo(string $nomrgo) Return Farecarg objects filtered by the nomrgo column
 * @method array findByCodpre(string $codpre) Return Farecarg objects filtered by the codpre column
 * @method array findByTiprgo(string $tiprgo) Return Farecarg objects filtered by the tiprgo column
 * @method array findByMonrgo(string $monrgo) Return Farecarg objects filtered by the monrgo column
 * @method array findByCalcul(string $calcul) Return Farecarg objects filtered by the calcul column
 * @method array findByCodcta(string $codcta) Return Farecarg objects filtered by the codcta column
 * @method array findById(int $id) Return Farecarg objects filtered by the id column
 *
 * @package    propel.generator.lib.model.facturacion.om
 */
abstract class BaseFarecargQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseFarecargQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Farecarg', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FarecargQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FarecargQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FarecargQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FarecargQuery) {
            return $criteria;
        }
        $query = new FarecargQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Farecarg|Farecarg[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FarecargPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FarecargPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Farecarg A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Farecarg A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "codrgo", "nomrgo", "codpre", "tiprgo", "monrgo", "calcul", "codcta", "id" FROM "farecarg" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Farecarg();
            $obj->hydrate($row);
            FarecargPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Farecarg|Farecarg[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Farecarg[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FarecargQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FarecargPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FarecargQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FarecargPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the codrgo column
     *
     * Example usage:
     * <code>
     * $query->filterByCodrgo('fooValue');   // WHERE codrgo = 'fooValue'
     * $query->filterByCodrgo('%fooValue%'); // WHERE codrgo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codrgo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FarecargQuery The current query, for fluid interface
     */
    public function filterByCodrgo($codrgo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codrgo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codrgo)) {
                $codrgo = str_replace('*', '%', $codrgo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FarecargPeer::CODRGO, $codrgo, $comparison);
    }

    /**
     * Filter the query on the nomrgo column
     *
     * Example usage:
     * <code>
     * $query->filterByNomrgo('fooValue');   // WHERE nomrgo = 'fooValue'
     * $query->filterByNomrgo('%fooValue%'); // WHERE nomrgo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomrgo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FarecargQuery The current query, for fluid interface
     */
    public function filterByNomrgo($nomrgo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomrgo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomrgo)) {
                $nomrgo = str_replace('*', '%', $nomrgo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FarecargPeer::NOMRGO, $nomrgo, $comparison);
    }

    /**
     * Filter the query on the codpre column
     *
     * Example usage:
     * <code>
     * $query->filterByCodpre('fooValue');   // WHERE codpre = 'fooValue'
     * $query->filterByCodpre('%fooValue%'); // WHERE codpre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codpre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FarecargQuery The current query, for fluid interface
     */
    public function filterByCodpre($codpre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codpre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codpre)) {
                $codpre = str_replace('*', '%', $codpre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FarecargPeer::CODPRE, $codpre, $comparison);
    }

    /**
     * Filter the query on the tiprgo column
     *
     * Example usage:
     * <code>
     * $query->filterByTiprgo('fooValue');   // WHERE tiprgo = 'fooValue'
     * $query->filterByTiprgo('%fooValue%'); // WHERE tiprgo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tiprgo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FarecargQuery The current query, for fluid interface
     */
    public function filterByTiprgo($tiprgo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tiprgo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tiprgo)) {
                $tiprgo = str_replace('*', '%', $tiprgo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FarecargPeer::TIPRGO, $tiprgo, $comparison);
    }

    /**
     * Filter the query on the monrgo column
     *
     * Example usage:
     * <code>
     * $query->filterByMonrgo(1234); // WHERE monrgo = 1234
     * $query->filterByMonrgo(array(12, 34)); // WHERE monrgo IN (12, 34)
     * $query->filterByMonrgo(array('min' => 12)); // WHERE monrgo >= 12
     * $query->filterByMonrgo(array('max' => 12)); // WHERE monrgo <= 12
     * </code>
     *
     * @param     mixed $monrgo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FarecargQuery The current query, for fluid interface
     */
    public function filterByMonrgo($monrgo = null, $comparison = null)
    {
        if (is_array($monrgo)) {
            $useMinMax = false;
            if (isset($monrgo['min'])) {
                $this->addUsingAlias(FarecargPeer::MONRGO, $monrgo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monrgo['max'])) {
                $this->addUsingAlias(FarecargPeer::MONRGO, $monrgo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FarecargPeer::MONRGO, $monrgo, $comparison);
    }

    /**
     * Filter the query on the calcul column
     *
     * Example usage:
     * <code>
     * $query->filterByCalcul('fooValue');   // WHERE calcul = 'fooValue'
     * $query->filterByCalcul('%fooValue%'); // WHERE calcul LIKE '%fooValue%'
     * </code>
     *
     * @param     string $calcul The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FarecargQuery The current query, for fluid interface
     */
    public function filterByCalcul($calcul = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($calcul)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $calcul)) {
                $calcul = str_replace('*', '%', $calcul);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FarecargPeer::CALCUL, $calcul, $comparison);
    }

    /**
     * Filter the query on the codcta column
     *
     * Example usage:
     * <code>
     * $query->filterByCodcta('fooValue');   // WHERE codcta = 'fooValue'
     * $query->filterByCodcta('%fooValue%'); // WHERE codcta LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codcta The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FarecargQuery The current query, for fluid interface
     */
    public function filterByCodcta($codcta = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codcta)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codcta)) {
                $codcta = str_replace('*', '%', $codcta);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FarecargPeer::CODCTA, $codcta, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FarecargQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FarecargPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FarecargPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FarecargPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Farecarg $farecarg Object to remove from the list of results
     *
     * @return FarecargQuery The current query, for fluid interface
     */
    public function prune($farecarg = null)
    {
        if ($farecarg) {
            $this->addUsingAlias(FarecargPeer::ID, $farecarg->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
