<?php


/**
 * Base class that represents a query for the 'fapropat' table.
 *
 * Contiene los registros de los Productos Patrocinantes
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:51 2015
 *
 * @method FapropatQuery orderByCodpropat($order = Criteria::ASC) Order by the codpropat column
 * @method FapropatQuery orderByDespropat($order = Criteria::ASC) Order by the despropat column
 * @method FapropatQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method FapropatQuery groupByCodpropat() Group by the codpropat column
 * @method FapropatQuery groupByDespropat() Group by the despropat column
 * @method FapropatQuery groupById() Group by the id column
 *
 * @method FapropatQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FapropatQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FapropatQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Fapropat findOne(PropelPDO $con = null) Return the first Fapropat matching the query
 * @method Fapropat findOneOrCreate(PropelPDO $con = null) Return the first Fapropat matching the query, or a new Fapropat object populated from the query conditions when no match is found
 *
 * @method Fapropat findOneByCodpropat(string $codpropat) Return the first Fapropat filtered by the codpropat column
 * @method Fapropat findOneByDespropat(string $despropat) Return the first Fapropat filtered by the despropat column
 *
 * @method array findByCodpropat(string $codpropat) Return Fapropat objects filtered by the codpropat column
 * @method array findByDespropat(string $despropat) Return Fapropat objects filtered by the despropat column
 * @method array findById(int $id) Return Fapropat objects filtered by the id column
 *
 * @package    propel.generator.lib.model.facturacion.om
 */
abstract class BaseFapropatQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseFapropatQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Fapropat', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FapropatQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FapropatQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FapropatQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FapropatQuery) {
            return $criteria;
        }
        $query = new FapropatQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Fapropat|Fapropat[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FapropatPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FapropatPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Fapropat A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Fapropat A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "codpropat", "despropat", "id" FROM "fapropat" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Fapropat();
            $obj->hydrate($row);
            FapropatPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Fapropat|Fapropat[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Fapropat[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FapropatQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FapropatPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FapropatQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FapropatPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the codpropat column
     *
     * Example usage:
     * <code>
     * $query->filterByCodpropat('fooValue');   // WHERE codpropat = 'fooValue'
     * $query->filterByCodpropat('%fooValue%'); // WHERE codpropat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codpropat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FapropatQuery The current query, for fluid interface
     */
    public function filterByCodpropat($codpropat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codpropat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codpropat)) {
                $codpropat = str_replace('*', '%', $codpropat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FapropatPeer::CODPROPAT, $codpropat, $comparison);
    }

    /**
     * Filter the query on the despropat column
     *
     * Example usage:
     * <code>
     * $query->filterByDespropat('fooValue');   // WHERE despropat = 'fooValue'
     * $query->filterByDespropat('%fooValue%'); // WHERE despropat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $despropat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FapropatQuery The current query, for fluid interface
     */
    public function filterByDespropat($despropat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($despropat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $despropat)) {
                $despropat = str_replace('*', '%', $despropat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FapropatPeer::DESPROPAT, $despropat, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FapropatQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FapropatPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FapropatPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FapropatPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Fapropat $fapropat Object to remove from the list of results
     *
     * @return FapropatQuery The current query, for fluid interface
     */
    public function prune($fapropat = null)
    {
        if ($fapropat) {
            $this->addUsingAlias(FapropatPeer::ID, $fapropat->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
