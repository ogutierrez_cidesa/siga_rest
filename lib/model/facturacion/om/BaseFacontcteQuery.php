<?php


/**
 * Base class that represents a query for the 'facontcte' table.
 *
 * null
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:47 2015
 *
 * @method FacontcteQuery orderByCodcli($order = Criteria::ASC) Order by the codcli column
 * @method FacontcteQuery orderByCodcont($order = Criteria::ASC) Order by the codcont column
 * @method FacontcteQuery orderByCorrcon($order = Criteria::ASC) Order by the corrcon column
 * @method FacontcteQuery orderByNomcont($order = Criteria::ASC) Order by the nomcont column
 * @method FacontcteQuery orderByCarcont($order = Criteria::ASC) Order by the carcont column
 * @method FacontcteQuery orderByCelcont($order = Criteria::ASC) Order by the celcont column
 * @method FacontcteQuery orderByTf1cont($order = Criteria::ASC) Order by the tf1cont column
 * @method FacontcteQuery orderByTf2cont($order = Criteria::ASC) Order by the tf2cont column
 * @method FacontcteQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method FacontcteQuery groupByCodcli() Group by the codcli column
 * @method FacontcteQuery groupByCodcont() Group by the codcont column
 * @method FacontcteQuery groupByCorrcon() Group by the corrcon column
 * @method FacontcteQuery groupByNomcont() Group by the nomcont column
 * @method FacontcteQuery groupByCarcont() Group by the carcont column
 * @method FacontcteQuery groupByCelcont() Group by the celcont column
 * @method FacontcteQuery groupByTf1cont() Group by the tf1cont column
 * @method FacontcteQuery groupByTf2cont() Group by the tf2cont column
 * @method FacontcteQuery groupById() Group by the id column
 *
 * @method FacontcteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FacontcteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FacontcteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Facontcte findOne(PropelPDO $con = null) Return the first Facontcte matching the query
 * @method Facontcte findOneOrCreate(PropelPDO $con = null) Return the first Facontcte matching the query, or a new Facontcte object populated from the query conditions when no match is found
 *
 * @method Facontcte findOneByCodcli(string $codcli) Return the first Facontcte filtered by the codcli column
 * @method Facontcte findOneByCodcont(string $codcont) Return the first Facontcte filtered by the codcont column
 * @method Facontcte findOneByCorrcon(string $corrcon) Return the first Facontcte filtered by the corrcon column
 * @method Facontcte findOneByNomcont(string $nomcont) Return the first Facontcte filtered by the nomcont column
 * @method Facontcte findOneByCarcont(string $carcont) Return the first Facontcte filtered by the carcont column
 * @method Facontcte findOneByCelcont(string $celcont) Return the first Facontcte filtered by the celcont column
 * @method Facontcte findOneByTf1cont(string $tf1cont) Return the first Facontcte filtered by the tf1cont column
 * @method Facontcte findOneByTf2cont(string $tf2cont) Return the first Facontcte filtered by the tf2cont column
 *
 * @method array findByCodcli(string $codcli) Return Facontcte objects filtered by the codcli column
 * @method array findByCodcont(string $codcont) Return Facontcte objects filtered by the codcont column
 * @method array findByCorrcon(string $corrcon) Return Facontcte objects filtered by the corrcon column
 * @method array findByNomcont(string $nomcont) Return Facontcte objects filtered by the nomcont column
 * @method array findByCarcont(string $carcont) Return Facontcte objects filtered by the carcont column
 * @method array findByCelcont(string $celcont) Return Facontcte objects filtered by the celcont column
 * @method array findByTf1cont(string $tf1cont) Return Facontcte objects filtered by the tf1cont column
 * @method array findByTf2cont(string $tf2cont) Return Facontcte objects filtered by the tf2cont column
 * @method array findById(int $id) Return Facontcte objects filtered by the id column
 *
 * @package    propel.generator.lib.model.facturacion.om
 */
abstract class BaseFacontcteQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseFacontcteQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Facontcte', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FacontcteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FacontcteQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FacontcteQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FacontcteQuery) {
            return $criteria;
        }
        $query = new FacontcteQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Facontcte|Facontcte[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FacontctePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FacontctePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Facontcte A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Facontcte A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "codcli", "codcont", "corrcon", "nomcont", "carcont", "celcont", "tf1cont", "tf2cont", "id" FROM "facontcte" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Facontcte();
            $obj->hydrate($row);
            FacontctePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Facontcte|Facontcte[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Facontcte[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FacontcteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FacontctePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FacontcteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FacontctePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the codcli column
     *
     * Example usage:
     * <code>
     * $query->filterByCodcli('fooValue');   // WHERE codcli = 'fooValue'
     * $query->filterByCodcli('%fooValue%'); // WHERE codcli LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codcli The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FacontcteQuery The current query, for fluid interface
     */
    public function filterByCodcli($codcli = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codcli)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codcli)) {
                $codcli = str_replace('*', '%', $codcli);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacontctePeer::CODCLI, $codcli, $comparison);
    }

    /**
     * Filter the query on the codcont column
     *
     * Example usage:
     * <code>
     * $query->filterByCodcont('fooValue');   // WHERE codcont = 'fooValue'
     * $query->filterByCodcont('%fooValue%'); // WHERE codcont LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codcont The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FacontcteQuery The current query, for fluid interface
     */
    public function filterByCodcont($codcont = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codcont)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codcont)) {
                $codcont = str_replace('*', '%', $codcont);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacontctePeer::CODCONT, $codcont, $comparison);
    }

    /**
     * Filter the query on the corrcon column
     *
     * Example usage:
     * <code>
     * $query->filterByCorrcon('fooValue');   // WHERE corrcon = 'fooValue'
     * $query->filterByCorrcon('%fooValue%'); // WHERE corrcon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $corrcon The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FacontcteQuery The current query, for fluid interface
     */
    public function filterByCorrcon($corrcon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($corrcon)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $corrcon)) {
                $corrcon = str_replace('*', '%', $corrcon);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacontctePeer::CORRCON, $corrcon, $comparison);
    }

    /**
     * Filter the query on the nomcont column
     *
     * Example usage:
     * <code>
     * $query->filterByNomcont('fooValue');   // WHERE nomcont = 'fooValue'
     * $query->filterByNomcont('%fooValue%'); // WHERE nomcont LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomcont The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FacontcteQuery The current query, for fluid interface
     */
    public function filterByNomcont($nomcont = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomcont)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomcont)) {
                $nomcont = str_replace('*', '%', $nomcont);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacontctePeer::NOMCONT, $nomcont, $comparison);
    }

    /**
     * Filter the query on the carcont column
     *
     * Example usage:
     * <code>
     * $query->filterByCarcont('fooValue');   // WHERE carcont = 'fooValue'
     * $query->filterByCarcont('%fooValue%'); // WHERE carcont LIKE '%fooValue%'
     * </code>
     *
     * @param     string $carcont The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FacontcteQuery The current query, for fluid interface
     */
    public function filterByCarcont($carcont = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($carcont)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $carcont)) {
                $carcont = str_replace('*', '%', $carcont);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacontctePeer::CARCONT, $carcont, $comparison);
    }

    /**
     * Filter the query on the celcont column
     *
     * Example usage:
     * <code>
     * $query->filterByCelcont('fooValue');   // WHERE celcont = 'fooValue'
     * $query->filterByCelcont('%fooValue%'); // WHERE celcont LIKE '%fooValue%'
     * </code>
     *
     * @param     string $celcont The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FacontcteQuery The current query, for fluid interface
     */
    public function filterByCelcont($celcont = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($celcont)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $celcont)) {
                $celcont = str_replace('*', '%', $celcont);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacontctePeer::CELCONT, $celcont, $comparison);
    }

    /**
     * Filter the query on the tf1cont column
     *
     * Example usage:
     * <code>
     * $query->filterByTf1cont('fooValue');   // WHERE tf1cont = 'fooValue'
     * $query->filterByTf1cont('%fooValue%'); // WHERE tf1cont LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tf1cont The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FacontcteQuery The current query, for fluid interface
     */
    public function filterByTf1cont($tf1cont = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tf1cont)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tf1cont)) {
                $tf1cont = str_replace('*', '%', $tf1cont);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacontctePeer::TF1CONT, $tf1cont, $comparison);
    }

    /**
     * Filter the query on the tf2cont column
     *
     * Example usage:
     * <code>
     * $query->filterByTf2cont('fooValue');   // WHERE tf2cont = 'fooValue'
     * $query->filterByTf2cont('%fooValue%'); // WHERE tf2cont LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tf2cont The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FacontcteQuery The current query, for fluid interface
     */
    public function filterByTf2cont($tf2cont = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tf2cont)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tf2cont)) {
                $tf2cont = str_replace('*', '%', $tf2cont);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacontctePeer::TF2CONT, $tf2cont, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FacontcteQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FacontctePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FacontctePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacontctePeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Facontcte $facontcte Object to remove from the list of results
     *
     * @return FacontcteQuery The current query, for fluid interface
     */
    public function prune($facontcte = null)
    {
        if ($facontcte) {
            $this->addUsingAlias(FacontctePeer::ID, $facontcte->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
