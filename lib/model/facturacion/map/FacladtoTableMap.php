<?php



/**
 * This class defines the structure of the 'facladto' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:47 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.facturacion.map
 */
class FacladtoTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.facturacion.map.FacladtoTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('facladto');
        $this->setPhpName('Facladto');
        $this->setClassname('Facladto');
        $this->setPackage('lib.model.facturacion');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('loguse', 'Loguse', 'VARCHAR', true, 8, null);
        $this->addColumn('descto', 'Descto', 'NUMERIC', true, 6, null);
        $this->addColumn('clave', 'Clave', 'VARCHAR', true, 8, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // FacladtoTableMap
