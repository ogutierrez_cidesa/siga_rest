<?php

/**
 * Subclass for representing a row from the 'fargoart'.
 *
 *
 *
 * @package    Roraima
 * @subpackage lib.model
 * @author     $Author: dmartinez $ <desarrollo@cidesa.com.ve>
 * @version SVN: $Id: Fargoart.php 50291 2012-12-10 21:33:35Z dmartinez $
 * 
 * @copyright  Copyright 2007, Cide S.A.
 * @license    http://opensource.org/licenses/gpl-2.0.php GPLv2
 */
class Fargoart extends BaseFargoart
{
  protected $nomrgo="";
  protected $codcta="";
  protected $recfij="";
  protected $tipo="";
  protected $monrgo2="0,00";

  public function afterHydrate()
  {
    $p= new Criteria();
    $p->add(FarecargPeer::CODRGO,self::getCodrgo());
    $result=FarecargPeer::doSelectOne($p);
    if ($result){
      $this->nomrgo=$result->getNomrgo();  
      if ($result->getCalcul()=='S')
        $this->recfij="Si";
      else
        $this->recfij="No";
      $this->codcta=$result->getCodcta();
      $this->tipo=$result->getTiprgo();
      $this->monrgo2=H::FormatoMonto($result->getMonrgo());
    }
    
  }

  /*public function getNomrgo()
  {
   return Herramientas::getX('CODRGO','Farecarg','Nomrgo',self::getCodrgo());
  }

  public function getRecfij()
  {
  	$re=Herramientas::getX('CODRGO','Farecarg','Calcul',self::getCodrgo());
  	if ($re=='S')
  	{
  		$valor="Si";
  	}else $valor="No";
   return $valor;
  }

  public function getCodcta()
  {
   return Herramientas::getX('CODRGO','Farecarg','Codcta',self::getCodrgo());
  }

  public function getTipo()
  {
   return Herramientas::getX('CODRGO','Farecarg','Tiprgo',self::getCodrgo());
  }

  public function getMonrgo2()
  {
   return number_format(Herramientas::getX('CODRGO','Farecarg','Monrgo',self::getCodrgo()), 2, ',', '.');
  }*/
}

