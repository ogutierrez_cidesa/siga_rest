<?php


/**
 * Base class that represents a query for the 'litippen' table.
 *
 * Tipos de penalizaciones manejables por el sistema
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:56 2015
 *
 * @method LitippenQuery orderByCodtippen($order = Criteria::ASC) Order by the codtippen column
 * @method LitippenQuery orderByNomtippen($order = Criteria::ASC) Order by the nomtippen column
 * @method LitippenQuery orderByDettippen($order = Criteria::ASC) Order by the dettippen column
 * @method LitippenQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method LitippenQuery groupByCodtippen() Group by the codtippen column
 * @method LitippenQuery groupByNomtippen() Group by the nomtippen column
 * @method LitippenQuery groupByDettippen() Group by the dettippen column
 * @method LitippenQuery groupById() Group by the id column
 *
 * @method LitippenQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LitippenQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LitippenQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method LitippenQuery leftJoinLipenalizaciones($relationAlias = null) Adds a LEFT JOIN clause to the query using the Lipenalizaciones relation
 * @method LitippenQuery rightJoinLipenalizaciones($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Lipenalizaciones relation
 * @method LitippenQuery innerJoinLipenalizaciones($relationAlias = null) Adds a INNER JOIN clause to the query using the Lipenalizaciones relation
 *
 * @method Litippen findOne(PropelPDO $con = null) Return the first Litippen matching the query
 * @method Litippen findOneOrCreate(PropelPDO $con = null) Return the first Litippen matching the query, or a new Litippen object populated from the query conditions when no match is found
 *
 * @method Litippen findOneByCodtippen(string $codtippen) Return the first Litippen filtered by the codtippen column
 * @method Litippen findOneByNomtippen(string $nomtippen) Return the first Litippen filtered by the nomtippen column
 * @method Litippen findOneByDettippen(string $dettippen) Return the first Litippen filtered by the dettippen column
 *
 * @method array findByCodtippen(string $codtippen) Return Litippen objects filtered by the codtippen column
 * @method array findByNomtippen(string $nomtippen) Return Litippen objects filtered by the nomtippen column
 * @method array findByDettippen(string $dettippen) Return Litippen objects filtered by the dettippen column
 * @method array findById(int $id) Return Litippen objects filtered by the id column
 *
 * @package    propel.generator.lib.model.licitaciones.om
 */
abstract class BaseLitippenQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLitippenQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Litippen', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LitippenQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LitippenQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LitippenQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LitippenQuery) {
            return $criteria;
        }
        $query = new LitippenQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Litippen|Litippen[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LitippenPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LitippenPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Litippen A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Litippen A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "codtippen", "nomtippen", "dettippen", "id" FROM "litippen" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Litippen();
            $obj->hydrate($row);
            LitippenPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Litippen|Litippen[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Litippen[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LitippenQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LitippenPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LitippenQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LitippenPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the codtippen column
     *
     * Example usage:
     * <code>
     * $query->filterByCodtippen('fooValue');   // WHERE codtippen = 'fooValue'
     * $query->filterByCodtippen('%fooValue%'); // WHERE codtippen LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codtippen The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LitippenQuery The current query, for fluid interface
     */
    public function filterByCodtippen($codtippen = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codtippen)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codtippen)) {
                $codtippen = str_replace('*', '%', $codtippen);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LitippenPeer::CODTIPPEN, $codtippen, $comparison);
    }

    /**
     * Filter the query on the nomtippen column
     *
     * Example usage:
     * <code>
     * $query->filterByNomtippen('fooValue');   // WHERE nomtippen = 'fooValue'
     * $query->filterByNomtippen('%fooValue%'); // WHERE nomtippen LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomtippen The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LitippenQuery The current query, for fluid interface
     */
    public function filterByNomtippen($nomtippen = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomtippen)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomtippen)) {
                $nomtippen = str_replace('*', '%', $nomtippen);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LitippenPeer::NOMTIPPEN, $nomtippen, $comparison);
    }

    /**
     * Filter the query on the dettippen column
     *
     * Example usage:
     * <code>
     * $query->filterByDettippen('fooValue');   // WHERE dettippen = 'fooValue'
     * $query->filterByDettippen('%fooValue%'); // WHERE dettippen LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dettippen The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LitippenQuery The current query, for fluid interface
     */
    public function filterByDettippen($dettippen = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dettippen)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dettippen)) {
                $dettippen = str_replace('*', '%', $dettippen);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LitippenPeer::DETTIPPEN, $dettippen, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LitippenQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(LitippenPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(LitippenPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LitippenPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query by a related Lipenalizaciones object
     *
     * @param   Lipenalizaciones|PropelObjectCollection $lipenalizaciones  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LitippenQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLipenalizaciones($lipenalizaciones, $comparison = null)
    {
        if ($lipenalizaciones instanceof Lipenalizaciones) {
            return $this
                ->addUsingAlias(LitippenPeer::CODTIPPEN, $lipenalizaciones->getCodtippen(), $comparison);
        } elseif ($lipenalizaciones instanceof PropelObjectCollection) {
            return $this
                ->useLipenalizacionesQuery()
                ->filterByPrimaryKeys($lipenalizaciones->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLipenalizaciones() only accepts arguments of type Lipenalizaciones or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Lipenalizaciones relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LitippenQuery The current query, for fluid interface
     */
    public function joinLipenalizaciones($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Lipenalizaciones');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Lipenalizaciones');
        }

        return $this;
    }

    /**
     * Use the Lipenalizaciones relation Lipenalizaciones object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   LipenalizacionesQuery A secondary query class using the current class as primary query
     */
    public function useLipenalizacionesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLipenalizaciones($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Lipenalizaciones', 'LipenalizacionesQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Litippen $litippen Object to remove from the list of results
     *
     * @return LitippenQuery The current query, for fluid interface
     */
    public function prune($litippen = null)
    {
        if ($litippen) {
            $this->addUsingAlias(LitippenPeer::ID, $litippen->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
