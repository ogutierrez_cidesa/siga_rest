<?php


/**
 * Base class that represents a query for the 'lianaofeleg' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:53 2015
 *
 * @method LianaofelegQuery orderByNumanaofe($order = Criteria::ASC) Order by the numanaofe column
 * @method LianaofelegQuery orderByCodcri($order = Criteria::ASC) Order by the codcri column
 * @method LianaofelegQuery orderByPunemp($order = Criteria::ASC) Order by the punemp column
 * @method LianaofelegQuery orderByPoremp($order = Criteria::ASC) Order by the poremp column
 * @method LianaofelegQuery orderByObserv($order = Criteria::ASC) Order by the observ column
 * @method LianaofelegQuery orderByTipconpub($order = Criteria::ASC) Order by the tipconpub column
 * @method LianaofelegQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method LianaofelegQuery groupByNumanaofe() Group by the numanaofe column
 * @method LianaofelegQuery groupByCodcri() Group by the codcri column
 * @method LianaofelegQuery groupByPunemp() Group by the punemp column
 * @method LianaofelegQuery groupByPoremp() Group by the poremp column
 * @method LianaofelegQuery groupByObserv() Group by the observ column
 * @method LianaofelegQuery groupByTipconpub() Group by the tipconpub column
 * @method LianaofelegQuery groupById() Group by the id column
 *
 * @method LianaofelegQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LianaofelegQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LianaofelegQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Lianaofeleg findOne(PropelPDO $con = null) Return the first Lianaofeleg matching the query
 * @method Lianaofeleg findOneOrCreate(PropelPDO $con = null) Return the first Lianaofeleg matching the query, or a new Lianaofeleg object populated from the query conditions when no match is found
 *
 * @method Lianaofeleg findOneByNumanaofe(string $numanaofe) Return the first Lianaofeleg filtered by the numanaofe column
 * @method Lianaofeleg findOneByCodcri(string $codcri) Return the first Lianaofeleg filtered by the codcri column
 * @method Lianaofeleg findOneByPunemp(string $punemp) Return the first Lianaofeleg filtered by the punemp column
 * @method Lianaofeleg findOneByPoremp(string $poremp) Return the first Lianaofeleg filtered by the poremp column
 * @method Lianaofeleg findOneByObserv(string $observ) Return the first Lianaofeleg filtered by the observ column
 * @method Lianaofeleg findOneByTipconpub(string $tipconpub) Return the first Lianaofeleg filtered by the tipconpub column
 *
 * @method array findByNumanaofe(string $numanaofe) Return Lianaofeleg objects filtered by the numanaofe column
 * @method array findByCodcri(string $codcri) Return Lianaofeleg objects filtered by the codcri column
 * @method array findByPunemp(string $punemp) Return Lianaofeleg objects filtered by the punemp column
 * @method array findByPoremp(string $poremp) Return Lianaofeleg objects filtered by the poremp column
 * @method array findByObserv(string $observ) Return Lianaofeleg objects filtered by the observ column
 * @method array findByTipconpub(string $tipconpub) Return Lianaofeleg objects filtered by the tipconpub column
 * @method array findById(int $id) Return Lianaofeleg objects filtered by the id column
 *
 * @package    propel.generator.lib.model.licitaciones.om
 */
abstract class BaseLianaofelegQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLianaofelegQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Lianaofeleg', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LianaofelegQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LianaofelegQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LianaofelegQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LianaofelegQuery) {
            return $criteria;
        }
        $query = new LianaofelegQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Lianaofeleg|Lianaofeleg[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LianaofelegPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LianaofelegPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Lianaofeleg A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Lianaofeleg A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "numanaofe", "codcri", "punemp", "poremp", "observ", "tipconpub", "id" FROM "lianaofeleg" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Lianaofeleg();
            $obj->hydrate($row);
            LianaofelegPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Lianaofeleg|Lianaofeleg[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Lianaofeleg[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LianaofelegQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LianaofelegPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LianaofelegQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LianaofelegPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the numanaofe column
     *
     * Example usage:
     * <code>
     * $query->filterByNumanaofe('fooValue');   // WHERE numanaofe = 'fooValue'
     * $query->filterByNumanaofe('%fooValue%'); // WHERE numanaofe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numanaofe The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LianaofelegQuery The current query, for fluid interface
     */
    public function filterByNumanaofe($numanaofe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numanaofe)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numanaofe)) {
                $numanaofe = str_replace('*', '%', $numanaofe);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LianaofelegPeer::NUMANAOFE, $numanaofe, $comparison);
    }

    /**
     * Filter the query on the codcri column
     *
     * Example usage:
     * <code>
     * $query->filterByCodcri('fooValue');   // WHERE codcri = 'fooValue'
     * $query->filterByCodcri('%fooValue%'); // WHERE codcri LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codcri The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LianaofelegQuery The current query, for fluid interface
     */
    public function filterByCodcri($codcri = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codcri)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codcri)) {
                $codcri = str_replace('*', '%', $codcri);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LianaofelegPeer::CODCRI, $codcri, $comparison);
    }

    /**
     * Filter the query on the punemp column
     *
     * Example usage:
     * <code>
     * $query->filterByPunemp(1234); // WHERE punemp = 1234
     * $query->filterByPunemp(array(12, 34)); // WHERE punemp IN (12, 34)
     * $query->filterByPunemp(array('min' => 12)); // WHERE punemp >= 12
     * $query->filterByPunemp(array('max' => 12)); // WHERE punemp <= 12
     * </code>
     *
     * @param     mixed $punemp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LianaofelegQuery The current query, for fluid interface
     */
    public function filterByPunemp($punemp = null, $comparison = null)
    {
        if (is_array($punemp)) {
            $useMinMax = false;
            if (isset($punemp['min'])) {
                $this->addUsingAlias(LianaofelegPeer::PUNEMP, $punemp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($punemp['max'])) {
                $this->addUsingAlias(LianaofelegPeer::PUNEMP, $punemp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LianaofelegPeer::PUNEMP, $punemp, $comparison);
    }

    /**
     * Filter the query on the poremp column
     *
     * Example usage:
     * <code>
     * $query->filterByPoremp(1234); // WHERE poremp = 1234
     * $query->filterByPoremp(array(12, 34)); // WHERE poremp IN (12, 34)
     * $query->filterByPoremp(array('min' => 12)); // WHERE poremp >= 12
     * $query->filterByPoremp(array('max' => 12)); // WHERE poremp <= 12
     * </code>
     *
     * @param     mixed $poremp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LianaofelegQuery The current query, for fluid interface
     */
    public function filterByPoremp($poremp = null, $comparison = null)
    {
        if (is_array($poremp)) {
            $useMinMax = false;
            if (isset($poremp['min'])) {
                $this->addUsingAlias(LianaofelegPeer::POREMP, $poremp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($poremp['max'])) {
                $this->addUsingAlias(LianaofelegPeer::POREMP, $poremp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LianaofelegPeer::POREMP, $poremp, $comparison);
    }

    /**
     * Filter the query on the observ column
     *
     * Example usage:
     * <code>
     * $query->filterByObserv('fooValue');   // WHERE observ = 'fooValue'
     * $query->filterByObserv('%fooValue%'); // WHERE observ LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observ The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LianaofelegQuery The current query, for fluid interface
     */
    public function filterByObserv($observ = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observ)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $observ)) {
                $observ = str_replace('*', '%', $observ);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LianaofelegPeer::OBSERV, $observ, $comparison);
    }

    /**
     * Filter the query on the tipconpub column
     *
     * Example usage:
     * <code>
     * $query->filterByTipconpub('fooValue');   // WHERE tipconpub = 'fooValue'
     * $query->filterByTipconpub('%fooValue%'); // WHERE tipconpub LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipconpub The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LianaofelegQuery The current query, for fluid interface
     */
    public function filterByTipconpub($tipconpub = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipconpub)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tipconpub)) {
                $tipconpub = str_replace('*', '%', $tipconpub);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LianaofelegPeer::TIPCONPUB, $tipconpub, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LianaofelegQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(LianaofelegPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(LianaofelegPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LianaofelegPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Lianaofeleg $lianaofeleg Object to remove from the list of results
     *
     * @return LianaofelegQuery The current query, for fluid interface
     */
    public function prune($lianaofeleg = null)
    {
        if ($lianaofeleg) {
            $this->addUsingAlias(LianaofelegPeer::ID, $lianaofeleg->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
