<?php


/**
 * Base class that represents a query for the 'liordcomdet' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:54 2015
 *
 * @method LiordcomdetQuery orderByNumord($order = Criteria::ASC) Order by the numord column
 * @method LiordcomdetQuery orderByCodart($order = Criteria::ASC) Order by the codart column
 * @method LiordcomdetQuery orderByUnimed($order = Criteria::ASC) Order by the unimed column
 * @method LiordcomdetQuery orderByCantid($order = Criteria::ASC) Order by the cantid column
 * @method LiordcomdetQuery orderByPreuni($order = Criteria::ASC) Order by the preuni column
 * @method LiordcomdetQuery orderByMonrec($order = Criteria::ASC) Order by the monrec column
 * @method LiordcomdetQuery orderBySubtot($order = Criteria::ASC) Order by the subtot column
 * @method LiordcomdetQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method LiordcomdetQuery groupByNumord() Group by the numord column
 * @method LiordcomdetQuery groupByCodart() Group by the codart column
 * @method LiordcomdetQuery groupByUnimed() Group by the unimed column
 * @method LiordcomdetQuery groupByCantid() Group by the cantid column
 * @method LiordcomdetQuery groupByPreuni() Group by the preuni column
 * @method LiordcomdetQuery groupByMonrec() Group by the monrec column
 * @method LiordcomdetQuery groupBySubtot() Group by the subtot column
 * @method LiordcomdetQuery groupById() Group by the id column
 *
 * @method LiordcomdetQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LiordcomdetQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LiordcomdetQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Liordcomdet findOne(PropelPDO $con = null) Return the first Liordcomdet matching the query
 * @method Liordcomdet findOneOrCreate(PropelPDO $con = null) Return the first Liordcomdet matching the query, or a new Liordcomdet object populated from the query conditions when no match is found
 *
 * @method Liordcomdet findOneByNumord(string $numord) Return the first Liordcomdet filtered by the numord column
 * @method Liordcomdet findOneByCodart(string $codart) Return the first Liordcomdet filtered by the codart column
 * @method Liordcomdet findOneByUnimed(string $unimed) Return the first Liordcomdet filtered by the unimed column
 * @method Liordcomdet findOneByCantid(string $cantid) Return the first Liordcomdet filtered by the cantid column
 * @method Liordcomdet findOneByPreuni(string $preuni) Return the first Liordcomdet filtered by the preuni column
 * @method Liordcomdet findOneByMonrec(string $monrec) Return the first Liordcomdet filtered by the monrec column
 * @method Liordcomdet findOneBySubtot(string $subtot) Return the first Liordcomdet filtered by the subtot column
 *
 * @method array findByNumord(string $numord) Return Liordcomdet objects filtered by the numord column
 * @method array findByCodart(string $codart) Return Liordcomdet objects filtered by the codart column
 * @method array findByUnimed(string $unimed) Return Liordcomdet objects filtered by the unimed column
 * @method array findByCantid(string $cantid) Return Liordcomdet objects filtered by the cantid column
 * @method array findByPreuni(string $preuni) Return Liordcomdet objects filtered by the preuni column
 * @method array findByMonrec(string $monrec) Return Liordcomdet objects filtered by the monrec column
 * @method array findBySubtot(string $subtot) Return Liordcomdet objects filtered by the subtot column
 * @method array findById(int $id) Return Liordcomdet objects filtered by the id column
 *
 * @package    propel.generator.lib.model.licitaciones.om
 */
abstract class BaseLiordcomdetQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLiordcomdetQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Liordcomdet', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LiordcomdetQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LiordcomdetQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LiordcomdetQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LiordcomdetQuery) {
            return $criteria;
        }
        $query = new LiordcomdetQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Liordcomdet|Liordcomdet[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LiordcomdetPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LiordcomdetPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Liordcomdet A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Liordcomdet A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "numord", "codart", "unimed", "cantid", "preuni", "monrec", "subtot", "id" FROM "liordcomdet" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Liordcomdet();
            $obj->hydrate($row);
            LiordcomdetPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Liordcomdet|Liordcomdet[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Liordcomdet[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LiordcomdetQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LiordcomdetPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LiordcomdetQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LiordcomdetPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the numord column
     *
     * Example usage:
     * <code>
     * $query->filterByNumord('fooValue');   // WHERE numord = 'fooValue'
     * $query->filterByNumord('%fooValue%'); // WHERE numord LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numord The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LiordcomdetQuery The current query, for fluid interface
     */
    public function filterByNumord($numord = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numord)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numord)) {
                $numord = str_replace('*', '%', $numord);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LiordcomdetPeer::NUMORD, $numord, $comparison);
    }

    /**
     * Filter the query on the codart column
     *
     * Example usage:
     * <code>
     * $query->filterByCodart('fooValue');   // WHERE codart = 'fooValue'
     * $query->filterByCodart('%fooValue%'); // WHERE codart LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codart The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LiordcomdetQuery The current query, for fluid interface
     */
    public function filterByCodart($codart = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codart)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codart)) {
                $codart = str_replace('*', '%', $codart);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LiordcomdetPeer::CODART, $codart, $comparison);
    }

    /**
     * Filter the query on the unimed column
     *
     * Example usage:
     * <code>
     * $query->filterByUnimed('fooValue');   // WHERE unimed = 'fooValue'
     * $query->filterByUnimed('%fooValue%'); // WHERE unimed LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unimed The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LiordcomdetQuery The current query, for fluid interface
     */
    public function filterByUnimed($unimed = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unimed)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $unimed)) {
                $unimed = str_replace('*', '%', $unimed);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LiordcomdetPeer::UNIMED, $unimed, $comparison);
    }

    /**
     * Filter the query on the cantid column
     *
     * Example usage:
     * <code>
     * $query->filterByCantid(1234); // WHERE cantid = 1234
     * $query->filterByCantid(array(12, 34)); // WHERE cantid IN (12, 34)
     * $query->filterByCantid(array('min' => 12)); // WHERE cantid >= 12
     * $query->filterByCantid(array('max' => 12)); // WHERE cantid <= 12
     * </code>
     *
     * @param     mixed $cantid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LiordcomdetQuery The current query, for fluid interface
     */
    public function filterByCantid($cantid = null, $comparison = null)
    {
        if (is_array($cantid)) {
            $useMinMax = false;
            if (isset($cantid['min'])) {
                $this->addUsingAlias(LiordcomdetPeer::CANTID, $cantid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cantid['max'])) {
                $this->addUsingAlias(LiordcomdetPeer::CANTID, $cantid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LiordcomdetPeer::CANTID, $cantid, $comparison);
    }

    /**
     * Filter the query on the preuni column
     *
     * Example usage:
     * <code>
     * $query->filterByPreuni(1234); // WHERE preuni = 1234
     * $query->filterByPreuni(array(12, 34)); // WHERE preuni IN (12, 34)
     * $query->filterByPreuni(array('min' => 12)); // WHERE preuni >= 12
     * $query->filterByPreuni(array('max' => 12)); // WHERE preuni <= 12
     * </code>
     *
     * @param     mixed $preuni The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LiordcomdetQuery The current query, for fluid interface
     */
    public function filterByPreuni($preuni = null, $comparison = null)
    {
        if (is_array($preuni)) {
            $useMinMax = false;
            if (isset($preuni['min'])) {
                $this->addUsingAlias(LiordcomdetPeer::PREUNI, $preuni['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($preuni['max'])) {
                $this->addUsingAlias(LiordcomdetPeer::PREUNI, $preuni['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LiordcomdetPeer::PREUNI, $preuni, $comparison);
    }

    /**
     * Filter the query on the monrec column
     *
     * Example usage:
     * <code>
     * $query->filterByMonrec(1234); // WHERE monrec = 1234
     * $query->filterByMonrec(array(12, 34)); // WHERE monrec IN (12, 34)
     * $query->filterByMonrec(array('min' => 12)); // WHERE monrec >= 12
     * $query->filterByMonrec(array('max' => 12)); // WHERE monrec <= 12
     * </code>
     *
     * @param     mixed $monrec The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LiordcomdetQuery The current query, for fluid interface
     */
    public function filterByMonrec($monrec = null, $comparison = null)
    {
        if (is_array($monrec)) {
            $useMinMax = false;
            if (isset($monrec['min'])) {
                $this->addUsingAlias(LiordcomdetPeer::MONREC, $monrec['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monrec['max'])) {
                $this->addUsingAlias(LiordcomdetPeer::MONREC, $monrec['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LiordcomdetPeer::MONREC, $monrec, $comparison);
    }

    /**
     * Filter the query on the subtot column
     *
     * Example usage:
     * <code>
     * $query->filterBySubtot(1234); // WHERE subtot = 1234
     * $query->filterBySubtot(array(12, 34)); // WHERE subtot IN (12, 34)
     * $query->filterBySubtot(array('min' => 12)); // WHERE subtot >= 12
     * $query->filterBySubtot(array('max' => 12)); // WHERE subtot <= 12
     * </code>
     *
     * @param     mixed $subtot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LiordcomdetQuery The current query, for fluid interface
     */
    public function filterBySubtot($subtot = null, $comparison = null)
    {
        if (is_array($subtot)) {
            $useMinMax = false;
            if (isset($subtot['min'])) {
                $this->addUsingAlias(LiordcomdetPeer::SUBTOT, $subtot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($subtot['max'])) {
                $this->addUsingAlias(LiordcomdetPeer::SUBTOT, $subtot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LiordcomdetPeer::SUBTOT, $subtot, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LiordcomdetQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(LiordcomdetPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(LiordcomdetPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LiordcomdetPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Liordcomdet $liordcomdet Object to remove from the list of results
     *
     * @return LiordcomdetQuery The current query, for fluid interface
     */
    public function prune($liordcomdet = null)
    {
        if ($liordcomdet) {
            $this->addUsingAlias(LiordcomdetPeer::ID, $liordcomdet->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
