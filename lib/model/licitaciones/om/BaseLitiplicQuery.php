<?php


/**
 * Base class that represents a query for the 'litiplic' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:51 2015
 *
 * @method LitiplicQuery orderByCodtiplic($order = Criteria::ASC) Order by the codtiplic column
 * @method LitiplicQuery orderByDestiplic($order = Criteria::ASC) Order by the destiplic column
 * @method LitiplicQuery orderByMaxunitri($order = Criteria::ASC) Order by the maxunitri column
 * @method LitiplicQuery orderByArtley($order = Criteria::ASC) Order by the artley column
 * @method LitiplicQuery orderByCanunitribie($order = Criteria::ASC) Order by the canunitribie column
 * @method LitiplicQuery orderByCanunitriobr($order = Criteria::ASC) Order by the canunitriobr column
 * @method LitiplicQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method LitiplicQuery groupByCodtiplic() Group by the codtiplic column
 * @method LitiplicQuery groupByDestiplic() Group by the destiplic column
 * @method LitiplicQuery groupByMaxunitri() Group by the maxunitri column
 * @method LitiplicQuery groupByArtley() Group by the artley column
 * @method LitiplicQuery groupByCanunitribie() Group by the canunitribie column
 * @method LitiplicQuery groupByCanunitriobr() Group by the canunitriobr column
 * @method LitiplicQuery groupById() Group by the id column
 *
 * @method LitiplicQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LitiplicQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LitiplicQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method LitiplicQuery leftJoinLireglic($relationAlias = null) Adds a LEFT JOIN clause to the query using the Lireglic relation
 * @method LitiplicQuery rightJoinLireglic($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Lireglic relation
 * @method LitiplicQuery innerJoinLireglic($relationAlias = null) Adds a INNER JOIN clause to the query using the Lireglic relation
 *
 * @method Litiplic findOne(PropelPDO $con = null) Return the first Litiplic matching the query
 * @method Litiplic findOneOrCreate(PropelPDO $con = null) Return the first Litiplic matching the query, or a new Litiplic object populated from the query conditions when no match is found
 *
 * @method Litiplic findOneByCodtiplic(string $codtiplic) Return the first Litiplic filtered by the codtiplic column
 * @method Litiplic findOneByDestiplic(string $destiplic) Return the first Litiplic filtered by the destiplic column
 * @method Litiplic findOneByMaxunitri(string $maxunitri) Return the first Litiplic filtered by the maxunitri column
 * @method Litiplic findOneByArtley(string $artley) Return the first Litiplic filtered by the artley column
 * @method Litiplic findOneByCanunitribie(string $canunitribie) Return the first Litiplic filtered by the canunitribie column
 * @method Litiplic findOneByCanunitriobr(string $canunitriobr) Return the first Litiplic filtered by the canunitriobr column
 *
 * @method array findByCodtiplic(string $codtiplic) Return Litiplic objects filtered by the codtiplic column
 * @method array findByDestiplic(string $destiplic) Return Litiplic objects filtered by the destiplic column
 * @method array findByMaxunitri(string $maxunitri) Return Litiplic objects filtered by the maxunitri column
 * @method array findByArtley(string $artley) Return Litiplic objects filtered by the artley column
 * @method array findByCanunitribie(string $canunitribie) Return Litiplic objects filtered by the canunitribie column
 * @method array findByCanunitriobr(string $canunitriobr) Return Litiplic objects filtered by the canunitriobr column
 * @method array findById(int $id) Return Litiplic objects filtered by the id column
 *
 * @package    propel.generator.lib.model.licitaciones.om
 */
abstract class BaseLitiplicQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLitiplicQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Litiplic', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LitiplicQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LitiplicQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LitiplicQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LitiplicQuery) {
            return $criteria;
        }
        $query = new LitiplicQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Litiplic|Litiplic[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LitiplicPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LitiplicPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Litiplic A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Litiplic A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "codtiplic", "destiplic", "maxunitri", "artley", "canunitribie", "canunitriobr", "id" FROM "litiplic" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Litiplic();
            $obj->hydrate($row);
            LitiplicPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Litiplic|Litiplic[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Litiplic[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LitiplicQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LitiplicPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LitiplicQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LitiplicPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the codtiplic column
     *
     * Example usage:
     * <code>
     * $query->filterByCodtiplic('fooValue');   // WHERE codtiplic = 'fooValue'
     * $query->filterByCodtiplic('%fooValue%'); // WHERE codtiplic LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codtiplic The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LitiplicQuery The current query, for fluid interface
     */
    public function filterByCodtiplic($codtiplic = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codtiplic)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codtiplic)) {
                $codtiplic = str_replace('*', '%', $codtiplic);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LitiplicPeer::CODTIPLIC, $codtiplic, $comparison);
    }

    /**
     * Filter the query on the destiplic column
     *
     * Example usage:
     * <code>
     * $query->filterByDestiplic('fooValue');   // WHERE destiplic = 'fooValue'
     * $query->filterByDestiplic('%fooValue%'); // WHERE destiplic LIKE '%fooValue%'
     * </code>
     *
     * @param     string $destiplic The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LitiplicQuery The current query, for fluid interface
     */
    public function filterByDestiplic($destiplic = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($destiplic)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $destiplic)) {
                $destiplic = str_replace('*', '%', $destiplic);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LitiplicPeer::DESTIPLIC, $destiplic, $comparison);
    }

    /**
     * Filter the query on the maxunitri column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxunitri(1234); // WHERE maxunitri = 1234
     * $query->filterByMaxunitri(array(12, 34)); // WHERE maxunitri IN (12, 34)
     * $query->filterByMaxunitri(array('min' => 12)); // WHERE maxunitri >= 12
     * $query->filterByMaxunitri(array('max' => 12)); // WHERE maxunitri <= 12
     * </code>
     *
     * @param     mixed $maxunitri The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LitiplicQuery The current query, for fluid interface
     */
    public function filterByMaxunitri($maxunitri = null, $comparison = null)
    {
        if (is_array($maxunitri)) {
            $useMinMax = false;
            if (isset($maxunitri['min'])) {
                $this->addUsingAlias(LitiplicPeer::MAXUNITRI, $maxunitri['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxunitri['max'])) {
                $this->addUsingAlias(LitiplicPeer::MAXUNITRI, $maxunitri['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LitiplicPeer::MAXUNITRI, $maxunitri, $comparison);
    }

    /**
     * Filter the query on the artley column
     *
     * Example usage:
     * <code>
     * $query->filterByArtley('fooValue');   // WHERE artley = 'fooValue'
     * $query->filterByArtley('%fooValue%'); // WHERE artley LIKE '%fooValue%'
     * </code>
     *
     * @param     string $artley The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LitiplicQuery The current query, for fluid interface
     */
    public function filterByArtley($artley = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($artley)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $artley)) {
                $artley = str_replace('*', '%', $artley);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LitiplicPeer::ARTLEY, $artley, $comparison);
    }

    /**
     * Filter the query on the canunitribie column
     *
     * Example usage:
     * <code>
     * $query->filterByCanunitribie(1234); // WHERE canunitribie = 1234
     * $query->filterByCanunitribie(array(12, 34)); // WHERE canunitribie IN (12, 34)
     * $query->filterByCanunitribie(array('min' => 12)); // WHERE canunitribie >= 12
     * $query->filterByCanunitribie(array('max' => 12)); // WHERE canunitribie <= 12
     * </code>
     *
     * @param     mixed $canunitribie The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LitiplicQuery The current query, for fluid interface
     */
    public function filterByCanunitribie($canunitribie = null, $comparison = null)
    {
        if (is_array($canunitribie)) {
            $useMinMax = false;
            if (isset($canunitribie['min'])) {
                $this->addUsingAlias(LitiplicPeer::CANUNITRIBIE, $canunitribie['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($canunitribie['max'])) {
                $this->addUsingAlias(LitiplicPeer::CANUNITRIBIE, $canunitribie['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LitiplicPeer::CANUNITRIBIE, $canunitribie, $comparison);
    }

    /**
     * Filter the query on the canunitriobr column
     *
     * Example usage:
     * <code>
     * $query->filterByCanunitriobr(1234); // WHERE canunitriobr = 1234
     * $query->filterByCanunitriobr(array(12, 34)); // WHERE canunitriobr IN (12, 34)
     * $query->filterByCanunitriobr(array('min' => 12)); // WHERE canunitriobr >= 12
     * $query->filterByCanunitriobr(array('max' => 12)); // WHERE canunitriobr <= 12
     * </code>
     *
     * @param     mixed $canunitriobr The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LitiplicQuery The current query, for fluid interface
     */
    public function filterByCanunitriobr($canunitriobr = null, $comparison = null)
    {
        if (is_array($canunitriobr)) {
            $useMinMax = false;
            if (isset($canunitriobr['min'])) {
                $this->addUsingAlias(LitiplicPeer::CANUNITRIOBR, $canunitriobr['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($canunitriobr['max'])) {
                $this->addUsingAlias(LitiplicPeer::CANUNITRIOBR, $canunitriobr['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LitiplicPeer::CANUNITRIOBR, $canunitriobr, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LitiplicQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(LitiplicPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(LitiplicPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LitiplicPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query by a related Lireglic object
     *
     * @param   Lireglic|PropelObjectCollection $lireglic  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LitiplicQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLireglic($lireglic, $comparison = null)
    {
        if ($lireglic instanceof Lireglic) {
            return $this
                ->addUsingAlias(LitiplicPeer::ID, $lireglic->getLitiplicId(), $comparison);
        } elseif ($lireglic instanceof PropelObjectCollection) {
            return $this
                ->useLireglicQuery()
                ->filterByPrimaryKeys($lireglic->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLireglic() only accepts arguments of type Lireglic or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Lireglic relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LitiplicQuery The current query, for fluid interface
     */
    public function joinLireglic($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Lireglic');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Lireglic');
        }

        return $this;
    }

    /**
     * Use the Lireglic relation Lireglic object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   LireglicQuery A secondary query class using the current class as primary query
     */
    public function useLireglicQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLireglic($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Lireglic', 'LireglicQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Litiplic $litiplic Object to remove from the list of results
     *
     * @return LitiplicQuery The current query, for fluid interface
     */
    public function prune($litiplic = null)
    {
        if ($litiplic) {
            $this->addUsingAlias(LitiplicPeer::ID, $litiplic->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
