<?php


/**
 * Base class that represents a query for the 'lidetactadd' table.
 *
 * Detalle de la actas por addendum
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:56 2015
 *
 * @method LidetactaddQuery orderByNumadd($order = Criteria::ASC) Order by the numadd column
 * @method LidetactaddQuery orderByNumact($order = Criteria::ASC) Order by the numact column
 * @method LidetactaddQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method LidetactaddQuery groupByNumadd() Group by the numadd column
 * @method LidetactaddQuery groupByNumact() Group by the numact column
 * @method LidetactaddQuery groupById() Group by the id column
 *
 * @method LidetactaddQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LidetactaddQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LidetactaddQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method LidetactaddQuery leftJoinLiaddendum($relationAlias = null) Adds a LEFT JOIN clause to the query using the Liaddendum relation
 * @method LidetactaddQuery rightJoinLiaddendum($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Liaddendum relation
 * @method LidetactaddQuery innerJoinLiaddendum($relationAlias = null) Adds a INNER JOIN clause to the query using the Liaddendum relation
 *
 * @method LidetactaddQuery leftJoinLiactas($relationAlias = null) Adds a LEFT JOIN clause to the query using the Liactas relation
 * @method LidetactaddQuery rightJoinLiactas($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Liactas relation
 * @method LidetactaddQuery innerJoinLiactas($relationAlias = null) Adds a INNER JOIN clause to the query using the Liactas relation
 *
 * @method Lidetactadd findOne(PropelPDO $con = null) Return the first Lidetactadd matching the query
 * @method Lidetactadd findOneOrCreate(PropelPDO $con = null) Return the first Lidetactadd matching the query, or a new Lidetactadd object populated from the query conditions when no match is found
 *
 * @method Lidetactadd findOneByNumadd(string $numadd) Return the first Lidetactadd filtered by the numadd column
 * @method Lidetactadd findOneByNumact(string $numact) Return the first Lidetactadd filtered by the numact column
 *
 * @method array findByNumadd(string $numadd) Return Lidetactadd objects filtered by the numadd column
 * @method array findByNumact(string $numact) Return Lidetactadd objects filtered by the numact column
 * @method array findById(int $id) Return Lidetactadd objects filtered by the id column
 *
 * @package    propel.generator.lib.model.licitaciones.om
 */
abstract class BaseLidetactaddQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLidetactaddQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Lidetactadd', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LidetactaddQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LidetactaddQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LidetactaddQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LidetactaddQuery) {
            return $criteria;
        }
        $query = new LidetactaddQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Lidetactadd|Lidetactadd[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LidetactaddPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LidetactaddPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Lidetactadd A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Lidetactadd A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "numadd", "numact", "id" FROM "lidetactadd" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Lidetactadd();
            $obj->hydrate($row);
            LidetactaddPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Lidetactadd|Lidetactadd[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Lidetactadd[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LidetactaddQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LidetactaddPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LidetactaddQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LidetactaddPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the numadd column
     *
     * Example usage:
     * <code>
     * $query->filterByNumadd('fooValue');   // WHERE numadd = 'fooValue'
     * $query->filterByNumadd('%fooValue%'); // WHERE numadd LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numadd The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LidetactaddQuery The current query, for fluid interface
     */
    public function filterByNumadd($numadd = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numadd)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numadd)) {
                $numadd = str_replace('*', '%', $numadd);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LidetactaddPeer::NUMADD, $numadd, $comparison);
    }

    /**
     * Filter the query on the numact column
     *
     * Example usage:
     * <code>
     * $query->filterByNumact('fooValue');   // WHERE numact = 'fooValue'
     * $query->filterByNumact('%fooValue%'); // WHERE numact LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numact The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LidetactaddQuery The current query, for fluid interface
     */
    public function filterByNumact($numact = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numact)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numact)) {
                $numact = str_replace('*', '%', $numact);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LidetactaddPeer::NUMACT, $numact, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LidetactaddQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(LidetactaddPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(LidetactaddPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LidetactaddPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query by a related Liaddendum object
     *
     * @param   Liaddendum|PropelObjectCollection $liaddendum The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LidetactaddQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLiaddendum($liaddendum, $comparison = null)
    {
        if ($liaddendum instanceof Liaddendum) {
            return $this
                ->addUsingAlias(LidetactaddPeer::NUMADD, $liaddendum->getNumadd(), $comparison);
        } elseif ($liaddendum instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LidetactaddPeer::NUMADD, $liaddendum->toKeyValue('PrimaryKey', 'Numadd'), $comparison);
        } else {
            throw new PropelException('filterByLiaddendum() only accepts arguments of type Liaddendum or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Liaddendum relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LidetactaddQuery The current query, for fluid interface
     */
    public function joinLiaddendum($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Liaddendum');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Liaddendum');
        }

        return $this;
    }

    /**
     * Use the Liaddendum relation Liaddendum object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   LiaddendumQuery A secondary query class using the current class as primary query
     */
    public function useLiaddendumQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLiaddendum($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Liaddendum', 'LiaddendumQuery');
    }

    /**
     * Filter the query by a related Liactas object
     *
     * @param   Liactas|PropelObjectCollection $liactas The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LidetactaddQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLiactas($liactas, $comparison = null)
    {
        if ($liactas instanceof Liactas) {
            return $this
                ->addUsingAlias(LidetactaddPeer::NUMACT, $liactas->getNumact(), $comparison);
        } elseif ($liactas instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LidetactaddPeer::NUMACT, $liactas->toKeyValue('PrimaryKey', 'Numact'), $comparison);
        } else {
            throw new PropelException('filterByLiactas() only accepts arguments of type Liactas or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Liactas relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LidetactaddQuery The current query, for fluid interface
     */
    public function joinLiactas($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Liactas');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Liactas');
        }

        return $this;
    }

    /**
     * Use the Liactas relation Liactas object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   LiactasQuery A secondary query class using the current class as primary query
     */
    public function useLiactasQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLiactas($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Liactas', 'LiactasQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Lidetactadd $lidetactadd Object to remove from the list of results
     *
     * @return LidetactaddQuery The current query, for fluid interface
     */
    public function prune($lidetactadd = null)
    {
        if ($lidetactadd) {
            $this->addUsingAlias(LidetactaddPeer::ID, $lidetactadd->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
