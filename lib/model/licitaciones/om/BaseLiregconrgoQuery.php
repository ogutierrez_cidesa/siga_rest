<?php


/**
 * Base class that represents a query for the 'liregconrgo' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:56 2015
 *
 * @method LiregconrgoQuery orderByNumcont($order = Criteria::ASC) Order by the numcont column
 * @method LiregconrgoQuery orderByCodrgo($order = Criteria::ASC) Order by the codrgo column
 * @method LiregconrgoQuery orderByMonrgo($order = Criteria::ASC) Order by the monrgo column
 * @method LiregconrgoQuery orderByTipconpub($order = Criteria::ASC) Order by the tipconpub column
 * @method LiregconrgoQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method LiregconrgoQuery groupByNumcont() Group by the numcont column
 * @method LiregconrgoQuery groupByCodrgo() Group by the codrgo column
 * @method LiregconrgoQuery groupByMonrgo() Group by the monrgo column
 * @method LiregconrgoQuery groupByTipconpub() Group by the tipconpub column
 * @method LiregconrgoQuery groupById() Group by the id column
 *
 * @method LiregconrgoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LiregconrgoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LiregconrgoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method LiregconrgoQuery leftJoinLicontrat($relationAlias = null) Adds a LEFT JOIN clause to the query using the Licontrat relation
 * @method LiregconrgoQuery rightJoinLicontrat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Licontrat relation
 * @method LiregconrgoQuery innerJoinLicontrat($relationAlias = null) Adds a INNER JOIN clause to the query using the Licontrat relation
 *
 * @method Liregconrgo findOne(PropelPDO $con = null) Return the first Liregconrgo matching the query
 * @method Liregconrgo findOneOrCreate(PropelPDO $con = null) Return the first Liregconrgo matching the query, or a new Liregconrgo object populated from the query conditions when no match is found
 *
 * @method Liregconrgo findOneByNumcont(string $numcont) Return the first Liregconrgo filtered by the numcont column
 * @method Liregconrgo findOneByCodrgo(string $codrgo) Return the first Liregconrgo filtered by the codrgo column
 * @method Liregconrgo findOneByMonrgo(string $monrgo) Return the first Liregconrgo filtered by the monrgo column
 * @method Liregconrgo findOneByTipconpub(string $tipconpub) Return the first Liregconrgo filtered by the tipconpub column
 *
 * @method array findByNumcont(string $numcont) Return Liregconrgo objects filtered by the numcont column
 * @method array findByCodrgo(string $codrgo) Return Liregconrgo objects filtered by the codrgo column
 * @method array findByMonrgo(string $monrgo) Return Liregconrgo objects filtered by the monrgo column
 * @method array findByTipconpub(string $tipconpub) Return Liregconrgo objects filtered by the tipconpub column
 * @method array findById(int $id) Return Liregconrgo objects filtered by the id column
 *
 * @package    propel.generator.lib.model.licitaciones.om
 */
abstract class BaseLiregconrgoQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLiregconrgoQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Liregconrgo', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LiregconrgoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LiregconrgoQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LiregconrgoQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LiregconrgoQuery) {
            return $criteria;
        }
        $query = new LiregconrgoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Liregconrgo|Liregconrgo[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LiregconrgoPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LiregconrgoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Liregconrgo A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Liregconrgo A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "numcont", "codrgo", "monrgo", "tipconpub", "id" FROM "liregconrgo" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Liregconrgo();
            $obj->hydrate($row);
            LiregconrgoPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Liregconrgo|Liregconrgo[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Liregconrgo[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LiregconrgoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LiregconrgoPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LiregconrgoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LiregconrgoPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the numcont column
     *
     * Example usage:
     * <code>
     * $query->filterByNumcont('fooValue');   // WHERE numcont = 'fooValue'
     * $query->filterByNumcont('%fooValue%'); // WHERE numcont LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numcont The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LiregconrgoQuery The current query, for fluid interface
     */
    public function filterByNumcont($numcont = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numcont)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numcont)) {
                $numcont = str_replace('*', '%', $numcont);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LiregconrgoPeer::NUMCONT, $numcont, $comparison);
    }

    /**
     * Filter the query on the codrgo column
     *
     * Example usage:
     * <code>
     * $query->filterByCodrgo('fooValue');   // WHERE codrgo = 'fooValue'
     * $query->filterByCodrgo('%fooValue%'); // WHERE codrgo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codrgo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LiregconrgoQuery The current query, for fluid interface
     */
    public function filterByCodrgo($codrgo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codrgo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codrgo)) {
                $codrgo = str_replace('*', '%', $codrgo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LiregconrgoPeer::CODRGO, $codrgo, $comparison);
    }

    /**
     * Filter the query on the monrgo column
     *
     * Example usage:
     * <code>
     * $query->filterByMonrgo(1234); // WHERE monrgo = 1234
     * $query->filterByMonrgo(array(12, 34)); // WHERE monrgo IN (12, 34)
     * $query->filterByMonrgo(array('min' => 12)); // WHERE monrgo >= 12
     * $query->filterByMonrgo(array('max' => 12)); // WHERE monrgo <= 12
     * </code>
     *
     * @param     mixed $monrgo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LiregconrgoQuery The current query, for fluid interface
     */
    public function filterByMonrgo($monrgo = null, $comparison = null)
    {
        if (is_array($monrgo)) {
            $useMinMax = false;
            if (isset($monrgo['min'])) {
                $this->addUsingAlias(LiregconrgoPeer::MONRGO, $monrgo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monrgo['max'])) {
                $this->addUsingAlias(LiregconrgoPeer::MONRGO, $monrgo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LiregconrgoPeer::MONRGO, $monrgo, $comparison);
    }

    /**
     * Filter the query on the tipconpub column
     *
     * Example usage:
     * <code>
     * $query->filterByTipconpub('fooValue');   // WHERE tipconpub = 'fooValue'
     * $query->filterByTipconpub('%fooValue%'); // WHERE tipconpub LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipconpub The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LiregconrgoQuery The current query, for fluid interface
     */
    public function filterByTipconpub($tipconpub = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipconpub)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tipconpub)) {
                $tipconpub = str_replace('*', '%', $tipconpub);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LiregconrgoPeer::TIPCONPUB, $tipconpub, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LiregconrgoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(LiregconrgoPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(LiregconrgoPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LiregconrgoPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query by a related Licontrat object
     *
     * @param   Licontrat|PropelObjectCollection $licontrat The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LiregconrgoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLicontrat($licontrat, $comparison = null)
    {
        if ($licontrat instanceof Licontrat) {
            return $this
                ->addUsingAlias(LiregconrgoPeer::NUMCONT, $licontrat->getNumcont(), $comparison);
        } elseif ($licontrat instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LiregconrgoPeer::NUMCONT, $licontrat->toKeyValue('PrimaryKey', 'Numcont'), $comparison);
        } else {
            throw new PropelException('filterByLicontrat() only accepts arguments of type Licontrat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Licontrat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LiregconrgoQuery The current query, for fluid interface
     */
    public function joinLicontrat($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Licontrat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Licontrat');
        }

        return $this;
    }

    /**
     * Use the Licontrat relation Licontrat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   LicontratQuery A secondary query class using the current class as primary query
     */
    public function useLicontratQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLicontrat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Licontrat', 'LicontratQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Liregconrgo $liregconrgo Object to remove from the list of results
     *
     * @return LiregconrgoQuery The current query, for fluid interface
     */
    public function prune($liregconrgo = null)
    {
        if ($liregconrgo) {
            $this->addUsingAlias(LiregconrgoPeer::ID, $liregconrgo->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
