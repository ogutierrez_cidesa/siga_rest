<?php


/**
 * Base class that represents a query for the 'litipact' table.
 *
 * Tipos de actas manejables por el sistema
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:54 2015
 *
 * @method LitipactQuery orderByCodtipact($order = Criteria::ASC) Order by the codtipact column
 * @method LitipactQuery orderByNomtipact($order = Criteria::ASC) Order by the nomtipact column
 * @method LitipactQuery orderByDettipact($order = Criteria::ASC) Order by the dettipact column
 * @method LitipactQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method LitipactQuery groupByCodtipact() Group by the codtipact column
 * @method LitipactQuery groupByNomtipact() Group by the nomtipact column
 * @method LitipactQuery groupByDettipact() Group by the dettipact column
 * @method LitipactQuery groupById() Group by the id column
 *
 * @method LitipactQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method LitipactQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method LitipactQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method LitipactQuery leftJoinLiactas($relationAlias = null) Adds a LEFT JOIN clause to the query using the Liactas relation
 * @method LitipactQuery rightJoinLiactas($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Liactas relation
 * @method LitipactQuery innerJoinLiactas($relationAlias = null) Adds a INNER JOIN clause to the query using the Liactas relation
 *
 * @method Litipact findOne(PropelPDO $con = null) Return the first Litipact matching the query
 * @method Litipact findOneOrCreate(PropelPDO $con = null) Return the first Litipact matching the query, or a new Litipact object populated from the query conditions when no match is found
 *
 * @method Litipact findOneByCodtipact(string $codtipact) Return the first Litipact filtered by the codtipact column
 * @method Litipact findOneByNomtipact(string $nomtipact) Return the first Litipact filtered by the nomtipact column
 * @method Litipact findOneByDettipact(string $dettipact) Return the first Litipact filtered by the dettipact column
 *
 * @method array findByCodtipact(string $codtipact) Return Litipact objects filtered by the codtipact column
 * @method array findByNomtipact(string $nomtipact) Return Litipact objects filtered by the nomtipact column
 * @method array findByDettipact(string $dettipact) Return Litipact objects filtered by the dettipact column
 * @method array findById(int $id) Return Litipact objects filtered by the id column
 *
 * @package    propel.generator.lib.model.licitaciones.om
 */
abstract class BaseLitipactQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseLitipactQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Litipact', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new LitipactQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   LitipactQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return LitipactQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof LitipactQuery) {
            return $criteria;
        }
        $query = new LitipactQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Litipact|Litipact[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LitipactPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(LitipactPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Litipact A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Litipact A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "codtipact", "nomtipact", "dettipact", "id" FROM "litipact" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Litipact();
            $obj->hydrate($row);
            LitipactPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Litipact|Litipact[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Litipact[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return LitipactQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LitipactPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return LitipactQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LitipactPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the codtipact column
     *
     * Example usage:
     * <code>
     * $query->filterByCodtipact('fooValue');   // WHERE codtipact = 'fooValue'
     * $query->filterByCodtipact('%fooValue%'); // WHERE codtipact LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codtipact The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LitipactQuery The current query, for fluid interface
     */
    public function filterByCodtipact($codtipact = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codtipact)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codtipact)) {
                $codtipact = str_replace('*', '%', $codtipact);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LitipactPeer::CODTIPACT, $codtipact, $comparison);
    }

    /**
     * Filter the query on the nomtipact column
     *
     * Example usage:
     * <code>
     * $query->filterByNomtipact('fooValue');   // WHERE nomtipact = 'fooValue'
     * $query->filterByNomtipact('%fooValue%'); // WHERE nomtipact LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomtipact The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LitipactQuery The current query, for fluid interface
     */
    public function filterByNomtipact($nomtipact = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomtipact)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomtipact)) {
                $nomtipact = str_replace('*', '%', $nomtipact);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LitipactPeer::NOMTIPACT, $nomtipact, $comparison);
    }

    /**
     * Filter the query on the dettipact column
     *
     * Example usage:
     * <code>
     * $query->filterByDettipact('fooValue');   // WHERE dettipact = 'fooValue'
     * $query->filterByDettipact('%fooValue%'); // WHERE dettipact LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dettipact The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LitipactQuery The current query, for fluid interface
     */
    public function filterByDettipact($dettipact = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dettipact)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dettipact)) {
                $dettipact = str_replace('*', '%', $dettipact);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LitipactPeer::DETTIPACT, $dettipact, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return LitipactQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(LitipactPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(LitipactPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LitipactPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query by a related Liactas object
     *
     * @param   Liactas|PropelObjectCollection $liactas  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 LitipactQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByLiactas($liactas, $comparison = null)
    {
        if ($liactas instanceof Liactas) {
            return $this
                ->addUsingAlias(LitipactPeer::CODTIPACT, $liactas->getCodtipact(), $comparison);
        } elseif ($liactas instanceof PropelObjectCollection) {
            return $this
                ->useLiactasQuery()
                ->filterByPrimaryKeys($liactas->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLiactas() only accepts arguments of type Liactas or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Liactas relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return LitipactQuery The current query, for fluid interface
     */
    public function joinLiactas($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Liactas');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Liactas');
        }

        return $this;
    }

    /**
     * Use the Liactas relation Liactas object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   LiactasQuery A secondary query class using the current class as primary query
     */
    public function useLiactasQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLiactas($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Liactas', 'LiactasQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Litipact $litipact Object to remove from the list of results
     *
     * @return LitipactQuery The current query, for fluid interface
     */
    public function prune($litipact = null)
    {
        if ($litipact) {
            $this->addUsingAlias(LitipactPeer::ID, $litipact->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
