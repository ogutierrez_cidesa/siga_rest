<?php



/**
 * This class defines the structure of the 'liregforpagcont' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:54 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.licitaciones.map
 */
class LiregforpagcontTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.licitaciones.map.LiregforpagcontTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('liregforpagcont');
        $this->setPhpName('Liregforpagcont');
        $this->setClassname('Liregforpagcont');
        $this->setPackage('lib.model.licitaciones');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignKey('numcont', 'Numcont', 'VARCHAR', 'licontrat', 'numcont', false, 20, null);
        $this->addColumn('desant', 'Desant', 'VARCHAR', false, 1000, null);
        $this->addColumn('porant', 'Porant', 'NUMERIC', false, 14, null);
        $this->addColumn('montot', 'Montot', 'NUMERIC', false, 14, null);
        $this->addColumn('monrec', 'Monrec', 'NUMERIC', false, 14, null);
        $this->addColumn('subtot', 'Subtot', 'NUMERIC', false, 14, null);
        $this->addColumn('poramoant', 'Poramoant', 'NUMERIC', false, 14, null);
        $this->addColumn('netpag', 'Netpag', 'NUMERIC', false, 14, null);
        $this->addColumn('fecant', 'Fecant', 'DATE', false, null, null);
        $this->addColumn('condic', 'Condic', 'VARCHAR', false, 1000, null);
        $this->addColumn('tipconpub', 'Tipconpub', 'VARCHAR', false, 1, null);
        $this->addColumn('numord', 'Numord', 'VARCHAR', false, 10, null);
        $this->addColumn('numche', 'Numche', 'VARCHAR', false, 10, null);
        $this->addColumn('feccom', 'Feccom', 'DATE', false, null, null);
        $this->addColumn('feccau', 'Feccau', 'DATE', false, null, null);
        $this->addColumn('fecpag', 'Fecpag', 'DATE', false, null, null);
        $this->addForeignKey('liforpag_id', 'LiforpagId', 'INTEGER', 'liforpag', 'id', false, null, null);
        $this->addColumn('numval', 'Numval', 'INTEGER', false, null, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Licontrat', 'Licontrat', RelationMap::MANY_TO_ONE, array('numcont' => 'numcont', ), null, null);
        $this->addRelation('Liforpag', 'Liforpag', RelationMap::MANY_TO_ONE, array('liforpag_id' => 'id', ), null, null);
        $this->addRelation('Liregforpagmodcont', 'Liregforpagmodcont', RelationMap::ONE_TO_MANY, array('id' => 'liregforpagcont_id', ), null, null, 'Liregforpagmodconts');
        $this->addRelation('Liregforpagaddcont', 'Liregforpagaddcont', RelationMap::ONE_TO_MANY, array('id' => 'liregforpagcont_id', ), null, null, 'Liregforpagaddconts');
    } // buildRelations()

} // LiregforpagcontTableMap
