<?php



/**
 * This class defines the structure of the 'lidetcroentmodcont' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:57 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.licitaciones.map
 */
class LidetcroentmodcontTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.licitaciones.map.LidetcroentmodcontTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('lidetcroentmodcont');
        $this->setPhpName('Lidetcroentmodcont');
        $this->setClassname('Lidetcroentmodcont');
        $this->setPackage('lib.model.licitaciones');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignKey('nummod', 'Nummod', 'VARCHAR', 'limodcont', 'nummod', false, 8, null);
        $this->addColumn('codart', 'Codart', 'VARCHAR', false, 32, null);
        $this->addColumn('cantid', 'Cantid', 'NUMERIC', false, 14, null);
        $this->addForeignKey('coduniadm', 'Coduniadm', 'VARCHAR', 'liuniadm', 'coduniadm', false, 4, null);
        $this->addColumn('fecentcont', 'Fecentcont', 'DATE', false, null, null);
        $this->addColumn('fecent', 'Fecent', 'DATE', false, null, null);
        $this->addColumn('condic', 'Condic', 'VARCHAR', false, 1000, null);
        $this->addColumn('cantidrec', 'Cantidrec', 'NUMERIC', false, 14, null);
        $this->addColumn('fecrec', 'Fecrec', 'DATE', false, null, null);
        $this->addColumn('observacion', 'Observacion', 'VARCHAR', false, 1000, null);
        $this->addColumn('tipconpub', 'Tipconpub', 'VARCHAR', false, 1, null);
        $this->addForeignKey('lidetcroentcontob_id', 'LidetcroentcontobId', 'VARCHAR', 'lidetcroentcontob', 'id', false, 4, null);
        $this->addColumn('numval', 'Numval', 'INTEGER', false, null, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Limodcont', 'Limodcont', RelationMap::MANY_TO_ONE, array('nummod' => 'nummod', ), null, null);
        $this->addRelation('Liuniadm', 'Liuniadm', RelationMap::MANY_TO_ONE, array('coduniadm' => 'coduniadm', ), null, null);
        $this->addRelation('Lidetcroentcontob', 'Lidetcroentcontob', RelationMap::MANY_TO_ONE, array('lidetcroentcontob_id' => 'id', ), null, null);
    } // buildRelations()

} // LidetcroentmodcontTableMap
