<?php



/**
 * This class defines the structure of the 'cpdiscre' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:38 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.presupuesto.map
 */
class CpdiscreTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.presupuesto.map.CpdiscreTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cpdiscre');
        $this->setPhpName('Cpdiscre');
        $this->setClassname('Cpdiscre');
        $this->setPackage('lib.model.presupuesto');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('sector', 'Sector', 'VARCHAR', false, 2, null);
        $this->addColumn('partida', 'Partida', 'VARCHAR', false, 3, null);
        $this->addColumn('monto', 'Monto', 'NUMERIC', false, 16, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CpdiscreTableMap
