<?php



/**
 * This class defines the structure of the 'cpmovtra' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:39 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.presupuesto.map
 */
class CpmovtraTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.presupuesto.map.CpmovtraTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cpmovtra');
        $this->setPhpName('Cpmovtra');
        $this->setClassname('Cpmovtra');
        $this->setPackage('lib.model.presupuesto');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignKey('reftra', 'Reftra', 'VARCHAR', 'cptrasla', 'reftra', true, 8, null);
        $this->addForeignKey('codori', 'Codori', 'VARCHAR', 'cpdeftit', 'codpre', true, 50, null);
        $this->addForeignKey('coddes', 'Coddes', 'VARCHAR', 'cpdeftit', 'codpre', true, 50, null);
        $this->addColumn('monmov', 'Monmov', 'NUMERIC', false, 14, null);
        $this->addColumn('stamov', 'Stamov', 'VARCHAR', false, 1, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Cptrasla', 'Cptrasla', RelationMap::MANY_TO_ONE, array('reftra' => 'reftra', ), null, null);
        $this->addRelation('CpdeftitRelatedByCodori', 'Cpdeftit', RelationMap::MANY_TO_ONE, array('codori' => 'codpre', ), null, null);
        $this->addRelation('CpdeftitRelatedByCoddes', 'Cpdeftit', RelationMap::MANY_TO_ONE, array('coddes' => 'codpre', ), null, null);
    } // buildRelations()

} // CpmovtraTableMap
