<?php



/**
 * This class defines the structure of the 'cpdefapr' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:39 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.presupuesto.map
 */
class CpdefaprTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.presupuesto.map.CpdefaprTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cpdefapr');
        $this->setPhpName('Cpdefapr');
        $this->setClassname('Cpdefapr');
        $this->setPackage('lib.model.presupuesto');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('stacon', 'Stacon', 'VARCHAR', false, 50, null);
        $this->addColumn('abrstacon', 'Abrstacon', 'VARCHAR', false, 5, null);
        $this->addColumn('stagob', 'Stagob', 'VARCHAR', false, 50, null);
        $this->addColumn('abrstagob', 'Abrstagob', 'VARCHAR', false, 5, null);
        $this->addColumn('stapre', 'Stapre', 'VARCHAR', false, 50, null);
        $this->addColumn('abrstapre', 'Abrstapre', 'VARCHAR', false, 5, null);
        $this->addColumn('staniv4', 'Staniv4', 'VARCHAR', false, 50, null);
        $this->addColumn('abrstaniv4', 'Abrstaniv4', 'VARCHAR', false, 5, null);
        $this->addColumn('staniv5', 'Staniv5', 'VARCHAR', false, 50, null);
        $this->addColumn('abrstaniv5', 'Abrstaniv5', 'VARCHAR', false, 5, null);
        $this->addColumn('staniv6', 'Staniv6', 'VARCHAR', false, 50, null);
        $this->addColumn('abrstaniv6', 'Abrstaniv6', 'VARCHAR', false, 5, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CpdefaprTableMap
