<?php



/**
 * This class defines the structure of the 'cpcausad' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:38 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.presupuesto.map
 */
class CpcausadTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.presupuesto.map.CpcausadTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cpcausad');
        $this->setPhpName('Cpcausad');
        $this->setClassname('Cpcausad');
        $this->setPackage('lib.model.presupuesto');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('refcau', 'Refcau', 'VARCHAR', true, 8, null);
        $this->addForeignKey('tipcau', 'Tipcau', 'VARCHAR', 'cpdoccau', 'tipcau', true, 4, null);
        $this->addColumn('feccau', 'Feccau', 'DATE', true, null, null);
        $this->addColumn('anocau', 'Anocau', 'VARCHAR', false, 4, null);
        $this->addForeignKey('refcom', 'Refcom', 'VARCHAR', 'cpcompro', 'refcom', false, 8, null);
        $this->addColumn('tipcom', 'Tipcom', 'VARCHAR', false, 4, null);
        $this->addColumn('descau', 'Descau', 'VARCHAR', false, 1000, null);
        $this->addColumn('desanu', 'Desanu', 'VARCHAR', false, 1000, null);
        $this->addColumn('moncau', 'Moncau', 'NUMERIC', false, 14, null);
        $this->addColumn('salpag', 'Salpag', 'NUMERIC', false, 14, null);
        $this->addColumn('salaju', 'Salaju', 'NUMERIC', false, 14, null);
        $this->addColumn('stacau', 'Stacau', 'VARCHAR', false, 1, null);
        $this->addColumn('fecanu', 'Fecanu', 'DATE', false, null, null);
        $this->addForeignKey('cedrif', 'Cedrif', 'VARCHAR', 'opbenefi', 'cedrif', false, 15, null);
        $this->addColumn('fecreg', 'Fecreg', 'TIMESTAMP', false, null, 'now');
        $this->addColumn('numcom', 'Numcom', 'VARCHAR', false, 8, null);
        $this->addColumn('coddirec', 'Coddirec', 'VARCHAR', false, 4, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Cpdoccau', 'Cpdoccau', RelationMap::MANY_TO_ONE, array('tipcau' => 'tipcau', ), null, null);
        $this->addRelation('Cpcompro', 'Cpcompro', RelationMap::MANY_TO_ONE, array('refcom' => 'refcom', ), null, null);
        $this->addRelation('Opbenefi', 'Opbenefi', RelationMap::MANY_TO_ONE, array('cedrif' => 'cedrif', ), null, null);
        $this->addRelation('Cpimpcau', 'Cpimpcau', RelationMap::ONE_TO_MANY, array('refcau' => 'refcau', ), null, null, 'Cpimpcaus');
    } // buildRelations()

} // CpcausadTableMap
