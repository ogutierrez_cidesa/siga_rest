<?php



/**
 * This class defines the structure of the 'cpajuste' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:37 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.presupuesto.map
 */
class CpajusteTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.presupuesto.map.CpajusteTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cpajuste');
        $this->setPhpName('Cpajuste');
        $this->setClassname('Cpajuste');
        $this->setPackage('lib.model.presupuesto');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('refaju', 'Refaju', 'VARCHAR', true, 8, null);
        $this->addForeignKey('tipaju', 'Tipaju', 'VARCHAR', 'cpdocaju', 'tipaju', true, 4, null);
        $this->addColumn('fecaju', 'Fecaju', 'DATE', true, null, null);
        $this->addColumn('anoaju', 'Anoaju', 'VARCHAR', true, 4, null);
        $this->addColumn('refere', 'Refere', 'VARCHAR', true, 8, null);
        $this->addColumn('desaju', 'Desaju', 'VARCHAR', false, 1000, null);
        $this->addColumn('desanu', 'Desanu', 'VARCHAR', false, 1000, null);
        $this->addColumn('totaju', 'Totaju', 'NUMERIC', false, 14, null);
        $this->addColumn('staaju', 'Staaju', 'VARCHAR', false, 1, null);
        $this->addColumn('fecanu', 'Fecanu', 'DATE', false, null, null);
        $this->addColumn('numcom', 'Numcom', 'VARCHAR', false, 8, null);
        $this->addColumn('cuoanu', 'Cuoanu', 'NUMERIC', false, 6, null);
        $this->addColumn('fecanudes', 'Fecanudes', 'DATE', false, null, null);
        $this->addColumn('fecanuhas', 'Fecanuhas', 'DATE', false, null, null);
        $this->addColumn('ordpag', 'Ordpag', 'VARCHAR', false, 1, null);
        $this->addColumn('fecenvcon', 'Fecenvcon', 'DATE', false, null, null);
        $this->addColumn('fecenvfin', 'Fecenvfin', 'DATE', false, null, null);
        $this->addColumn('fecreg', 'Fecreg', 'TIMESTAMP', false, null, 'now');
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Cpdocaju', 'Cpdocaju', RelationMap::MANY_TO_ONE, array('tipaju' => 'tipaju', ), null, null);
        $this->addRelation('Cpmovaju', 'Cpmovaju', RelationMap::ONE_TO_MANY, array('refaju' => 'refaju', ), null, null, 'Cpmovajus');
    } // buildRelations()

} // CpajusteTableMap
