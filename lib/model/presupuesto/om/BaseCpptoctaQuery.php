<?php


/**
 * Base class that represents a query for the 'cpptocta' table.
 *
 * Tabla para registrar los Puntos de Cuenta
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:39 2015
 *
 * @method CpptoctaQuery orderByNumpta($order = Criteria::ASC) Order by the numpta column
 * @method CpptoctaQuery orderByFecpta($order = Criteria::ASC) Order by the fecpta column
 * @method CpptoctaQuery orderByCodubiori($order = Criteria::ASC) Order by the codubiori column
 * @method CpptoctaQuery orderByCodubides($order = Criteria::ASC) Order by the codubides column
 * @method CpptoctaQuery orderByAsunto($order = Criteria::ASC) Order by the asunto column
 * @method CpptoctaQuery orderByMotivo($order = Criteria::ASC) Order by the motivo column
 * @method CpptoctaQuery orderByReccon($order = Criteria::ASC) Order by the reccon column
 * @method CpptoctaQuery orderByLoguse($order = Criteria::ASC) Order by the loguse column
 * @method CpptoctaQuery orderByAprpto($order = Criteria::ASC) Order by the aprpto column
 * @method CpptoctaQuery orderByUsuapr($order = Criteria::ASC) Order by the usuapr column
 * @method CpptoctaQuery orderByFecapr($order = Criteria::ASC) Order by the fecapr column
 * @method CpptoctaQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method CpptoctaQuery groupByNumpta() Group by the numpta column
 * @method CpptoctaQuery groupByFecpta() Group by the fecpta column
 * @method CpptoctaQuery groupByCodubiori() Group by the codubiori column
 * @method CpptoctaQuery groupByCodubides() Group by the codubides column
 * @method CpptoctaQuery groupByAsunto() Group by the asunto column
 * @method CpptoctaQuery groupByMotivo() Group by the motivo column
 * @method CpptoctaQuery groupByReccon() Group by the reccon column
 * @method CpptoctaQuery groupByLoguse() Group by the loguse column
 * @method CpptoctaQuery groupByAprpto() Group by the aprpto column
 * @method CpptoctaQuery groupByUsuapr() Group by the usuapr column
 * @method CpptoctaQuery groupByFecapr() Group by the fecapr column
 * @method CpptoctaQuery groupById() Group by the id column
 *
 * @method CpptoctaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CpptoctaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CpptoctaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Cpptocta findOne(PropelPDO $con = null) Return the first Cpptocta matching the query
 * @method Cpptocta findOneOrCreate(PropelPDO $con = null) Return the first Cpptocta matching the query, or a new Cpptocta object populated from the query conditions when no match is found
 *
 * @method Cpptocta findOneByNumpta(string $numpta) Return the first Cpptocta filtered by the numpta column
 * @method Cpptocta findOneByFecpta(string $fecpta) Return the first Cpptocta filtered by the fecpta column
 * @method Cpptocta findOneByCodubiori(string $codubiori) Return the first Cpptocta filtered by the codubiori column
 * @method Cpptocta findOneByCodubides(string $codubides) Return the first Cpptocta filtered by the codubides column
 * @method Cpptocta findOneByAsunto(string $asunto) Return the first Cpptocta filtered by the asunto column
 * @method Cpptocta findOneByMotivo(string $motivo) Return the first Cpptocta filtered by the motivo column
 * @method Cpptocta findOneByReccon(string $reccon) Return the first Cpptocta filtered by the reccon column
 * @method Cpptocta findOneByLoguse(string $loguse) Return the first Cpptocta filtered by the loguse column
 * @method Cpptocta findOneByAprpto(string $aprpto) Return the first Cpptocta filtered by the aprpto column
 * @method Cpptocta findOneByUsuapr(string $usuapr) Return the first Cpptocta filtered by the usuapr column
 * @method Cpptocta findOneByFecapr(string $fecapr) Return the first Cpptocta filtered by the fecapr column
 *
 * @method array findByNumpta(string $numpta) Return Cpptocta objects filtered by the numpta column
 * @method array findByFecpta(string $fecpta) Return Cpptocta objects filtered by the fecpta column
 * @method array findByCodubiori(string $codubiori) Return Cpptocta objects filtered by the codubiori column
 * @method array findByCodubides(string $codubides) Return Cpptocta objects filtered by the codubides column
 * @method array findByAsunto(string $asunto) Return Cpptocta objects filtered by the asunto column
 * @method array findByMotivo(string $motivo) Return Cpptocta objects filtered by the motivo column
 * @method array findByReccon(string $reccon) Return Cpptocta objects filtered by the reccon column
 * @method array findByLoguse(string $loguse) Return Cpptocta objects filtered by the loguse column
 * @method array findByAprpto(string $aprpto) Return Cpptocta objects filtered by the aprpto column
 * @method array findByUsuapr(string $usuapr) Return Cpptocta objects filtered by the usuapr column
 * @method array findByFecapr(string $fecapr) Return Cpptocta objects filtered by the fecapr column
 * @method array findById(int $id) Return Cpptocta objects filtered by the id column
 *
 * @package    propel.generator.lib.model.presupuesto.om
 */
abstract class BaseCpptoctaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCpptoctaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Cpptocta', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CpptoctaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CpptoctaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CpptoctaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CpptoctaQuery) {
            return $criteria;
        }
        $query = new CpptoctaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Cpptocta|Cpptocta[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CpptoctaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CpptoctaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cpptocta A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cpptocta A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "numpta", "fecpta", "codubiori", "codubides", "asunto", "motivo", "reccon", "loguse", "aprpto", "usuapr", "fecapr", "id" FROM "cpptocta" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Cpptocta();
            $obj->hydrate($row);
            CpptoctaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Cpptocta|Cpptocta[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Cpptocta[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CpptoctaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CpptoctaPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CpptoctaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CpptoctaPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the numpta column
     *
     * Example usage:
     * <code>
     * $query->filterByNumpta('fooValue');   // WHERE numpta = 'fooValue'
     * $query->filterByNumpta('%fooValue%'); // WHERE numpta LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numpta The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpptoctaQuery The current query, for fluid interface
     */
    public function filterByNumpta($numpta = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numpta)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numpta)) {
                $numpta = str_replace('*', '%', $numpta);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpptoctaPeer::NUMPTA, $numpta, $comparison);
    }

    /**
     * Filter the query on the fecpta column
     *
     * Example usage:
     * <code>
     * $query->filterByFecpta('2011-03-14'); // WHERE fecpta = '2011-03-14'
     * $query->filterByFecpta('now'); // WHERE fecpta = '2011-03-14'
     * $query->filterByFecpta(array('max' => 'yesterday')); // WHERE fecpta > '2011-03-13'
     * </code>
     *
     * @param     mixed $fecpta The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpptoctaQuery The current query, for fluid interface
     */
    public function filterByFecpta($fecpta = null, $comparison = null)
    {
        if (is_array($fecpta)) {
            $useMinMax = false;
            if (isset($fecpta['min'])) {
                $this->addUsingAlias(CpptoctaPeer::FECPTA, $fecpta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fecpta['max'])) {
                $this->addUsingAlias(CpptoctaPeer::FECPTA, $fecpta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CpptoctaPeer::FECPTA, $fecpta, $comparison);
    }

    /**
     * Filter the query on the codubiori column
     *
     * Example usage:
     * <code>
     * $query->filterByCodubiori('fooValue');   // WHERE codubiori = 'fooValue'
     * $query->filterByCodubiori('%fooValue%'); // WHERE codubiori LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codubiori The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpptoctaQuery The current query, for fluid interface
     */
    public function filterByCodubiori($codubiori = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codubiori)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codubiori)) {
                $codubiori = str_replace('*', '%', $codubiori);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpptoctaPeer::CODUBIORI, $codubiori, $comparison);
    }

    /**
     * Filter the query on the codubides column
     *
     * Example usage:
     * <code>
     * $query->filterByCodubides('fooValue');   // WHERE codubides = 'fooValue'
     * $query->filterByCodubides('%fooValue%'); // WHERE codubides LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codubides The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpptoctaQuery The current query, for fluid interface
     */
    public function filterByCodubides($codubides = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codubides)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codubides)) {
                $codubides = str_replace('*', '%', $codubides);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpptoctaPeer::CODUBIDES, $codubides, $comparison);
    }

    /**
     * Filter the query on the asunto column
     *
     * Example usage:
     * <code>
     * $query->filterByAsunto('fooValue');   // WHERE asunto = 'fooValue'
     * $query->filterByAsunto('%fooValue%'); // WHERE asunto LIKE '%fooValue%'
     * </code>
     *
     * @param     string $asunto The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpptoctaQuery The current query, for fluid interface
     */
    public function filterByAsunto($asunto = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($asunto)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $asunto)) {
                $asunto = str_replace('*', '%', $asunto);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpptoctaPeer::ASUNTO, $asunto, $comparison);
    }

    /**
     * Filter the query on the motivo column
     *
     * Example usage:
     * <code>
     * $query->filterByMotivo('fooValue');   // WHERE motivo = 'fooValue'
     * $query->filterByMotivo('%fooValue%'); // WHERE motivo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $motivo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpptoctaQuery The current query, for fluid interface
     */
    public function filterByMotivo($motivo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($motivo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $motivo)) {
                $motivo = str_replace('*', '%', $motivo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpptoctaPeer::MOTIVO, $motivo, $comparison);
    }

    /**
     * Filter the query on the reccon column
     *
     * Example usage:
     * <code>
     * $query->filterByReccon('fooValue');   // WHERE reccon = 'fooValue'
     * $query->filterByReccon('%fooValue%'); // WHERE reccon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $reccon The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpptoctaQuery The current query, for fluid interface
     */
    public function filterByReccon($reccon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($reccon)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $reccon)) {
                $reccon = str_replace('*', '%', $reccon);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpptoctaPeer::RECCON, $reccon, $comparison);
    }

    /**
     * Filter the query on the loguse column
     *
     * Example usage:
     * <code>
     * $query->filterByLoguse('fooValue');   // WHERE loguse = 'fooValue'
     * $query->filterByLoguse('%fooValue%'); // WHERE loguse LIKE '%fooValue%'
     * </code>
     *
     * @param     string $loguse The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpptoctaQuery The current query, for fluid interface
     */
    public function filterByLoguse($loguse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($loguse)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $loguse)) {
                $loguse = str_replace('*', '%', $loguse);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpptoctaPeer::LOGUSE, $loguse, $comparison);
    }

    /**
     * Filter the query on the aprpto column
     *
     * Example usage:
     * <code>
     * $query->filterByAprpto('fooValue');   // WHERE aprpto = 'fooValue'
     * $query->filterByAprpto('%fooValue%'); // WHERE aprpto LIKE '%fooValue%'
     * </code>
     *
     * @param     string $aprpto The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpptoctaQuery The current query, for fluid interface
     */
    public function filterByAprpto($aprpto = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($aprpto)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $aprpto)) {
                $aprpto = str_replace('*', '%', $aprpto);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpptoctaPeer::APRPTO, $aprpto, $comparison);
    }

    /**
     * Filter the query on the usuapr column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuapr('fooValue');   // WHERE usuapr = 'fooValue'
     * $query->filterByUsuapr('%fooValue%'); // WHERE usuapr LIKE '%fooValue%'
     * </code>
     *
     * @param     string $usuapr The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpptoctaQuery The current query, for fluid interface
     */
    public function filterByUsuapr($usuapr = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($usuapr)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $usuapr)) {
                $usuapr = str_replace('*', '%', $usuapr);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpptoctaPeer::USUAPR, $usuapr, $comparison);
    }

    /**
     * Filter the query on the fecapr column
     *
     * Example usage:
     * <code>
     * $query->filterByFecapr('2011-03-14'); // WHERE fecapr = '2011-03-14'
     * $query->filterByFecapr('now'); // WHERE fecapr = '2011-03-14'
     * $query->filterByFecapr(array('max' => 'yesterday')); // WHERE fecapr > '2011-03-13'
     * </code>
     *
     * @param     mixed $fecapr The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpptoctaQuery The current query, for fluid interface
     */
    public function filterByFecapr($fecapr = null, $comparison = null)
    {
        if (is_array($fecapr)) {
            $useMinMax = false;
            if (isset($fecapr['min'])) {
                $this->addUsingAlias(CpptoctaPeer::FECAPR, $fecapr['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fecapr['max'])) {
                $this->addUsingAlias(CpptoctaPeer::FECAPR, $fecapr['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CpptoctaPeer::FECAPR, $fecapr, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpptoctaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CpptoctaPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CpptoctaPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CpptoctaPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Cpptocta $cpptocta Object to remove from the list of results
     *
     * @return CpptoctaQuery The current query, for fluid interface
     */
    public function prune($cpptocta = null)
    {
        if ($cpptocta) {
            $this->addUsingAlias(CpptoctaPeer::ID, $cpptocta->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
