<?php


/**
 * Base class that represents a query for the 'cpmovadi' table.
 *
 * Tabla que contiene información referente a los movimientos de las adiciones.
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:38 2015
 *
 * @method CpmovadiQuery orderByRefadi($order = Criteria::ASC) Order by the refadi column
 * @method CpmovadiQuery orderByCodpre($order = Criteria::ASC) Order by the codpre column
 * @method CpmovadiQuery orderByPerpre($order = Criteria::ASC) Order by the perpre column
 * @method CpmovadiQuery orderByMonmov($order = Criteria::ASC) Order by the monmov column
 * @method CpmovadiQuery orderByStamov($order = Criteria::ASC) Order by the stamov column
 * @method CpmovadiQuery orderByTipo($order = Criteria::ASC) Order by the tipo column
 * @method CpmovadiQuery orderByMonto($order = Criteria::ASC) Order by the monto column
 * @method CpmovadiQuery orderByIva($order = Criteria::ASC) Order by the iva column
 * @method CpmovadiQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method CpmovadiQuery groupByRefadi() Group by the refadi column
 * @method CpmovadiQuery groupByCodpre() Group by the codpre column
 * @method CpmovadiQuery groupByPerpre() Group by the perpre column
 * @method CpmovadiQuery groupByMonmov() Group by the monmov column
 * @method CpmovadiQuery groupByStamov() Group by the stamov column
 * @method CpmovadiQuery groupByTipo() Group by the tipo column
 * @method CpmovadiQuery groupByMonto() Group by the monto column
 * @method CpmovadiQuery groupByIva() Group by the iva column
 * @method CpmovadiQuery groupById() Group by the id column
 *
 * @method CpmovadiQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CpmovadiQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CpmovadiQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CpmovadiQuery leftJoinCpadidis($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cpadidis relation
 * @method CpmovadiQuery rightJoinCpadidis($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cpadidis relation
 * @method CpmovadiQuery innerJoinCpadidis($relationAlias = null) Adds a INNER JOIN clause to the query using the Cpadidis relation
 *
 * @method CpmovadiQuery leftJoinCpdeftit($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cpdeftit relation
 * @method CpmovadiQuery rightJoinCpdeftit($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cpdeftit relation
 * @method CpmovadiQuery innerJoinCpdeftit($relationAlias = null) Adds a INNER JOIN clause to the query using the Cpdeftit relation
 *
 * @method Cpmovadi findOne(PropelPDO $con = null) Return the first Cpmovadi matching the query
 * @method Cpmovadi findOneOrCreate(PropelPDO $con = null) Return the first Cpmovadi matching the query, or a new Cpmovadi object populated from the query conditions when no match is found
 *
 * @method Cpmovadi findOneByRefadi(string $refadi) Return the first Cpmovadi filtered by the refadi column
 * @method Cpmovadi findOneByCodpre(string $codpre) Return the first Cpmovadi filtered by the codpre column
 * @method Cpmovadi findOneByPerpre(string $perpre) Return the first Cpmovadi filtered by the perpre column
 * @method Cpmovadi findOneByMonmov(string $monmov) Return the first Cpmovadi filtered by the monmov column
 * @method Cpmovadi findOneByStamov(string $stamov) Return the first Cpmovadi filtered by the stamov column
 * @method Cpmovadi findOneByTipo(string $tipo) Return the first Cpmovadi filtered by the tipo column
 * @method Cpmovadi findOneByMonto(string $monto) Return the first Cpmovadi filtered by the monto column
 * @method Cpmovadi findOneByIva(string $iva) Return the first Cpmovadi filtered by the iva column
 *
 * @method array findByRefadi(string $refadi) Return Cpmovadi objects filtered by the refadi column
 * @method array findByCodpre(string $codpre) Return Cpmovadi objects filtered by the codpre column
 * @method array findByPerpre(string $perpre) Return Cpmovadi objects filtered by the perpre column
 * @method array findByMonmov(string $monmov) Return Cpmovadi objects filtered by the monmov column
 * @method array findByStamov(string $stamov) Return Cpmovadi objects filtered by the stamov column
 * @method array findByTipo(string $tipo) Return Cpmovadi objects filtered by the tipo column
 * @method array findByMonto(string $monto) Return Cpmovadi objects filtered by the monto column
 * @method array findByIva(string $iva) Return Cpmovadi objects filtered by the iva column
 * @method array findById(int $id) Return Cpmovadi objects filtered by the id column
 *
 * @package    propel.generator.lib.model.presupuesto.om
 */
abstract class BaseCpmovadiQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCpmovadiQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Cpmovadi', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CpmovadiQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CpmovadiQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CpmovadiQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CpmovadiQuery) {
            return $criteria;
        }
        $query = new CpmovadiQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Cpmovadi|Cpmovadi[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CpmovadiPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CpmovadiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cpmovadi A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cpmovadi A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "refadi", "codpre", "perpre", "monmov", "stamov", "tipo", "monto", "iva", "id" FROM "cpmovadi" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Cpmovadi();
            $obj->hydrate($row);
            CpmovadiPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Cpmovadi|Cpmovadi[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Cpmovadi[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CpmovadiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CpmovadiPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CpmovadiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CpmovadiPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the refadi column
     *
     * Example usage:
     * <code>
     * $query->filterByRefadi('fooValue');   // WHERE refadi = 'fooValue'
     * $query->filterByRefadi('%fooValue%'); // WHERE refadi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refadi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpmovadiQuery The current query, for fluid interface
     */
    public function filterByRefadi($refadi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refadi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refadi)) {
                $refadi = str_replace('*', '%', $refadi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpmovadiPeer::REFADI, $refadi, $comparison);
    }

    /**
     * Filter the query on the codpre column
     *
     * Example usage:
     * <code>
     * $query->filterByCodpre('fooValue');   // WHERE codpre = 'fooValue'
     * $query->filterByCodpre('%fooValue%'); // WHERE codpre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codpre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpmovadiQuery The current query, for fluid interface
     */
    public function filterByCodpre($codpre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codpre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codpre)) {
                $codpre = str_replace('*', '%', $codpre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpmovadiPeer::CODPRE, $codpre, $comparison);
    }

    /**
     * Filter the query on the perpre column
     *
     * Example usage:
     * <code>
     * $query->filterByPerpre('fooValue');   // WHERE perpre = 'fooValue'
     * $query->filterByPerpre('%fooValue%'); // WHERE perpre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $perpre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpmovadiQuery The current query, for fluid interface
     */
    public function filterByPerpre($perpre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($perpre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $perpre)) {
                $perpre = str_replace('*', '%', $perpre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpmovadiPeer::PERPRE, $perpre, $comparison);
    }

    /**
     * Filter the query on the monmov column
     *
     * Example usage:
     * <code>
     * $query->filterByMonmov(1234); // WHERE monmov = 1234
     * $query->filterByMonmov(array(12, 34)); // WHERE monmov IN (12, 34)
     * $query->filterByMonmov(array('min' => 12)); // WHERE monmov >= 12
     * $query->filterByMonmov(array('max' => 12)); // WHERE monmov <= 12
     * </code>
     *
     * @param     mixed $monmov The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpmovadiQuery The current query, for fluid interface
     */
    public function filterByMonmov($monmov = null, $comparison = null)
    {
        if (is_array($monmov)) {
            $useMinMax = false;
            if (isset($monmov['min'])) {
                $this->addUsingAlias(CpmovadiPeer::MONMOV, $monmov['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monmov['max'])) {
                $this->addUsingAlias(CpmovadiPeer::MONMOV, $monmov['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CpmovadiPeer::MONMOV, $monmov, $comparison);
    }

    /**
     * Filter the query on the stamov column
     *
     * Example usage:
     * <code>
     * $query->filterByStamov('fooValue');   // WHERE stamov = 'fooValue'
     * $query->filterByStamov('%fooValue%'); // WHERE stamov LIKE '%fooValue%'
     * </code>
     *
     * @param     string $stamov The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpmovadiQuery The current query, for fluid interface
     */
    public function filterByStamov($stamov = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($stamov)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $stamov)) {
                $stamov = str_replace('*', '%', $stamov);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpmovadiPeer::STAMOV, $stamov, $comparison);
    }

    /**
     * Filter the query on the tipo column
     *
     * Example usage:
     * <code>
     * $query->filterByTipo('fooValue');   // WHERE tipo = 'fooValue'
     * $query->filterByTipo('%fooValue%'); // WHERE tipo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpmovadiQuery The current query, for fluid interface
     */
    public function filterByTipo($tipo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tipo)) {
                $tipo = str_replace('*', '%', $tipo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpmovadiPeer::TIPO, $tipo, $comparison);
    }

    /**
     * Filter the query on the monto column
     *
     * Example usage:
     * <code>
     * $query->filterByMonto(1234); // WHERE monto = 1234
     * $query->filterByMonto(array(12, 34)); // WHERE monto IN (12, 34)
     * $query->filterByMonto(array('min' => 12)); // WHERE monto >= 12
     * $query->filterByMonto(array('max' => 12)); // WHERE monto <= 12
     * </code>
     *
     * @param     mixed $monto The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpmovadiQuery The current query, for fluid interface
     */
    public function filterByMonto($monto = null, $comparison = null)
    {
        if (is_array($monto)) {
            $useMinMax = false;
            if (isset($monto['min'])) {
                $this->addUsingAlias(CpmovadiPeer::MONTO, $monto['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monto['max'])) {
                $this->addUsingAlias(CpmovadiPeer::MONTO, $monto['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CpmovadiPeer::MONTO, $monto, $comparison);
    }

    /**
     * Filter the query on the iva column
     *
     * Example usage:
     * <code>
     * $query->filterByIva(1234); // WHERE iva = 1234
     * $query->filterByIva(array(12, 34)); // WHERE iva IN (12, 34)
     * $query->filterByIva(array('min' => 12)); // WHERE iva >= 12
     * $query->filterByIva(array('max' => 12)); // WHERE iva <= 12
     * </code>
     *
     * @param     mixed $iva The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpmovadiQuery The current query, for fluid interface
     */
    public function filterByIva($iva = null, $comparison = null)
    {
        if (is_array($iva)) {
            $useMinMax = false;
            if (isset($iva['min'])) {
                $this->addUsingAlias(CpmovadiPeer::IVA, $iva['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($iva['max'])) {
                $this->addUsingAlias(CpmovadiPeer::IVA, $iva['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CpmovadiPeer::IVA, $iva, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpmovadiQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CpmovadiPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CpmovadiPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CpmovadiPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query by a related Cpadidis object
     *
     * @param   Cpadidis|PropelObjectCollection $cpadidis The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CpmovadiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCpadidis($cpadidis, $comparison = null)
    {
        if ($cpadidis instanceof Cpadidis) {
            return $this
                ->addUsingAlias(CpmovadiPeer::REFADI, $cpadidis->getRefadi(), $comparison);
        } elseif ($cpadidis instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CpmovadiPeer::REFADI, $cpadidis->toKeyValue('PrimaryKey', 'Refadi'), $comparison);
        } else {
            throw new PropelException('filterByCpadidis() only accepts arguments of type Cpadidis or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cpadidis relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CpmovadiQuery The current query, for fluid interface
     */
    public function joinCpadidis($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cpadidis');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cpadidis');
        }

        return $this;
    }

    /**
     * Use the Cpadidis relation Cpadidis object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CpadidisQuery A secondary query class using the current class as primary query
     */
    public function useCpadidisQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCpadidis($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cpadidis', 'CpadidisQuery');
    }

    /**
     * Filter the query by a related Cpdeftit object
     *
     * @param   Cpdeftit|PropelObjectCollection $cpdeftit The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CpmovadiQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCpdeftit($cpdeftit, $comparison = null)
    {
        if ($cpdeftit instanceof Cpdeftit) {
            return $this
                ->addUsingAlias(CpmovadiPeer::CODPRE, $cpdeftit->getCodpre(), $comparison);
        } elseif ($cpdeftit instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CpmovadiPeer::CODPRE, $cpdeftit->toKeyValue('PrimaryKey', 'Codpre'), $comparison);
        } else {
            throw new PropelException('filterByCpdeftit() only accepts arguments of type Cpdeftit or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cpdeftit relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CpmovadiQuery The current query, for fluid interface
     */
    public function joinCpdeftit($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cpdeftit');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cpdeftit');
        }

        return $this;
    }

    /**
     * Use the Cpdeftit relation Cpdeftit object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CpdeftitQuery A secondary query class using the current class as primary query
     */
    public function useCpdeftitQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCpdeftit($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cpdeftit', 'CpdeftitQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Cpmovadi $cpmovadi Object to remove from the list of results
     *
     * @return CpmovadiQuery The current query, for fluid interface
     */
    public function prune($cpmovadi = null)
    {
        if ($cpmovadi) {
            $this->addUsingAlias(CpmovadiPeer::ID, $cpmovadi->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
