<?php


/**
 * Base class that represents a query for the 'cpimpcom' table.
 *
 * Tabla que contiene información referente a las imputaciones de compromiso.
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:38 2015
 *
 * @method CpimpcomQuery orderByRefcom($order = Criteria::ASC) Order by the refcom column
 * @method CpimpcomQuery orderByCodpre($order = Criteria::ASC) Order by the codpre column
 * @method CpimpcomQuery orderByMonimp($order = Criteria::ASC) Order by the monimp column
 * @method CpimpcomQuery orderByMoncau($order = Criteria::ASC) Order by the moncau column
 * @method CpimpcomQuery orderByMonpag($order = Criteria::ASC) Order by the monpag column
 * @method CpimpcomQuery orderByMonaju($order = Criteria::ASC) Order by the monaju column
 * @method CpimpcomQuery orderByStaimp($order = Criteria::ASC) Order by the staimp column
 * @method CpimpcomQuery orderByRefere($order = Criteria::ASC) Order by the refere column
 * @method CpimpcomQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method CpimpcomQuery groupByRefcom() Group by the refcom column
 * @method CpimpcomQuery groupByCodpre() Group by the codpre column
 * @method CpimpcomQuery groupByMonimp() Group by the monimp column
 * @method CpimpcomQuery groupByMoncau() Group by the moncau column
 * @method CpimpcomQuery groupByMonpag() Group by the monpag column
 * @method CpimpcomQuery groupByMonaju() Group by the monaju column
 * @method CpimpcomQuery groupByStaimp() Group by the staimp column
 * @method CpimpcomQuery groupByRefere() Group by the refere column
 * @method CpimpcomQuery groupById() Group by the id column
 *
 * @method CpimpcomQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CpimpcomQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CpimpcomQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CpimpcomQuery leftJoinCpcompro($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cpcompro relation
 * @method CpimpcomQuery rightJoinCpcompro($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cpcompro relation
 * @method CpimpcomQuery innerJoinCpcompro($relationAlias = null) Adds a INNER JOIN clause to the query using the Cpcompro relation
 *
 * @method CpimpcomQuery leftJoinCpdeftit($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cpdeftit relation
 * @method CpimpcomQuery rightJoinCpdeftit($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cpdeftit relation
 * @method CpimpcomQuery innerJoinCpdeftit($relationAlias = null) Adds a INNER JOIN clause to the query using the Cpdeftit relation
 *
 * @method Cpimpcom findOne(PropelPDO $con = null) Return the first Cpimpcom matching the query
 * @method Cpimpcom findOneOrCreate(PropelPDO $con = null) Return the first Cpimpcom matching the query, or a new Cpimpcom object populated from the query conditions when no match is found
 *
 * @method Cpimpcom findOneByRefcom(string $refcom) Return the first Cpimpcom filtered by the refcom column
 * @method Cpimpcom findOneByCodpre(string $codpre) Return the first Cpimpcom filtered by the codpre column
 * @method Cpimpcom findOneByMonimp(string $monimp) Return the first Cpimpcom filtered by the monimp column
 * @method Cpimpcom findOneByMoncau(string $moncau) Return the first Cpimpcom filtered by the moncau column
 * @method Cpimpcom findOneByMonpag(string $monpag) Return the first Cpimpcom filtered by the monpag column
 * @method Cpimpcom findOneByMonaju(string $monaju) Return the first Cpimpcom filtered by the monaju column
 * @method Cpimpcom findOneByStaimp(string $staimp) Return the first Cpimpcom filtered by the staimp column
 * @method Cpimpcom findOneByRefere(string $refere) Return the first Cpimpcom filtered by the refere column
 *
 * @method array findByRefcom(string $refcom) Return Cpimpcom objects filtered by the refcom column
 * @method array findByCodpre(string $codpre) Return Cpimpcom objects filtered by the codpre column
 * @method array findByMonimp(string $monimp) Return Cpimpcom objects filtered by the monimp column
 * @method array findByMoncau(string $moncau) Return Cpimpcom objects filtered by the moncau column
 * @method array findByMonpag(string $monpag) Return Cpimpcom objects filtered by the monpag column
 * @method array findByMonaju(string $monaju) Return Cpimpcom objects filtered by the monaju column
 * @method array findByStaimp(string $staimp) Return Cpimpcom objects filtered by the staimp column
 * @method array findByRefere(string $refere) Return Cpimpcom objects filtered by the refere column
 * @method array findById(int $id) Return Cpimpcom objects filtered by the id column
 *
 * @package    propel.generator.lib.model.presupuesto.om
 */
abstract class BaseCpimpcomQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCpimpcomQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Cpimpcom', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CpimpcomQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CpimpcomQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CpimpcomQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CpimpcomQuery) {
            return $criteria;
        }
        $query = new CpimpcomQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Cpimpcom|Cpimpcom[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CpimpcomPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CpimpcomPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cpimpcom A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cpimpcom A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "refcom", "codpre", "monimp", "moncau", "monpag", "monaju", "staimp", "refere", "id" FROM "cpimpcom" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Cpimpcom();
            $obj->hydrate($row);
            CpimpcomPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Cpimpcom|Cpimpcom[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Cpimpcom[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CpimpcomQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CpimpcomPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CpimpcomQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CpimpcomPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the refcom column
     *
     * Example usage:
     * <code>
     * $query->filterByRefcom('fooValue');   // WHERE refcom = 'fooValue'
     * $query->filterByRefcom('%fooValue%'); // WHERE refcom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refcom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpimpcomQuery The current query, for fluid interface
     */
    public function filterByRefcom($refcom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refcom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refcom)) {
                $refcom = str_replace('*', '%', $refcom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpimpcomPeer::REFCOM, $refcom, $comparison);
    }

    /**
     * Filter the query on the codpre column
     *
     * Example usage:
     * <code>
     * $query->filterByCodpre('fooValue');   // WHERE codpre = 'fooValue'
     * $query->filterByCodpre('%fooValue%'); // WHERE codpre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codpre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpimpcomQuery The current query, for fluid interface
     */
    public function filterByCodpre($codpre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codpre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codpre)) {
                $codpre = str_replace('*', '%', $codpre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpimpcomPeer::CODPRE, $codpre, $comparison);
    }

    /**
     * Filter the query on the monimp column
     *
     * Example usage:
     * <code>
     * $query->filterByMonimp(1234); // WHERE monimp = 1234
     * $query->filterByMonimp(array(12, 34)); // WHERE monimp IN (12, 34)
     * $query->filterByMonimp(array('min' => 12)); // WHERE monimp >= 12
     * $query->filterByMonimp(array('max' => 12)); // WHERE monimp <= 12
     * </code>
     *
     * @param     mixed $monimp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpimpcomQuery The current query, for fluid interface
     */
    public function filterByMonimp($monimp = null, $comparison = null)
    {
        if (is_array($monimp)) {
            $useMinMax = false;
            if (isset($monimp['min'])) {
                $this->addUsingAlias(CpimpcomPeer::MONIMP, $monimp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monimp['max'])) {
                $this->addUsingAlias(CpimpcomPeer::MONIMP, $monimp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CpimpcomPeer::MONIMP, $monimp, $comparison);
    }

    /**
     * Filter the query on the moncau column
     *
     * Example usage:
     * <code>
     * $query->filterByMoncau(1234); // WHERE moncau = 1234
     * $query->filterByMoncau(array(12, 34)); // WHERE moncau IN (12, 34)
     * $query->filterByMoncau(array('min' => 12)); // WHERE moncau >= 12
     * $query->filterByMoncau(array('max' => 12)); // WHERE moncau <= 12
     * </code>
     *
     * @param     mixed $moncau The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpimpcomQuery The current query, for fluid interface
     */
    public function filterByMoncau($moncau = null, $comparison = null)
    {
        if (is_array($moncau)) {
            $useMinMax = false;
            if (isset($moncau['min'])) {
                $this->addUsingAlias(CpimpcomPeer::MONCAU, $moncau['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($moncau['max'])) {
                $this->addUsingAlias(CpimpcomPeer::MONCAU, $moncau['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CpimpcomPeer::MONCAU, $moncau, $comparison);
    }

    /**
     * Filter the query on the monpag column
     *
     * Example usage:
     * <code>
     * $query->filterByMonpag(1234); // WHERE monpag = 1234
     * $query->filterByMonpag(array(12, 34)); // WHERE monpag IN (12, 34)
     * $query->filterByMonpag(array('min' => 12)); // WHERE monpag >= 12
     * $query->filterByMonpag(array('max' => 12)); // WHERE monpag <= 12
     * </code>
     *
     * @param     mixed $monpag The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpimpcomQuery The current query, for fluid interface
     */
    public function filterByMonpag($monpag = null, $comparison = null)
    {
        if (is_array($monpag)) {
            $useMinMax = false;
            if (isset($monpag['min'])) {
                $this->addUsingAlias(CpimpcomPeer::MONPAG, $monpag['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monpag['max'])) {
                $this->addUsingAlias(CpimpcomPeer::MONPAG, $monpag['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CpimpcomPeer::MONPAG, $monpag, $comparison);
    }

    /**
     * Filter the query on the monaju column
     *
     * Example usage:
     * <code>
     * $query->filterByMonaju(1234); // WHERE monaju = 1234
     * $query->filterByMonaju(array(12, 34)); // WHERE monaju IN (12, 34)
     * $query->filterByMonaju(array('min' => 12)); // WHERE monaju >= 12
     * $query->filterByMonaju(array('max' => 12)); // WHERE monaju <= 12
     * </code>
     *
     * @param     mixed $monaju The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpimpcomQuery The current query, for fluid interface
     */
    public function filterByMonaju($monaju = null, $comparison = null)
    {
        if (is_array($monaju)) {
            $useMinMax = false;
            if (isset($monaju['min'])) {
                $this->addUsingAlias(CpimpcomPeer::MONAJU, $monaju['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monaju['max'])) {
                $this->addUsingAlias(CpimpcomPeer::MONAJU, $monaju['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CpimpcomPeer::MONAJU, $monaju, $comparison);
    }

    /**
     * Filter the query on the staimp column
     *
     * Example usage:
     * <code>
     * $query->filterByStaimp('fooValue');   // WHERE staimp = 'fooValue'
     * $query->filterByStaimp('%fooValue%'); // WHERE staimp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $staimp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpimpcomQuery The current query, for fluid interface
     */
    public function filterByStaimp($staimp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($staimp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $staimp)) {
                $staimp = str_replace('*', '%', $staimp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpimpcomPeer::STAIMP, $staimp, $comparison);
    }

    /**
     * Filter the query on the refere column
     *
     * Example usage:
     * <code>
     * $query->filterByRefere('fooValue');   // WHERE refere = 'fooValue'
     * $query->filterByRefere('%fooValue%'); // WHERE refere LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refere The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpimpcomQuery The current query, for fluid interface
     */
    public function filterByRefere($refere = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refere)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refere)) {
                $refere = str_replace('*', '%', $refere);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CpimpcomPeer::REFERE, $refere, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CpimpcomQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CpimpcomPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CpimpcomPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CpimpcomPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query by a related Cpcompro object
     *
     * @param   Cpcompro|PropelObjectCollection $cpcompro The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CpimpcomQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCpcompro($cpcompro, $comparison = null)
    {
        if ($cpcompro instanceof Cpcompro) {
            return $this
                ->addUsingAlias(CpimpcomPeer::REFCOM, $cpcompro->getRefcom(), $comparison);
        } elseif ($cpcompro instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CpimpcomPeer::REFCOM, $cpcompro->toKeyValue('PrimaryKey', 'Refcom'), $comparison);
        } else {
            throw new PropelException('filterByCpcompro() only accepts arguments of type Cpcompro or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cpcompro relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CpimpcomQuery The current query, for fluid interface
     */
    public function joinCpcompro($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cpcompro');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cpcompro');
        }

        return $this;
    }

    /**
     * Use the Cpcompro relation Cpcompro object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CpcomproQuery A secondary query class using the current class as primary query
     */
    public function useCpcomproQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCpcompro($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cpcompro', 'CpcomproQuery');
    }

    /**
     * Filter the query by a related Cpdeftit object
     *
     * @param   Cpdeftit|PropelObjectCollection $cpdeftit The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CpimpcomQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCpdeftit($cpdeftit, $comparison = null)
    {
        if ($cpdeftit instanceof Cpdeftit) {
            return $this
                ->addUsingAlias(CpimpcomPeer::CODPRE, $cpdeftit->getCodpre(), $comparison);
        } elseif ($cpdeftit instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CpimpcomPeer::CODPRE, $cpdeftit->toKeyValue('PrimaryKey', 'Codpre'), $comparison);
        } else {
            throw new PropelException('filterByCpdeftit() only accepts arguments of type Cpdeftit or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cpdeftit relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CpimpcomQuery The current query, for fluid interface
     */
    public function joinCpdeftit($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cpdeftit');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cpdeftit');
        }

        return $this;
    }

    /**
     * Use the Cpdeftit relation Cpdeftit object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CpdeftitQuery A secondary query class using the current class as primary query
     */
    public function useCpdeftitQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCpdeftit($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cpdeftit', 'CpdeftitQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Cpimpcom $cpimpcom Object to remove from the list of results
     *
     * @return CpimpcomQuery The current query, for fluid interface
     */
    public function prune($cpimpcom = null)
    {
        if ($cpimpcom) {
            $this->addUsingAlias(CpimpcomPeer::ID, $cpimpcom->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
